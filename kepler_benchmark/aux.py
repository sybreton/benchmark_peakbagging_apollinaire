import numpy as np
import pandas as pd


def read_pkb (filename) :

    a = np.loadtxt (filename)
    df = pd.DataFrame (data=a)
    df = df.set_index ([0,1])
    df.index = df.index.rename (['n', 'l'])

    return df