#!/usr/bin/env python
# coding: utf-8

# In[1]:


import apollinaire as apn


# In[2]:


list_kic = [6603624, 5184732, 6106415, 6225718, 12069449, 12069424]

for kic in list_kic :
    
    resDir = str (kic).zfill (9)

    filename = glob.glob (path.join ('data/kplr' + str(kic).zfill (9) + '*_COR_filt_inp.fits'))[0]
    hdu = fits.open (filename) [0]
    data = np.array (hdu.data)
    t = data[:,0]
    v = data[:,1]
    dt = np.median (t[1:] - t[:-1]) * 86400
    freq, psd = apn.psd.series_to_psd (v, dt=dt, correct_dc=True)
    freq = freq*1e6
    psd = psd*1e-6
    
    back = np.loadtxt (path.join (resDir, 'background.dat'))
    df_a2z = apn.peakbagging.read_a2z (path.join (resDir, 'modes_param.a2z'))
    
    aux_o = df_a2z.loc[(df_a2z[1]!='a')&(df_a2z[0]!='a'), 0].astype (np.int_)
    orders = range (np.amin (aux_o) + 1, np.amax (aux_o) + 1)
    
    list_n = []
    list_l = []
    list_K = []
    for n in orders :
        #print ('Order', n)
        #print ('Even pair')
        psw, ps, p0, _, = apn.peakbagging.bayes_factor (freq, psd, back, df_a2z, n, strategy='order', l02=True, size_window=50,
                                      thin=20, discard=200, instr='geometric', hdf5Dir=resDir,
                                      add_ampl=True, parallelise=True)
        
        Ksw, Ks = apn.peakbagging.compute_K (psw, ps, p0) 
        #print (Ksw, Ks)
        list_n.append (n)
        list_l.append (0)
        list_K.append (Ks)
        list_n.append (n - 1)
        list_l.append (2)
        list_K.append (Ksw)
        
        #print ('Odd pair')
        psw, ps, p0, _, = apn.peakbagging.bayes_factor (freq, psd, back, df_a2z, n, strategy='order', l02=False, size_window=50,
                                      thin=20, discard=200, instr='geometric', hdf5Dir=resDir,
                                      add_ampl=True, parallelise=True, fit_amp=True)
        
        Ksw, Ks = apn.peakbagging.compute_K (psw, ps, p0)
        #print (Ksw, Ks)
        
        list_n.append (n)
        list_l.append (1)
        list_K.append (Ks)
        list_n.append (n - 1)
        list_l.append (3)
        list_K.append (Ksw)
        
    list_n = np.array (list_n)
    list_l = np.array (list_l)
    list_K = np.array (list_K)
    
    np.savetxt (path.join (resDir, 'quality_assurance.dat'), np.c_[list_n, list_l, list_K], fmt='%-s')
    
    #break


# In[ ]:




