import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob
from os import path
import os
from astropy.time import Time
from matplotlib.dates import (datestr2num, YearLocator, AutoDateLocator,
                             ConciseDateFormatter)
from tqdm import tqdm

def make_stamps (days, origin) :

    aux = Time (origin, format='isot')
    jd_origin = aux.jd
    jdstamps = days + jd_origin
    t = Time (jdstamps, format='jd')
    dates = datestr2num (t.isot)

    return dates
  

def get_pkb_files (pkbDir) :
  
    list_pkb = glob.glob (path.join (pkbDir, '*.pkb'))

    return list_pkb

def pkb_frame (pkb) :

    df = pd.DataFrame (data=pkb)
    df = df.set_index ([0,1])

    return df

def make_average (list_pkb) :

    list_pkb.copy().sort ()
    elt = list_pkb.pop (0)
    pkb = np.loadtxt (elt)
    pkbf = pkb_frame (pkb) 
    pkbf['avg'] = pkbf[2] / pkbf[3]**2
    pkbf['weight'] = 1. / pkbf[3]**2

    for elt in list_pkb :
        pkb = np.loadtxt (elt)
        aux = pkb_frame (pkb)
        pkbf['avg'] = pkbf['avg'] + aux[2] / aux[3]**2
        pkbf['weight'] = pkbf['weight'] + 1. / aux[3]**2

    pkbf['avg'] = pkbf['avg'] / pkbf['weight']

    return pkbf['avg']

def compare_param (order=20, index_param_snb=2, index_param_ds=2,
                   filename=None, plot=True) :
    
    nmode_considered = 0
    one_sigma = 0
    three_sigma = 0
    
    list_snb = get_pkb_files ('pkb_summaries')
    list_snb.sort ()
    list_ds = glob.glob ('fit_david/*.txt')
    list_ds.sort ()
    summary_dc = np.loadtxt ('duty_cycle_golf.txt')
    
    if plot :
        fig, axs = plt.subplots (3, 1, figsize=(18, 18))
    else :
        fig = None
            
    for ii, (f1, f2) in tqdm (enumerate (zip (list_snb, list_ds))) :
        
        if summary_dc[ii,1] < 0.90 :
            continue
        
        pkb1 = np.loadtxt (f1)
        pkb2 = np.loadtxt (f2)
        pkb1 = pd.DataFrame (data=pkb1)
        pkb2 = pd.DataFrame (data=pkb2)
        
        start = int (f1[-13:-9])
        end = int (f1[-8:-4])
        x = (end + start) / 2
        x = make_stamps (x, '1996-04-11')
        
        if ii==0 :
            label_snb = 'SNB'
            label_ds = 'DS'
        if ii!=0 :
            label_snb = None
            label_ds = None
        
        for l in range (3) :
                      
            o = order
            if l in [2, 3] :
                o = o - 1
            
            cond1 = (pkb1[1]==l)&(pkb1[0]==o)
            cond2 = (pkb2[1]==l)&(pkb2[0]==o)
            

            y1 = pkb1.loc[cond1, index_param_snb] 
            yerr1 = pkb1.loc[cond1, index_param_snb+1]
            y2 = pkb2.loc[cond2, index_param_ds] 
            yerr2 = pkb2.loc[cond2, index_param_ds+1]
            nmode_considered += 1
            
            if index_param_snb==4:
                y1 = y1*1e6
                yerr1 = yerr1*1e6
                if l==1 :
                    y2 = 2*y2
                    yerr2 = 2*yerr2
                if l==2 :
                    a = 2/0.59 + 1 #see Salabert et al. 2011
                    y2 = a*y2
                    yerr2 = a*yerr2
            
            #print (y2)
            
            if y2.iloc[0]<y1.iloc[0]-yerr1.iloc[0] or y2.iloc[0]>y1.iloc[0]+yerr1.iloc[0] :
                #print (l, y1.iloc[0], y2.iloc[0])
                one_sigma += 1
            if y2.iloc[0]<y1.iloc[0]-3*yerr1.iloc[0] or y2.iloc[0]>y1.iloc[0]+3*yerr1.iloc[0] :
                three_sigma += 1
                
            #print (l, y1.iloc[0], y2.iloc[0])
                
            if plot :
                axs[l].set_title ('$\ell$={}'.format(l))
                axs[l].errorbar (x, y1-y2, yerr=yerr1, color='cornflowerblue', ls='', 
                                 marker='x', capsize=3, label=label_snb)
                axs[l].errorbar (x, 0, yerr=yerr2, color='grey', ls='', 
                                 marker='x', capsize=3, label=label_ds)

    
    if plot :
        locator = AutoDateLocator ()
        locator.intervald ['YEARLY'] = [2]
        formatter = ConciseDateFormatter (locator)

        for ax in axs :
            ax.set_xlabel ('Date (year)')
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter (formatter)


        axs[0].legend ()
        fig.suptitle ('$n=$ {}'.format(order))
        if filename is not None :
            plt.savefig (filename, format='pdf')
    
    return nmode_considered, one_sigma, three_sigma, fig

def plot_shift_frequency_range (pkbDir, compare_david=False) :

    list_pkb = get_pkb_files (pkbDir)
    avg = make_average ([list_pkb[0], list_pkb[4]])
    list_pkb = get_pkb_files (pkbDir)
    list_pkb.sort ()
    
    if compare_david :
        df_ref = pd.read_csv ('tables/salabert_2015.csv', index_col=0)

    fig_range, axs_range = plt.subplots (3, 3, figsize=(24, 20))

    fig_glob, axs_glob = plt.subplots (3, 1, figsize=(12,36))

    range_freq_down = [1800, 2450, 3110]
    range_freq_up = [2450, 3110, 3790]

    color = ['cornflowerblue', 'cornflowerblue', 'cornflowerblue']
    color_d = ['maroon', 'maroon', 'maroon']
    label = [r'1800-2450 $\mu$Hz', r'2450-3110 $\mu$Hz', r'3110-3790 $\mu$Hz']
    filenames = ['shift_snb_1800_2450_1year.txt', 'shift_snb_2450_3110_1year.txt',
               'shift_snb_3110_3790_1year.txt']
    filenames_d = ['shift_ds_1800_2450_1year.txt', 'shift_ds_2450_3110_1year.txt',
               'shift_ds_3110_3790_1year.txt']

    for jj, (f_low, f_up, c, c2, lab, filename, filename_d, axs) in enumerate (zip (range_freq_down, 
                                                              range_freq_up, color, color_d, label,
                                                              filenames, filenames_d, axs_range)) :
        

        all_l = []
        all_e = []

        l0=[] 
        l1=[] 
        l2=[] 
        l3=[]
        e0=[] 
        e1=[]
        e2=[]
        e3=[]
        xxx = []
  
        for ii, elt in enumerate (list_pkb) :
        
            if ii == 69 :
                break
        
            start = int (elt[-13:-9])
            end = int (elt[-8:-4])
            x = (end + start) / 2
            pkb = np.loadtxt (elt)
            df = pkb_frame (pkb)
            df = df.loc[(df[2]>f_low)&(df[2]<f_up)]

            df['freq_shift'] = (df[2] - avg) / df[3]**2
            m = df.groupby (level=1).sum ()
            df['err_square'] = 1 / (df[3]**2)
            err_square = df.groupby (level=1).sum () 
            err = np.sqrt (1 / err_square['err_square'])
            
            xxx.append (x)
            df = df.reset_index ()

            l0.append (m.loc[0., 'freq_shift'] / (1. / df.loc[df[1]==0, 3]**2).sum ())
            l1.append (m.loc[1., 'freq_shift'] / (1. / df.loc[df[1]==1, 3]**2).sum ())    
            l2.append (m.loc[2., 'freq_shift'] / (1. / df.loc[df[1]==2, 3]**2).sum ())

            all_l.append (df.loc[df[1]!=3, 'freq_shift'].sum () / (1 / (df.loc[df[1]!=3, 3]**2)).sum ())
            
            e0.append (err.loc[0.])
            e1.append (err.loc[1.])
            e2.append (err.loc[2.])

            all_e.append (np.sqrt (1 / df.loc[df[1]!=3, 'err_square'].sum ()) ) 

        xxx = np.array (xxx)
        xxx = make_stamps (xxx, '1996-04-11')
        l0 = np.array (l0)
        l1 = np.array (l1)
        l2 = np.array (l2)
        all_l = np.array (all_l)
        e0 = np.array (e0)
        e1 = np.array (e1)
        e2 = np.array (e2)
        all_e = np.array (all_e)
        
        if compare_david :
            
            l0_d=[] 
            l1_d=[] 
            l2_d=[] 
            e0_d=[] 
            e1_d=[]
            e2_d=[]
            
            all_l_ds = []
            all_e_ds = []
        
            for ii in range (1, 70) :
                
                df = df_ref.loc[df_ref['Index']==ii]
                df = df.set_index (['n', 'l'])
                df.index = df.index.rename ([0, 1])
                df['freq_shift'] = (df['nu'] - avg) / df['e_nu']**2
                df = df.loc[(df['nu']>f_low)&(df['nu']<f_up)]
                m = df.groupby (level=1).sum ()
                df['err_square'] = 1 / (df['e_nu']**2)
                err_square = df.groupby (level=1).sum () 
                err = np.sqrt (1 / err_square['err_square'])

                df = df.reset_index ()
                
                l0_d.append (m.loc[0, 'freq_shift'] / (1. / df.loc[df[1]==0, 'e_nu']**2).sum ())
                l1_d.append (m.loc[1, 'freq_shift'] / (1. / df.loc[df[1]==1, 'e_nu']**2).sum ())    
                l2_d.append (m.loc[2, 'freq_shift'] / (1. / df.loc[df[1]==2, 'e_nu']**2).sum ())
                
                all_l_ds.append (df.loc[df[1]!=3, 'freq_shift'].sum () / (1 / (df.loc[df[1]!=3, 'e_nu']**2)).sum ())

                e0_d.append (err.loc[0])
                e1_d.append (err.loc[1])
                e2_d.append (err.loc[2])
                
                all_e_ds.append (np.sqrt (1 / df.loc[df[1]!=3, 'err_square'].sum ()) ) 
                
            l0_d = np.array (l0_d)
            l1_d = np.array (l1_d)
            l2_d = np.array (l2_d)
            e0_d = np.array (e0_d)
            e1_d = np.array (e1_d)
            e2_d = np.array (e2_d)
            
            all_l_ds = np.array (all_l_ds)
            all_e_ds = np.array (all_e_ds)
                        
            axs[0].errorbar (xxx, l0_d, yerr=e0_d, color=c2, marker='o', capsize=3., linestyle='', linewidth=0.5, label='Salabert et al (2015)')
            axs[1].errorbar (xxx, l1_d, yerr=e1_d, color=c2, marker='o', capsize=3., linestyle='', linewidth=0.5, label='Salabert et al (2015)')
            axs[2].errorbar (xxx, l2_d, yerr=e2_d, color=c2, marker='o', capsize=3., linestyle='', linewidth=0.5, label='Salabert et al (2015)')
            
            axs_glob[jj].errorbar (xxx, all_l_ds, yerr=all_e_ds, color=c2, marker='o', 
                              capsize=3., linestyle='-', linewidth=0.5, label='Salabert et al (2015)')
            
        axs[0].errorbar (xxx, l0, yerr=e0, color=c, marker='o', capsize=3., linestyle='', linewidth=0.5, label=r'$\mathtt{apollinaire}$')
        axs[1].errorbar (xxx, l1, yerr=e1, color=c, marker='o', capsize=3., linestyle='', linewidth=0.5, label=r'$\mathtt{apollinaire}$')
        axs[2].errorbar (xxx, l2, yerr=e2, color=c, marker='o', capsize=3., linestyle='', linewidth=0.5, label=r'$\mathtt{apollinaire}$')

        axs_glob[jj].errorbar (xxx, all_l, yerr=all_e, color=c, marker='o', capsize=3., 
                          linestyle='-', linewidth=0.5, label=r'$\mathtt{apollinaire}$')

    locator = AutoDateLocator ()
    locator.intervald ['YEARLY'] = [2]
    formatter = ConciseDateFormatter (locator)
    
    for axs in axs_range :
        axs[0].legend ()
        axs[0].set_xlabel ('Date (year)')
        axs[1].set_xlabel ('Date (year)')
        axs[2].set_xlabel ('Date (year)')
        axs[0].set_ylabel (r'Frequency shift $\ell=0$ ($\mu$Hz)')
        axs[1].set_ylabel (r'Frequency shift $\ell=1$ ($\mu$Hz)')
        axs[2].set_ylabel (r'Frequency shift $\ell=2$ ($\mu$Hz)')
        axs[0].xaxis.set_major_locator(locator)
        axs[0].xaxis.set_major_formatter (formatter)
        axs[1].xaxis.set_major_locator(locator)
        axs[1].xaxis.set_major_formatter (formatter)
        axs[2].xaxis.set_major_locator(locator)
        axs[2].xaxis.set_major_formatter (formatter)
    
    axs_glob[2].set_xlabel ('Date (year)')
    axs_glob[0].set_ylabel (r'[1800-2450] $\mu$Hz' '\n' 'Frequency shift <0,1,2> ($\mu$Hz)')
    axs_glob[1].set_ylabel (r'[2450-3110] $\mu$Hz' '\n' 'Frequency shift <0,1,2> ($\mu$Hz)')
    axs_glob[2].set_ylabel (r'[3110-3790] $\mu$Hz' '\n' 'Frequency shift <0,1,2> ($\mu$Hz)')

    for ax in axs_glob :
        ax.legend ()
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter (formatter)

    fig_range.savefig ('plots/solar/frequency_shifts_golf_frequency_range_snb_ds.pdf', format='pdf')
    #fig_med.savefig ('plots/solar/frequency_shifts_golf_medium_frequency_range_snb_ds.pdf', format='pdf')
    #fig_high.savefig ('plots/solar/frequency_shifts_golf_high_frequency_range_snb_ds.pdf', format='pdf')
    #fig_glob.savefig ('plots/solar/frequency_shifts_golf_mean_frequency_range_snb_ds.pdf', format='pdf')
    plt.close ()
    
    return 
  
def sigma_metric_freq () :
    
    list_snb = get_pkb_files ('pkb_summaries')
    list_snb.sort ()
    list_ds = glob.glob ('fit_david/*.txt')
    list_ds.sort ()
    summary_dc = np.loadtxt ('duty_cycle_golf.txt')
    
    metric = []
            
    for ii, (f1, f2) in tqdm (enumerate (zip (list_snb, list_ds))) :
        
        if summary_dc[ii,1] < 0.90 :
            continue
        
        pkb1 = np.loadtxt (f1)
        pkb2 = np.loadtxt (f2)
        pkb1 = pd.DataFrame (data=pkb1)
        pkb2 = pd.DataFrame (data=pkb2)
        
        pkb1 = pkb1.loc[pkb1[1]!=3]
        pkb2 = pkb2.loc[pkb2[1]!=3]
        
        aux = ((pkb2[2]-pkb1[2]) / np.sqrt (pkb2[3]**2+pkb1[3]**2)).to_numpy ()
        metric.append (aux)
    
    metric = np.ravel (np.array (metric))
    
    return metric 

if __name__ == '__main__' :

    plot_shift_frequency_range ('pkb_summaries', compare_david=True)

