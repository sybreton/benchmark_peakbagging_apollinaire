import numpy as np
import pandas as pd
import glob
from os import path
import os
import matplotlib.pyplot as plt
from tqdm import tqdm


def read_pkb (filename) :
    
    a = np.loadtxt (filename)
    df = pkb_frame (a)
    
    return df