# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:01:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0735 0.0972 0.0981 21.2374 1.2130 1.3268 4.0609 0.2566 0.2596 90.0000 0.0000 0.0000 0.4397 0.1615 0.1139 0.0000 0.0000 0.0000
26 0 3710.0386 0.0744 0.0720 29.7284 1.2866 1.3967 3.9141 0.1519 0.1537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
