# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:59:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9336 0.1000 0.0991 18.2865 0.9605 1.0790 4.3212 0.2607 0.2349 90.0000 0.0000 0.0000 0.3141 0.1903 0.1565 0.0000 0.0000 0.0000
26 0 3709.9570 0.0676 0.0655 31.4215 1.3698 1.3726 4.0125 0.1489 0.1506 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
