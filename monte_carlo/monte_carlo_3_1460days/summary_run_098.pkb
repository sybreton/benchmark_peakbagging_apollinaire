# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:48:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8446 0.0846 0.0892 23.8516 1.4124 1.4957 3.4769 0.2079 0.2231 90.0000 0.0000 0.0000 0.5491 0.0897 0.0756 0.0000 0.0000 0.0000
26 0 3710.0738 0.0675 0.0661 30.1717 1.3259 1.3990 3.8755 0.1436 0.1447 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
