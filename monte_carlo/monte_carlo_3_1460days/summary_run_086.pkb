# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:18:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2069 0.1093 0.1150 19.5067 0.9996 1.1113 4.5581 0.2622 0.2570 90.0000 0.0000 0.0000 0.3551 0.2049 0.1594 0.0000 0.0000 0.0000
26 0 3710.0643 0.0738 0.0751 30.8894 1.3724 1.4878 3.7813 0.1551 0.1559 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
