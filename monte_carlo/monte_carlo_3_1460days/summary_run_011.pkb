# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:12:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9413 0.1008 0.1005 21.2486 1.2718 1.3138 3.7999 0.2403 0.2557 90.0000 0.0000 0.0000 0.5453 0.1238 0.0965 0.0000 0.0000 0.0000
26 0 3709.9218 0.0727 0.0721 29.8217 1.3129 1.3306 4.0335 0.1494 0.1586 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
