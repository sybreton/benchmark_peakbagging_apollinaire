# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:12:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9666 0.0968 0.1007 20.5272 1.1595 1.2832 4.0319 0.2453 0.2457 90.0000 0.0000 0.0000 0.3696 0.1832 0.1312 0.0000 0.0000 0.0000
26 0 3709.9602 0.0745 0.0752 30.8632 1.2582 1.3730 4.0771 0.1504 0.1584 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
