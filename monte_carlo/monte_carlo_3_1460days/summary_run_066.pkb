# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:29:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9242 0.1001 0.1019 19.3163 1.0402 1.1919 4.1966 0.2516 0.2392 90.0000 0.0000 0.0000 0.3428 0.1920 0.1419 0.0000 0.0000 0.0000
26 0 3709.9489 0.0734 0.0730 30.6650 1.2871 1.3521 4.0121 0.1458 0.1507 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
