# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:44:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0828 0.0852 0.0872 21.9075 1.1160 1.2252 3.9247 0.2052 0.2054 90.0000 0.0000 0.0000 0.2377 0.1535 0.1444 0.0000 0.0000 0.0000
26 0 3710.0371 0.0685 0.0664 32.4981 1.4199 1.5233 3.8122 0.1412 0.1464 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
