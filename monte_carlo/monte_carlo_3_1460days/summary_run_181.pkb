# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:14:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0073 0.0889 0.0885 22.0290 1.2667 1.3650 3.7773 0.2279 0.2270 90.0000 0.0000 0.0000 0.4052 0.1583 0.1101 0.0000 0.0000 0.0000
26 0 3709.8952 0.0731 0.0734 29.8830 1.2514 1.3886 3.9442 0.1495 0.1508 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
