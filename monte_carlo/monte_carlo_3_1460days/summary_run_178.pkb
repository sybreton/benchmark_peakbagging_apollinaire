# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:07:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9064 0.0925 0.0917 19.9639 1.0453 1.1931 4.2013 0.2395 0.2322 90.0000 0.0000 0.0000 0.3015 0.1807 0.1439 0.0000 0.0000 0.0000
26 0 3710.0748 0.0724 0.0697 29.2264 1.2759 1.3509 4.0090 0.1578 0.1585 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
