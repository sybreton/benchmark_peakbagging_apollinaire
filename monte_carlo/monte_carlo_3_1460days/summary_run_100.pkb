# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:53:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0381 0.0918 0.0970 20.7436 1.0730 1.1791 4.0245 0.2211 0.2159 90.0000 0.0000 0.0000 0.2809 0.1683 0.1430 0.0000 0.0000 0.0000
26 0 3709.9012 0.0790 0.0789 27.8635 1.2039 1.2601 4.2195 0.1609 0.1677 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
