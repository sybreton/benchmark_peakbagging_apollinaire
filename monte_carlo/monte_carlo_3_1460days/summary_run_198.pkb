# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:56:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0061 0.0936 0.0954 19.7454 1.0825 1.1762 4.2085 0.2484 0.2451 90.0000 0.0000 0.0000 0.3265 0.1888 0.1447 0.0000 0.0000 0.0000
26 0 3710.0931 0.0681 0.0671 31.3900 1.3429 1.4259 3.9806 0.1452 0.1513 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
