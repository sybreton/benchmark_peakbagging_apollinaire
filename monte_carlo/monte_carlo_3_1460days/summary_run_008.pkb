# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:05:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8659 0.0915 0.0925 21.0071 1.1408 1.2972 3.9398 0.2391 0.2323 90.0000 0.0000 0.0000 0.3288 0.1846 0.1348 0.0000 0.0000 0.0000
26 0 3710.0101 0.0723 0.0755 30.3751 1.2859 1.3421 4.1511 0.1508 0.1572 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
