# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:48:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0371 0.1012 0.1043 19.6977 0.9767 1.0394 4.3609 0.2185 0.2175 90.0000 0.0000 0.0000 0.1985 0.1341 0.1512 0.0000 0.0000 0.0000
26 0 3709.9386 0.0819 0.0799 28.3567 1.2369 1.2441 4.1132 0.1606 0.1660 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
