# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:40:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9059 0.1017 0.1080 20.5300 1.2191 1.2905 3.8481 0.2326 0.2499 90.0000 0.0000 0.0000 0.5202 0.1303 0.0991 0.0000 0.0000 0.0000
26 0 3709.9361 0.0766 0.0723 31.1888 1.3295 1.4063 4.0049 0.1486 0.1474 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
