# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:42:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8147 0.0932 0.0916 21.8886 1.2812 1.4280 3.6727 0.2378 0.2387 90.0000 0.0000 0.0000 0.4007 0.1622 0.1173 0.0000 0.0000 0.0000
26 0 3709.9345 0.0794 0.0749 29.0159 1.1735 1.2593 4.3266 0.1553 0.1580 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
