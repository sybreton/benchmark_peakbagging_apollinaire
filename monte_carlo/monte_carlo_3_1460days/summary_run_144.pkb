# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:42:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0095 0.0923 0.0991 21.3696 1.1056 1.2266 4.0467 0.2180 0.2146 90.0000 0.0000 0.0000 0.3236 0.1894 0.1458 0.0000 0.0000 0.0000
26 0 3710.0355 0.0683 0.0656 32.6955 1.4301 1.5204 3.6663 0.1408 0.1473 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
