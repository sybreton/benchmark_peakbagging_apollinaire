# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:00:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9887 0.0914 0.0959 20.5754 1.0943 1.1707 3.9600 0.2160 0.2124 90.0000 0.0000 0.0000 0.2893 0.1774 0.1448 0.0000 0.0000 0.0000
26 0 3709.9726 0.0708 0.0703 32.4795 1.4171 1.4973 3.8729 0.1422 0.1534 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
