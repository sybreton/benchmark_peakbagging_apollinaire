# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:18:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0094 0.1022 0.1051 19.2539 1.0715 1.1890 4.2952 0.2557 0.2558 90.0000 0.0000 0.0000 0.3995 0.1967 0.1389 0.0000 0.0000 0.0000
26 0 3710.0071 0.0713 0.0722 30.0563 1.2977 1.4157 3.9083 0.1504 0.1563 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
