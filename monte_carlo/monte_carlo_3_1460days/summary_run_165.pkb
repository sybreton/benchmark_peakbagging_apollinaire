# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:34:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0109 0.0879 0.0906 22.2774 1.2227 1.3529 3.8424 0.2219 0.2161 90.0000 0.0000 0.0000 0.3746 0.1642 0.1199 0.0000 0.0000 0.0000
26 0 3710.1272 0.0703 0.0707 30.5543 1.3324 1.3975 3.8332 0.1459 0.1506 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
