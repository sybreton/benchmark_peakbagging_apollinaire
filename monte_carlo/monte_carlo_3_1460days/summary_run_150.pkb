# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:57:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9649 0.0948 0.1000 19.0699 0.9058 1.0078 4.5098 0.2292 0.2264 90.0000 0.0000 0.0000 0.2529 0.1652 0.1618 0.0000 0.0000 0.0000
26 0 3710.0448 0.0725 0.0722 29.0181 1.2806 1.3129 4.0146 0.1564 0.1630 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
