# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:48:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9337 0.0964 0.0987 19.4502 1.0473 1.1404 4.2454 0.2376 0.2373 90.0000 0.0000 0.0000 0.3200 0.1869 0.1499 0.0000 0.0000 0.0000
26 0 3709.9371 0.0753 0.0723 30.2463 1.2909 1.3575 4.0437 0.1495 0.1545 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
