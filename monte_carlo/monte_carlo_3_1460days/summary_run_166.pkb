# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:37:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9871 0.0991 0.0992 20.6927 1.1175 1.2541 4.0345 0.2402 0.2357 90.0000 0.0000 0.0000 0.3444 0.1871 0.1363 0.0000 0.0000 0.0000
26 0 3709.9008 0.0782 0.0763 28.6085 1.2243 1.2789 4.1963 0.1639 0.1637 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
