# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:10:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9504 0.0946 0.1008 19.5760 1.0409 1.1666 4.1497 0.2387 0.2325 90.0000 0.0000 0.0000 0.3478 0.1935 0.1449 0.0000 0.0000 0.0000
26 0 3709.9157 0.0789 0.0771 28.7459 1.2022 1.3006 4.2351 0.1560 0.1568 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
