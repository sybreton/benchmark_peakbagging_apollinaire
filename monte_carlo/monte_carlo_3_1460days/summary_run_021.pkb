# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:37:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0477 0.0929 0.1005 22.0760 1.2932 1.4146 3.8306 0.2444 0.2563 90.0000 0.0000 0.0000 0.4324 0.1567 0.1055 0.0000 0.0000 0.0000
26 0 3709.9037 0.0768 0.0774 29.3390 1.2204 1.3010 4.0565 0.1518 0.1553 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
