# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:32:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0297 0.0953 0.0984 21.7103 1.2448 1.3517 4.1415 0.2550 0.2586 90.0000 0.0000 0.0000 0.4359 0.1764 0.1193 0.0000 0.0000 0.0000
26 0 3710.0291 0.0722 0.0695 30.1500 1.3097 1.3905 3.9425 0.1550 0.1591 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
