# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:47:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9547 0.0924 0.0951 20.4694 1.1722 1.3236 3.8876 0.2465 0.2500 90.0000 0.0000 0.0000 0.3964 0.1672 0.1156 0.0000 0.0000 0.0000
26 0 3709.8952 0.0726 0.0714 30.5272 1.2996 1.3969 4.0035 0.1519 0.1526 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
