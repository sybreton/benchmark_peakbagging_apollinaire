# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:22:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8545 0.0879 0.0861 20.5059 1.0424 1.1666 3.8625 0.2054 0.2042 90.0000 0.0000 0.0000 0.2386 0.1503 0.1418 0.0000 0.0000 0.0000
26 0 3709.9458 0.0717 0.0715 30.3712 1.2385 1.3414 4.0725 0.1444 0.1518 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
