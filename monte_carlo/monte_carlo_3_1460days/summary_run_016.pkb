# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:25:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9470 0.1056 0.1065 22.6973 1.3329 1.4191 3.6939 0.2171 0.2294 90.0000 0.0000 0.0000 0.5610 0.1125 0.0914 0.0000 0.0000 0.0000
26 0 3709.8939 0.0757 0.0759 31.4361 1.3500 1.4850 3.8479 0.1456 0.1506 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
