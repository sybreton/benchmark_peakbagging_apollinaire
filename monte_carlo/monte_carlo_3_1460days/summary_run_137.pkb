# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:25:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0588 0.0945 0.0986 19.3536 0.9843 1.0828 4.3259 0.2289 0.2270 90.0000 0.0000 0.0000 0.2714 0.1717 0.1558 0.0000 0.0000 0.0000
26 0 3710.0414 0.0694 0.0684 31.0925 1.3683 1.4888 3.8034 0.1487 0.1487 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
