# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:02:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9485 0.0970 0.0981 19.4496 0.9599 1.0466 4.3405 0.2270 0.2135 90.0000 0.0000 0.0000 0.2305 0.1558 0.1599 0.0000 0.0000 0.0000
26 0 3709.9320 0.0751 0.0747 30.1089 1.2914 1.3698 4.0318 0.1557 0.1592 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
