# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:53:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1087 0.0972 0.1016 20.3146 1.0228 1.0986 4.2603 0.2188 0.2153 90.0000 0.0000 0.0000 0.2244 0.1465 0.1466 0.0000 0.0000 0.0000
26 0 3709.8750 0.0754 0.0753 30.0179 1.3028 1.3904 3.9622 0.1524 0.1541 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
