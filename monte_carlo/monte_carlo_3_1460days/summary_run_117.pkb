# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:35:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0298 0.0948 0.0932 21.4147 1.1287 1.3217 4.0802 0.2465 0.2370 90.0000 0.0000 0.0000 0.3740 0.1897 0.1301 0.0000 0.0000 0.0000
26 0 3709.9939 0.0741 0.0754 29.0403 1.2339 1.3281 3.9581 0.1562 0.1588 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
