# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:50:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9825 0.0921 0.0947 20.2669 1.0987 1.1798 4.1408 0.2340 0.2297 90.0000 0.0000 0.0000 0.3383 0.1875 0.1396 0.0000 0.0000 0.0000
26 0 3710.0834 0.0700 0.0689 31.1523 1.3525 1.4039 3.8490 0.1436 0.1476 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
