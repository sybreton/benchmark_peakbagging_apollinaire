# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:51:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0623 0.1148 0.1217 21.0892 1.2396 1.3553 3.9927 0.2350 0.2483 90.0000 0.0000 0.0000 0.5679 0.1368 0.1085 0.0000 0.0000 0.0000
26 0 3709.9789 0.0842 0.0834 28.1057 1.2294 1.2765 4.0835 0.1594 0.1641 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
