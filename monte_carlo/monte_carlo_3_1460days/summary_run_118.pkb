# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:38:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1463 0.0934 0.0979 21.1508 1.1630 1.3006 4.2464 0.2566 0.2623 90.0000 0.0000 0.0000 0.4304 0.1812 0.1230 0.0000 0.0000 0.0000
26 0 3710.2034 0.0756 0.0715 29.7913 1.3352 1.3646 3.9283 0.1482 0.1553 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
