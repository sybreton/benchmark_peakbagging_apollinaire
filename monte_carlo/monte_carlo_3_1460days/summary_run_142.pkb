# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:37:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9875 0.0959 0.1007 19.9021 1.0414 1.1752 4.1232 0.2371 0.2354 90.0000 0.0000 0.0000 0.3357 0.1918 0.1443 0.0000 0.0000 0.0000
26 0 3709.8926 0.0736 0.0718 30.3239 1.2779 1.3515 4.0058 0.1553 0.1535 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
