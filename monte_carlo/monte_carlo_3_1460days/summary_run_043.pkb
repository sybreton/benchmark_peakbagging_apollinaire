# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:32:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0388 0.0933 0.0955 21.0176 1.2064 1.3134 4.1182 0.2595 0.2627 90.0000 0.0000 0.0000 0.4168 0.1864 0.1239 0.0000 0.0000 0.0000
26 0 3710.1205 0.0756 0.0730 28.4765 1.2050 1.2617 4.2001 0.1591 0.1623 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
