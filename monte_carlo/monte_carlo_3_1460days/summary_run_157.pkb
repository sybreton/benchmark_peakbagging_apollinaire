# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:15:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0247 0.0931 0.0970 22.4914 1.3629 1.3991 3.6607 0.2255 0.2385 90.0000 0.0000 0.0000 0.5191 0.1144 0.0900 0.0000 0.0000 0.0000
26 0 3709.9427 0.0750 0.0735 30.0820 1.3062 1.3607 3.9782 0.1500 0.1501 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
