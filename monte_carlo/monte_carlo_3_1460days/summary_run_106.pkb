# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:08:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0104 0.0932 0.0980 20.1878 0.9971 1.1104 4.1337 0.2162 0.2065 90.0000 0.0000 0.0000 0.2506 0.1635 0.1530 0.0000 0.0000 0.0000
26 0 3709.9571 0.0745 0.0721 30.5742 1.2959 1.3841 4.0028 0.1454 0.1506 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
