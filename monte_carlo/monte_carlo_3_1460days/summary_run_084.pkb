# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:13:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0300 0.0848 0.0893 22.4618 1.1395 1.2157 4.0534 0.2080 0.2043 90.0000 0.0000 0.0000 0.2683 0.1663 0.1425 0.0000 0.0000 0.0000
26 0 3710.0247 0.0675 0.0678 32.8212 1.4471 1.5474 3.7473 0.1424 0.1486 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
