# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:55:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8984 0.0962 0.1004 20.1942 1.0465 1.1698 4.2004 0.2416 0.2342 90.0000 0.0000 0.0000 0.3333 0.1877 0.1384 0.0000 0.0000 0.0000
26 0 3709.9094 0.0649 0.0638 34.5261 1.5319 1.6597 3.6727 0.1421 0.1460 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
