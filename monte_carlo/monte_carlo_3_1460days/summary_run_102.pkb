# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:58:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0838 0.0895 0.0928 20.3168 1.0119 1.1335 4.2552 0.2301 0.2144 90.0000 0.0000 0.0000 0.2272 0.1483 0.1497 0.0000 0.0000 0.0000
26 0 3709.9866 0.0709 0.0685 31.0388 1.3301 1.4124 3.9377 0.1490 0.1487 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
