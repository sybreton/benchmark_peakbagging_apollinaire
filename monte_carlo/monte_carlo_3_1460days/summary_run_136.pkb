# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:22:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9225 0.0856 0.0880 20.9661 1.0280 1.1152 4.0713 0.2032 0.2018 90.0000 0.0000 0.0000 0.2041 0.1324 0.1377 0.0000 0.0000 0.0000
26 0 3709.9926 0.0679 0.0685 31.6457 1.3548 1.4539 3.8985 0.1481 0.1525 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
