# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:59:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0522 0.0856 0.0866 21.9003 1.0908 1.2601 3.9063 0.2095 0.2039 90.0000 0.0000 0.0000 0.2761 0.1676 0.1414 0.0000 0.0000 0.0000
26 0 3710.1150 0.0719 0.0731 28.7511 1.2126 1.3308 4.0499 0.1561 0.1569 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
