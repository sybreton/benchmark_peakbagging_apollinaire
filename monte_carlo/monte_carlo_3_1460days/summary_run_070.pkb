# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:39:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1993 0.1003 0.1062 20.9440 1.1788 1.2808 4.2026 0.2611 0.2642 90.0000 0.0000 0.0000 0.4961 0.1631 0.1146 0.0000 0.0000 0.0000
26 0 3710.1053 0.0722 0.0711 31.2647 1.3817 1.4900 3.7627 0.1508 0.1545 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
