# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:37:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9294 0.0916 0.0944 23.3458 1.3029 1.4251 3.6680 0.2225 0.2304 90.0000 0.0000 0.0000 0.5985 0.0933 0.0768 0.0000 0.0000 0.0000
26 0 3710.0772 0.0705 0.0707 30.4858 1.3204 1.4155 3.9335 0.1461 0.1536 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
