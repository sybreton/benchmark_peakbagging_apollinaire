# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:27:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1272 0.0838 0.0878 21.9814 1.1167 1.2706 4.0438 0.2203 0.2117 90.0000 0.0000 0.0000 0.2681 0.1655 0.1415 0.0000 0.0000 0.0000
26 0 3710.1389 0.0693 0.0691 29.2382 1.2614 1.3440 3.8971 0.1478 0.1521 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
