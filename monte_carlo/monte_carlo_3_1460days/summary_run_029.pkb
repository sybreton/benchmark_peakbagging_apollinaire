# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:57:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0874 0.0963 0.1014 21.2698 1.1998 1.2995 4.1349 0.2604 0.2635 90.0000 0.0000 0.0000 0.4501 0.1590 0.1155 0.0000 0.0000 0.0000
26 0 3710.0493 0.0728 0.0747 29.3989 1.3091 1.3548 3.9484 0.1527 0.1581 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
