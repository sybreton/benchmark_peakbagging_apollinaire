# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:35:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0823 0.1017 0.1110 19.5708 0.9801 1.0653 4.3583 0.2289 0.2250 90.0000 0.0000 0.0000 0.2612 0.1664 0.1566 0.0000 0.0000 0.0000
26 0 3709.9637 0.0766 0.0763 30.0166 1.2799 1.4066 3.9279 0.1558 0.1563 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
