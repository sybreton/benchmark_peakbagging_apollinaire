# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:49:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1158 0.1025 0.1091 21.2102 1.2507 1.3621 3.8456 0.2332 0.2533 90.0000 0.0000 0.0000 0.5283 0.1364 0.0985 0.0000 0.0000 0.0000
26 0 3709.9668 0.0772 0.0743 31.4475 1.3316 1.4206 3.9986 0.1495 0.1567 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
