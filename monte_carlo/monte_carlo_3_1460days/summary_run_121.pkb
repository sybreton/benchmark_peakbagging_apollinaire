# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:45:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9278 0.0978 0.0993 22.3208 1.2903 1.3636 3.8360 0.2337 0.2456 90.0000 0.0000 0.0000 0.5423 0.1146 0.0901 0.0000 0.0000 0.0000
26 0 3709.9797 0.0734 0.0712 30.7785 1.3265 1.4355 3.9051 0.1488 0.1513 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
