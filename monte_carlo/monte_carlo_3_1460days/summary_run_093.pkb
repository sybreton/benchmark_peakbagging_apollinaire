# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:36:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1850 0.1013 0.1071 19.5514 1.0075 1.1234 4.3449 0.2419 0.2268 90.0000 0.0000 0.0000 0.2966 0.1769 0.1490 0.0000 0.0000 0.0000
26 0 3709.9603 0.0774 0.0760 29.8311 1.2922 1.3684 4.0024 0.1547 0.1559 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
