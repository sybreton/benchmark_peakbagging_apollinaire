# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:35:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0357 0.0936 0.0976 21.4651 1.1417 1.2336 4.0398 0.2253 0.2189 90.0000 0.0000 0.0000 0.3323 0.1865 0.1396 0.0000 0.0000 0.0000
26 0 3710.0459 0.0762 0.0723 28.6725 1.2446 1.3365 4.0152 0.1511 0.1582 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
