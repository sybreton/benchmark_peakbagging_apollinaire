# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:27:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0067 0.0863 0.0888 21.8812 1.1829 1.2927 3.9290 0.2331 0.2299 90.0000 0.0000 0.0000 0.3581 0.1850 0.1264 0.0000 0.0000 0.0000
26 0 3710.0057 0.0677 0.0673 32.1080 1.3742 1.4723 3.8870 0.1425 0.1480 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
