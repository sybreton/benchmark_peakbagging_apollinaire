# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:15:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9522 0.0974 0.1003 20.6204 1.2288 1.3171 3.8754 0.2355 0.2449 90.0000 0.0000 0.0000 0.4481 0.1663 0.1134 0.0000 0.0000 0.0000
26 0 3709.9786 0.0749 0.0741 31.2418 1.3518 1.4508 3.9735 0.1479 0.1566 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
