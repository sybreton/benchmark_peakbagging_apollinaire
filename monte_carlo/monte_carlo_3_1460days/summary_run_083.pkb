# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:11:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0932 0.0887 0.0941 20.3280 1.0653 1.1802 4.1938 0.2404 0.2353 90.0000 0.0000 0.0000 0.3239 0.1885 0.1419 0.0000 0.0000 0.0000
26 0 3710.0798 0.0657 0.0665 32.0854 1.4403 1.4614 3.6920 0.1393 0.1445 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
