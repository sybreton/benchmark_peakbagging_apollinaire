# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:43:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0645 0.1024 0.1039 20.5800 1.1355 1.2555 4.1593 0.2406 0.2430 90.0000 0.0000 0.0000 0.3869 0.1924 0.1346 0.0000 0.0000 0.0000
26 0 3709.9583 0.0740 0.0715 30.0139 1.3099 1.3716 4.0020 0.1519 0.1578 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
