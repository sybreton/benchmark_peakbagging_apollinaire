# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:34:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9670 0.0878 0.0919 20.8120 1.0371 1.1806 4.1259 0.2236 0.2178 90.0000 0.0000 0.0000 0.2664 0.1663 0.1470 0.0000 0.0000 0.0000
26 0 3709.9548 0.0727 0.0716 29.1967 1.2641 1.3553 4.0336 0.1503 0.1557 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
