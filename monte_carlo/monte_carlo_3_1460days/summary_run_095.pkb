# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:41:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0198 0.0863 0.0874 22.8551 1.3829 1.5238 3.6492 0.2357 0.2406 90.0000 0.0000 0.0000 0.4175 0.1492 0.1053 0.0000 0.0000 0.0000
26 0 3709.9587 0.0742 0.0718 29.6504 1.2757 1.3337 4.0738 0.1511 0.1537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
