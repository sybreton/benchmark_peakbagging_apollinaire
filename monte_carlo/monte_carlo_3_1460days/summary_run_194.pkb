# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:47:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0188 0.0982 0.1020 19.4937 1.0760 1.1853 4.2480 0.2585 0.2524 90.0000 0.0000 0.0000 0.4012 0.1972 0.1316 0.0000 0.0000 0.0000
26 0 3710.0094 0.0704 0.0687 30.7914 1.2872 1.3944 3.9163 0.1422 0.1434 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
