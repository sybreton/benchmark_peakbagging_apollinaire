# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:21:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1052 0.0879 0.0901 21.7527 1.1896 1.2614 4.0982 0.2408 0.2450 90.0000 0.0000 0.0000 0.3702 0.1760 0.1235 0.0000 0.0000 0.0000
26 0 3710.0725 0.0687 0.0662 32.4128 1.4198 1.5523 3.7228 0.1403 0.1436 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
