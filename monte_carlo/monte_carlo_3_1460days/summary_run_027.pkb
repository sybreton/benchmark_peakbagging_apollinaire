# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:52:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8582 0.0874 0.0904 21.7735 1.1283 1.2806 3.8709 0.2085 0.2021 90.0000 0.0000 0.0000 0.2866 0.1736 0.1365 0.0000 0.0000 0.0000
26 0 3709.9351 0.0751 0.0731 29.5259 1.2735 1.3095 4.1347 0.1517 0.1564 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
