# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:57:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0558 0.0946 0.0983 19.8042 1.0123 1.0978 4.1757 0.2172 0.2226 90.0000 0.0000 0.0000 0.1896 0.1274 0.1410 0.0000 0.0000 0.0000
26 0 3709.8692 0.0794 0.0783 28.9059 1.2467 1.2471 4.3068 0.1600 0.1687 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
