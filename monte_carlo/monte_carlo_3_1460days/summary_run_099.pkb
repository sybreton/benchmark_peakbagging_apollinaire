# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:51:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0091 0.0867 0.0890 21.6811 1.2329 1.3543 3.8332 0.2446 0.2420 90.0000 0.0000 0.0000 0.4102 0.1625 0.1083 0.0000 0.0000 0.0000
26 0 3710.0359 0.0660 0.0669 32.6535 1.4193 1.5715 3.7359 0.1398 0.1421 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
