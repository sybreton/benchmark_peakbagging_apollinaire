# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:37:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7929 0.0957 0.0981 21.6552 1.2887 1.4232 3.6611 0.2407 0.2556 90.0000 0.0000 0.0000 0.5155 0.1130 0.0871 0.0000 0.0000 0.0000
26 0 3709.9282 0.0753 0.0759 29.2938 1.2215 1.3316 4.2211 0.1574 0.1617 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
