# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:43:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0196 0.1052 0.1105 20.2793 1.1880 1.2654 4.0846 0.2440 0.2585 90.0000 0.0000 0.0000 0.4353 0.1818 0.1289 0.0000 0.0000 0.0000
26 0 3710.0419 0.0817 0.0789 27.8383 1.1933 1.2496 4.1816 0.1591 0.1666 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
