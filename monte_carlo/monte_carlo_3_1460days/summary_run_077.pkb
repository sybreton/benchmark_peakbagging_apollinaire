# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:56:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0158 0.0894 0.0892 20.4737 1.2114 1.3152 3.9016 0.2613 0.2647 90.0000 0.0000 0.0000 0.4050 0.1751 0.1202 0.0000 0.0000 0.0000
26 0 3709.9562 0.0676 0.0669 30.8425 1.3418 1.4279 3.8782 0.1449 0.1498 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
