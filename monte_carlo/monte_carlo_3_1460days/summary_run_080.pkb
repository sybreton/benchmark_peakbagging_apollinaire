# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:03:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9452 0.0928 0.0940 20.6749 1.0900 1.2506 4.1154 0.2448 0.2374 90.0000 0.0000 0.0000 0.3660 0.1926 0.1338 0.0000 0.0000 0.0000
26 0 3709.9151 0.0722 0.0707 30.0200 1.2843 1.3727 4.0265 0.1504 0.1538 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
