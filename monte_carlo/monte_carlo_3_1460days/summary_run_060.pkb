# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:14:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0428 0.0994 0.1035 21.3270 1.1643 1.2726 4.1096 0.2386 0.2439 90.0000 0.0000 0.0000 0.4332 0.1748 0.1218 0.0000 0.0000 0.0000
26 0 3710.0765 0.0778 0.0782 29.1899 1.2526 1.3094 4.0450 0.1537 0.1572 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
