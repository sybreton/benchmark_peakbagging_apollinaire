# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:07:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0096 0.0903 0.0920 21.6494 1.1571 1.3035 3.9976 0.2287 0.2252 90.0000 0.0000 0.0000 0.3888 0.1716 0.1190 0.0000 0.0000 0.0000
26 0 3710.1325 0.0732 0.0705 30.6022 1.3256 1.4433 3.9390 0.1481 0.1546 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
