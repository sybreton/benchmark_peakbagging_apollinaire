# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:31:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0605 0.1129 0.1191 20.3933 1.2506 1.3271 3.9878 0.2488 0.2586 90.0000 0.0000 0.0000 0.5739 0.1256 0.0998 0.0000 0.0000 0.0000
26 0 3710.0066 0.0783 0.0810 29.6262 1.2704 1.3236 4.0118 0.1605 0.1642 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
