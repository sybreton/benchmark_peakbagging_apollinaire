# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:03:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0531 0.1004 0.1024 21.5099 1.2849 1.3544 3.8998 0.2539 0.2709 90.0000 0.0000 0.0000 0.5626 0.1228 0.0941 0.0000 0.0000 0.0000
26 0 3710.1034 0.0738 0.0739 29.4310 1.2333 1.3104 4.0190 0.1499 0.1558 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
