# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:19:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9118 0.0885 0.0890 22.1430 1.2747 1.4066 3.7818 0.2360 0.2384 90.0000 0.0000 0.0000 0.4351 0.1512 0.1065 0.0000 0.0000 0.0000
26 0 3710.0887 0.0690 0.0699 30.8037 1.3143 1.4165 3.9131 0.1474 0.1505 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
