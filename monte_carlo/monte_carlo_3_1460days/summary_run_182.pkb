# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:17:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0037 0.0846 0.0878 20.6300 1.1321 1.2362 3.8400 0.2222 0.2128 90.0000 0.0000 0.0000 0.2766 0.1703 0.1358 0.0000 0.0000 0.0000
26 0 3709.9325 0.0725 0.0687 30.4224 1.2870 1.3883 4.0797 0.1503 0.1510 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
