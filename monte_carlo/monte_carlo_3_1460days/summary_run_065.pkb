# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:26:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8641 0.0966 0.1011 20.9555 1.2466 1.3392 3.9869 0.2562 0.2634 90.0000 0.0000 0.0000 0.4837 0.1607 0.1110 0.0000 0.0000 0.0000
26 0 3709.9727 0.0725 0.0713 31.3525 1.3642 1.3973 3.9517 0.1485 0.1525 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
