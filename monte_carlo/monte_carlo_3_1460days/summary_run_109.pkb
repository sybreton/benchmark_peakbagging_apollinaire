# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:15:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0867 0.1050 0.1111 18.8762 1.0084 1.1673 4.2898 0.2590 0.2523 90.0000 0.0000 0.0000 0.3417 0.1950 0.1473 0.0000 0.0000 0.0000
26 0 3709.8558 0.0753 0.0738 31.5214 1.3562 1.4392 3.9846 0.1483 0.1575 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
