# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:07:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9064 0.0885 0.0884 20.0662 0.9997 1.0965 4.1167 0.2079 0.2038 90.0000 0.0000 0.0000 0.2184 0.1450 0.1507 0.0000 0.0000 0.0000
26 0 3710.0566 0.0713 0.0693 29.0834 1.2119 1.2928 4.0872 0.1471 0.1497 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
