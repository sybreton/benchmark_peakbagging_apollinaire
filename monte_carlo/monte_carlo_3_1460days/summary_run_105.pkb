# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:05:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9661 0.0846 0.0881 20.2859 0.9595 1.0375 4.1277 0.1925 0.1975 90.0000 0.0000 0.0000 0.1459 0.1018 0.1250 0.0000 0.0000 0.0000
26 0 3709.9710 0.0724 0.0692 30.3246 1.2965 1.3596 4.0323 0.1479 0.1565 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
