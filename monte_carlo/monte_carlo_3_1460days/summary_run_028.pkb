# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:54:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0558 0.1106 0.1159 20.5984 1.2277 1.2893 4.0945 0.2481 0.2638 90.0000 0.0000 0.0000 0.5288 0.1459 0.1082 0.0000 0.0000 0.0000
26 0 3709.9101 0.0866 0.0858 27.1252 1.1470 1.2085 4.3759 0.1656 0.1740 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
