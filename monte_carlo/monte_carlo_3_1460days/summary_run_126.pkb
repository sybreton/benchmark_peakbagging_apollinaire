# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:58:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9845 0.0902 0.0901 21.5884 1.1429 1.2417 3.9387 0.2140 0.2116 90.0000 0.0000 0.0000 0.2725 0.1670 0.1436 0.0000 0.0000 0.0000
26 0 3709.9195 0.0787 0.0813 27.4819 1.1651 1.2217 4.2221 0.1575 0.1643 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
