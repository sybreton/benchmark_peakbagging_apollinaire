# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:19:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1528 0.0909 0.0947 24.9736 1.4670 1.6265 3.5474 0.2164 0.2220 90.0000 0.0000 0.0000 0.5912 0.0935 0.0782 0.0000 0.0000 0.0000
26 0 3710.0444 0.0753 0.0725 29.5211 1.2834 1.3893 3.9045 0.1501 0.1514 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
