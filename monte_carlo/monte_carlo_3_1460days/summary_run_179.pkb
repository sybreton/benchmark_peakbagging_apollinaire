# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:09:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0262 0.0878 0.0914 20.3082 1.0184 1.0930 4.1479 0.2108 0.2117 90.0000 0.0000 0.0000 0.2069 0.1404 0.1449 0.0000 0.0000 0.0000
26 0 3710.0588 0.0728 0.0724 29.1866 1.2420 1.2866 4.0937 0.1512 0.1595 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
