# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:27:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0292 0.0973 0.1026 20.4829 1.0830 1.2196 4.1981 0.2309 0.2384 90.0000 0.0000 0.0000 0.4026 0.1893 0.1365 0.0000 0.0000 0.0000
26 0 3710.1051 0.0704 0.0712 29.5453 1.3083 1.4019 3.8376 0.1450 0.1547 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
