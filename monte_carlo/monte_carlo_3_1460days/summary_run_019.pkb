# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:32:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9191 0.0824 0.0832 22.7415 1.1275 1.3034 3.8664 0.2004 0.1919 90.0000 0.0000 0.0000 0.2473 0.1560 0.1383 0.0000 0.0000 0.0000
26 0 3709.9898 0.0697 0.0676 31.1699 1.3684 1.4180 3.9412 0.1456 0.1497 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
