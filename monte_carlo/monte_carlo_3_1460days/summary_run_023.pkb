# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:42:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0787 0.0977 0.1005 19.9327 0.9968 1.1081 4.4090 0.2391 0.2291 90.0000 0.0000 0.0000 0.2935 0.1823 0.1555 0.0000 0.0000 0.0000
26 0 3710.0469 0.0717 0.0695 31.1227 1.3668 1.4714 3.7865 0.1514 0.1560 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
