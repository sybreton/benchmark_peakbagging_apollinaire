# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:45:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0576 0.0913 0.0963 21.3751 1.1851 1.3222 3.9269 0.2304 0.2365 90.0000 0.0000 0.0000 0.4235 0.1655 0.1146 0.0000 0.0000 0.0000
26 0 3710.0760 0.0641 0.0636 34.6165 1.5667 1.6618 3.5345 0.1397 0.1410 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
