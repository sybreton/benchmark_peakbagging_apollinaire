# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:10:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0753 0.1079 0.1128 19.0655 1.1177 1.2131 4.2631 0.2731 0.2785 90.0000 0.0000 0.0000 0.4483 0.1829 0.1246 0.0000 0.0000 0.0000
26 0 3710.0221 0.0779 0.0797 29.4205 1.2528 1.2978 4.1577 0.1551 0.1599 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
