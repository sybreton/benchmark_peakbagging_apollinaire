# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:53:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0754 0.0976 0.0995 21.6017 1.2433 1.3555 4.0105 0.2422 0.2504 90.0000 0.0000 0.0000 0.4798 0.1456 0.1050 0.0000 0.0000 0.0000
26 0 3709.9776 0.0808 0.0792 27.8588 1.1746 1.2478 4.1741 0.1595 0.1650 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
