# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:32:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0786 0.0908 0.0952 20.9310 1.1085 1.2264 4.1549 0.2305 0.2309 90.0000 0.0000 0.0000 0.3095 0.1829 0.1457 0.0000 0.0000 0.0000
26 0 3710.0843 0.0770 0.0748 27.7326 1.2155 1.2547 4.1549 0.1599 0.1603 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
