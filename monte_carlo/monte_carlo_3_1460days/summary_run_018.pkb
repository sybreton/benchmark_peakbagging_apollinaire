# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:30:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9437 0.1035 0.1066 21.5803 1.2454 1.3475 3.8969 0.2396 0.2520 90.0000 0.0000 0.0000 0.5807 0.1121 0.0889 0.0000 0.0000 0.0000
26 0 3709.9968 0.0760 0.0748 29.3947 1.2450 1.3496 3.9917 0.1510 0.1584 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
