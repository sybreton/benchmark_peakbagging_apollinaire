# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:17:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0341 0.0948 0.1035 20.5589 1.2004 1.3142 3.9970 0.2485 0.2584 90.0000 0.0000 0.0000 0.4178 0.1663 0.1172 0.0000 0.0000 0.0000
26 0 3710.0004 0.0753 0.0748 30.6861 1.3193 1.4121 4.0155 0.1543 0.1560 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
