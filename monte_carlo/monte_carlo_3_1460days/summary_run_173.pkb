# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:54:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9001 0.0888 0.0913 23.9647 1.4467 1.5605 3.3155 0.2175 0.2236 90.0000 0.0000 0.0000 0.5497 0.0872 0.0721 0.0000 0.0000 0.0000
26 0 3709.9271 0.0749 0.0737 28.9208 1.2390 1.3013 4.1373 0.1564 0.1595 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
