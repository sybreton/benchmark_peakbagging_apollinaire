# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:40:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1788 0.1116 0.1159 19.7823 1.1139 1.2155 4.5389 0.2653 0.2855 90.0000 0.0000 0.0000 0.4709 0.1945 0.1325 0.0000 0.0000 0.0000
26 0 3710.0803 0.0701 0.0724 30.5141 1.3393 1.4603 3.6600 0.1522 0.1509 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
