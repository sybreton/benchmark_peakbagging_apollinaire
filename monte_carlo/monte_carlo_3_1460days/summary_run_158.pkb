# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:17:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0978 0.1059 0.1116 23.2725 1.3568 1.4611 3.7341 0.2166 0.2342 90.0000 0.0000 0.0000 0.6225 0.0973 0.0846 0.0000 0.0000 0.0000
26 0 3710.0376 0.0806 0.0791 28.3894 1.2507 1.3102 4.0158 0.1575 0.1589 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
