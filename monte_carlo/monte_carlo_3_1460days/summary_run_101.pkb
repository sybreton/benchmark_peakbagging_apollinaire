# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:55:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9232 0.0957 0.1024 20.4388 1.1236 1.2195 4.0924 0.2390 0.2370 90.0000 0.0000 0.0000 0.3661 0.1924 0.1373 0.0000 0.0000 0.0000
26 0 3709.9469 0.0788 0.0779 28.2351 1.2022 1.2328 4.1606 0.1601 0.1656 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
