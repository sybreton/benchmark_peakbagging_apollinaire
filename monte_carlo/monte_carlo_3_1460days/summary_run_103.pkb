# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:00:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9863 0.0935 0.1006 20.1676 1.0639 1.1993 4.1569 0.2424 0.2297 90.0000 0.0000 0.0000 0.3210 0.1901 0.1479 0.0000 0.0000 0.0000
26 0 3709.9889 0.0743 0.0740 29.3881 1.2472 1.3537 4.0838 0.1538 0.1607 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
