# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:30:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0925 0.0915 0.0932 20.8240 1.0875 1.1695 4.0685 0.2273 0.2256 90.0000 0.0000 0.0000 0.2637 0.1629 0.1424 0.0000 0.0000 0.0000
26 0 3709.9908 0.0716 0.0704 31.5320 1.3328 1.4202 3.8661 0.1458 0.1436 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
