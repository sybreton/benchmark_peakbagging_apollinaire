# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:39:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9666 0.0878 0.0884 21.8034 1.1769 1.2957 3.8658 0.2293 0.2176 90.0000 0.0000 0.0000 0.3173 0.1770 0.1331 0.0000 0.0000 0.0000
26 0 3709.9273 0.0710 0.0687 31.8759 1.3802 1.4645 3.9328 0.1417 0.1537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
