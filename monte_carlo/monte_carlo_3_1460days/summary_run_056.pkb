# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:04:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9103 0.0879 0.0877 19.7907 1.0172 1.0976 4.0411 0.2152 0.2118 90.0000 0.0000 0.0000 0.2571 0.1629 0.1450 0.0000 0.0000 0.0000
26 0 3710.1022 0.0731 0.0721 28.0625 1.1650 1.2290 4.1497 0.1488 0.1531 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
