# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:24:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1831 0.0949 0.1025 19.7777 1.0095 1.1316 4.3314 0.2346 0.2304 90.0000 0.0000 0.0000 0.2688 0.1727 0.1613 0.0000 0.0000 0.0000
26 0 3710.0181 0.0712 0.0713 30.2575 1.3140 1.3629 3.7951 0.1463 0.1488 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
