# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:24:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0377 0.0820 0.0868 21.8827 1.0866 1.2125 3.8774 0.1985 0.1942 90.0000 0.0000 0.0000 0.2294 0.1481 0.1405 0.0000 0.0000 0.0000
26 0 3710.0193 0.0700 0.0686 30.1760 1.3099 1.3700 3.9065 0.1412 0.1505 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
