# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:09:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9924 0.0957 0.0952 20.3764 1.0437 1.1517 4.1772 0.2311 0.2243 90.0000 0.0000 0.0000 0.3123 0.1823 0.1446 0.0000 0.0000 0.0000
26 0 3710.0336 0.0713 0.0717 30.1562 1.2834 1.3802 3.9419 0.1478 0.1529 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
