# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:07:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0492 0.0977 0.1002 22.3374 1.3470 1.3949 3.7948 0.2362 0.2450 90.0000 0.0000 0.0000 0.5919 0.1058 0.0847 0.0000 0.0000 0.0000
26 0 3710.0493 0.0707 0.0724 29.8721 1.2599 1.3648 3.9432 0.1513 0.1559 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
