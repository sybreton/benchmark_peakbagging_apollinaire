# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:55:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1064 0.0891 0.0971 19.5609 1.0047 1.0780 4.0156 0.2073 0.2118 90.0000 0.0000 0.0000 0.1755 0.1190 0.1389 0.0000 0.0000 0.0000
26 0 3709.8918 0.0845 0.0806 27.1159 1.1211 1.2104 4.4249 0.1611 0.1667 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
