# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:14:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9074 0.1007 0.1026 22.7693 1.3686 1.4318 3.5036 0.2126 0.2233 90.0000 0.0000 0.0000 0.6061 0.0886 0.0779 0.0000 0.0000 0.0000
26 0 3710.0151 0.0782 0.0744 29.2952 1.2403 1.3479 4.0912 0.1588 0.1562 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
