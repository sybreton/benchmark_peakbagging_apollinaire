# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:52:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9357 0.0984 0.1043 20.1236 1.1796 1.2583 3.9817 0.2358 0.2438 90.0000 0.0000 0.0000 0.3930 0.1861 0.1332 0.0000 0.0000 0.0000
26 0 3709.9160 0.0807 0.0802 27.9078 1.2028 1.2602 4.2564 0.1620 0.1667 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
