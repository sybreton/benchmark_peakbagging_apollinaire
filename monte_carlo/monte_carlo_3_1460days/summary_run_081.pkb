# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:06:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0199 0.0916 0.0917 21.6466 1.1446 1.2697 3.9945 0.2193 0.2133 90.0000 0.0000 0.0000 0.2937 0.1718 0.1443 0.0000 0.0000 0.0000
26 0 3709.9959 0.0744 0.0742 29.3112 1.2571 1.3402 3.9918 0.1558 0.1580 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
