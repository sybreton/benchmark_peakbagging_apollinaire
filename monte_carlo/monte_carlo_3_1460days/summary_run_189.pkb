# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:34:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2089 0.0985 0.0989 19.5933 1.0171 1.1407 4.3332 0.2485 0.2371 90.0000 0.0000 0.0000 0.3134 0.1900 0.1521 0.0000 0.0000 0.0000
26 0 3710.0201 0.0763 0.0744 28.0895 1.2351 1.2972 4.1075 0.1575 0.1672 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
