# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:21:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0994 0.0939 0.0945 21.0069 1.1155 1.2230 4.1834 0.2308 0.2282 90.0000 0.0000 0.0000 0.3560 0.1891 0.1343 0.0000 0.0000 0.0000
26 0 3710.0398 0.0729 0.0723 29.2909 1.2887 1.3956 3.9014 0.1519 0.1589 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
