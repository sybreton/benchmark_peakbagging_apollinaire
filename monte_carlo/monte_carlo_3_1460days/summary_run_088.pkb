# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:23:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8940 0.0921 0.0949 21.6972 1.2116 1.3238 3.8895 0.2345 0.2319 90.0000 0.0000 0.0000 0.4476 0.1569 0.1071 0.0000 0.0000 0.0000
26 0 3710.0055 0.0738 0.0710 29.6947 1.2785 1.3420 3.9241 0.1521 0.1456 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
