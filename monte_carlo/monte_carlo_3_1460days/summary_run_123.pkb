# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:50:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9229 0.0996 0.1033 22.9840 1.3940 1.4854 3.6739 0.2361 0.2426 90.0000 0.0000 0.0000 0.5854 0.0995 0.0790 0.0000 0.0000 0.0000
26 0 3709.9921 0.0787 0.0748 30.1565 1.2817 1.3973 4.0314 0.1506 0.1551 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
