# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:11:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1456 0.0913 0.0930 23.1806 1.3979 1.4875 3.7503 0.2407 0.2516 90.0000 0.0000 0.0000 0.4579 0.1416 0.1035 0.0000 0.0000 0.0000
26 0 3709.9759 0.0732 0.0709 29.4779 1.2805 1.3755 3.9215 0.1499 0.1528 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
