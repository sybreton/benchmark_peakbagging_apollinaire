# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:53:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0395 0.0963 0.1010 22.0225 1.3177 1.4446 3.7979 0.2519 0.2539 90.0000 0.0000 0.0000 0.5113 0.1249 0.0968 0.0000 0.0000 0.0000
26 0 3709.9908 0.0805 0.0820 26.3694 1.1287 1.1991 4.3722 0.1723 0.1760 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
