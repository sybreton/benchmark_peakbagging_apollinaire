# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:50:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0432 0.0934 0.0970 22.7393 1.3642 1.4807 3.5754 0.2286 0.2365 90.0000 0.0000 0.0000 0.5255 0.1071 0.0848 0.0000 0.0000 0.0000
26 0 3709.9847 0.0681 0.0669 32.6728 1.4010 1.5014 3.8102 0.1445 0.1479 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
