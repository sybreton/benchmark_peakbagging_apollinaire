# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:25:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0527 0.1007 0.1038 21.0279 1.2589 1.3124 3.8928 0.2437 0.2588 90.0000 0.0000 0.0000 0.5615 0.1238 0.0946 0.0000 0.0000 0.0000
26 0 3710.0130 0.0742 0.0726 29.3547 1.2677 1.3277 3.9558 0.1517 0.1539 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
