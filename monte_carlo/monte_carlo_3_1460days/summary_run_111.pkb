# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:20:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9629 0.0896 0.0900 21.6075 1.1288 1.2950 3.9082 0.2277 0.2229 90.0000 0.0000 0.0000 0.3391 0.1786 0.1264 0.0000 0.0000 0.0000
26 0 3709.9613 0.0748 0.0720 29.8805 1.2918 1.3942 3.8879 0.1475 0.1532 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
