# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:09:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8962 0.0833 0.0885 22.4175 1.2756 1.3851 3.6769 0.2247 0.2323 90.0000 0.0000 0.0000 0.4081 0.1478 0.1022 0.0000 0.0000 0.0000
26 0 3710.1695 0.0707 0.0707 30.3285 1.2871 1.3765 3.9253 0.1476 0.1489 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
