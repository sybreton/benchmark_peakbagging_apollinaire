# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:40:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0502 0.0895 0.0941 20.8573 1.1306 1.2496 3.9857 0.2286 0.2282 90.0000 0.0000 0.0000 0.3108 0.1802 0.1359 0.0000 0.0000 0.0000
26 0 3709.9771 0.0755 0.0755 28.3440 1.2284 1.2731 4.1882 0.1559 0.1590 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
