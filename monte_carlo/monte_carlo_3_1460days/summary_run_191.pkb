# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:39:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9703 0.0931 0.0966 20.3391 1.1901 1.2929 3.9591 0.2573 0.2586 90.0000 0.0000 0.0000 0.4277 0.1633 0.1119 0.0000 0.0000 0.0000
26 0 3709.9910 0.0764 0.0775 28.9728 1.2191 1.2871 4.3040 0.1590 0.1663 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
