# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:10:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8609 0.0919 0.0895 20.3391 1.1052 1.1924 4.0251 0.2231 0.2313 90.0000 0.0000 0.0000 0.3613 0.1903 0.1304 0.0000 0.0000 0.0000
26 0 3710.1064 0.0698 0.0683 30.6880 1.3173 1.4132 3.9455 0.1451 0.1535 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
