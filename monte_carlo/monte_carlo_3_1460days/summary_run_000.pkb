# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:45:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9769 0.0893 0.0876 22.4546 1.3254 1.4180 3.7597 0.2359 0.2488 90.0000 0.0000 0.0000 0.4401 0.1447 0.0990 0.0000 0.0000 0.0000
26 0 3709.9742 0.0744 0.0708 29.5980 1.2734 1.3438 4.0419 0.1556 0.1610 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
