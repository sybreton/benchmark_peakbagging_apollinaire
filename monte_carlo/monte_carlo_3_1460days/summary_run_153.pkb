# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:05:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9077 0.0935 0.0916 19.7144 1.0370 1.1421 4.0270 0.2339 0.2270 90.0000 0.0000 0.0000 0.2991 0.1776 0.1437 0.0000 0.0000 0.0000
26 0 3709.9859 0.0724 0.0709 30.2278 1.3076 1.3679 4.0917 0.1506 0.1537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
