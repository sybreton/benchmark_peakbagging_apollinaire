# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:50:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9570 0.0936 0.0986 20.4915 1.0872 1.2339 4.1179 0.2277 0.2230 90.0000 0.0000 0.0000 0.3306 0.1907 0.1438 0.0000 0.0000 0.0000
26 0 3710.0328 0.0718 0.0719 31.1218 1.3331 1.4442 3.9374 0.1461 0.1461 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
