# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:07:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9353 0.0945 0.0978 21.5799 1.2848 1.3832 3.8856 0.2457 0.2495 90.0000 0.0000 0.0000 0.4512 0.1605 0.1062 0.0000 0.0000 0.0000
26 0 3709.9979 0.0735 0.0734 29.3520 1.2309 1.3183 4.0832 0.1528 0.1604 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
