# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:12:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0605 0.0990 0.1005 19.4512 1.0390 1.1840 4.2008 0.2454 0.2381 90.0000 0.0000 0.0000 0.3527 0.1974 0.1462 0.0000 0.0000 0.0000
26 0 3710.0097 0.0786 0.0775 28.1164 1.1561 1.2985 4.2686 0.1620 0.1592 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
