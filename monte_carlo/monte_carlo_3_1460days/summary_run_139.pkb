# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:30:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0629 0.0912 0.0939 20.4206 1.1566 1.2401 4.1018 0.2458 0.2574 90.0000 0.0000 0.0000 0.3826 0.1874 0.1287 0.0000 0.0000 0.0000
26 0 3710.0210 0.0728 0.0720 29.1617 1.2524 1.3495 3.9673 0.1507 0.1607 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
