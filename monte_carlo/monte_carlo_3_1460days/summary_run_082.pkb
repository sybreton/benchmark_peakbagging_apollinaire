# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:08:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9646 0.1013 0.1018 20.3268 1.2130 1.2937 3.9470 0.2496 0.2686 90.0000 0.0000 0.0000 0.5156 0.1401 0.1027 0.0000 0.0000 0.0000
26 0 3710.0024 0.0711 0.0705 30.2695 1.2912 1.3649 3.9757 0.1493 0.1528 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
