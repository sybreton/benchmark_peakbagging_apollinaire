# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:46:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9778 0.0937 0.0973 21.2214 1.1481 1.2311 4.1741 0.2328 0.2381 90.0000 0.0000 0.0000 0.4234 0.1914 0.1273 0.0000 0.0000 0.0000
26 0 3710.1581 0.0702 0.0731 29.7442 1.2990 1.3635 3.7748 0.1435 0.1490 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
