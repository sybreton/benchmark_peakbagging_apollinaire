# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:27:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9813 0.0974 0.1026 18.8505 0.8958 0.9890 4.4096 0.2145 0.2160 90.0000 0.0000 0.0000 0.1760 0.1196 0.1501 0.0000 0.0000 0.0000
26 0 3710.0127 0.0720 0.0720 30.1625 1.2890 1.3771 4.0100 0.1536 0.1544 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
