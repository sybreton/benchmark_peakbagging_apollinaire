# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:29:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0409 0.0897 0.0908 21.4901 1.2182 1.3537 3.9169 0.2429 0.2447 90.0000 0.0000 0.0000 0.3974 0.1755 0.1184 0.0000 0.0000 0.0000
26 0 3710.1050 0.0734 0.0716 29.5919 1.2257 1.3055 4.0204 0.1494 0.1526 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
