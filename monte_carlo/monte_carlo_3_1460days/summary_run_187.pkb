# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:29:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9652 0.0928 0.0946 20.6268 1.1581 1.2778 3.9640 0.2473 0.2407 90.0000 0.0000 0.0000 0.3935 0.1837 0.1238 0.0000 0.0000 0.0000
26 0 3710.0697 0.0734 0.0734 29.3030 1.2170 1.3643 4.0741 0.1585 0.1586 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
