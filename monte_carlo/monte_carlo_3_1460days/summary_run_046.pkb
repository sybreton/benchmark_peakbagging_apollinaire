# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:39:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9212 0.0931 0.0949 19.3965 0.9509 1.0516 4.3237 0.2224 0.2138 90.0000 0.0000 0.0000 0.2242 0.1467 0.1555 0.0000 0.0000 0.0000
26 0 3709.9245 0.0692 0.0684 31.3044 1.3796 1.4750 3.8542 0.1476 0.1463 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
