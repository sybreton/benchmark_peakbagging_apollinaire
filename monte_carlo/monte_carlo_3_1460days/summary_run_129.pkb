# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:05:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9661 0.0918 0.0945 20.0251 1.0135 1.1440 4.0850 0.2214 0.2103 90.0000 0.0000 0.0000 0.2917 0.1761 0.1480 0.0000 0.0000 0.0000
26 0 3709.9772 0.0693 0.0680 31.5587 1.3763 1.4730 3.7954 0.1473 0.1537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
