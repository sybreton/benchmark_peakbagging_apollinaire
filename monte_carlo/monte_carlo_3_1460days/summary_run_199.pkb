# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:59:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9798 0.0921 0.0964 20.1709 1.0876 1.1855 4.1503 0.2439 0.2341 90.0000 0.0000 0.0000 0.3416 0.1886 0.1385 0.0000 0.0000 0.0000
26 0 3709.9472 0.0758 0.0747 27.9276 1.2103 1.2769 4.1093 0.1606 0.1598 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
