# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:17:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9989 0.0936 0.0946 19.6496 0.9855 1.0783 4.3117 0.2229 0.2260 90.0000 0.0000 0.0000 0.2353 0.1521 0.1533 0.0000 0.0000 0.0000
26 0 3710.0005 0.0661 0.0645 32.1161 1.3893 1.4951 3.8241 0.1514 0.1533 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
