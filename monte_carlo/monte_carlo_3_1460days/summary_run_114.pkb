# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:28:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9587 0.0996 0.1033 21.5618 1.2752 1.3690 3.6929 0.2226 0.2378 90.0000 0.0000 0.0000 0.5452 0.1118 0.0860 0.0000 0.0000 0.0000
26 0 3709.9015 0.0726 0.0717 31.2847 1.3797 1.4092 3.9554 0.1495 0.1541 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
