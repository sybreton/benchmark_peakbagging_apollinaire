# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:52:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1106 0.0938 0.0973 21.0519 1.0825 1.2071 4.2203 0.2345 0.2403 90.0000 0.0000 0.0000 0.3747 0.1936 0.1333 0.0000 0.0000 0.0000
26 0 3710.0741 0.0738 0.0728 29.2279 1.3058 1.3720 3.8683 0.1519 0.1567 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
