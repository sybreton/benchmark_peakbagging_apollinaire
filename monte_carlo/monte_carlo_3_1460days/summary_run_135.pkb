# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:20:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8718 0.0898 0.0932 21.2428 1.2566 1.3640 3.9333 0.2617 0.2696 90.0000 0.0000 0.0000 0.4226 0.1665 0.1108 0.0000 0.0000 0.0000
26 0 3709.9870 0.0720 0.0700 29.6321 1.2704 1.3078 4.0373 0.1522 0.1583 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
