# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:42:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1293 0.0984 0.1006 22.2582 1.3009 1.3791 3.9112 0.2407 0.2471 90.0000 0.0000 0.0000 0.5172 0.1254 0.0942 0.0000 0.0000 0.0000
26 0 3710.1214 0.0761 0.0751 30.9232 1.3401 1.4331 3.8514 0.1524 0.1543 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
