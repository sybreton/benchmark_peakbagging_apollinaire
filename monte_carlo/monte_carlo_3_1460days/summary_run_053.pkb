# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:56:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9269 0.0940 0.0950 21.2561 1.2013 1.3327 3.9108 0.2367 0.2433 90.0000 0.0000 0.0000 0.4376 0.1519 0.1062 0.0000 0.0000 0.0000
26 0 3709.9956 0.0710 0.0683 30.3560 1.2847 1.4075 3.9569 0.1437 0.1520 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
