# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:57:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0122 0.1066 0.1121 20.1872 1.1101 1.2205 4.2289 0.2480 0.2440 90.0000 0.0000 0.0000 0.3956 0.1950 0.1385 0.0000 0.0000 0.0000
26 0 3709.9630 0.0766 0.0741 31.0806 1.3474 1.4596 3.9613 0.1521 0.1560 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
