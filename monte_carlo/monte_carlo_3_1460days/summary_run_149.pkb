# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:55:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0454 0.1042 0.1020 21.2084 1.2644 1.3289 3.8631 0.2565 0.2777 90.0000 0.0000 0.0000 0.5675 0.1185 0.0913 0.0000 0.0000 0.0000
26 0 3710.0071 0.0701 0.0683 30.0900 1.2818 1.3819 3.8994 0.1444 0.1491 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
