# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:51:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0125 0.0982 0.1045 20.1697 1.1164 1.2113 4.1081 0.2395 0.2509 90.0000 0.0000 0.0000 0.4220 0.1734 0.1211 0.0000 0.0000 0.0000
26 0 3710.0916 0.0738 0.0726 30.4441 1.2960 1.3838 3.9854 0.1526 0.1611 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
