# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:20:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1598 0.1008 0.1058 19.8331 1.0506 1.1612 4.2548 0.2356 0.2356 90.0000 0.0000 0.0000 0.2914 0.1803 0.1528 0.0000 0.0000 0.0000
26 0 3709.8492 0.0765 0.0737 31.5041 1.3777 1.4538 3.9563 0.1479 0.1571 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
