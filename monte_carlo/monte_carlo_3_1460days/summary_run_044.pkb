# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:34:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1138 0.0984 0.1003 20.5799 1.1424 1.2585 4.1609 0.2660 0.2650 90.0000 0.0000 0.0000 0.4385 0.1766 0.1188 0.0000 0.0000 0.0000
26 0 3710.0567 0.0745 0.0708 29.9522 1.2926 1.3916 3.9545 0.1527 0.1569 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
