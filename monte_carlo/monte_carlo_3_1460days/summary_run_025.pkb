# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:47:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0555 0.0930 0.0982 20.8757 1.0906 1.1652 4.1051 0.2262 0.2172 90.0000 0.0000 0.0000 0.2850 0.1735 0.1440 0.0000 0.0000 0.0000
26 0 3709.9721 0.0785 0.0789 29.6776 1.2714 1.3266 4.1390 0.1536 0.1604 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
