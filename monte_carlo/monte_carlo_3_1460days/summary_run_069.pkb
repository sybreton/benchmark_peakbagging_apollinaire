# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:36:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9599 0.0917 0.0957 20.1554 0.9917 1.0673 4.1561 0.1999 0.2045 90.0000 0.0000 0.0000 0.1789 0.1226 0.1426 0.0000 0.0000 0.0000
26 0 3709.9452 0.0761 0.0765 29.5705 1.2304 1.3160 4.2165 0.1565 0.1571 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
