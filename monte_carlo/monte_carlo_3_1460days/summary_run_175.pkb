# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:59:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1005 0.0980 0.1000 21.5122 1.2265 1.2829 4.0869 0.2462 0.2617 90.0000 0.0000 0.0000 0.5125 0.1364 0.1026 0.0000 0.0000 0.0000
26 0 3710.0800 0.0697 0.0710 30.1015 1.3168 1.4264 3.8524 0.1445 0.1553 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
