# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:02:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9142 0.0984 0.1006 20.2512 1.1784 1.2847 4.0548 0.2714 0.2693 90.0000 0.0000 0.0000 0.4500 0.1582 0.1102 0.0000 0.0000 0.0000
26 0 3709.9256 0.0735 0.0731 29.0907 1.2228 1.3296 4.0364 0.1534 0.1548 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
