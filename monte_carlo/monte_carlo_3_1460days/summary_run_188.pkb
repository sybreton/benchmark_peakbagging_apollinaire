# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:32:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9563 0.0939 0.0951 20.8868 1.2676 1.3805 3.8340 0.2467 0.2511 90.0000 0.0000 0.0000 0.4853 0.1365 0.0995 0.0000 0.0000 0.0000
26 0 3710.0466 0.0705 0.0699 31.4503 1.3023 1.4066 3.9699 0.1461 0.1505 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
