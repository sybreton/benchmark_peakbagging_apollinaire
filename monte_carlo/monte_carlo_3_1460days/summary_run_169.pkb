# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:44:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9370 0.0921 0.0965 20.6997 1.1584 1.2991 3.9073 0.2472 0.2344 90.0000 0.0000 0.0000 0.4012 0.1785 0.1242 0.0000 0.0000 0.0000
26 0 3709.9913 0.0721 0.0729 29.0715 1.2386 1.2892 4.0741 0.1528 0.1560 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
