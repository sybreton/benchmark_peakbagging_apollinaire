# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:02:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9109 0.0916 0.0901 21.1748 1.1614 1.2354 3.9773 0.2292 0.2348 90.0000 0.0000 0.0000 0.3454 0.1856 0.1333 0.0000 0.0000 0.0000
26 0 3709.9200 0.0711 0.0710 30.2679 1.2862 1.4558 4.0370 0.1581 0.1574 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
