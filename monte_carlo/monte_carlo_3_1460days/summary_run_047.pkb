# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:42:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9854 0.0932 0.0965 20.7875 1.0960 1.2330 4.0630 0.2331 0.2291 90.0000 0.0000 0.0000 0.3747 0.1851 0.1303 0.0000 0.0000 0.0000
26 0 3709.9895 0.0716 0.0707 30.0968 1.2973 1.3743 3.9268 0.1470 0.1523 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
