# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:49:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0245 0.0908 0.0967 20.5497 1.1633 1.2915 4.0883 0.2547 0.2531 90.0000 0.0000 0.0000 0.3810 0.1872 0.1272 0.0000 0.0000 0.0000
26 0 3710.1679 0.0693 0.0703 31.2056 1.3317 1.4166 3.8804 0.1478 0.1507 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
