# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:52:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8803 0.0848 0.0887 23.5316 1.3242 1.4732 3.6782 0.2236 0.2214 90.0000 0.0000 0.0000 0.3607 0.1678 0.1167 0.0000 0.0000 0.0000
26 0 3709.9333 0.0766 0.0723 29.7681 1.2429 1.2812 4.1028 0.1487 0.1537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
