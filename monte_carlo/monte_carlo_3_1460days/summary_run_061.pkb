# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:16:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0450 0.1014 0.1033 21.7429 1.2610 1.3287 3.8900 0.2321 0.2553 90.0000 0.0000 0.0000 0.5713 0.1108 0.0871 0.0000 0.0000 0.0000
26 0 3709.9941 0.0805 0.0792 28.4832 1.1894 1.3001 4.1959 0.1602 0.1710 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
