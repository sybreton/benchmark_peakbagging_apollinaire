# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:27:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9949 0.0874 0.0889 21.4325 1.0627 1.1908 3.8887 0.2013 0.1948 90.0000 0.0000 0.0000 0.1956 0.1327 0.1385 0.0000 0.0000 0.0000
26 0 3709.9772 0.0725 0.0717 30.8996 1.3011 1.3921 4.0185 0.1480 0.1540 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
