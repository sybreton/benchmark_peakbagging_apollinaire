# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:04:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1599 0.0945 0.0976 20.8991 1.0956 1.1552 4.1710 0.2190 0.2184 90.0000 0.0000 0.0000 0.2954 0.1759 0.1490 0.0000 0.0000 0.0000
26 0 3710.0233 0.0715 0.0733 30.8070 1.3328 1.4160 3.8580 0.1475 0.1526 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
