# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:25:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1143 0.0928 0.0965 21.5865 1.1698 1.3317 4.0800 0.2375 0.2342 90.0000 0.0000 0.0000 0.3626 0.1883 0.1322 0.0000 0.0000 0.0000
26 0 3710.0355 0.0789 0.0796 27.7349 1.1762 1.2897 4.2172 0.1609 0.1626 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
