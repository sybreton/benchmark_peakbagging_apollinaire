# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:28:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1358 0.0982 0.1012 20.1583 1.0058 1.1064 4.3723 0.2374 0.2371 90.0000 0.0000 0.0000 0.2760 0.1702 0.1493 0.0000 0.0000 0.0000
26 0 3710.0138 0.0677 0.0680 32.0219 1.4136 1.4995 3.8250 0.1471 0.1504 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
