# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:00:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2244 0.1069 0.1154 21.0751 1.1556 1.2850 4.2341 0.2464 0.2474 90.0000 0.0000 0.0000 0.4756 0.1731 0.1261 0.0000 0.0000 0.0000
26 0 3710.0426 0.0780 0.0764 30.4755 1.3847 1.4668 3.7292 0.1500 0.1524 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
