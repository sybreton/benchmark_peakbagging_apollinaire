# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:02:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1441 0.0970 0.0976 20.8098 1.1286 1.2420 4.1366 0.2425 0.2458 90.0000 0.0000 0.0000 0.3829 0.1819 0.1280 0.0000 0.0000 0.0000
26 0 3709.9894 0.0738 0.0724 30.8601 1.3215 1.4154 3.9468 0.1514 0.1548 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
