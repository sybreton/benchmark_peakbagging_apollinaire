# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:10:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9597 0.0897 0.0935 22.7247 1.2947 1.3627 3.7027 0.2203 0.2294 90.0000 0.0000 0.0000 0.4618 0.1347 0.1001 0.0000 0.0000 0.0000
26 0 3710.0131 0.0711 0.0688 31.3556 1.3871 1.4888 3.7856 0.1433 0.1485 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
