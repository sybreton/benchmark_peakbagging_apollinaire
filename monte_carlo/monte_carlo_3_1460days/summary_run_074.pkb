# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:49:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0882 0.1003 0.1034 20.6926 1.0718 1.1875 4.2007 0.2184 0.2278 90.0000 0.0000 0.0000 0.3297 0.1950 0.1472 0.0000 0.0000 0.0000
26 0 3710.0775 0.0745 0.0731 31.5682 1.3382 1.4772 3.8057 0.1448 0.1506 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
