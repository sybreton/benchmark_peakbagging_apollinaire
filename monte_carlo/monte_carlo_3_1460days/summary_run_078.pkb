# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:58:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8165 0.0952 0.0951 21.1043 1.2711 1.3846 3.6680 0.2281 0.2380 90.0000 0.0000 0.0000 0.4809 0.1267 0.0961 0.0000 0.0000 0.0000
26 0 3709.9660 0.0730 0.0726 30.4302 1.2728 1.3592 4.1769 0.1532 0.1539 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
