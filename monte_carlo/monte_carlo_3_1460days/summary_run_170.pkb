# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:47:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9832 0.0903 0.0931 21.0361 1.1023 1.2829 3.9907 0.2398 0.2296 90.0000 0.0000 0.0000 0.3109 0.1843 0.1384 0.0000 0.0000 0.0000
26 0 3709.9418 0.0698 0.0697 29.0025 1.2248 1.3231 4.0334 0.1543 0.1575 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
