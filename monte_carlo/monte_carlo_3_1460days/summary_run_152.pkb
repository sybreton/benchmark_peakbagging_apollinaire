# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:02:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2099 0.0907 0.0966 21.7329 1.0931 1.2336 4.1611 0.2240 0.2197 90.0000 0.0000 0.0000 0.2720 0.1735 0.1464 0.0000 0.0000 0.0000
26 0 3709.9578 0.0732 0.0705 30.8849 1.3481 1.4327 3.8891 0.1471 0.1512 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
