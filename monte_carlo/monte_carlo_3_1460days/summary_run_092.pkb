# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:33:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0765 0.0928 0.0968 20.6411 1.1546 1.2859 4.1151 0.2435 0.2446 90.0000 0.0000 0.0000 0.3734 0.1894 0.1328 0.0000 0.0000 0.0000
26 0 3710.0682 0.0724 0.0698 30.8010 1.3232 1.4230 3.9454 0.1450 0.1436 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
