# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:01:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1278 0.0970 0.0988 22.2786 1.2025 1.3146 4.0471 0.2364 0.2288 90.0000 0.0000 0.0000 0.3887 0.1766 0.1223 0.0000 0.0000 0.0000
26 0 3710.0401 0.0727 0.0696 30.8368 1.3816 1.5243 3.7034 0.1551 0.1518 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
