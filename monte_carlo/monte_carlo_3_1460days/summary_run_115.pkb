# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:30:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9151 0.0919 0.0942 20.9973 1.1046 1.2246 3.9268 0.2184 0.2162 90.0000 0.0000 0.0000 0.2590 0.1580 0.1448 0.0000 0.0000 0.0000
26 0 3709.8724 0.0726 0.0723 31.0880 1.3132 1.4066 4.0632 0.1523 0.1571 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
