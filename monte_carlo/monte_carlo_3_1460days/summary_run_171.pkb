# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:49:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0999 0.0943 0.0983 23.1912 1.3144 1.4331 3.8623 0.2243 0.2275 90.0000 0.0000 0.0000 0.4386 0.1474 0.1086 0.0000 0.0000 0.0000
26 0 3710.0168 0.0788 0.0780 28.5943 1.2139 1.3288 4.0708 0.1563 0.1606 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
