# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:45:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9162 0.0988 0.1029 21.6986 1.3039 1.3481 3.7787 0.2469 0.2658 90.0000 0.0000 0.0000 0.6224 0.0954 0.0777 0.0000 0.0000 0.0000
26 0 3709.9930 0.0734 0.0719 31.0105 1.2981 1.3833 4.0225 0.1498 0.1539 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
