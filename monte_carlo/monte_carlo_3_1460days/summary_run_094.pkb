# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:38:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0034 0.0988 0.1055 19.1739 1.0440 1.1195 4.3732 0.2708 0.2516 90.0000 0.0000 0.0000 0.3731 0.2002 0.1458 0.0000 0.0000 0.0000
26 0 3710.0186 0.0710 0.0703 30.6432 1.3384 1.4488 3.8411 0.1429 0.1514 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
