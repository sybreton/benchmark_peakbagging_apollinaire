# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:20:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8350 0.0892 0.0926 20.5771 1.1574 1.3208 3.8630 0.2359 0.2361 90.0000 0.0000 0.0000 0.4053 0.1768 0.1169 0.0000 0.0000 0.0000
26 0 3710.0257 0.0715 0.0712 29.9354 1.2830 1.3457 4.0143 0.1471 0.1569 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
