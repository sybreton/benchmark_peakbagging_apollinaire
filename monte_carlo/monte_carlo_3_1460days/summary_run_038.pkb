# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:19:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1511 0.0944 0.0973 19.8230 0.9858 1.1178 4.2533 0.2344 0.2208 90.0000 0.0000 0.0000 0.2527 0.1642 0.1514 0.0000 0.0000 0.0000
26 0 3710.0909 0.0709 0.0711 29.9533 1.3029 1.4367 3.8824 0.1483 0.1504 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
