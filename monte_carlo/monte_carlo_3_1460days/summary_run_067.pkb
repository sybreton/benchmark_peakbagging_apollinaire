# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:31:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9342 0.0941 0.0948 19.9696 1.0361 1.1538 4.2276 0.2408 0.2434 90.0000 0.0000 0.0000 0.3552 0.1916 0.1425 0.0000 0.0000 0.0000
26 0 3710.0814 0.0650 0.0640 32.3928 1.4622 1.5908 3.6212 0.1464 0.1509 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
