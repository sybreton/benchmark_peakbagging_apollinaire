# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:04:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0805 0.0921 0.0922 20.6468 1.0820 1.2452 4.0065 0.2304 0.2198 90.0000 0.0000 0.0000 0.3133 0.1784 0.1395 0.0000 0.0000 0.0000
26 0 3709.9017 0.0712 0.0706 30.4056 1.2978 1.3992 4.0536 0.1531 0.1601 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
