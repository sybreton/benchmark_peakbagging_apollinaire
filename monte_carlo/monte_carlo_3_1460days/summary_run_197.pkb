# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:54:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0411 0.0881 0.0920 20.4433 1.0868 1.2081 4.0537 0.2348 0.2300 90.0000 0.0000 0.0000 0.3068 0.1760 0.1352 0.0000 0.0000 0.0000
26 0 3709.9760 0.0702 0.0676 30.3671 1.3166 1.3928 3.9359 0.1465 0.1541 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
