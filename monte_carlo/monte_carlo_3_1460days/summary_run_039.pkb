# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:22:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1161 0.0991 0.1044 20.2171 1.0464 1.1565 4.3565 0.2460 0.2349 90.0000 0.0000 0.0000 0.3280 0.1916 0.1473 0.0000 0.0000 0.0000
26 0 3710.0533 0.0764 0.0749 29.1544 1.2839 1.3348 4.0755 0.1562 0.1593 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
