# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:12:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0252 0.0903 0.0888 20.9607 1.0164 1.1156 4.1984 0.2096 0.2103 90.0000 0.0000 0.0000 0.2469 0.1541 0.1471 0.0000 0.0000 0.0000
26 0 3710.1746 0.0714 0.0675 29.9215 1.3216 1.4358 3.8426 0.1464 0.1538 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
