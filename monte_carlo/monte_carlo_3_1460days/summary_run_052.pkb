# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 00:54:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0376 0.1035 0.1076 19.7991 1.0056 1.1495 4.4140 0.2428 0.2339 90.0000 0.0000 0.0000 0.3275 0.1983 0.1578 0.0000 0.0000 0.0000
26 0 3710.0455 0.0792 0.0771 28.8158 1.2075 1.3458 3.9950 0.1567 0.1582 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
