# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 05:22:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0335 0.1009 0.1056 20.4005 1.1543 1.2470 4.0450 0.2407 0.2409 90.0000 0.0000 0.0000 0.4390 0.1815 0.1240 0.0000 0.0000 0.0000
26 0 3710.0135 0.0688 0.0694 32.7464 1.4078 1.5040 3.7945 0.1436 0.1450 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
