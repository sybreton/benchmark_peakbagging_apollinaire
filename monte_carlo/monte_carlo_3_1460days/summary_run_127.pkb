# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 04:00:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9118 0.0910 0.0876 20.6932 0.9881 1.0885 4.1959 0.2121 0.2067 90.0000 0.0000 0.0000 0.2237 0.1456 0.1497 0.0000 0.0000 0.0000
26 0 3710.0333 0.0757 0.0730 28.0939 1.2300 1.2839 4.1081 0.1557 0.1595 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
