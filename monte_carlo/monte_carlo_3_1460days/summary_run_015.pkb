# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:22:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1356 0.0870 0.0888 21.5479 1.0717 1.1587 4.0427 0.2082 0.2019 90.0000 0.0000 0.0000 0.2248 0.1464 0.1415 0.0000 0.0000 0.0000
26 0 3710.0465 0.0735 0.0702 30.3557 1.3014 1.4216 3.8915 0.1497 0.1528 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
