# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 23:15:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9920 0.0961 0.0960 21.1395 1.1808 1.2892 4.0645 0.2496 0.2481 90.0000 0.0000 0.0000 0.4384 0.1730 0.1124 0.0000 0.0000 0.0000
26 0 3710.0549 0.0767 0.0736 29.0770 1.2661 1.3233 4.1243 0.1552 0.1624 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
