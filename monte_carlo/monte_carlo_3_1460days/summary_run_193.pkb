# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 06:44:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9473 0.0947 0.0951 22.4338 1.2918 1.4018 3.7787 0.2411 0.2436 90.0000 0.0000 0.0000 0.5128 0.1162 0.0892 0.0000 0.0000 0.0000
26 0 3710.1206 0.0720 0.0719 29.9140 1.2642 1.3113 4.0373 0.1526 0.1515 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
