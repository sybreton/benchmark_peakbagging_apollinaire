# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:06:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0003 0.0928 0.0923 19.9874 1.0222 1.1337 4.0837 0.2208 0.2115 90.0000 0.0000 0.0000 0.2261 0.1476 0.1457 0.0000 0.0000 0.0000
26 0 3709.9406 0.0772 0.0771 26.9811 1.1163 1.1907 4.3639 0.1626 0.1727 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
