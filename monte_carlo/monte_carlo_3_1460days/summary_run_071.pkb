# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 01:41:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9319 0.0903 0.0956 20.9848 1.1671 1.2854 3.9149 0.2271 0.2305 90.0000 0.0000 0.0000 0.3384 0.1844 0.1312 0.0000 0.0000 0.0000
26 0 3710.0781 0.0730 0.0704 30.0954 1.2461 1.3384 3.9845 0.1453 0.1522 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
