# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:16:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9100 0.0927 0.0922 21.8002 1.3010 1.4203 3.5915 0.2307 0.2351 90.0000 0.0000 0.0000 0.4361 0.1422 0.0993 0.0000 0.0000 0.0000
26 0 3709.9588 0.0790 0.0757 27.7600 1.1557 1.2069 4.2963 0.1616 0.1616 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
