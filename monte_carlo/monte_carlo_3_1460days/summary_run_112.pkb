# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 03:23:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9497 0.0959 0.0971 20.4020 1.2108 1.3734 3.8932 0.2570 0.2737 90.0000 0.0000 0.0000 0.4550 0.1639 0.1076 0.0000 0.0000 0.0000
26 0 3709.9014 0.0753 0.0739 29.0928 1.2362 1.2918 4.1182 0.1583 0.1648 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
