# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 02:26:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9993 0.0944 0.0941 21.2262 1.0583 1.1642 4.1486 0.2221 0.2051 90.0000 0.0000 0.0000 0.2502 0.1549 0.1482 0.0000 0.0000 0.0000
26 0 3710.0135 0.0754 0.0723 29.9480 1.3042 1.3803 3.9280 0.1509 0.1539 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
