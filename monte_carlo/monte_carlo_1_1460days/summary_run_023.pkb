# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:37:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9925 0.0158 0.0158 13.2806 1.3869 1.5449 0.3753 0.0332 0.0382 90.0000 0.0000 0.0000 0.4111 0.0086 0.0087 0.0000 0.0000 0.0000
12 0 1822.0191 0.0145 0.0146 21.7019 2.7187 3.2611 0.3460 0.0305 0.0322 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
