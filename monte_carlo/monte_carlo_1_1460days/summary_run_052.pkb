# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:43:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0108 0.0158 0.0159 13.8795 1.4279 1.6281 0.3763 0.0333 0.0363 90.0000 0.0000 0.0000 0.4021 0.0082 0.0083 0.0000 0.0000 0.0000
12 0 1822.0054 0.0128 0.0130 26.7279 3.6410 4.4266 0.3005 0.0285 0.0299 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
