# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:12:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0141 0.0164 0.0156 14.1308 1.4618 1.6946 0.3670 0.0324 0.0352 90.0000 0.0000 0.0000 0.3988 0.0087 0.0085 0.0000 0.0000 0.0000
12 0 1822.0103 0.0149 0.0147 19.1715 2.3976 3.0172 0.3550 0.0343 0.0355 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
