# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:46:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0147 0.0139 0.0142 15.1244 1.5792 1.8020 0.3289 0.0296 0.0321 90.0000 0.0000 0.0000 0.4000 0.0074 0.0072 0.0000 0.0000 0.0000
12 0 1821.9995 0.0145 0.0144 19.2347 2.5591 3.1082 0.3329 0.0318 0.0343 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
