# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:54:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0175 0.0137 0.0140 14.2300 1.5220 1.7625 0.3309 0.0313 0.0345 90.0000 0.0000 0.0000 0.3999 0.0081 0.0082 0.0000 0.0000 0.0000
12 0 1822.0113 0.0148 0.0147 20.9638 2.5968 3.2112 0.3458 0.0313 0.0325 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
