# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:55:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9905 0.0143 0.0140 14.0072 1.4549 1.6166 0.3415 0.0307 0.0340 90.0000 0.0000 0.0000 0.4153 0.0076 0.0075 0.0000 0.0000 0.0000
12 0 1821.9935 0.0152 0.0151 18.9107 2.4221 2.9291 0.3550 0.0337 0.0360 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
