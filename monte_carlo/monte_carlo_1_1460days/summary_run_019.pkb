# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:28:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0033 0.0156 0.0152 14.2324 1.4601 1.6428 0.3641 0.0301 0.0335 90.0000 0.0000 0.0000 0.3964 0.0083 0.0086 0.0000 0.0000 0.0000
12 0 1822.0142 0.0156 0.0149 19.4891 2.4367 2.9633 0.3662 0.0343 0.0362 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
