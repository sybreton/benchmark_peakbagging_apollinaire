# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:50:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9813 0.0141 0.0141 14.5443 1.5170 1.7090 0.3310 0.0285 0.0317 90.0000 0.0000 0.0000 0.3969 0.0074 0.0074 0.0000 0.0000 0.0000
12 0 1821.9954 0.0159 0.0162 16.9882 2.1067 2.6095 0.3793 0.0347 0.0365 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
