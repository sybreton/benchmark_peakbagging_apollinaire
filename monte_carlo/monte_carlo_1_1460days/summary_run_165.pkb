# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:01:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9931 0.0131 0.0129 15.7199 1.6574 1.8303 0.2978 0.0254 0.0283 90.0000 0.0000 0.0000 0.3967 0.0069 0.0071 0.0000 0.0000 0.0000
12 0 1821.9881 0.0154 0.0154 19.2255 2.3158 2.8695 0.3751 0.0331 0.0346 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
