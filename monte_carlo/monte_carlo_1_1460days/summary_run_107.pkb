# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:49:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0266 0.0156 0.0161 12.7207 1.3448 1.5247 0.3521 0.0317 0.0354 90.0000 0.0000 0.0000 0.4136 0.0083 0.0086 0.0000 0.0000 0.0000
12 0 1821.9793 0.0160 0.0160 17.8446 2.2110 2.7385 0.3737 0.0353 0.0373 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
