# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:54:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9900 0.0155 0.0155 14.0943 1.4401 1.6233 0.3673 0.0319 0.0355 90.0000 0.0000 0.0000 0.4026 0.0082 0.0084 0.0000 0.0000 0.0000
12 0 1822.0087 0.0149 0.0147 20.9966 2.6602 3.2208 0.3473 0.0313 0.0334 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
