# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:05:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9964 0.0159 0.0160 14.2833 1.4451 1.6514 0.3616 0.0316 0.0338 90.0000 0.0000 0.0000 0.3891 0.0086 0.0085 0.0000 0.0000 0.0000
12 0 1822.0147 0.0159 0.0155 17.8011 2.2494 2.6755 0.3773 0.0337 0.0375 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
