# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:17:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0100 0.0144 0.0141 14.9626 1.5969 1.8291 0.3276 0.0296 0.0330 90.0000 0.0000 0.0000 0.3975 0.0078 0.0078 0.0000 0.0000 0.0000
12 0 1822.0138 0.0159 0.0163 16.7631 2.1096 2.5425 0.3738 0.0346 0.0372 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
