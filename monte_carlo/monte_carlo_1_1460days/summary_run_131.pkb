# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:44:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9791 0.0149 0.0154 13.5305 1.4490 1.6249 0.3594 0.0330 0.0377 90.0000 0.0000 0.0000 0.3964 0.0083 0.0083 0.0000 0.0000 0.0000
12 0 1821.9965 0.0160 0.0164 17.2215 2.1678 2.5706 0.3890 0.0346 0.0377 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
