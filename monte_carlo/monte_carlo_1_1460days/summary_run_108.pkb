# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:51:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0008 0.0152 0.0148 14.6316 1.4937 1.7022 0.3541 0.0303 0.0334 90.0000 0.0000 0.0000 0.4135 0.0083 0.0081 0.0000 0.0000 0.0000
12 0 1821.9787 0.0159 0.0153 18.4612 2.1945 2.6888 0.3884 0.0336 0.0355 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
