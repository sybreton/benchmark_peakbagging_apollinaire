# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:12:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0216 0.0154 0.0155 13.9361 1.4742 1.7292 0.3508 0.0335 0.0353 90.0000 0.0000 0.0000 0.3992 0.0089 0.0088 0.0000 0.0000 0.0000
12 0 1822.0227 0.0143 0.0141 22.3853 2.7398 3.3927 0.3403 0.0306 0.0318 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
