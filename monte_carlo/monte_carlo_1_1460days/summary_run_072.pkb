# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:29:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0395 0.0187 0.0191 11.6003 1.2031 1.3542 0.4290 0.0388 0.0440 90.0000 0.0000 0.0000 0.4038 0.0098 0.0096 0.0000 0.0000 0.0000
12 0 1822.0292 0.0171 0.0173 15.4066 1.8941 2.3265 0.4277 0.0395 0.0427 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
