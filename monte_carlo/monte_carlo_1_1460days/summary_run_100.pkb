# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:33:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9775 0.0170 0.0173 12.4781 1.3132 1.4253 0.3809 0.0332 0.0372 90.0000 0.0000 0.0000 0.4059 0.0090 0.0087 0.0000 0.0000 0.0000
12 0 1821.9959 0.0137 0.0137 23.3265 3.0395 3.8978 0.3181 0.0305 0.0323 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
