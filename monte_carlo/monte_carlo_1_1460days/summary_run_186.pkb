# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:48:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9891 0.0129 0.0133 15.3638 1.6539 1.9021 0.3083 0.0284 0.0309 90.0000 0.0000 0.0000 0.4021 0.0067 0.0068 0.0000 0.0000 0.0000
12 0 1822.0121 0.0148 0.0154 18.8277 2.3821 2.7990 0.3638 0.0334 0.0357 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
