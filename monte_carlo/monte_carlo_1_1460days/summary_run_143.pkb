# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:11:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9831 0.0155 0.0157 12.6762 1.3106 1.4658 0.3606 0.0322 0.0356 90.0000 0.0000 0.0000 0.4050 0.0087 0.0086 0.0000 0.0000 0.0000
12 0 1821.9878 0.0146 0.0143 19.7712 2.6424 3.1442 0.3227 0.0312 0.0327 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
