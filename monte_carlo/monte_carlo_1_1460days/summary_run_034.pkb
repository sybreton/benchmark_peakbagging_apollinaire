# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:02:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0212 0.0149 0.0151 13.0850 1.3795 1.5708 0.3419 0.0312 0.0344 90.0000 0.0000 0.0000 0.3965 0.0079 0.0079 0.0000 0.0000 0.0000
12 0 1821.9948 0.0128 0.0132 26.4545 3.6478 4.5356 0.2956 0.0285 0.0309 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
