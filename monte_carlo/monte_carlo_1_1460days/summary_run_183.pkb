# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:42:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9898 0.0164 0.0160 12.3895 1.3775 1.5235 0.3708 0.0352 0.0394 90.0000 0.0000 0.0000 0.4008 0.0086 0.0085 0.0000 0.0000 0.0000
12 0 1821.9995 0.0126 0.0126 26.2977 3.7805 4.4910 0.2823 0.0279 0.0308 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
