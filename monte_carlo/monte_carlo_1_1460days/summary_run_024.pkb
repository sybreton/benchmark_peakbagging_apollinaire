# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:40:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0002 0.0133 0.0130 15.1930 1.6087 1.9229 0.2997 0.0277 0.0303 90.0000 0.0000 0.0000 0.4073 0.0070 0.0071 0.0000 0.0000 0.0000
12 0 1821.9885 0.0153 0.0150 19.2024 2.4010 2.9719 0.3646 0.0334 0.0346 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
