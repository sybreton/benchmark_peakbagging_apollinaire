# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:20:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9970 0.0154 0.0154 14.0442 1.4784 1.6916 0.3490 0.0309 0.0339 90.0000 0.0000 0.0000 0.3913 0.0083 0.0085 0.0000 0.0000 0.0000
12 0 1821.9866 0.0123 0.0124 28.0409 4.0260 5.0982 0.2697 0.0276 0.0299 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
