# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:28:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9942 0.0153 0.0152 14.9513 1.5245 1.7592 0.3566 0.0310 0.0335 90.0000 0.0000 0.0000 0.4004 0.0080 0.0080 0.0000 0.0000 0.0000
12 0 1821.9944 0.0148 0.0146 17.9282 2.2992 2.8449 0.3424 0.0330 0.0350 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
