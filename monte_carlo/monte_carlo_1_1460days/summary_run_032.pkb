# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:58:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9874 0.0136 0.0138 15.1959 1.6274 1.8622 0.3350 0.0314 0.0347 90.0000 0.0000 0.0000 0.3994 0.0081 0.0080 0.0000 0.0000 0.0000
12 0 1822.0021 0.0157 0.0155 17.6991 2.2059 2.7413 0.3574 0.0339 0.0358 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
