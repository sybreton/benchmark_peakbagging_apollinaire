# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:09:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0096 0.0160 0.0158 13.7728 1.4589 1.6578 0.3581 0.0325 0.0363 90.0000 0.0000 0.0000 0.3893 0.0081 0.0084 0.0000 0.0000 0.0000
12 0 1821.9904 0.0137 0.0141 23.1611 2.9624 3.8029 0.3203 0.0306 0.0307 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
