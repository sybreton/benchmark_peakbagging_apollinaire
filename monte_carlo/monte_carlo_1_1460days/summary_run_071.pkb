# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:27:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9759 0.0143 0.0142 14.6276 1.4537 1.7265 0.3276 0.0288 0.0314 90.0000 0.0000 0.0000 0.3892 0.0072 0.0075 0.0000 0.0000 0.0000
12 0 1821.9914 0.0157 0.0158 17.7051 2.2753 2.7525 0.3644 0.0352 0.0370 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
