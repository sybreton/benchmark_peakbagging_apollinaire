# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:49:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9966 0.0146 0.0142 14.5854 1.4921 1.6926 0.3337 0.0286 0.0324 90.0000 0.0000 0.0000 0.4032 0.0076 0.0076 0.0000 0.0000 0.0000
12 0 1821.9954 0.0139 0.0138 23.3287 3.0823 3.8423 0.3147 0.0311 0.0316 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
