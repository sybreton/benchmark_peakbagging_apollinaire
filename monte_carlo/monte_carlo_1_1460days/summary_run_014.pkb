# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:17:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0158 0.0158 0.0154 13.5411 1.4528 1.6877 0.3490 0.0306 0.0347 90.0000 0.0000 0.0000 0.4037 0.0086 0.0087 0.0000 0.0000 0.0000
12 0 1822.0055 0.0145 0.0149 20.8487 2.6276 3.2117 0.3483 0.0325 0.0333 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
