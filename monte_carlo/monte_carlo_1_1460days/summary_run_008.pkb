# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:03:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9857 0.0191 0.0190 11.3044 1.2861 1.4265 0.3930 0.0385 0.0439 90.0000 0.0000 0.0000 0.3834 0.0103 0.0099 0.0000 0.0000 0.0000
12 0 1821.9755 0.0155 0.0157 17.7591 2.2389 2.7782 0.3590 0.0337 0.0353 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
