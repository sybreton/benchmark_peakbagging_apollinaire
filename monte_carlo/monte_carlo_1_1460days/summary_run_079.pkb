# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:45:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9928 0.0154 0.0155 13.6206 1.4365 1.6118 0.3598 0.0326 0.0356 90.0000 0.0000 0.0000 0.4113 0.0082 0.0085 0.0000 0.0000 0.0000
12 0 1822.0128 0.0149 0.0149 21.4539 2.6793 3.2059 0.3496 0.0308 0.0333 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
