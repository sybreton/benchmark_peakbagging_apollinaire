# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:14:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0057 0.0147 0.0147 14.8347 1.4851 1.6898 0.3548 0.0309 0.0337 90.0000 0.0000 0.0000 0.3867 0.0074 0.0076 0.0000 0.0000 0.0000
12 0 1821.9968 0.0142 0.0145 21.9760 2.8633 3.4768 0.3385 0.0311 0.0333 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
