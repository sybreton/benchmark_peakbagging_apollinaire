# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:22:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0039 0.0176 0.0181 11.6511 1.2520 1.3824 0.3933 0.0352 0.0396 90.0000 0.0000 0.0000 0.4081 0.0089 0.0088 0.0000 0.0000 0.0000
12 0 1822.0068 0.0139 0.0142 21.1068 2.7991 3.5491 0.3216 0.0316 0.0328 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
