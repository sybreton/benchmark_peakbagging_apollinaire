# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:12:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9939 0.0172 0.0174 12.6132 1.3555 1.5358 0.3888 0.0361 0.0417 90.0000 0.0000 0.0000 0.3860 0.0095 0.0093 0.0000 0.0000 0.0000
12 0 1821.9963 0.0160 0.0161 19.3722 2.3079 2.6728 0.3945 0.0333 0.0364 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
