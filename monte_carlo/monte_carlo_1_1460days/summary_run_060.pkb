# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:02:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0023 0.0139 0.0138 15.5216 1.5640 1.8078 0.3294 0.0281 0.0299 90.0000 0.0000 0.0000 0.4050 0.0071 0.0073 0.0000 0.0000 0.0000
12 0 1822.0132 0.0160 0.0156 17.7465 2.2249 2.7311 0.3755 0.0348 0.0378 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
