# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:23:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9802 0.0168 0.0167 12.3415 1.3378 1.5158 0.3849 0.0353 0.0410 90.0000 0.0000 0.0000 0.4085 0.0095 0.0096 0.0000 0.0000 0.0000
12 0 1822.0073 0.0139 0.0139 23.5798 2.9920 3.7630 0.3312 0.0298 0.0315 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
