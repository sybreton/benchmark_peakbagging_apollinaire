# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:25:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9925 0.0152 0.0153 14.4719 1.4685 1.7431 0.3505 0.0310 0.0328 90.0000 0.0000 0.0000 0.3983 0.0086 0.0086 0.0000 0.0000 0.0000
12 0 1821.9836 0.0140 0.0151 21.5717 2.7142 3.3591 0.3432 0.0321 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
