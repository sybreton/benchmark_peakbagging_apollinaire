# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:25:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0096 0.0180 0.0180 12.3706 1.3396 1.4697 0.3891 0.0354 0.0405 90.0000 0.0000 0.0000 0.3906 0.0089 0.0090 0.0000 0.0000 0.0000
12 0 1821.9952 0.0127 0.0129 23.5449 3.3148 4.2249 0.2838 0.0295 0.0314 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
