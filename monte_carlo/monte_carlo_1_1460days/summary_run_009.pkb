# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:06:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9930 0.0139 0.0141 15.2191 1.6085 1.8515 0.3387 0.0307 0.0334 90.0000 0.0000 0.0000 0.3924 0.0076 0.0078 0.0000 0.0000 0.0000
12 0 1822.0045 0.0139 0.0139 22.1948 2.8836 3.5607 0.3214 0.0307 0.0319 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
