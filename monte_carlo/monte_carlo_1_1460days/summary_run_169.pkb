# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:10:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0043 0.0138 0.0133 15.8442 1.6636 1.9562 0.3167 0.0281 0.0304 90.0000 0.0000 0.0000 0.3998 0.0069 0.0069 0.0000 0.0000 0.0000
12 0 1821.9892 0.0136 0.0138 25.4595 3.3308 4.2049 0.3068 0.0290 0.0306 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
