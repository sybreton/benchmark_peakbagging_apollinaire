# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:07:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0002 0.0131 0.0133 15.0193 1.6285 1.8423 0.3097 0.0286 0.0322 90.0000 0.0000 0.0000 0.3930 0.0071 0.0070 0.0000 0.0000 0.0000
12 0 1822.0108 0.0165 0.0168 17.2529 2.0694 2.5655 0.4048 0.0365 0.0375 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
