# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:24:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0091 0.0154 0.0158 14.6006 1.4965 1.7275 0.3555 0.0308 0.0348 90.0000 0.0000 0.0000 0.4017 0.0080 0.0079 0.0000 0.0000 0.0000
12 0 1821.9725 0.0128 0.0132 22.7450 3.0403 3.8323 0.2928 0.0287 0.0313 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
