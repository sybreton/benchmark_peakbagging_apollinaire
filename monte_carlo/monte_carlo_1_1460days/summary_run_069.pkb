# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:22:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9790 0.0149 0.0142 13.7795 1.4496 1.6231 0.3408 0.0304 0.0334 90.0000 0.0000 0.0000 0.4018 0.0080 0.0077 0.0000 0.0000 0.0000
12 0 1822.0058 0.0154 0.0152 19.6350 2.4656 3.0173 0.3662 0.0332 0.0368 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
