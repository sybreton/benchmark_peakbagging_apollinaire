# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:39:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0033 0.0160 0.0161 14.3691 1.4958 1.6745 0.3716 0.0328 0.0368 90.0000 0.0000 0.0000 0.3972 0.0087 0.0085 0.0000 0.0000 0.0000
12 0 1821.9564 0.0153 0.0153 18.2170 2.3032 2.7382 0.3636 0.0335 0.0363 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
