# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:04:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0156 0.0152 0.0149 13.7958 1.4634 1.6835 0.3318 0.0302 0.0338 90.0000 0.0000 0.0000 0.3853 0.0074 0.0075 0.0000 0.0000 0.0000
12 0 1822.0076 0.0142 0.0142 20.9465 2.7574 3.3460 0.3287 0.0319 0.0331 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
