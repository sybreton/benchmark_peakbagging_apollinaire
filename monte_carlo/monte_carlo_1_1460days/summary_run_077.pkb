# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:40:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9995 0.0156 0.0157 13.2888 1.4173 1.6037 0.3459 0.0313 0.0354 90.0000 0.0000 0.0000 0.3929 0.0087 0.0087 0.0000 0.0000 0.0000
12 0 1821.9935 0.0160 0.0160 17.0799 2.1297 2.5903 0.3750 0.0354 0.0367 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
