# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:16:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0038 0.0161 0.0162 13.2461 1.3574 1.5505 0.3761 0.0337 0.0377 90.0000 0.0000 0.0000 0.4026 0.0087 0.0087 0.0000 0.0000 0.0000
12 0 1822.0238 0.0152 0.0154 19.2031 2.3823 2.9208 0.3610 0.0326 0.0343 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
