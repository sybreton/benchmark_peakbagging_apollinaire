# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:52:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0116 0.0163 0.0169 12.9216 1.3770 1.5366 0.3869 0.0361 0.0397 90.0000 0.0000 0.0000 0.4013 0.0090 0.0092 0.0000 0.0000 0.0000
12 0 1821.9942 0.0144 0.0145 21.2799 2.6592 3.3159 0.3480 0.0314 0.0327 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
