# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:08:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9947 0.0144 0.0147 14.8532 1.5537 1.8105 0.3234 0.0286 0.0310 90.0000 0.0000 0.0000 0.3930 0.0076 0.0076 0.0000 0.0000 0.0000
12 0 1821.9884 0.0143 0.0141 23.2539 3.0127 3.7187 0.3256 0.0298 0.0315 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
