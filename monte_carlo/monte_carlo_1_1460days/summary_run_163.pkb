# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:56:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0203 0.0141 0.0138 14.5325 1.5073 1.7404 0.3273 0.0290 0.0318 90.0000 0.0000 0.0000 0.4042 0.0075 0.0073 0.0000 0.0000 0.0000
12 0 1822.0079 0.0141 0.0144 22.4213 2.8438 3.4058 0.3344 0.0303 0.0326 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
