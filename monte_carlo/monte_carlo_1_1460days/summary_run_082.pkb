# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:52:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9789 0.0148 0.0147 13.8796 1.4282 1.6546 0.3515 0.0323 0.0350 90.0000 0.0000 0.0000 0.4032 0.0080 0.0080 0.0000 0.0000 0.0000
12 0 1822.0084 0.0138 0.0137 23.6358 3.0286 3.8704 0.3241 0.0304 0.0313 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
