# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:31:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9891 0.0122 0.0120 17.0251 1.8201 2.1436 0.2905 0.0260 0.0291 90.0000 0.0000 0.0000 0.4052 0.0066 0.0065 0.0000 0.0000 0.0000
12 0 1822.0205 0.0149 0.0146 18.1025 2.2961 2.8387 0.3543 0.0334 0.0355 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
