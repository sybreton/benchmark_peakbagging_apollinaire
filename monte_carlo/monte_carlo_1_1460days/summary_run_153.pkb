# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:34:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9931 0.0143 0.0141 14.5153 1.4817 1.7218 0.3271 0.0287 0.0315 90.0000 0.0000 0.0000 0.4082 0.0076 0.0075 0.0000 0.0000 0.0000
12 0 1821.9958 0.0146 0.0147 20.1656 2.4778 3.0595 0.3564 0.0320 0.0337 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
