# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:18:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0090 0.0164 0.0159 13.2779 1.4108 1.6172 0.3717 0.0343 0.0390 90.0000 0.0000 0.0000 0.4008 0.0091 0.0089 0.0000 0.0000 0.0000
12 0 1822.0192 0.0155 0.0155 17.6548 2.2555 2.8179 0.3529 0.0337 0.0350 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
