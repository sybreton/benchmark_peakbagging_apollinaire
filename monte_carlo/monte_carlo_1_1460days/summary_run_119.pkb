# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:16:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0033 0.0180 0.0184 12.2747 1.3101 1.4755 0.4056 0.0382 0.0438 90.0000 0.0000 0.0000 0.4070 0.0093 0.0092 0.0000 0.0000 0.0000
12 0 1822.0519 0.0162 0.0160 15.7833 2.0422 2.4860 0.3837 0.0366 0.0403 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
