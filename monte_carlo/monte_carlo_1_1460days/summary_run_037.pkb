# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:09:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0035 0.0154 0.0157 13.6845 1.4192 1.6547 0.3421 0.0304 0.0331 90.0000 0.0000 0.0000 0.3910 0.0080 0.0078 0.0000 0.0000 0.0000
12 0 1821.9810 0.0166 0.0166 16.2586 2.0289 2.4943 0.4153 0.0392 0.0417 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
