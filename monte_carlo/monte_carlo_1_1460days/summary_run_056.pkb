# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:53:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9995 0.0163 0.0166 12.4306 1.3586 1.4677 0.3564 0.0328 0.0370 90.0000 0.0000 0.0000 0.4003 0.0089 0.0087 0.0000 0.0000 0.0000
12 0 1821.9827 0.0168 0.0171 16.2968 1.9815 2.4740 0.4192 0.0389 0.0412 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
