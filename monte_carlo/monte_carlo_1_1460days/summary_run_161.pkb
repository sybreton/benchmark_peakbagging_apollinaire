# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:52:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0210 0.0148 0.0148 14.7240 1.5140 1.7176 0.3517 0.0308 0.0341 90.0000 0.0000 0.0000 0.4101 0.0078 0.0077 0.0000 0.0000 0.0000
12 0 1822.0038 0.0161 0.0161 17.7532 2.1817 2.5399 0.4086 0.0368 0.0400 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
