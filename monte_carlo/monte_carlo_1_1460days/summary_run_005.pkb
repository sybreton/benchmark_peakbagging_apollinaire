# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:57:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9811 0.0152 0.0153 13.7301 1.4692 1.6845 0.3437 0.0316 0.0346 90.0000 0.0000 0.0000 0.3852 0.0086 0.0087 0.0000 0.0000 0.0000
12 0 1822.0345 0.0152 0.0154 18.2479 2.2737 2.8614 0.3608 0.0337 0.0349 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
