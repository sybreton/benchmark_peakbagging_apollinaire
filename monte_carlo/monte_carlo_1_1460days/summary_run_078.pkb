# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:43:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0308 0.0152 0.0150 13.7586 1.4452 1.6338 0.3523 0.0314 0.0353 90.0000 0.0000 0.0000 0.3940 0.0080 0.0079 0.0000 0.0000 0.0000
12 0 1821.9931 0.0169 0.0167 16.6916 2.0389 2.5390 0.4019 0.0369 0.0383 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
