# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:06:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9875 0.0149 0.0144 12.9630 1.4098 1.5392 0.3375 0.0312 0.0355 90.0000 0.0000 0.0000 0.4078 0.0074 0.0076 0.0000 0.0000 0.0000
12 0 1822.0168 0.0154 0.0153 19.5821 2.3990 2.9164 0.3711 0.0331 0.0345 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
