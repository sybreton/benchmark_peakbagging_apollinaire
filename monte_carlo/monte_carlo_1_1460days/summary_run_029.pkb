# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:51:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0049 0.0140 0.0139 15.7304 1.6031 1.8368 0.3424 0.0291 0.0324 90.0000 0.0000 0.0000 0.4055 0.0075 0.0076 0.0000 0.0000 0.0000
12 0 1822.0277 0.0151 0.0154 19.2754 2.3968 2.8647 0.3603 0.0316 0.0345 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
