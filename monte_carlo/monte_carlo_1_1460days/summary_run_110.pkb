# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:56:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0044 0.0138 0.0139 14.7844 1.5553 1.7690 0.3254 0.0295 0.0318 90.0000 0.0000 0.0000 0.3946 0.0074 0.0077 0.0000 0.0000 0.0000
12 0 1822.0040 0.0153 0.0156 20.5521 2.5064 2.9754 0.3805 0.0328 0.0350 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
