# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:15:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9907 0.0146 0.0144 14.6331 1.5184 1.7023 0.3266 0.0278 0.0308 90.0000 0.0000 0.0000 0.4139 0.0074 0.0073 0.0000 0.0000 0.0000
12 0 1821.9969 0.0151 0.0157 18.2602 2.2648 2.8130 0.3591 0.0334 0.0350 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
