# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:19:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9937 0.0140 0.0139 14.0613 1.5566 1.6859 0.3341 0.0307 0.0359 90.0000 0.0000 0.0000 0.4166 0.0079 0.0080 0.0000 0.0000 0.0000
12 0 1822.0058 0.0134 0.0135 24.2783 3.2329 4.0151 0.3013 0.0283 0.0303 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
