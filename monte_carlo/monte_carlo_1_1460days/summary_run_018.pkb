# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:26:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9991 0.0146 0.0140 14.3095 1.5323 1.7597 0.3311 0.0306 0.0333 90.0000 0.0000 0.0000 0.3969 0.0079 0.0078 0.0000 0.0000 0.0000
12 0 1822.0015 0.0172 0.0171 15.2011 1.8844 2.3231 0.4198 0.0404 0.0428 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
