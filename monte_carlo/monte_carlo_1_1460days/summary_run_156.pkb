# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:41:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0076 0.0145 0.0147 14.7356 1.4935 1.7309 0.3454 0.0303 0.0317 90.0000 0.0000 0.0000 0.4061 0.0080 0.0079 0.0000 0.0000 0.0000
12 0 1821.9933 0.0148 0.0148 20.7148 2.5286 3.1162 0.3536 0.0314 0.0332 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
