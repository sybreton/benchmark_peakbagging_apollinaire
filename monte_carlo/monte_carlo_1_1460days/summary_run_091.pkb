# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:12:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0035 0.0133 0.0131 15.9745 1.6467 1.9609 0.3178 0.0280 0.0306 90.0000 0.0000 0.0000 0.4129 0.0071 0.0071 0.0000 0.0000 0.0000
12 0 1822.0292 0.0155 0.0153 17.6006 2.2268 2.6250 0.3665 0.0337 0.0373 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
