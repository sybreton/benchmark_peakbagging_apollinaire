# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:01:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9900 0.0130 0.0135 14.7415 1.5769 1.7742 0.3096 0.0286 0.0319 90.0000 0.0000 0.0000 0.3983 0.0072 0.0071 0.0000 0.0000 0.0000
12 0 1821.9858 0.0167 0.0166 16.6490 2.0724 2.5126 0.3973 0.0363 0.0389 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
