# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:46:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0018 0.0134 0.0134 15.2921 1.5724 1.8725 0.3256 0.0297 0.0318 90.0000 0.0000 0.0000 0.3990 0.0071 0.0072 0.0000 0.0000 0.0000
12 0 1822.0121 0.0139 0.0142 22.8649 2.8662 3.5704 0.3235 0.0284 0.0306 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
