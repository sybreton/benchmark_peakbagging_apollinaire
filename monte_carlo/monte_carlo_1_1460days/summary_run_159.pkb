# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:47:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9566 0.0155 0.0153 13.8232 1.4487 1.6453 0.3609 0.0324 0.0354 90.0000 0.0000 0.0000 0.3920 0.0086 0.0085 0.0000 0.0000 0.0000
12 0 1822.0238 0.0157 0.0154 19.4463 2.4565 2.9637 0.3659 0.0329 0.0363 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
