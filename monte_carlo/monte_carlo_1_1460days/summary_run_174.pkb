# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:21:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0193 0.0152 0.0149 14.7680 1.5073 1.7743 0.3500 0.0303 0.0321 90.0000 0.0000 0.0000 0.4072 0.0083 0.0085 0.0000 0.0000 0.0000
12 0 1821.9951 0.0141 0.0138 23.6000 3.1020 3.8064 0.3235 0.0299 0.0324 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
