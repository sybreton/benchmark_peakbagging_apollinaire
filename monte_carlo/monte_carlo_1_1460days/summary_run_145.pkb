# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:16:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0056 0.0124 0.0130 16.5057 1.7848 2.0557 0.3031 0.0281 0.0296 90.0000 0.0000 0.0000 0.4001 0.0069 0.0069 0.0000 0.0000 0.0000
12 0 1821.9990 0.0149 0.0142 22.4797 2.7702 3.3914 0.3560 0.0315 0.0327 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
