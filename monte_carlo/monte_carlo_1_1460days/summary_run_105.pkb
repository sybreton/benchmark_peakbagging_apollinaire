# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:44:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0154 0.0140 0.0138 14.4080 1.5258 1.8315 0.3272 0.0306 0.0331 90.0000 0.0000 0.0000 0.4058 0.0083 0.0083 0.0000 0.0000 0.0000
12 0 1821.9994 0.0130 0.0130 23.0318 3.0954 3.9566 0.2907 0.0282 0.0298 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
