# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:53:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9817 0.0151 0.0150 15.3741 1.5139 1.7632 0.3675 0.0309 0.0323 90.0000 0.0000 0.0000 0.4101 0.0081 0.0079 0.0000 0.0000 0.0000
12 0 1821.9924 0.0148 0.0146 20.4256 2.5312 3.0659 0.3535 0.0328 0.0344 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
