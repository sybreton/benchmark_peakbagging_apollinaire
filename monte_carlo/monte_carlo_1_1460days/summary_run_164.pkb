# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:59:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0194 0.0175 0.0173 12.6151 1.3650 1.5196 0.4032 0.0371 0.0427 90.0000 0.0000 0.0000 0.4051 0.0094 0.0093 0.0000 0.0000 0.0000
12 0 1822.0232 0.0149 0.0152 18.6150 2.3210 2.8400 0.3641 0.0329 0.0345 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
