# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:35:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0047 0.0135 0.0134 15.1598 1.5964 1.8380 0.3202 0.0294 0.0313 90.0000 0.0000 0.0000 0.3967 0.0072 0.0069 0.0000 0.0000 0.0000
12 0 1821.9940 0.0142 0.0140 23.1328 2.9483 3.6566 0.3340 0.0315 0.0331 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
