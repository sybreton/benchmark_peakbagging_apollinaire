# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:07:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9913 0.0146 0.0148 15.0194 1.5566 1.8126 0.3439 0.0307 0.0333 90.0000 0.0000 0.0000 0.3933 0.0078 0.0077 0.0000 0.0000 0.0000
12 0 1822.0003 0.0153 0.0159 18.1887 2.2282 2.7162 0.3727 0.0331 0.0362 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
