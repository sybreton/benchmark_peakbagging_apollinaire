# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:14:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9821 0.0174 0.0179 11.7991 1.2677 1.4278 0.3740 0.0338 0.0392 90.0000 0.0000 0.0000 0.4033 0.0093 0.0091 0.0000 0.0000 0.0000
12 0 1821.9956 0.0154 0.0156 17.6835 2.1768 2.6209 0.3766 0.0351 0.0376 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
