# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:32:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0026 0.0135 0.0136 15.3636 1.6102 1.8737 0.3094 0.0278 0.0302 90.0000 0.0000 0.0000 0.3964 0.0073 0.0074 0.0000 0.0000 0.0000
12 0 1821.9950 0.0157 0.0159 18.6453 2.2979 2.7326 0.3806 0.0334 0.0361 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
