# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:29:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0262 0.0147 0.0147 14.7097 1.5403 1.7231 0.3404 0.0298 0.0340 90.0000 0.0000 0.0000 0.3812 0.0083 0.0082 0.0000 0.0000 0.0000
12 0 1822.0029 0.0142 0.0148 20.9569 2.6246 3.1929 0.3428 0.0312 0.0334 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
