# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:35:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9970 0.0134 0.0136 16.9917 1.7319 1.9573 0.3244 0.0268 0.0288 90.0000 0.0000 0.0000 0.4099 0.0072 0.0071 0.0000 0.0000 0.0000
12 0 1822.0017 0.0152 0.0148 19.7818 2.4508 2.9567 0.3589 0.0322 0.0340 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
