# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:13:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9924 0.0135 0.0137 16.1150 1.6742 1.9122 0.3370 0.0294 0.0325 90.0000 0.0000 0.0000 0.4027 0.0074 0.0073 0.0000 0.0000 0.0000
12 0 1822.0061 0.0145 0.0144 21.9582 2.7629 3.3431 0.3474 0.0310 0.0326 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
