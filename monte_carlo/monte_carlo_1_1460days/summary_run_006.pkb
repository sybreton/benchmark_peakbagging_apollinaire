# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:59:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0108 0.0153 0.0153 12.3943 1.3515 1.5460 0.3419 0.0325 0.0355 90.0000 0.0000 0.0000 0.4028 0.0086 0.0087 0.0000 0.0000 0.0000
12 0 1821.9915 0.0148 0.0149 21.1150 2.5742 3.2352 0.3557 0.0321 0.0343 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
