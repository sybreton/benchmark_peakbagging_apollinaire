# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:36:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0046 0.0176 0.0171 11.7249 1.2917 1.4621 0.3934 0.0379 0.0437 90.0000 0.0000 0.0000 0.3966 0.0087 0.0087 0.0000 0.0000 0.0000
12 0 1821.9870 0.0149 0.0149 21.1719 2.5802 3.1371 0.3677 0.0323 0.0346 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
