# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:48:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0118 0.0174 0.0169 11.9019 1.3007 1.4816 0.3939 0.0381 0.0419 90.0000 0.0000 0.0000 0.4033 0.0093 0.0092 0.0000 0.0000 0.0000
12 0 1821.9890 0.0130 0.0130 25.1746 3.4174 4.3104 0.2945 0.0288 0.0304 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
