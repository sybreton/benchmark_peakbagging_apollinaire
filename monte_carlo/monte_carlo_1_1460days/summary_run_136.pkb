# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:55:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0129 0.0153 0.0156 13.7103 1.4322 1.6282 0.3640 0.0333 0.0373 90.0000 0.0000 0.0000 0.4070 0.0080 0.0079 0.0000 0.0000 0.0000
12 0 1822.0226 0.0153 0.0153 20.0440 2.4616 2.9917 0.3625 0.0312 0.0335 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
