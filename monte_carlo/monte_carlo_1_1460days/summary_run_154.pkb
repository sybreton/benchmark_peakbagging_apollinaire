# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:36:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0099 0.0139 0.0137 16.6610 1.6929 1.9796 0.3315 0.0283 0.0307 90.0000 0.0000 0.0000 0.4013 0.0073 0.0069 0.0000 0.0000 0.0000
12 0 1822.0111 0.0165 0.0165 16.6420 1.9632 2.4527 0.3918 0.0340 0.0358 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
