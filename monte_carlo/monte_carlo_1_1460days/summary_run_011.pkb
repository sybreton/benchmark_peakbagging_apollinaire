# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:10:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9814 0.0141 0.0141 15.3276 1.5879 1.8984 0.3248 0.0289 0.0308 90.0000 0.0000 0.0000 0.4059 0.0075 0.0073 0.0000 0.0000 0.0000
12 0 1821.9929 0.0161 0.0161 16.9587 2.1737 2.6089 0.3739 0.0353 0.0375 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
