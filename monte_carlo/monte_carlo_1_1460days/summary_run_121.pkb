# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:21:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0109 0.0158 0.0157 13.4351 1.3898 1.5624 0.3666 0.0323 0.0363 90.0000 0.0000 0.0000 0.3931 0.0084 0.0086 0.0000 0.0000 0.0000
12 0 1822.0243 0.0152 0.0150 19.6422 2.4713 3.0423 0.3475 0.0316 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
