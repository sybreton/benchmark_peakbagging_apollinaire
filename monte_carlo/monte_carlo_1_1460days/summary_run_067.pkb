# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:18:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0028 0.0174 0.0177 12.5825 1.3540 1.5203 0.3710 0.0344 0.0385 90.0000 0.0000 0.0000 0.3812 0.0096 0.0093 0.0000 0.0000 0.0000
12 0 1821.9761 0.0154 0.0157 17.9960 2.1992 2.6796 0.3811 0.0349 0.0360 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
