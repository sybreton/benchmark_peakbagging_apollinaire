# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:08:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0051 0.0171 0.0172 11.9864 1.2669 1.4449 0.3725 0.0334 0.0380 90.0000 0.0000 0.0000 0.3954 0.0088 0.0087 0.0000 0.0000 0.0000
12 0 1821.9976 0.0159 0.0158 17.1514 2.1894 2.6130 0.3730 0.0353 0.0374 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
