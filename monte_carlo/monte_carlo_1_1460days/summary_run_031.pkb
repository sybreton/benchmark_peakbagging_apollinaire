# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:56:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9961 0.0123 0.0121 16.2158 1.7656 2.1080 0.2856 0.0267 0.0285 90.0000 0.0000 0.0000 0.4012 0.0067 0.0068 0.0000 0.0000 0.0000
12 0 1821.9760 0.0158 0.0158 18.4944 2.2929 2.7324 0.3839 0.0353 0.0371 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
