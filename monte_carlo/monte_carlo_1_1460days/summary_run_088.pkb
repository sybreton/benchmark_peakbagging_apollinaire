# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:06:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0002 0.0190 0.0186 11.9233 1.2727 1.4106 0.4282 0.0404 0.0457 90.0000 0.0000 0.0000 0.4016 0.0097 0.0093 0.0000 0.0000 0.0000
12 0 1822.0034 0.0152 0.0154 17.8775 2.2545 2.7858 0.3466 0.0322 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
