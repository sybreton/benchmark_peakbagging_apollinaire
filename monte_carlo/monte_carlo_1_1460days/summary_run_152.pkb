# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:31:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0005 0.0163 0.0162 13.1586 1.4406 1.6337 0.3715 0.0349 0.0396 90.0000 0.0000 0.0000 0.3967 0.0090 0.0088 0.0000 0.0000 0.0000
12 0 1822.0125 0.0142 0.0145 21.1603 2.7345 3.2889 0.3336 0.0311 0.0333 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
