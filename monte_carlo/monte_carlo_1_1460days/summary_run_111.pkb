# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:58:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0155 0.0140 0.0143 14.2218 1.5542 1.7349 0.3281 0.0305 0.0338 90.0000 0.0000 0.0000 0.3981 0.0079 0.0079 0.0000 0.0000 0.0000
12 0 1821.9984 0.0151 0.0150 19.3344 2.3704 2.9541 0.3563 0.0329 0.0347 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
