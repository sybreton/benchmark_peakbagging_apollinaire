# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:19:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0103 0.0167 0.0167 12.7341 1.2904 1.4707 0.3878 0.0338 0.0380 90.0000 0.0000 0.0000 0.4018 0.0085 0.0088 0.0000 0.0000 0.0000
12 0 1821.9787 0.0144 0.0143 22.0217 2.7609 3.6374 0.3305 0.0314 0.0316 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
