# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:20:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0046 0.0156 0.0155 13.4943 1.4253 1.6042 0.3487 0.0312 0.0342 90.0000 0.0000 0.0000 0.4032 0.0084 0.0082 0.0000 0.0000 0.0000
12 0 1821.9828 0.0160 0.0158 18.0075 2.2295 2.7120 0.3853 0.0355 0.0372 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
