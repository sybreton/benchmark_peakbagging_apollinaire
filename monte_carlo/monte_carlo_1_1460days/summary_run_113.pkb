# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:03:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0040 0.0115 0.0117 18.5486 2.0039 2.2992 0.2840 0.0247 0.0271 90.0000 0.0000 0.0000 0.4050 0.0064 0.0065 0.0000 0.0000 0.0000
12 0 1821.9916 0.0149 0.0150 18.6823 2.3538 2.7784 0.3599 0.0337 0.0358 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
