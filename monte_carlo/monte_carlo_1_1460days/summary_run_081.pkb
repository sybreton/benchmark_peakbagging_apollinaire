# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:50:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9830 0.0135 0.0139 15.8195 1.7041 1.9305 0.3027 0.0270 0.0296 90.0000 0.0000 0.0000 0.4082 0.0074 0.0073 0.0000 0.0000 0.0000
12 0 1822.0032 0.0150 0.0151 18.9912 2.4505 2.9708 0.3407 0.0321 0.0338 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
