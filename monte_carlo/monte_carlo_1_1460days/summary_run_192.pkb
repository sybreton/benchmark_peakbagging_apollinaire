# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:02:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9985 0.0152 0.0154 14.5038 1.4468 1.6422 0.3637 0.0313 0.0345 90.0000 0.0000 0.0000 0.3923 0.0079 0.0076 0.0000 0.0000 0.0000
12 0 1822.0223 0.0145 0.0145 20.6976 2.5705 3.2269 0.3416 0.0316 0.0318 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
