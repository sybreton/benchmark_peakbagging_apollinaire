# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:39:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0087 0.0144 0.0145 14.4291 1.4923 1.7320 0.3412 0.0300 0.0320 90.0000 0.0000 0.0000 0.4042 0.0082 0.0081 0.0000 0.0000 0.0000
12 0 1822.0029 0.0156 0.0154 18.2085 2.2797 2.7527 0.3632 0.0338 0.0361 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
