# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:31:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9809 0.0166 0.0163 12.0213 1.3362 1.5257 0.3771 0.0374 0.0411 90.0000 0.0000 0.0000 0.3915 0.0088 0.0088 0.0000 0.0000 0.0000
12 0 1821.9966 0.0154 0.0155 17.9462 2.2590 2.7228 0.3618 0.0340 0.0359 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
