# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:35:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9923 0.0133 0.0129 15.3238 1.5852 1.8793 0.3127 0.0287 0.0301 90.0000 0.0000 0.0000 0.4031 0.0075 0.0075 0.0000 0.0000 0.0000
12 0 1822.0230 0.0164 0.0161 16.4276 2.0288 2.4293 0.3847 0.0346 0.0377 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
