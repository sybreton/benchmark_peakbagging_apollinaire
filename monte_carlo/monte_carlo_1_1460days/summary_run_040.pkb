# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:16:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9904 0.0156 0.0161 13.6364 1.4051 1.5723 0.3781 0.0334 0.0376 90.0000 0.0000 0.0000 0.3999 0.0086 0.0088 0.0000 0.0000 0.0000
12 0 1821.9994 0.0150 0.0150 20.3083 2.5732 2.9854 0.3523 0.0313 0.0342 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
