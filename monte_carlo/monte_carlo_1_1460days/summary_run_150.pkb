# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:27:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0054 0.0147 0.0150 14.6915 1.5445 1.7055 0.3444 0.0299 0.0331 90.0000 0.0000 0.0000 0.4018 0.0080 0.0080 0.0000 0.0000 0.0000
12 0 1822.0014 0.0158 0.0157 18.5181 2.3071 2.7146 0.3704 0.0331 0.0352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
