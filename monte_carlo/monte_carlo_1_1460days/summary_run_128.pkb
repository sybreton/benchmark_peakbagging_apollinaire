# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:37:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9977 0.0131 0.0135 16.5495 1.7033 1.9981 0.3107 0.0267 0.0286 90.0000 0.0000 0.0000 0.3942 0.0074 0.0072 0.0000 0.0000 0.0000
12 0 1822.0062 0.0172 0.0171 14.6576 1.8776 2.2867 0.4106 0.0393 0.0430 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
