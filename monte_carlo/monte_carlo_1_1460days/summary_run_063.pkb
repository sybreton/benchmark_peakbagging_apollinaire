# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:08:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0104 0.0188 0.0185 11.4319 1.2846 1.4235 0.4050 0.0398 0.0462 90.0000 0.0000 0.0000 0.3839 0.0102 0.0102 0.0000 0.0000 0.0000
12 0 1821.9983 0.0138 0.0137 23.1033 3.0191 3.8159 0.3050 0.0291 0.0312 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
