# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:51:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0014 0.0166 0.0158 12.4839 1.3744 1.5112 0.3801 0.0358 0.0407 90.0000 0.0000 0.0000 0.4005 0.0089 0.0089 0.0000 0.0000 0.0000
12 0 1822.0201 0.0148 0.0150 19.3264 2.5102 3.0553 0.3439 0.0323 0.0343 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
