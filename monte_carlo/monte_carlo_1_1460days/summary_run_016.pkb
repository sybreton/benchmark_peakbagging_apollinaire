# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:21:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9743 0.0168 0.0164 12.6706 1.3254 1.5016 0.3974 0.0368 0.0417 90.0000 0.0000 0.0000 0.4089 0.0091 0.0088 0.0000 0.0000 0.0000
12 0 1821.9995 0.0151 0.0157 20.4188 2.4955 2.9616 0.3709 0.0323 0.0340 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
