# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:31:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0113 0.0155 0.0160 14.2272 1.4752 1.6248 0.3713 0.0324 0.0365 90.0000 0.0000 0.0000 0.3918 0.0086 0.0086 0.0000 0.0000 0.0000
12 0 1821.9976 0.0159 0.0158 17.0679 2.1458 2.6055 0.3809 0.0357 0.0380 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
