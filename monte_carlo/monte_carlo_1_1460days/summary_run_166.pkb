# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:03:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0096 0.0137 0.0135 15.5455 1.6106 1.8804 0.3138 0.0279 0.0304 90.0000 0.0000 0.0000 0.3925 0.0074 0.0073 0.0000 0.0000 0.0000
12 0 1821.9998 0.0161 0.0160 18.0282 2.1950 2.7885 0.3839 0.0355 0.0368 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
