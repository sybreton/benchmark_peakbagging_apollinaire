# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:15:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9859 0.0156 0.0156 14.0144 1.4677 1.6183 0.3596 0.0316 0.0364 90.0000 0.0000 0.0000 0.4112 0.0088 0.0086 0.0000 0.0000 0.0000
12 0 1821.9792 0.0139 0.0138 22.0282 2.8422 3.5468 0.3149 0.0300 0.0315 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
