# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:55:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0034 0.0170 0.0165 11.8061 1.3094 1.4627 0.3928 0.0389 0.0446 90.0000 0.0000 0.0000 0.4274 0.0090 0.0091 0.0000 0.0000 0.0000
12 0 1822.0174 0.0156 0.0151 17.8837 2.2644 2.7158 0.3613 0.0334 0.0371 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
