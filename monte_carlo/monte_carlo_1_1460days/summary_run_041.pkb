# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:18:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0052 0.0172 0.0172 12.6149 1.2876 1.4514 0.3932 0.0335 0.0375 90.0000 0.0000 0.0000 0.4028 0.0089 0.0086 0.0000 0.0000 0.0000
12 0 1822.0087 0.0165 0.0167 17.1739 2.0523 2.5367 0.4160 0.0368 0.0391 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
