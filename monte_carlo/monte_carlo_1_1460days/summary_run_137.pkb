# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:57:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9806 0.0132 0.0132 17.5723 1.8251 2.0873 0.3214 0.0276 0.0303 90.0000 0.0000 0.0000 0.4025 0.0073 0.0071 0.0000 0.0000 0.0000
12 0 1821.9649 0.0143 0.0143 20.9773 2.7009 3.4125 0.3146 0.0306 0.0316 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
