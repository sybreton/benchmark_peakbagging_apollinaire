# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:11:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9839 0.0139 0.0135 15.6564 1.6153 1.9142 0.3310 0.0298 0.0324 90.0000 0.0000 0.0000 0.4023 0.0081 0.0080 0.0000 0.0000 0.0000
12 0 1822.0110 0.0161 0.0159 18.1412 2.2434 2.6977 0.3891 0.0349 0.0378 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
