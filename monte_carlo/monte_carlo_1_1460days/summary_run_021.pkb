# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:33:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9723 0.0139 0.0139 14.4001 1.5338 1.7569 0.3275 0.0301 0.0335 90.0000 0.0000 0.0000 0.3913 0.0074 0.0073 0.0000 0.0000 0.0000
12 0 1821.9917 0.0142 0.0142 20.8456 2.6728 3.2901 0.3287 0.0304 0.0319 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
