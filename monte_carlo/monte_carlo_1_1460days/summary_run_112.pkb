# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:00:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9962 0.0176 0.0170 13.0323 1.3943 1.5145 0.3672 0.0323 0.0361 90.0000 0.0000 0.0000 0.3753 0.0094 0.0092 0.0000 0.0000 0.0000
12 0 1821.9962 0.0143 0.0146 20.5850 2.6152 3.2208 0.3397 0.0312 0.0331 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
