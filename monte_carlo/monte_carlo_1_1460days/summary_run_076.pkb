# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:38:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9836 0.0146 0.0152 14.4914 1.4793 1.7006 0.3614 0.0323 0.0351 90.0000 0.0000 0.0000 0.3926 0.0079 0.0080 0.0000 0.0000 0.0000
12 0 1821.9816 0.0139 0.0136 23.7184 3.0362 3.8127 0.3253 0.0306 0.0316 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
