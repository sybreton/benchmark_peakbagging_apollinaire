# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:28:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9795 0.0122 0.0126 17.6031 1.8645 2.1703 0.3009 0.0270 0.0290 90.0000 0.0000 0.0000 0.4187 0.0068 0.0067 0.0000 0.0000 0.0000
12 0 1822.0012 0.0138 0.0138 21.6359 2.9138 3.5809 0.3140 0.0316 0.0326 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
