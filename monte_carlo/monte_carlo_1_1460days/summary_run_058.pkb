# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:57:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0566 0.0163 0.0165 13.5657 1.4035 1.5921 0.3748 0.0330 0.0384 90.0000 0.0000 0.0000 0.4034 0.0088 0.0088 0.0000 0.0000 0.0000
12 0 1821.9760 0.0131 0.0132 26.2432 3.5983 4.5430 0.2982 0.0297 0.0304 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
