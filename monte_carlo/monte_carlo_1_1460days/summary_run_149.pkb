# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:25:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0078 0.0125 0.0122 17.8019 1.9165 2.2148 0.2935 0.0256 0.0282 90.0000 0.0000 0.0000 0.3909 0.0067 0.0066 0.0000 0.0000 0.0000
12 0 1822.0103 0.0155 0.0151 18.5257 2.3491 2.8207 0.3575 0.0336 0.0353 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
