# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:09:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0108 0.0134 0.0132 16.6900 1.7403 2.0288 0.3153 0.0285 0.0300 90.0000 0.0000 0.0000 0.3882 0.0070 0.0072 0.0000 0.0000 0.0000
12 0 1822.0346 0.0170 0.0173 15.3640 1.8788 2.3492 0.4137 0.0389 0.0411 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
