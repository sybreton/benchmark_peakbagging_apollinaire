# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:04:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0146 0.0144 0.0143 14.4701 1.5244 1.7204 0.3469 0.0315 0.0337 90.0000 0.0000 0.0000 0.4008 0.0078 0.0078 0.0000 0.0000 0.0000
12 0 1822.0137 0.0155 0.0156 17.4737 2.1526 2.6997 0.3670 0.0346 0.0372 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
