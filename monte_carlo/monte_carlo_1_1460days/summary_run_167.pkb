# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:05:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0013 0.0143 0.0142 15.3424 1.5560 1.7960 0.3430 0.0293 0.0325 90.0000 0.0000 0.0000 0.4021 0.0074 0.0077 0.0000 0.0000 0.0000
12 0 1822.0112 0.0152 0.0150 19.4973 2.3630 2.9392 0.3586 0.0326 0.0337 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
