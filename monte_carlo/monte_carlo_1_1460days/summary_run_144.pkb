# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:13:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9998 0.0165 0.0167 13.8715 1.3954 1.5974 0.3659 0.0310 0.0344 90.0000 0.0000 0.0000 0.4043 0.0078 0.0079 0.0000 0.0000 0.0000
12 0 1821.9948 0.0160 0.0164 17.0322 2.1461 2.5541 0.3833 0.0353 0.0376 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
