# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:24:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0186 0.0156 0.0161 12.3139 1.3529 1.5217 0.3829 0.0382 0.0436 90.0000 0.0000 0.0000 0.3949 0.0083 0.0085 0.0000 0.0000 0.0000
12 0 1822.0206 0.0148 0.0144 22.7322 2.7265 3.4605 0.3564 0.0325 0.0339 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
