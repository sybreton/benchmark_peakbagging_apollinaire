# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:34:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9855 0.0120 0.0119 16.9265 1.8596 2.2047 0.2897 0.0267 0.0299 90.0000 0.0000 0.0000 0.4010 0.0068 0.0071 0.0000 0.0000 0.0000
12 0 1821.9967 0.0156 0.0154 16.9908 2.1751 2.6439 0.3675 0.0347 0.0370 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
