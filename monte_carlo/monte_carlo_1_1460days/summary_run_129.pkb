# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:39:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9963 0.0158 0.0165 12.2242 1.2972 1.4710 0.3667 0.0330 0.0370 90.0000 0.0000 0.0000 0.4040 0.0088 0.0086 0.0000 0.0000 0.0000
12 0 1822.0001 0.0161 0.0153 19.3676 2.3818 2.8342 0.3719 0.0324 0.0357 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
