# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:50:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9881 0.0154 0.0161 12.4111 1.3478 1.5454 0.3644 0.0342 0.0390 90.0000 0.0000 0.0000 0.3901 0.0088 0.0083 0.0000 0.0000 0.0000
12 0 1822.0154 0.0123 0.0126 26.8462 3.7866 4.7346 0.2822 0.0280 0.0302 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
