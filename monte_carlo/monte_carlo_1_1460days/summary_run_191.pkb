# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:00:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9906 0.0168 0.0169 13.2663 1.3897 1.5756 0.3841 0.0343 0.0371 90.0000 0.0000 0.0000 0.3925 0.0085 0.0082 0.0000 0.0000 0.0000
12 0 1822.0044 0.0141 0.0138 22.0836 2.8513 3.4826 0.3211 0.0302 0.0317 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
