# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:47:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0012 0.0157 0.0157 13.6184 1.4023 1.5673 0.3554 0.0312 0.0339 90.0000 0.0000 0.0000 0.4029 0.0085 0.0082 0.0000 0.0000 0.0000
12 0 1821.9983 0.0149 0.0144 19.7564 2.4682 3.0692 0.3433 0.0320 0.0336 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
