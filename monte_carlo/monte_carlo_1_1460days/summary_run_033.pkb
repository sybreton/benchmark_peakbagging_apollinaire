# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:00:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0109 0.0149 0.0152 13.3095 1.3797 1.5726 0.3407 0.0300 0.0340 90.0000 0.0000 0.0000 0.3942 0.0077 0.0078 0.0000 0.0000 0.0000
12 0 1822.0299 0.0142 0.0146 20.9034 2.6752 3.3556 0.3313 0.0310 0.0332 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
