# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:48:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0117 0.0144 0.0145 15.7741 1.6191 1.8784 0.3380 0.0294 0.0329 90.0000 0.0000 0.0000 0.4024 0.0078 0.0079 0.0000 0.0000 0.0000
12 0 1822.0220 0.0149 0.0147 20.0043 2.5266 3.0963 0.3435 0.0321 0.0330 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
