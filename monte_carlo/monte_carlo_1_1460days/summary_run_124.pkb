# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:28:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0123 0.0166 0.0167 12.5229 1.4125 1.5392 0.3839 0.0370 0.0421 90.0000 0.0000 0.0000 0.4048 0.0098 0.0096 0.0000 0.0000 0.0000
12 0 1821.9784 0.0138 0.0135 22.0632 2.8933 3.6624 0.3166 0.0307 0.0328 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
