# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:35:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9704 0.0161 0.0159 12.8974 1.3803 1.5600 0.3618 0.0328 0.0368 90.0000 0.0000 0.0000 0.4021 0.0088 0.0087 0.0000 0.0000 0.0000
12 0 1822.0225 0.0154 0.0151 19.6390 2.4803 2.9682 0.3457 0.0315 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
