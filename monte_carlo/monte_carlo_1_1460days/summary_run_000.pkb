# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:45:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0103 0.0156 0.0155 12.9202 1.3442 1.5146 0.3446 0.0305 0.0340 90.0000 0.0000 0.0000 0.4006 0.0081 0.0081 0.0000 0.0000 0.0000
12 0 1821.9931 0.0149 0.0150 18.9756 2.4163 2.8624 0.3536 0.0334 0.0352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
