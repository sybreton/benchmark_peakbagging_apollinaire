# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:56:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9980 0.0131 0.0128 15.8992 1.6641 1.9105 0.3141 0.0278 0.0298 90.0000 0.0000 0.0000 0.4047 0.0069 0.0068 0.0000 0.0000 0.0000
12 0 1822.0328 0.0147 0.0140 19.5207 2.5414 3.2486 0.3286 0.0320 0.0335 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
