# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:22:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9749 0.0149 0.0152 15.1921 1.4864 1.7580 0.3504 0.0301 0.0320 90.0000 0.0000 0.0000 0.4055 0.0077 0.0076 0.0000 0.0000 0.0000
12 0 1821.9976 0.0171 0.0171 16.7096 1.9702 2.4025 0.4096 0.0356 0.0370 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
