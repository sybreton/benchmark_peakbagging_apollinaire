# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:53:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9772 0.0135 0.0136 15.5335 1.6624 1.9053 0.3343 0.0308 0.0335 90.0000 0.0000 0.0000 0.4035 0.0080 0.0080 0.0000 0.0000 0.0000
12 0 1822.0236 0.0135 0.0138 23.9243 3.0552 3.8005 0.3202 0.0296 0.0303 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
