# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:24:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0061 0.0144 0.0145 15.2214 1.5812 1.8643 0.3392 0.0299 0.0327 90.0000 0.0000 0.0000 0.4004 0.0073 0.0074 0.0000 0.0000 0.0000
12 0 1822.0294 0.0146 0.0145 20.9610 2.6223 3.2328 0.3455 0.0315 0.0327 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
