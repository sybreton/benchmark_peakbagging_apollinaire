# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:41:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9917 0.0154 0.0155 13.8257 1.4754 1.6529 0.3560 0.0328 0.0360 90.0000 0.0000 0.0000 0.3925 0.0079 0.0080 0.0000 0.0000 0.0000
12 0 1822.0104 0.0170 0.0168 15.7115 1.9931 2.4447 0.4084 0.0391 0.0421 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
