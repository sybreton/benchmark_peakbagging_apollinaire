# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:48:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0078 0.0124 0.0124 16.1472 1.7668 2.0590 0.2986 0.0283 0.0305 90.0000 0.0000 0.0000 0.4021 0.0072 0.0073 0.0000 0.0000 0.0000
12 0 1821.9936 0.0170 0.0170 15.1095 1.8742 2.2394 0.4178 0.0396 0.0427 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
