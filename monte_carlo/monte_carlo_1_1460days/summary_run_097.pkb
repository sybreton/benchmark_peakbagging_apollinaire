# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:26:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0319 0.0137 0.0141 15.5432 1.5903 1.8229 0.3297 0.0293 0.0315 90.0000 0.0000 0.0000 0.3897 0.0077 0.0074 0.0000 0.0000 0.0000
12 0 1822.0050 0.0146 0.0151 19.3851 2.4129 2.8975 0.3555 0.0322 0.0340 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
