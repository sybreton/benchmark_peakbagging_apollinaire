# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:30:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0138 0.0161 0.0161 13.6038 1.4370 1.6281 0.3807 0.0349 0.0401 90.0000 0.0000 0.0000 0.4034 0.0086 0.0086 0.0000 0.0000 0.0000
12 0 1821.9859 0.0154 0.0152 18.8264 2.3523 2.8795 0.3621 0.0327 0.0352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
