# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:46:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9894 0.0130 0.0126 16.3334 1.7762 2.0329 0.3073 0.0273 0.0303 90.0000 0.0000 0.0000 0.4059 0.0074 0.0076 0.0000 0.0000 0.0000
12 0 1822.0028 0.0173 0.0170 14.6716 1.8515 2.2968 0.4014 0.0382 0.0405 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
