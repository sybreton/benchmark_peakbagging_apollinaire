# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:41:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9855 0.0120 0.0122 16.9814 1.8912 2.2910 0.2913 0.0282 0.0307 90.0000 0.0000 0.0000 0.4036 0.0072 0.0074 0.0000 0.0000 0.0000
12 0 1821.9909 0.0133 0.0137 22.6019 2.9188 3.7324 0.3154 0.0302 0.0309 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
