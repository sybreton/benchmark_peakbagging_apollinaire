# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:01:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9749 0.0183 0.0185 11.5299 1.2446 1.4135 0.4289 0.0404 0.0470 90.0000 0.0000 0.0000 0.4048 0.0096 0.0094 0.0000 0.0000 0.0000
12 0 1821.9969 0.0157 0.0155 18.7581 2.2705 2.8767 0.3642 0.0332 0.0342 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
