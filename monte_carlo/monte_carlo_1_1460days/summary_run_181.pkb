# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:37:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0087 0.0151 0.0150 13.3788 1.4074 1.5951 0.3510 0.0313 0.0342 90.0000 0.0000 0.0000 0.3960 0.0081 0.0079 0.0000 0.0000 0.0000
12 0 1821.9881 0.0150 0.0149 19.9322 2.5095 3.0263 0.3503 0.0315 0.0338 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
