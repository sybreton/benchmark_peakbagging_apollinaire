# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:38:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0005 0.0148 0.0146 15.0072 1.5712 1.7903 0.3378 0.0300 0.0333 90.0000 0.0000 0.0000 0.3855 0.0077 0.0076 0.0000 0.0000 0.0000
12 0 1822.0082 0.0150 0.0146 20.6801 2.6432 3.2711 0.3402 0.0314 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
