# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:46:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0147 0.0160 0.0158 13.7399 1.4507 1.6489 0.3704 0.0335 0.0374 90.0000 0.0000 0.0000 0.3895 0.0084 0.0082 0.0000 0.0000 0.0000
12 0 1821.9883 0.0172 0.0170 15.9527 1.9996 2.4436 0.4216 0.0394 0.0423 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
