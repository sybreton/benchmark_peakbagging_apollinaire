# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:30:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0142 0.0144 0.0143 13.0647 1.4062 1.6114 0.3385 0.0317 0.0352 90.0000 0.0000 0.0000 0.4073 0.0078 0.0076 0.0000 0.0000 0.0000
12 0 1822.0027 0.0159 0.0157 18.7948 2.3363 2.7571 0.3739 0.0335 0.0362 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
