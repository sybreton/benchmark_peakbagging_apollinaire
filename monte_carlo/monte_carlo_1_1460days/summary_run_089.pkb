# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:08:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0133 0.0171 0.0175 11.7206 1.2932 1.4527 0.3833 0.0366 0.0419 90.0000 0.0000 0.0000 0.3887 0.0092 0.0091 0.0000 0.0000 0.0000
12 0 1822.0013 0.0131 0.0134 28.5240 3.8775 4.8070 0.3143 0.0299 0.0315 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
