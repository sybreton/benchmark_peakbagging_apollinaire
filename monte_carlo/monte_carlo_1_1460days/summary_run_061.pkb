# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:04:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9902 0.0179 0.0176 12.0144 1.2996 1.4633 0.4063 0.0388 0.0427 90.0000 0.0000 0.0000 0.3940 0.0092 0.0090 0.0000 0.0000 0.0000
12 0 1822.0098 0.0145 0.0144 21.1813 2.6848 3.2839 0.3378 0.0306 0.0333 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
