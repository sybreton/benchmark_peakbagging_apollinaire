# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:11:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9816 0.0164 0.0169 13.9763 1.4154 1.6096 0.3784 0.0328 0.0363 90.0000 0.0000 0.0000 0.4046 0.0084 0.0083 0.0000 0.0000 0.0000
12 0 1822.0097 0.0152 0.0153 19.0694 2.4541 2.9117 0.3451 0.0317 0.0344 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
