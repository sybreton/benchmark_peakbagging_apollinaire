# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:13:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0143 0.0129 0.0131 15.6754 1.6880 1.9332 0.3051 0.0279 0.0308 90.0000 0.0000 0.0000 0.4063 0.0070 0.0069 0.0000 0.0000 0.0000
12 0 1821.9831 0.0156 0.0156 19.1169 2.3824 2.8574 0.3734 0.0346 0.0365 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
