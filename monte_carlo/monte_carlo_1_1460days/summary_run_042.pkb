# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:21:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9985 0.0135 0.0129 15.7410 1.6485 1.8960 0.3144 0.0278 0.0304 90.0000 0.0000 0.0000 0.4067 0.0069 0.0070 0.0000 0.0000 0.0000
12 0 1822.0062 0.0170 0.0172 16.0040 1.9381 2.3917 0.4165 0.0389 0.0415 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
