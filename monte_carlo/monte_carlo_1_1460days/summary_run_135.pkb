# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:53:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0153 0.0196 0.0196 11.0628 1.2681 1.4184 0.4295 0.0435 0.0505 90.0000 0.0000 0.0000 0.3942 0.0107 0.0106 0.0000 0.0000 0.0000
12 0 1821.9955 0.0142 0.0144 21.1366 2.7186 3.2829 0.3348 0.0307 0.0328 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
