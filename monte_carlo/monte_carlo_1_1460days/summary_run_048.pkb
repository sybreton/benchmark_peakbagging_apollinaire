# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:34:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0010 0.0170 0.0170 12.7301 1.3446 1.4924 0.4014 0.0366 0.0411 90.0000 0.0000 0.0000 0.4102 0.0089 0.0089 0.0000 0.0000 0.0000
12 0 1821.9978 0.0146 0.0142 21.9778 2.7873 3.4200 0.3439 0.0326 0.0344 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
