# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:00:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9968 0.0240 0.0232 9.6107 1.0789 1.3326 0.5013 0.0555 0.0592 90.0000 0.0000 0.0000 0.4165 0.0126 0.0122 0.0000 0.0000 0.0000
12 0 1821.9963 0.0148 0.0153 17.8395 2.2543 2.8168 0.3523 0.0331 0.0352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
