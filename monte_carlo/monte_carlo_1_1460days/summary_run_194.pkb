# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 06:07:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0109 0.0148 0.0146 13.8081 1.4556 1.6865 0.3509 0.0322 0.0359 90.0000 0.0000 0.0000 0.4111 0.0079 0.0078 0.0000 0.0000 0.0000
12 0 1822.0199 0.0151 0.0148 19.1078 2.3838 2.8492 0.3645 0.0326 0.0348 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
