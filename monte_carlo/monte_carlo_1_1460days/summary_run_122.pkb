# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:23:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9781 0.0169 0.0171 12.6346 1.3356 1.4891 0.3897 0.0357 0.0413 90.0000 0.0000 0.0000 0.4007 0.0090 0.0086 0.0000 0.0000 0.0000
12 0 1822.0042 0.0150 0.0156 19.1463 2.3709 2.8147 0.3663 0.0333 0.0352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
