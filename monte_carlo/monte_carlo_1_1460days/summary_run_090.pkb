# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:10:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9893 0.0162 0.0158 13.6896 1.4171 1.6406 0.3770 0.0338 0.0386 90.0000 0.0000 0.0000 0.3995 0.0089 0.0089 0.0000 0.0000 0.0000
12 0 1822.0009 0.0155 0.0156 17.7075 2.3011 2.8300 0.3515 0.0346 0.0365 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
