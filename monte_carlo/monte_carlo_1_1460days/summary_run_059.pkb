# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:59:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9898 0.0137 0.0131 14.3452 1.5544 1.8076 0.3110 0.0289 0.0321 90.0000 0.0000 0.0000 0.3950 0.0072 0.0073 0.0000 0.0000 0.0000
12 0 1821.9891 0.0170 0.0169 16.1012 2.0240 2.4469 0.4198 0.0389 0.0423 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
