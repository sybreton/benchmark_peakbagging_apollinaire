# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:15:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9929 0.0142 0.0140 14.0253 1.5205 1.7104 0.3339 0.0310 0.0332 90.0000 0.0000 0.0000 0.4024 0.0080 0.0078 0.0000 0.0000 0.0000
12 0 1821.9969 0.0141 0.0142 21.9385 2.7622 3.4209 0.3305 0.0295 0.0314 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
