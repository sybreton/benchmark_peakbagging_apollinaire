# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:38:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0196 0.0166 0.0166 12.6709 1.3275 1.4463 0.3762 0.0334 0.0377 90.0000 0.0000 0.0000 0.4017 0.0084 0.0082 0.0000 0.0000 0.0000
12 0 1822.0001 0.0134 0.0132 24.8169 3.2892 4.1373 0.3138 0.0297 0.0309 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
