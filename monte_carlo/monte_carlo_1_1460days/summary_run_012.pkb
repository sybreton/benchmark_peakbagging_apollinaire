# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:12:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9973 0.0158 0.0161 13.3705 1.4030 1.6119 0.3729 0.0335 0.0377 90.0000 0.0000 0.0000 0.4013 0.0086 0.0086 0.0000 0.0000 0.0000
12 0 1822.0184 0.0181 0.0177 13.2144 1.6376 2.0438 0.4312 0.0428 0.0451 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
