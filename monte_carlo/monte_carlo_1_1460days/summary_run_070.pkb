# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:24:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0041 0.0161 0.0161 13.5738 1.4314 1.6337 0.3459 0.0314 0.0346 90.0000 0.0000 0.0000 0.4050 0.0080 0.0086 0.0000 0.0000 0.0000
12 0 1822.0055 0.0145 0.0145 19.3281 2.4852 3.1456 0.3388 0.0322 0.0337 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
