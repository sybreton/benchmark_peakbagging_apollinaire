# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:26:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0223 0.0141 0.0141 15.2069 1.6249 1.8752 0.3223 0.0296 0.0317 90.0000 0.0000 0.0000 0.3895 0.0077 0.0076 0.0000 0.0000 0.0000
12 0 1821.9911 0.0155 0.0151 18.3030 2.3448 2.8022 0.3595 0.0330 0.0360 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
