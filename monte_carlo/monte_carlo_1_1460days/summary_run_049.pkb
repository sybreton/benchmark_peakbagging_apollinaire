# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:37:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9932 0.0146 0.0140 14.5594 1.5189 1.7375 0.3325 0.0291 0.0325 90.0000 0.0000 0.0000 0.4091 0.0078 0.0078 0.0000 0.0000 0.0000
12 0 1821.9831 0.0154 0.0146 19.2682 2.4516 3.0401 0.3385 0.0323 0.0340 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
