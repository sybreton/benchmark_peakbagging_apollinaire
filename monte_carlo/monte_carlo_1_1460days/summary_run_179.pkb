# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:33:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9868 0.0159 0.0164 13.1611 1.4339 1.6086 0.3819 0.0358 0.0402 90.0000 0.0000 0.0000 0.3964 0.0090 0.0089 0.0000 0.0000 0.0000
12 0 1821.9931 0.0132 0.0139 23.1293 3.0832 3.7710 0.3013 0.0289 0.0305 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
