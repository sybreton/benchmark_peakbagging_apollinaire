# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:42:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9868 0.0144 0.0141 14.7121 1.5245 1.7139 0.3363 0.0295 0.0317 90.0000 0.0000 0.0000 0.4013 0.0079 0.0077 0.0000 0.0000 0.0000
12 0 1822.0245 0.0138 0.0136 21.8129 2.9156 3.6635 0.3068 0.0306 0.0318 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
