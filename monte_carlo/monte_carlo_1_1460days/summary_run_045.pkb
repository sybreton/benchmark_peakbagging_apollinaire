# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:27:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9839 0.0141 0.0145 14.9494 1.5767 1.8001 0.3278 0.0295 0.0322 90.0000 0.0000 0.0000 0.4049 0.0079 0.0079 0.0000 0.0000 0.0000
12 0 1821.9627 0.0169 0.0164 16.5047 2.0433 2.4389 0.3926 0.0350 0.0383 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
