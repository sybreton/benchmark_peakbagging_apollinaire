# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:17:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0115 0.0157 0.0156 13.2720 1.4163 1.6331 0.3582 0.0334 0.0378 90.0000 0.0000 0.0000 0.3865 0.0084 0.0082 0.0000 0.0000 0.0000
12 0 1822.0252 0.0157 0.0162 18.6514 2.3324 2.7412 0.3793 0.0338 0.0364 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
