# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:45:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9957 0.0140 0.0137 15.1408 1.5978 1.7885 0.3207 0.0283 0.0311 90.0000 0.0000 0.0000 0.4086 0.0076 0.0079 0.0000 0.0000 0.0000
12 0 1821.9812 0.0145 0.0142 22.4216 2.8226 3.5633 0.3275 0.0308 0.0319 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
