# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:02:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9814 0.0164 0.0162 12.9416 1.3657 1.5562 0.3571 0.0317 0.0355 90.0000 0.0000 0.0000 0.4080 0.0085 0.0084 0.0000 0.0000 0.0000
12 0 1821.9832 0.0156 0.0153 19.0102 2.3360 2.7884 0.3867 0.0359 0.0384 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
