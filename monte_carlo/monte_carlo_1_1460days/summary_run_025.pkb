# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:42:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0029 0.0157 0.0155 14.2584 1.4562 1.6450 0.3540 0.0305 0.0336 90.0000 0.0000 0.0000 0.3949 0.0082 0.0079 0.0000 0.0000 0.0000
12 0 1822.0029 0.0146 0.0147 20.2966 2.5937 3.1791 0.3363 0.0326 0.0336 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
