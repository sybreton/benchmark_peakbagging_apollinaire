# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:19:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9881 0.0158 0.0153 12.6784 1.3902 1.5432 0.3397 0.0316 0.0349 90.0000 0.0000 0.0000 0.3944 0.0083 0.0081 0.0000 0.0000 0.0000
12 0 1822.0332 0.0139 0.0138 23.0613 2.9851 3.7385 0.3285 0.0310 0.0322 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
