# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:44:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9921 0.0167 0.0164 14.3469 1.4628 1.5664 0.3736 0.0311 0.0361 90.0000 0.0000 0.0000 0.3888 0.0089 0.0090 0.0000 0.0000 0.0000
12 0 1821.9772 0.0156 0.0158 19.3928 2.3617 2.9027 0.3758 0.0339 0.0352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
