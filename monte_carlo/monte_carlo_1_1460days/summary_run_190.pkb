# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:57:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0050 0.0184 0.0179 11.3116 1.2318 1.4121 0.3985 0.0382 0.0435 90.0000 0.0000 0.0000 0.4024 0.0094 0.0097 0.0000 0.0000 0.0000
12 0 1821.9937 0.0141 0.0141 21.3960 2.6828 3.2986 0.3369 0.0308 0.0324 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
