# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:54:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0189 0.0157 0.0158 14.7408 1.4884 1.7184 0.3562 0.0297 0.0329 90.0000 0.0000 0.0000 0.3901 0.0083 0.0081 0.0000 0.0000 0.0000
12 0 1822.0125 0.0142 0.0142 23.3487 2.9193 3.6639 0.3426 0.0313 0.0322 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
