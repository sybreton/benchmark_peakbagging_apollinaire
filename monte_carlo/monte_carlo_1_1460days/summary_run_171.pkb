# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 05:14:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9808 0.0220 0.0220 9.5069 1.0810 1.2298 0.4612 0.0470 0.0513 90.0000 0.0000 0.0000 0.3924 0.0112 0.0110 0.0000 0.0000 0.0000
12 0 1822.0137 0.0145 0.0146 19.1948 2.4932 3.1038 0.3264 0.0317 0.0332 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
