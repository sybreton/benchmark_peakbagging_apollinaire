# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 01:59:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9923 0.0188 0.0186 11.8608 1.3016 1.4874 0.3893 0.0380 0.0423 90.0000 0.0000 0.0000 0.3898 0.0101 0.0096 0.0000 0.0000 0.0000
12 0 1822.0155 0.0162 0.0154 18.4724 2.2572 2.7977 0.3699 0.0340 0.0356 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
