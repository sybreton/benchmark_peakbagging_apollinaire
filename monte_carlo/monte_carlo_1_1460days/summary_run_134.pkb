# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 03:51:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9957 0.0139 0.0136 14.2239 1.5023 1.7424 0.3307 0.0301 0.0329 90.0000 0.0000 0.0000 0.4115 0.0077 0.0080 0.0000 0.0000 0.0000
12 0 1821.9884 0.0137 0.0136 23.8762 3.1253 4.0034 0.2996 0.0293 0.0302 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
