# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:47:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0076 0.0138 0.0145 15.2796 1.5790 1.8794 0.3276 0.0293 0.0310 90.0000 0.0000 0.0000 0.4034 0.0075 0.0072 0.0000 0.0000 0.0000
12 0 1821.9871 0.0142 0.0145 22.0834 2.7248 3.4983 0.3357 0.0314 0.0317 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
