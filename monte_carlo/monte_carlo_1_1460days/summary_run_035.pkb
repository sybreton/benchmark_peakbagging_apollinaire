# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:05:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9913 0.0152 0.0150 15.1626 1.6029 1.7919 0.3425 0.0310 0.0334 90.0000 0.0000 0.0000 0.3946 0.0083 0.0083 0.0000 0.0000 0.0000
12 0 1821.9763 0.0144 0.0140 22.7341 2.8953 3.5353 0.3368 0.0315 0.0326 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
