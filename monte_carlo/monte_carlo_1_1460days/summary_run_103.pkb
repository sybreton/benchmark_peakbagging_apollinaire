# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:40:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9938 0.0161 0.0160 13.3575 1.3507 1.5839 0.3502 0.0302 0.0330 90.0000 0.0000 0.0000 0.4015 0.0079 0.0081 0.0000 0.0000 0.0000
12 0 1821.9914 0.0143 0.0145 20.1307 2.6380 3.1807 0.3400 0.0321 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
