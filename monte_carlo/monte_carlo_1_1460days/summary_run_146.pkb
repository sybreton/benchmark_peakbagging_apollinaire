# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:18:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0033 0.0183 0.0185 12.2751 1.3027 1.4493 0.4031 0.0369 0.0427 90.0000 0.0000 0.0000 0.3935 0.0102 0.0098 0.0000 0.0000 0.0000
12 0 1822.0075 0.0150 0.0149 19.8863 2.4536 2.9837 0.3495 0.0318 0.0338 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
