# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:50:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0147 0.0138 0.0138 15.5445 1.5740 1.8363 0.3213 0.0271 0.0295 90.0000 0.0000 0.0000 0.3981 0.0073 0.0071 0.0000 0.0000 0.0000
12 0 1821.9912 0.0162 0.0164 17.7140 2.1789 2.5896 0.3995 0.0370 0.0391 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
