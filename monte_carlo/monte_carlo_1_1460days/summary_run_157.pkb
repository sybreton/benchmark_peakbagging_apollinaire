# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 04:43:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0043 0.0169 0.0172 12.1927 1.3157 1.4810 0.3672 0.0340 0.0385 90.0000 0.0000 0.0000 0.3906 0.0096 0.0096 0.0000 0.0000 0.0000
12 0 1821.9912 0.0118 0.0117 32.9166 4.8957 5.9948 0.2601 0.0268 0.0281 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
