# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 23:44:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0158 0.0139 0.0147 16.0862 1.6528 1.9006 0.3429 0.0292 0.0326 90.0000 0.0000 0.0000 0.3944 0.0078 0.0080 0.0000 0.0000 0.0000
12 0 1822.0070 0.0154 0.0156 18.7153 2.2835 2.7891 0.3755 0.0345 0.0354 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
