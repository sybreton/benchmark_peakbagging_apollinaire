# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:03:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0023 0.0143 0.0139 15.1445 1.5676 1.8543 0.3256 0.0290 0.0319 90.0000 0.0000 0.0000 0.3900 0.0073 0.0072 0.0000 0.0000 0.0000
12 0 1821.9964 0.0148 0.0148 19.9934 2.5411 3.0968 0.3403 0.0324 0.0341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
