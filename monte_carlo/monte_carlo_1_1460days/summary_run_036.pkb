# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 00:07:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0039 0.0141 0.0141 15.4754 1.6526 1.9039 0.3213 0.0286 0.0316 90.0000 0.0000 0.0000 0.3940 0.0078 0.0075 0.0000 0.0000 0.0000
12 0 1821.9549 0.0151 0.0151 20.3519 2.4618 3.1199 0.3540 0.0316 0.0340 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
