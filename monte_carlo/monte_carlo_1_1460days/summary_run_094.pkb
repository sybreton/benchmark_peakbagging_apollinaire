# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 02:19:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0007 0.0138 0.0140 15.2868 1.5996 1.7955 0.3397 0.0298 0.0334 90.0000 0.0000 0.0000 0.3963 0.0076 0.0074 0.0000 0.0000 0.0000
12 0 1822.0075 0.0155 0.0155 16.9332 2.1712 2.6455 0.3656 0.0350 0.0374 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
