# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:40:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0923 0.0502 0.0452 10.1650 2.0612 2.6204 0.5651 0.1087 0.1433 90.0000 0.0000 0.0000 0.4289 0.0161 0.0161 0.0000 0.0000 0.0000
20 1 2963.0651 0.0545 0.0533 26.5793 3.7762 4.5778 1.0952 0.1229 0.1322 90.0000 0.0000 0.0000 0.2710 0.1434 0.0985 0.0000 0.0000 0.0000
