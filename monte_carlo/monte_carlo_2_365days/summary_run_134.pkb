# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:32:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1339 0.1055 0.1071 6.7228 1.4260 1.7152 0.8889 0.1867 0.2476 90.0000 0.0000 0.0000 0.3629 0.0365 0.0335 0.0000 0.0000 0.0000
20 1 2963.0534 0.0573 0.0566 27.1626 3.6147 4.4920 1.1325 0.1157 0.1271 90.0000 0.0000 0.0000 0.2537 0.1509 0.1134 0.0000 0.0000 0.0000
