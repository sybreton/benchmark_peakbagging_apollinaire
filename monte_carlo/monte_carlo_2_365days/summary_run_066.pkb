# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:51:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1877 0.1182 0.1413 5.3452 1.2816 1.6598 1.2591 0.3152 0.4528 90.0000 0.0000 0.0000 0.4032 0.0643 0.0477 0.0000 0.0000 0.0000
20 1 2963.0028 0.0492 0.0499 34.6254 4.8465 5.7122 0.9130 0.0991 0.1079 90.0000 0.0000 0.0000 0.4644 0.0671 0.0569 0.0000 0.0000 0.0000
