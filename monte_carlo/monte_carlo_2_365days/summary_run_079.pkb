# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:24:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0017 0.0935 0.1027 5.2651 1.4836 1.8798 0.9892 0.2831 0.4631 90.0000 0.0000 0.0000 0.4415 0.0372 0.0323 0.0000 0.0000 0.0000
20 1 2962.9139 0.0563 0.0548 28.8848 4.1008 4.6765 1.0272 0.1066 0.1227 90.0000 0.0000 0.0000 0.4741 0.0777 0.0661 0.0000 0.0000 0.0000
