# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:01:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9435 0.0892 0.0933 6.2968 1.4059 1.8116 1.0470 0.2448 0.3239 90.0000 0.0000 0.0000 0.3937 0.0394 0.0318 0.0000 0.0000 0.0000
20 1 2962.9909 0.0504 0.0513 31.2451 4.6203 5.6363 0.9549 0.1102 0.1207 90.0000 0.0000 0.0000 0.3224 0.1067 0.0764 0.0000 0.0000 0.0000
