# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:53:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9348 0.1206 0.1112 4.6165 1.2187 1.5777 1.2249 0.3409 0.5169 90.0000 0.0000 0.0000 0.3728 0.0539 0.0405 0.0000 0.0000 0.0000
20 1 2962.9641 0.0537 0.0528 31.7345 4.3203 5.2219 0.9361 0.0964 0.1063 90.0000 0.0000 0.0000 0.4636 0.0713 0.0588 0.0000 0.0000 0.0000
