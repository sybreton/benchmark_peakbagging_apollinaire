# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:54:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8728 0.1045 0.1112 6.5053 1.2430 1.5147 1.0746 0.2036 0.2578 90.0000 0.0000 0.0000 0.3861 0.0383 0.0345 0.0000 0.0000 0.0000
20 1 2963.0578 0.0554 0.0550 25.8432 3.5015 4.2379 1.2123 0.1237 0.1305 90.0000 0.0000 0.0000 0.1378 0.0922 0.1106 0.0000 0.0000 0.0000
