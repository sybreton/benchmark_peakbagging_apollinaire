# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:57:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.2361 0.1298 0.1352 4.5095 1.0129 1.2042 1.2959 0.2917 0.4070 90.0000 0.0000 0.0000 0.3348 0.0620 0.0497 0.0000 0.0000 0.0000
20 1 2962.9563 0.0552 0.0544 26.9757 3.7897 4.4852 1.1499 0.1165 0.1250 90.0000 0.0000 0.0000 0.1701 0.1145 0.1205 0.0000 0.0000 0.0000
