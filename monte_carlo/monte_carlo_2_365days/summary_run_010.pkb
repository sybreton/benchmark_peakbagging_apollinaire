# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:34:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9912 0.0953 0.1095 6.5723 1.4946 1.9543 0.9107 0.2111 0.2719 90.0000 0.0000 0.0000 0.3676 0.0485 0.0363 0.0000 0.0000 0.0000
20 1 2963.0911 0.0557 0.0547 30.3185 4.1660 4.9353 1.0401 0.1098 0.1254 90.0000 0.0000 0.0000 0.4757 0.0814 0.0682 0.0000 0.0000 0.0000
