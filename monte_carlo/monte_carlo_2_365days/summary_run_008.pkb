# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:29:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1020 0.1087 0.1183 5.3860 1.1616 1.4204 1.1548 0.2549 0.3432 90.0000 0.0000 0.0000 0.3978 0.0410 0.0383 0.0000 0.0000 0.0000
20 1 2963.0478 0.0628 0.0585 24.3015 3.4452 4.1679 1.1654 0.1272 0.1373 90.0000 0.0000 0.0000 0.2636 0.1558 0.1211 0.0000 0.0000 0.0000
