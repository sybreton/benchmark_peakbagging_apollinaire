# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:35:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8440 0.1627 0.1600 3.7190 0.7751 0.9705 1.8982 0.4372 0.5670 90.0000 0.0000 0.0000 0.3970 0.0859 0.0648 0.0000 0.0000 0.0000
20 1 2962.9647 0.0517 0.0499 30.9562 4.3146 5.3392 0.9086 0.1023 0.1113 90.0000 0.0000 0.0000 0.4548 0.0697 0.0590 0.0000 0.0000 0.0000
