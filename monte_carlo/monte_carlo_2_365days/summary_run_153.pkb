# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:35:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9559 0.0930 0.0948 6.7250 1.2885 1.5803 0.7711 0.1418 0.1795 90.0000 0.0000 0.0000 0.3573 0.0261 0.0251 0.0000 0.0000 0.0000
20 1 2962.9105 0.0490 0.0471 38.8464 5.6079 6.9622 0.9240 0.0989 0.1079 90.0000 0.0000 0.0000 0.2827 0.1140 0.0797 0.0000 0.0000 0.0000
