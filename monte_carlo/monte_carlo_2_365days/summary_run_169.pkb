# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:32:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9156 0.0811 0.0845 7.5662 1.6432 2.0180 0.8387 0.1725 0.2416 90.0000 0.0000 0.0000 0.4134 0.0343 0.0291 0.0000 0.0000 0.0000
20 1 2963.0284 0.0479 0.0479 32.1586 4.6694 5.5242 0.8879 0.0947 0.1090 90.0000 0.0000 0.0000 0.4199 0.0687 0.0571 0.0000 0.0000 0.0000
