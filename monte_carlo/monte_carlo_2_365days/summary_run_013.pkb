# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:41:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9533 0.1003 0.0883 7.1570 1.5364 1.9651 0.8291 0.1795 0.2339 90.0000 0.0000 0.0000 0.3793 0.0318 0.0294 0.0000 0.0000 0.0000
20 1 2962.9618 0.0573 0.0571 23.5564 3.2390 3.8495 1.2205 0.1257 0.1362 90.0000 0.0000 0.0000 0.2156 0.1379 0.1231 0.0000 0.0000 0.0000
