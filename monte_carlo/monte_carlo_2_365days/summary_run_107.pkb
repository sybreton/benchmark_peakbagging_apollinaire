# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:35:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9826 0.0930 0.0806 8.3031 1.6181 2.0658 0.7328 0.1385 0.1733 90.0000 0.0000 0.0000 0.3648 0.0277 0.0253 0.0000 0.0000 0.0000
20 1 2962.9671 0.0542 0.0551 30.3106 4.1947 5.0616 1.0624 0.1083 0.1179 90.0000 0.0000 0.0000 0.2702 0.1370 0.1018 0.0000 0.0000 0.0000
