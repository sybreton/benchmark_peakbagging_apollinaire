# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:18:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1107 0.1273 0.1235 4.9722 1.1606 1.4298 1.2701 0.2935 0.4366 90.0000 0.0000 0.0000 0.4064 0.0523 0.0403 0.0000 0.0000 0.0000
20 1 2962.9784 0.0513 0.0522 35.0061 4.6850 5.6338 0.9506 0.0944 0.1071 90.0000 0.0000 0.0000 0.4741 0.0701 0.0606 0.0000 0.0000 0.0000
