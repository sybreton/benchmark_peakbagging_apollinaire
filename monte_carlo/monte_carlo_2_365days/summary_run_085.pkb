# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:39:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1319 0.1150 0.1193 5.4382 1.1222 1.3871 1.1617 0.2421 0.3177 90.0000 0.0000 0.0000 0.3985 0.0452 0.0404 0.0000 0.0000 0.0000
20 1 2963.0014 0.0577 0.0565 25.9683 3.5594 4.3472 1.1232 0.1214 0.1393 90.0000 0.0000 0.0000 0.4760 0.0949 0.0728 0.0000 0.0000 0.0000
