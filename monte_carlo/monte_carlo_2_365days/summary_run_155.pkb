# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:42:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9510 0.1417 0.1381 4.2971 0.9345 1.1902 1.6861 0.4055 0.5428 90.0000 0.0000 0.0000 0.3622 0.0809 0.0556 0.0000 0.0000 0.0000
20 1 2963.0564 0.0529 0.0540 31.0536 4.3838 5.2158 1.0212 0.1115 0.1208 90.0000 0.0000 0.0000 0.3481 0.1131 0.0785 0.0000 0.0000 0.0000
