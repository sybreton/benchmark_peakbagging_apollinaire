# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:37:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8851 0.1235 0.0889 6.8409 1.4209 1.8317 0.7943 0.1754 0.2142 90.0000 0.0000 0.0000 0.3869 0.0255 0.0346 0.0000 0.0000 0.0000
20 1 2963.0172 0.0567 0.0563 27.3602 3.8002 4.4066 1.0950 0.1157 0.1293 90.0000 0.0000 0.0000 0.3949 0.1098 0.0802 0.0000 0.0000 0.0000
