# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:45:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9106 0.1181 0.1105 4.9811 1.1869 1.5918 1.4072 0.3727 0.5170 90.0000 0.0000 0.0000 0.3516 0.0682 0.0448 0.0000 0.0000 0.0000
20 1 2963.0931 0.0592 0.0594 21.4875 3.0772 3.7752 1.2693 0.1496 0.1627 90.0000 0.0000 0.0000 0.2686 0.1520 0.1161 0.0000 0.0000 0.0000
