# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:30:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9980 0.0637 0.0736 8.7022 1.8927 2.2602 0.6869 0.1369 0.1888 90.0000 0.0000 0.0000 0.4020 0.0269 0.0227 0.0000 0.0000 0.0000
20 1 2963.0190 0.0669 0.0651 24.0852 3.4509 4.1332 1.2254 0.1391 0.1540 90.0000 0.0000 0.0000 0.2822 0.1632 0.1314 0.0000 0.0000 0.0000
