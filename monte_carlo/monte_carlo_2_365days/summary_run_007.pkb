# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:26:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.2173 0.1307 0.0957 6.9150 1.5344 1.9137 0.9475 0.2094 0.2859 90.0000 0.0000 0.0000 0.4647 0.0463 0.0343 0.0000 0.0000 0.0000
20 1 2963.1257 0.0510 0.0520 29.6379 4.2967 5.3026 0.9194 0.1062 0.1236 90.0000 0.0000 0.0000 0.3996 0.0826 0.0611 0.0000 0.0000 0.0000
