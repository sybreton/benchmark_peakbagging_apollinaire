# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:46:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9785 0.1304 0.1421 6.1184 1.1583 1.4421 1.0528 0.1950 0.2401 90.0000 0.0000 0.0000 0.3948 0.0456 0.0399 0.0000 0.0000 0.0000
20 1 2963.0058 0.0627 0.0628 25.4243 3.5983 4.2864 1.1763 0.1360 0.1471 90.0000 0.0000 0.0000 0.3819 0.1415 0.0955 0.0000 0.0000 0.0000
