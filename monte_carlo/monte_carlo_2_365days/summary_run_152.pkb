# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:31:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1103 0.1107 0.1102 5.9491 1.1332 1.4006 1.1837 0.2285 0.3019 90.0000 0.0000 0.0000 0.3665 0.0466 0.0423 0.0000 0.0000 0.0000
20 1 2962.9966 0.0556 0.0572 25.5949 3.5173 4.0484 1.2352 0.1291 0.1398 90.0000 0.0000 0.0000 0.2614 0.1508 0.1159 0.0000 0.0000 0.0000
