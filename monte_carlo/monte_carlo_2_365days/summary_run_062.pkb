# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:41:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9750 0.1199 0.1251 5.9205 1.2707 1.5711 0.9572 0.2003 0.2791 90.0000 0.0000 0.0000 0.4066 0.0414 0.0409 0.0000 0.0000 0.0000
20 1 2962.9556 0.0508 0.0501 34.4339 4.7644 5.7980 0.9291 0.0984 0.1071 90.0000 0.0000 0.0000 0.4149 0.0736 0.0591 0.0000 0.0000 0.0000
