# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:56:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9020 0.1644 0.1698 3.7024 0.8105 1.0089 1.4496 0.3318 0.4433 90.0000 0.0000 0.0000 0.4250 0.0639 0.0579 0.0000 0.0000 0.0000
20 1 2963.1112 0.0558 0.0539 28.3353 4.0564 4.9410 0.9616 0.1057 0.1213 90.0000 0.0000 0.0000 0.3847 0.0939 0.0708 0.0000 0.0000 0.0000
