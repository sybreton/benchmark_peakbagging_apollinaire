# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:47:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1187 0.1500 0.1554 4.3236 0.9355 1.1592 1.2792 0.2808 0.3735 90.0000 0.0000 0.0000 0.3290 0.0605 0.0559 0.0000 0.0000 0.0000
20 1 2962.9467 0.0403 0.0417 46.7277 7.2084 8.9929 0.7453 0.0874 0.0935 90.0000 0.0000 0.0000 0.4309 0.0505 0.0440 0.0000 0.0000 0.0000
