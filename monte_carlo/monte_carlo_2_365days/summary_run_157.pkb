# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:49:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9727 0.1249 0.1234 5.7120 1.1244 1.4369 1.1870 0.2426 0.3149 90.0000 0.0000 0.0000 0.3999 0.0414 0.0411 0.0000 0.0000 0.0000
20 1 2962.9371 0.0543 0.0524 28.3572 4.0942 5.0669 1.0824 0.1167 0.1267 90.0000 0.0000 0.0000 0.2201 0.1385 0.1096 0.0000 0.0000 0.0000
