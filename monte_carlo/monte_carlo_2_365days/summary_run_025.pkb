# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:09:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0495 0.0792 0.1027 6.8114 1.6463 2.1499 0.7863 0.1946 0.2728 90.0000 0.0000 0.0000 0.4128 0.0279 0.0251 0.0000 0.0000 0.0000
20 1 2963.0311 0.0481 0.0487 38.6144 5.4860 7.1125 0.8577 0.0947 0.1034 90.0000 0.0000 0.0000 0.4110 0.0688 0.0559 0.0000 0.0000 0.0000
