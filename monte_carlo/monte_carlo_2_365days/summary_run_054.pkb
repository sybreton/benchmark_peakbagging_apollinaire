# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:20:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9342 0.0950 0.0987 6.6821 1.3081 1.5985 0.9159 0.1735 0.2247 90.0000 0.0000 0.0000 0.3848 0.0312 0.0296 0.0000 0.0000 0.0000
20 1 2963.0515 0.0510 0.0522 32.9608 4.6020 5.6780 1.0340 0.1157 0.1240 90.0000 0.0000 0.0000 0.3449 0.1122 0.0772 0.0000 0.0000 0.0000
