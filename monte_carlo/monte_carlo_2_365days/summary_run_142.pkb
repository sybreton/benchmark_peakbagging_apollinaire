# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:58:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1099 0.1605 0.1925 4.1339 1.1091 1.3378 1.2267 0.3131 0.5038 90.0000 0.0000 0.0000 0.3909 0.0792 0.0519 0.0000 0.0000 0.0000
20 1 2962.9866 0.0535 0.0543 33.6973 4.8546 5.9643 0.9473 0.1068 0.1197 90.0000 0.0000 0.0000 0.3802 0.0945 0.0723 0.0000 0.0000 0.0000
