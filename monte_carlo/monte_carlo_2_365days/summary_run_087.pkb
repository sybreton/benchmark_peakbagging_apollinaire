# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:44:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0086 0.0682 0.0688 7.2576 1.6755 2.1609 0.7404 0.1757 0.2462 90.0000 0.0000 0.0000 0.3948 0.0242 0.0248 0.0000 0.0000 0.0000
20 1 2963.0421 0.0549 0.0556 29.4265 4.1507 5.0010 1.0625 0.1169 0.1336 90.0000 0.0000 0.0000 0.3303 0.1271 0.0890 0.0000 0.0000 0.0000
