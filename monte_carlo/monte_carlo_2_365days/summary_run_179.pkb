# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:03:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0076 0.0661 0.0629 7.4988 1.5462 1.9798 0.6267 0.1264 0.1712 90.0000 0.0000 0.0000 0.4254 0.0221 0.0204 0.0000 0.0000 0.0000
20 1 2962.9270 0.0513 0.0515 29.8890 4.2237 5.4318 0.9964 0.1122 0.1210 90.0000 0.0000 0.0000 0.2881 0.1277 0.0832 0.0000 0.0000 0.0000
