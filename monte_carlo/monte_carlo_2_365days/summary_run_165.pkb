# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:18:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9718 0.0785 0.0818 6.7903 1.4237 1.7511 0.8377 0.1686 0.2325 90.0000 0.0000 0.0000 0.4350 0.0302 0.0263 0.0000 0.0000 0.0000
20 1 2962.9580 0.0543 0.0552 29.9448 4.1314 5.0104 0.9898 0.1055 0.1200 90.0000 0.0000 0.0000 0.4574 0.0801 0.0617 0.0000 0.0000 0.0000
