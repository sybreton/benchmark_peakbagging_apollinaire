# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:43:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1106 0.1147 0.0990 5.3461 1.2833 1.7405 1.2220 0.3264 0.4521 90.0000 0.0000 0.0000 0.3821 0.0374 0.0333 0.0000 0.0000 0.0000
20 1 2963.0706 0.0542 0.0552 26.6526 3.7237 4.7347 1.1073 0.1286 0.1325 90.0000 0.0000 0.0000 0.2492 0.1449 0.1067 0.0000 0.0000 0.0000
