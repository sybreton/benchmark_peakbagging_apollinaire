# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:23:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9080 0.1088 0.1133 5.6268 1.3263 1.6512 0.8703 0.1950 0.2731 90.0000 0.0000 0.0000 0.3651 0.0512 0.0402 0.0000 0.0000 0.0000
20 1 2963.1233 0.0544 0.0525 33.7019 4.5845 5.6845 0.9240 0.0980 0.1068 90.0000 0.0000 0.0000 0.4993 0.0656 0.0575 0.0000 0.0000 0.0000
