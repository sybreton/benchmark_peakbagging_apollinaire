# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:15:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.2323 0.1618 0.1489 4.4373 1.0435 1.3222 1.3956 0.3448 0.4937 90.0000 0.0000 0.0000 0.3629 0.0718 0.0616 0.0000 0.0000 0.0000
20 1 2963.0473 0.0513 0.0534 31.1064 4.2009 5.1085 0.9286 0.0936 0.1051 90.0000 0.0000 0.0000 0.4970 0.0654 0.0578 0.0000 0.0000 0.0000
