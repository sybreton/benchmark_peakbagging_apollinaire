# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:55:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9709 0.0877 0.1109 8.0532 1.7319 2.3980 0.8417 0.1919 0.2403 90.0000 0.0000 0.0000 0.3705 0.0388 0.0297 0.0000 0.0000 0.0000
20 1 2962.9808 0.0529 0.0523 28.5211 4.0096 4.8977 0.9847 0.1073 0.1180 90.0000 0.0000 0.0000 0.2705 0.1262 0.0924 0.0000 0.0000 0.0000
