# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:01:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1118 0.1009 0.1151 6.4747 1.4705 2.1011 1.1369 0.2887 0.3645 90.0000 0.0000 0.0000 0.3698 0.0622 0.0492 0.0000 0.0000 0.0000
20 1 2963.0245 0.0541 0.0543 28.0511 4.1133 4.9696 1.0517 0.1209 0.1396 90.0000 0.0000 0.0000 0.3160 0.1276 0.0884 0.0000 0.0000 0.0000
