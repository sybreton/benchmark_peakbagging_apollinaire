# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:01:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9935 0.0616 0.0652 8.5621 1.8662 2.3214 0.7457 0.1589 0.2204 90.0000 0.0000 0.0000 0.4191 0.0246 0.0233 0.0000 0.0000 0.0000
20 1 2963.0555 0.0513 0.0509 28.6410 4.0851 5.0789 1.0327 0.1173 0.1240 90.0000 0.0000 0.0000 0.2536 0.1338 0.0950 0.0000 0.0000 0.0000
