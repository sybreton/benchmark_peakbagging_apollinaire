# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:48:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0349 0.0992 0.1119 5.7096 1.3738 1.9349 0.8802 0.2315 0.2913 90.0000 0.0000 0.0000 0.3076 0.0387 0.0326 0.0000 0.0000 0.0000
20 1 2962.9417 0.0578 0.0568 24.7520 3.4151 4.1612 1.1531 0.1177 0.1334 90.0000 0.0000 0.0000 0.2247 0.1353 0.1150 0.0000 0.0000 0.0000
