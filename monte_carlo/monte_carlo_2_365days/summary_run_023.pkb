# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:05:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0177 0.1126 0.1107 5.8580 1.2218 1.5255 0.9550 0.2008 0.2611 90.0000 0.0000 0.0000 0.3489 0.0357 0.0366 0.0000 0.0000 0.0000
20 1 2963.0438 0.0493 0.0499 30.2669 4.6479 5.6692 0.9493 0.1211 0.1345 90.0000 0.0000 0.0000 0.3433 0.1072 0.0720 0.0000 0.0000 0.0000
