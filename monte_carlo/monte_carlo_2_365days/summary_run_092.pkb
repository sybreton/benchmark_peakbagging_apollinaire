# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:57:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1093 0.0988 0.1049 6.0645 1.2994 1.5973 1.0073 0.2153 0.2944 90.0000 0.0000 0.0000 0.4049 0.0362 0.0324 0.0000 0.0000 0.0000
20 1 2962.8896 0.0567 0.0545 31.1413 4.1734 4.9932 1.0594 0.1070 0.1191 90.0000 0.0000 0.0000 0.4433 0.0851 0.0715 0.0000 0.0000 0.0000
