# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:49:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.7393 0.0704 0.0687 7.5918 1.6738 2.1115 0.7033 0.1554 0.2064 90.0000 0.0000 0.0000 0.3790 0.0242 0.0230 0.0000 0.0000 0.0000
20 1 2963.0836 0.0610 0.0607 23.8769 3.3183 4.1238 1.2059 0.1374 0.1535 90.0000 0.0000 0.0000 0.4359 0.1287 0.0885 0.0000 0.0000 0.0000
