# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:46:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8539 0.0941 0.0999 5.9762 1.3254 1.6806 1.0321 0.2434 0.3283 90.0000 0.0000 0.0000 0.3932 0.0339 0.0299 0.0000 0.0000 0.0000
20 1 2963.0745 0.0528 0.0552 28.9676 4.0120 4.7629 1.0710 0.1184 0.1321 90.0000 0.0000 0.0000 0.4290 0.0930 0.0704 0.0000 0.0000 0.0000
