# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:07:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9543 0.0808 0.0863 7.8080 1.4662 1.9047 0.8235 0.1561 0.1941 90.0000 0.0000 0.0000 0.4258 0.0282 0.0253 0.0000 0.0000 0.0000
20 1 2963.0293 0.0530 0.0532 31.3773 4.5148 5.5362 1.0215 0.1156 0.1247 90.0000 0.0000 0.0000 0.2920 0.1316 0.0897 0.0000 0.0000 0.0000
