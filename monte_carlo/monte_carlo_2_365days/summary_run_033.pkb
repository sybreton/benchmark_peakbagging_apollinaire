# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:27:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9503 0.1083 0.1170 5.2130 1.2912 1.6260 1.1565 0.2970 0.4314 90.0000 0.0000 0.0000 0.3718 0.0520 0.0385 0.0000 0.0000 0.0000
20 1 2962.9840 0.0544 0.0542 30.6267 4.1961 5.0574 1.0074 0.1054 0.1211 90.0000 0.0000 0.0000 0.4631 0.0803 0.0642 0.0000 0.0000 0.0000
