# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:13:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8773 0.1148 0.1141 5.6442 1.1060 1.3979 1.2730 0.2557 0.3371 90.0000 0.0000 0.0000 0.3055 0.0583 0.0470 0.0000 0.0000 0.0000
20 1 2962.9002 0.0512 0.0521 31.4243 4.3801 5.3314 1.0050 0.1069 0.1231 90.0000 0.0000 0.0000 0.4805 0.0756 0.0613 0.0000 0.0000 0.0000
