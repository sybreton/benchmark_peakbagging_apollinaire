# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:19:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9042 0.1404 0.1335 4.6464 1.0284 1.2738 1.4078 0.3273 0.4513 90.0000 0.0000 0.0000 0.3815 0.0525 0.0451 0.0000 0.0000 0.0000
20 1 2962.9999 0.0448 0.0441 38.3510 5.7512 7.0737 0.7744 0.0899 0.0991 90.0000 0.0000 0.0000 0.4487 0.0546 0.0476 0.0000 0.0000 0.0000
