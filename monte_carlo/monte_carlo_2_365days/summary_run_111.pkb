# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:24:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9345 0.1408 0.1561 3.9920 0.9832 1.2548 1.6264 0.4340 0.6211 90.0000 0.0000 0.0000 0.3829 0.0714 0.0531 0.0000 0.0000 0.0000
20 1 2962.9701 0.0493 0.0502 31.1266 4.5403 5.4767 0.9919 0.1103 0.1172 90.0000 0.0000 0.0000 0.2999 0.1209 0.0806 0.0000 0.0000 0.0000
