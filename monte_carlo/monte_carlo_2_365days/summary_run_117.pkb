# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:41:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0768 0.0965 0.1010 5.5950 1.2491 1.5654 1.0555 0.2400 0.3261 90.0000 0.0000 0.0000 0.4373 0.0363 0.0330 0.0000 0.0000 0.0000
20 1 2962.9954 0.0732 0.0737 24.8898 3.4566 4.0970 1.1528 0.1293 0.1472 90.0000 0.0000 0.0000 0.4778 0.1261 0.0911 0.0000 0.0000 0.0000
