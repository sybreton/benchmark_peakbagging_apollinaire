# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:46:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0707 0.0685 0.0611 8.7709 2.0575 2.3750 0.6418 0.1313 0.2001 90.0000 0.0000 0.0000 0.4720 0.0283 0.0233 0.0000 0.0000 0.0000
20 1 2963.0472 0.0502 0.0507 34.8426 4.9992 6.0827 0.9020 0.1020 0.1124 90.0000 0.0000 0.0000 0.4334 0.0731 0.0591 0.0000 0.0000 0.0000
