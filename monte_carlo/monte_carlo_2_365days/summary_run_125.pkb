# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:04:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9022 0.1240 0.1190 4.9550 1.1438 1.4582 1.4191 0.3499 0.4904 90.0000 0.0000 0.0000 0.3687 0.0679 0.0477 0.0000 0.0000 0.0000
20 1 2963.0236 0.0546 0.0539 30.4499 4.1200 5.1509 0.9772 0.1037 0.1112 90.0000 0.0000 0.0000 0.4410 0.0821 0.0659 0.0000 0.0000 0.0000
