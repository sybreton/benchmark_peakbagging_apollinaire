# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:16:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9092 0.0641 0.0661 9.4355 1.9494 2.3669 0.6963 0.1340 0.1851 90.0000 0.0000 0.0000 0.4391 0.0241 0.0221 0.0000 0.0000 0.0000
20 1 2963.0138 0.0522 0.0517 28.2464 3.9558 4.8791 1.0211 0.1103 0.1185 90.0000 0.0000 0.0000 0.2596 0.1377 0.0935 0.0000 0.0000 0.0000
