# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:35:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0173 0.0974 0.1148 6.4887 1.2963 1.6843 0.9011 0.1809 0.2317 90.0000 0.0000 0.0000 0.3979 0.0329 0.0336 0.0000 0.0000 0.0000
20 1 2963.0148 0.0503 0.0513 33.1596 4.5436 5.6195 0.9812 0.1052 0.1175 90.0000 0.0000 0.0000 0.4031 0.0838 0.0682 0.0000 0.0000 0.0000
