# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:25:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9712 0.1042 0.1103 5.7291 1.1895 1.4191 0.9398 0.1888 0.2581 90.0000 0.0000 0.0000 0.4221 0.0367 0.0347 0.0000 0.0000 0.0000
20 1 2962.9260 0.0461 0.0478 37.6308 5.5045 6.6372 0.8598 0.0951 0.1090 90.0000 0.0000 0.0000 0.4008 0.0712 0.0582 0.0000 0.0000 0.0000
