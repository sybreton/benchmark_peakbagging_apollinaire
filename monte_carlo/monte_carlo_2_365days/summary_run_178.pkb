# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:00:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8242 0.0991 0.0981 7.2587 1.5095 1.9041 0.9564 0.2004 0.2734 90.0000 0.0000 0.0000 0.3969 0.0355 0.0324 0.0000 0.0000 0.0000
20 1 2962.9737 0.0567 0.0548 26.3716 3.7396 4.4383 1.0981 0.1251 0.1442 90.0000 0.0000 0.0000 0.4374 0.1019 0.0748 0.0000 0.0000 0.0000
