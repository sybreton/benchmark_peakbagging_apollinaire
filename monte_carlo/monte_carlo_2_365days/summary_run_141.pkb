# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:54:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8476 0.0908 0.1066 6.2750 1.5534 1.7791 0.8056 0.1832 0.2970 90.0000 0.0000 0.0000 0.4219 0.0344 0.0294 0.0000 0.0000 0.0000
20 1 2963.1293 0.0574 0.0569 25.9147 3.4688 4.4969 1.1224 0.1187 0.1282 90.0000 0.0000 0.0000 0.2982 0.1500 0.1021 0.0000 0.0000 0.0000
