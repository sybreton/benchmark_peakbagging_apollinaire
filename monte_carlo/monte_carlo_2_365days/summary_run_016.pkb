# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:48:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9162 0.1224 0.1236 4.8418 1.0450 1.2671 1.2161 0.2671 0.3563 90.0000 0.0000 0.0000 0.4248 0.0445 0.0408 0.0000 0.0000 0.0000
20 1 2963.0595 0.0458 0.0471 35.0875 5.0321 6.1354 0.8173 0.0890 0.0972 90.0000 0.0000 0.0000 0.4415 0.0604 0.0527 0.0000 0.0000 0.0000
