# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:58:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1193 0.1638 0.2014 4.3227 1.1730 1.3480 1.3563 0.3462 0.5841 90.0000 0.0000 0.0000 0.3780 0.1044 0.0566 0.0000 0.0000 0.0000
20 1 2963.0069 0.0553 0.0551 29.4782 4.0530 4.7529 1.0382 0.1103 0.1249 90.0000 0.0000 0.0000 0.4189 0.0984 0.0751 0.0000 0.0000 0.0000
