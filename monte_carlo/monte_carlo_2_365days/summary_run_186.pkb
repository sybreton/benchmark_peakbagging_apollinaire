# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:23:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0833 0.0910 0.0773 6.7548 1.4703 2.0483 0.8701 0.2185 0.2576 90.0000 0.0000 0.0000 0.3862 0.0284 0.0264 0.0000 0.0000 0.0000
20 1 2962.9996 0.0553 0.0565 25.0418 3.4070 4.3289 1.1594 0.1261 0.1298 90.0000 0.0000 0.0000 0.2551 0.1449 0.1085 0.0000 0.0000 0.0000
