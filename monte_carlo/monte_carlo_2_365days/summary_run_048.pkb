# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:05:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0518 0.0913 0.0989 6.7558 1.4472 1.8615 0.9027 0.1979 0.2524 90.0000 0.0000 0.0000 0.3996 0.0369 0.0323 0.0000 0.0000 0.0000
20 1 2963.0249 0.0485 0.0475 29.6373 4.1753 5.0789 0.9008 0.0997 0.1139 90.0000 0.0000 0.0000 0.5510 0.0567 0.0504 0.0000 0.0000 0.0000
