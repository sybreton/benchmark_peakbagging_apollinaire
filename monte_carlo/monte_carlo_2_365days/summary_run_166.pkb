# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:22:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9651 0.1324 0.0975 5.6311 1.5257 2.2420 1.0175 0.3132 0.4191 90.0000 0.0000 0.0000 0.3804 0.0535 0.0349 0.0000 0.0000 0.0000
20 1 2962.9797 0.0514 0.0507 34.3886 4.9787 6.1651 0.8982 0.1031 0.1134 90.0000 0.0000 0.0000 0.4115 0.0752 0.0603 0.0000 0.0000 0.0000
