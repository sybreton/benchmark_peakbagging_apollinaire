# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:45:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9852 0.0849 0.0818 7.0254 1.4568 1.7348 0.8779 0.1725 0.2435 90.0000 0.0000 0.0000 0.4656 0.0309 0.0280 0.0000 0.0000 0.0000
20 1 2962.9832 0.0551 0.0537 28.3721 3.8791 4.6861 0.9698 0.1006 0.1123 90.0000 0.0000 0.0000 0.4495 0.0769 0.0640 0.0000 0.0000 0.0000
