# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:46:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8587 0.1291 0.1396 3.8258 1.0409 1.3262 1.2984 0.3723 0.5682 90.0000 0.0000 0.0000 0.3698 0.0538 0.0444 0.0000 0.0000 0.0000
20 1 2963.0885 0.0517 0.0506 33.2878 4.9312 5.7750 0.9470 0.1024 0.1120 90.0000 0.0000 0.0000 0.2628 0.1296 0.0888 0.0000 0.0000 0.0000
