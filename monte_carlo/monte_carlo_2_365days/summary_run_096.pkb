# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:08:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.2742 0.0998 0.1043 5.8985 1.3436 1.6446 0.9218 0.2061 0.2868 90.0000 0.0000 0.0000 0.3211 0.0346 0.0308 0.0000 0.0000 0.0000
20 1 2962.9515 0.0431 0.0436 34.4804 5.1088 6.3374 0.8151 0.0974 0.1084 90.0000 0.0000 0.0000 0.4819 0.0542 0.0492 0.0000 0.0000 0.0000
