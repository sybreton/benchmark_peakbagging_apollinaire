# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:14:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8768 0.0786 0.0853 7.0902 1.4946 1.8692 0.8522 0.1830 0.2323 90.0000 0.0000 0.0000 0.4336 0.0300 0.0286 0.0000 0.0000 0.0000
20 1 2962.8933 0.0587 0.0595 24.6803 3.4790 4.1162 1.1323 0.1231 0.1406 90.0000 0.0000 0.0000 0.3914 0.1250 0.0863 0.0000 0.0000 0.0000
