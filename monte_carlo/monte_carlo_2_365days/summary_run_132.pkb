# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:26:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0799 0.1109 0.0997 6.6427 1.2655 1.5880 0.8104 0.1486 0.1913 90.0000 0.0000 0.0000 0.3642 0.0295 0.0307 0.0000 0.0000 0.0000
20 1 2962.9813 0.0503 0.0522 31.8231 4.3675 5.4403 0.9211 0.0963 0.1051 90.0000 0.0000 0.0000 0.4100 0.0773 0.0625 0.0000 0.0000 0.0000
