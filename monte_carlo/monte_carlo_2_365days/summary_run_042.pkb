# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:50:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9770 0.1139 0.1115 6.1044 1.2242 1.5451 1.0150 0.2040 0.2672 90.0000 0.0000 0.0000 0.4188 0.0384 0.0363 0.0000 0.0000 0.0000
20 1 2963.0368 0.0571 0.0593 24.3609 3.5418 4.3778 1.1501 0.1377 0.1520 90.0000 0.0000 0.0000 0.3111 0.1553 0.1050 0.0000 0.0000 0.0000
