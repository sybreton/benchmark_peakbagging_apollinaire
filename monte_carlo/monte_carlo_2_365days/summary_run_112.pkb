# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:27:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0935 0.0980 0.0963 7.2853 1.4818 1.9344 0.7852 0.1613 0.2028 90.0000 0.0000 0.0000 0.3577 0.0344 0.0347 0.0000 0.0000 0.0000
20 1 2963.0620 0.0582 0.0577 29.3769 4.0706 4.9964 1.0557 0.1136 0.1291 90.0000 0.0000 0.0000 0.4274 0.0934 0.0741 0.0000 0.0000 0.0000
