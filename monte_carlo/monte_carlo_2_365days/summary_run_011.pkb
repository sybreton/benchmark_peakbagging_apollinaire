# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:36:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1215 0.1082 0.1061 5.1580 1.2234 1.6261 1.2574 0.3276 0.4475 90.0000 0.0000 0.0000 0.3473 0.0512 0.0387 0.0000 0.0000 0.0000
20 1 2962.9490 0.0634 0.0627 21.8546 3.0047 3.6720 1.3323 0.1491 0.1574 90.0000 0.0000 0.0000 0.3265 0.1747 0.1174 0.0000 0.0000 0.0000
