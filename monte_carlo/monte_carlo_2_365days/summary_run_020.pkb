# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:58:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0714 0.0903 0.0951 5.9323 1.5787 2.0209 1.0113 0.2738 0.4208 90.0000 0.0000 0.0000 0.4145 0.0463 0.0336 0.0000 0.0000 0.0000
20 1 2963.0534 0.0524 0.0518 36.7295 5.3682 6.6638 0.8982 0.0997 0.1092 90.0000 0.0000 0.0000 0.3477 0.0982 0.0703 0.0000 0.0000 0.0000
