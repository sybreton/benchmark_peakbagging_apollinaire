# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:38:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0992 0.0820 0.0716 7.9343 1.6976 2.1335 0.7912 0.1688 0.2274 90.0000 0.0000 0.0000 0.3975 0.0267 0.0266 0.0000 0.0000 0.0000
20 1 2963.0498 0.0527 0.0533 29.5718 4.2056 4.9603 0.9638 0.1046 0.1191 90.0000 0.0000 0.0000 0.3944 0.0899 0.0699 0.0000 0.0000 0.0000
