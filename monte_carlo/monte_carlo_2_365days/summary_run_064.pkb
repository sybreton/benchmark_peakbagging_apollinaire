# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:46:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9684 0.1207 0.1199 5.1027 1.0968 1.3625 1.1362 0.2427 0.3449 90.0000 0.0000 0.0000 0.3571 0.0444 0.0394 0.0000 0.0000 0.0000
20 1 2963.0515 0.0614 0.0619 21.4454 2.8934 3.5519 1.2966 0.1380 0.1460 90.0000 0.0000 0.0000 0.2089 0.1374 0.1410 0.0000 0.0000 0.0000
