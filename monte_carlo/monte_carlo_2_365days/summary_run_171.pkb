# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:38:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0325 0.1174 0.1243 5.1123 1.1010 1.4117 1.1366 0.2550 0.3300 90.0000 0.0000 0.0000 0.4409 0.0444 0.0419 0.0000 0.0000 0.0000
20 1 2962.9583 0.0567 0.0547 27.5201 3.9186 4.7141 1.0646 0.1224 0.1339 90.0000 0.0000 0.0000 0.3555 0.1159 0.0821 0.0000 0.0000 0.0000
