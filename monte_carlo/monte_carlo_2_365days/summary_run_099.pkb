# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:15:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.6235 0.1203 0.1263 3.8305 0.6924 1.0562 1.6273 0.3601 0.3632 90.0000 0.0000 0.0000 0.1591 0.0992 0.0848 0.0000 0.0000 0.0000
20 1 2962.9915 0.0532 0.0534 28.5062 4.1692 5.0387 1.0505 0.1194 0.1285 90.0000 0.0000 0.0000 0.2896 0.1299 0.0902 0.0000 0.0000 0.0000
