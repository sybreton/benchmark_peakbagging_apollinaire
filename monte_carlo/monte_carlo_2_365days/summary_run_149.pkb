# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:22:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8970 0.1482 0.1379 5.4421 1.1662 1.4421 1.0510 0.2259 0.3071 90.0000 0.0000 0.0000 0.4243 0.0463 0.0394 0.0000 0.0000 0.0000
20 1 2962.9925 0.0462 0.0465 36.0884 5.4878 6.9592 0.8676 0.1021 0.1123 90.0000 0.0000 0.0000 0.2886 0.1028 0.0732 0.0000 0.0000 0.0000
