# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:35:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1152 0.1142 0.1197 6.1798 1.2781 1.5525 0.8788 0.1783 0.2456 90.0000 0.0000 0.0000 0.4379 0.0344 0.0373 0.0000 0.0000 0.0000
20 1 2963.0723 0.0557 0.0548 24.8949 3.5825 4.3245 1.0933 0.1242 0.1420 90.0000 0.0000 0.0000 0.3876 0.1174 0.0816 0.0000 0.0000 0.0000
