# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:28:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.2734 0.1458 0.1577 4.3446 0.9668 1.2213 1.3750 0.3128 0.4333 90.0000 0.0000 0.0000 0.3424 0.0753 0.0544 0.0000 0.0000 0.0000
20 1 2962.9697 0.0507 0.0493 30.7075 4.3777 5.3210 1.0214 0.1064 0.1145 90.0000 0.0000 0.0000 0.1884 0.1164 0.1024 0.0000 0.0000 0.0000
