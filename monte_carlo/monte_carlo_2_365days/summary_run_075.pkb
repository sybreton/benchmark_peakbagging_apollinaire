# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:14:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0432 0.1053 0.1042 5.5470 1.2069 1.4246 0.8355 0.1719 0.2531 90.0000 0.0000 0.0000 0.4108 0.0285 0.0277 0.0000 0.0000 0.0000
20 1 2963.0942 0.0540 0.0536 30.5953 4.3935 5.4534 0.9537 0.1072 0.1179 90.0000 0.0000 0.0000 0.3729 0.0963 0.0713 0.0000 0.0000 0.0000
