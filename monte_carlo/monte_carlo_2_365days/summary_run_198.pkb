# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:59:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0730 0.1258 0.1086 5.3187 1.2298 1.4319 1.0691 0.2406 0.3601 90.0000 0.0000 0.0000 0.4628 0.0451 0.0354 0.0000 0.0000 0.0000
20 1 2963.0621 0.0524 0.0529 28.6903 3.9980 5.0083 1.0267 0.1139 0.1229 90.0000 0.0000 0.0000 0.3031 0.1320 0.0892 0.0000 0.0000 0.0000
