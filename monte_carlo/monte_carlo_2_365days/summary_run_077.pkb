# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:19:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0266 0.0555 0.0520 9.5198 2.5700 3.0076 0.6005 0.1419 0.2358 90.0000 0.0000 0.0000 0.4465 0.0237 0.0204 0.0000 0.0000 0.0000
20 1 2962.9790 0.0506 0.0533 33.4043 4.4105 5.4865 0.9328 0.0974 0.1060 90.0000 0.0000 0.0000 0.5047 0.0669 0.0578 0.0000 0.0000 0.0000
