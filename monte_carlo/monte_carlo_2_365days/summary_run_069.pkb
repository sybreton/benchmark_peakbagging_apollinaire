# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:58:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1740 0.1208 0.1190 6.0563 1.1854 1.4783 1.0413 0.2042 0.2589 90.0000 0.0000 0.0000 0.4004 0.0455 0.0400 0.0000 0.0000 0.0000
20 1 2963.0106 0.0537 0.0503 28.3097 4.1791 5.1220 1.0584 0.1241 0.1435 90.0000 0.0000 0.0000 0.3825 0.1119 0.0737 0.0000 0.0000 0.0000
