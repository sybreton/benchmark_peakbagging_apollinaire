# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:21:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9399 0.0910 0.0999 6.2304 1.3745 1.6997 0.8413 0.1803 0.2471 90.0000 0.0000 0.0000 0.4366 0.0365 0.0304 0.0000 0.0000 0.0000
20 1 2963.0794 0.0472 0.0484 38.5359 5.6485 6.7598 0.8780 0.0953 0.1084 90.0000 0.0000 0.0000 0.4057 0.0679 0.0573 0.0000 0.0000 0.0000
