# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:31:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0971 0.1260 0.1109 5.5678 1.0967 1.3733 1.0861 0.2196 0.2783 90.0000 0.0000 0.0000 0.3959 0.0370 0.0367 0.0000 0.0000 0.0000
20 1 2963.0028 0.0563 0.0553 23.3493 3.4974 4.0694 1.1643 0.1396 0.1555 90.0000 0.0000 0.0000 0.3302 0.1521 0.0976 0.0000 0.0000 0.0000
