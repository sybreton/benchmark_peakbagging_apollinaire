# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:08:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0641 0.0756 0.0648 7.9412 1.8767 2.3775 0.7653 0.1811 0.2608 90.0000 0.0000 0.0000 0.4165 0.0241 0.0217 0.0000 0.0000 0.0000
20 1 2963.0663 0.0536 0.0550 31.5909 4.4183 5.2150 0.9929 0.1052 0.1167 90.0000 0.0000 0.0000 0.3906 0.0972 0.0703 0.0000 0.0000 0.0000
