# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:37:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1969 0.1361 0.1270 5.0222 1.1938 1.4805 1.1482 0.2766 0.4171 90.0000 0.0000 0.0000 0.4056 0.0429 0.0399 0.0000 0.0000 0.0000
20 1 2962.9263 0.0574 0.0575 26.8550 3.8489 4.6740 1.0062 0.1157 0.1309 90.0000 0.0000 0.0000 0.4287 0.0931 0.0705 0.0000 0.0000 0.0000
