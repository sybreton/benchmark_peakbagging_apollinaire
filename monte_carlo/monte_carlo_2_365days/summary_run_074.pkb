# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:11:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9673 0.0878 0.0909 7.6925 1.5623 1.9154 0.9434 0.1843 0.2421 90.0000 0.0000 0.0000 0.4140 0.0349 0.0295 0.0000 0.0000 0.0000
20 1 2963.0351 0.0512 0.0523 28.4016 4.1757 5.1829 1.0013 0.1158 0.1246 90.0000 0.0000 0.0000 0.2930 0.1218 0.0851 0.0000 0.0000 0.0000
