# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:55:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0242 0.0944 0.0867 6.6150 1.4256 1.7333 0.9209 0.1943 0.2649 90.0000 0.0000 0.0000 0.4672 0.0332 0.0300 0.0000 0.0000 0.0000
20 1 2963.0418 0.0595 0.0579 27.2695 3.6629 4.3710 1.0630 0.1084 0.1232 90.0000 0.0000 0.0000 0.5360 0.0775 0.0646 0.0000 0.0000 0.0000
