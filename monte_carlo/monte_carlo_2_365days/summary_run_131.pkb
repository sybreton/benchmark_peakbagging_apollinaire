# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:22:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8555 0.1591 0.1110 5.1788 1.2892 1.7853 0.9294 0.2524 0.3347 90.0000 0.0000 0.0000 0.4182 0.0394 0.0338 0.0000 0.0000 0.0000
20 1 2962.8980 0.0561 0.0575 25.8897 3.6549 4.5172 1.1493 0.1299 0.1471 90.0000 0.0000 0.0000 0.3806 0.1231 0.0880 0.0000 0.0000 0.0000
