# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:16:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0030 0.1001 0.1036 6.1238 1.3121 1.5534 0.8860 0.1808 0.2433 90.0000 0.0000 0.0000 0.3702 0.0354 0.0328 0.0000 0.0000 0.0000
20 1 2962.9789 0.0539 0.0532 30.0221 4.1964 5.0866 1.0093 0.1091 0.1207 90.0000 0.0000 0.0000 0.3904 0.0959 0.0722 0.0000 0.0000 0.0000
