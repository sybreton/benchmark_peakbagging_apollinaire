# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:29:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9353 0.0971 0.0922 7.0226 1.2621 1.5408 1.0292 0.1821 0.2351 90.0000 0.0000 0.0000 0.4409 0.0308 0.0277 0.0000 0.0000 0.0000
20 1 2962.9868 0.0587 0.0589 23.8846 3.2212 3.8985 1.2726 0.1379 0.1493 90.0000 0.0000 0.0000 0.3207 0.1677 0.1115 0.0000 0.0000 0.0000
