# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:41:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9255 0.1038 0.1012 5.8085 1.2236 1.5211 0.9571 0.2037 0.2676 90.0000 0.0000 0.0000 0.4438 0.0349 0.0329 0.0000 0.0000 0.0000
20 1 2962.9797 0.0539 0.0541 27.5709 4.0048 4.6663 0.9926 0.1118 0.1258 90.0000 0.0000 0.0000 0.4533 0.0755 0.0658 0.0000 0.0000 0.0000
