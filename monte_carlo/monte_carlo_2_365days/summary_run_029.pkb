# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:18:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8508 0.1151 0.1241 5.2085 1.1487 1.4552 1.2112 0.2784 0.3842 90.0000 0.0000 0.0000 0.3830 0.0458 0.0412 0.0000 0.0000 0.0000
20 1 2962.8383 0.0519 0.0525 32.0611 4.4426 5.3247 1.0153 0.1055 0.1168 90.0000 0.0000 0.0000 0.3354 0.1148 0.0796 0.0000 0.0000 0.0000
