# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:00:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0057 0.0946 0.0851 7.2082 1.6180 2.1185 0.8298 0.1875 0.2503 90.0000 0.0000 0.0000 0.3810 0.0372 0.0313 0.0000 0.0000 0.0000
20 1 2963.0077 0.0539 0.0532 31.7257 4.4261 5.3030 0.8957 0.0945 0.1092 90.0000 0.0000 0.0000 0.4413 0.0730 0.0610 0.0000 0.0000 0.0000
