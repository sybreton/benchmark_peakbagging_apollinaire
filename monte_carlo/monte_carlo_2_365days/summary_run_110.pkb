# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:21:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0134 0.0895 0.0991 6.9717 1.4610 1.8614 0.8021 0.1643 0.2226 90.0000 0.0000 0.0000 0.4445 0.0305 0.0275 0.0000 0.0000 0.0000
20 1 2963.0023 0.0527 0.0535 32.3820 4.4545 5.3748 1.0079 0.1055 0.1202 90.0000 0.0000 0.0000 0.4387 0.0803 0.0653 0.0000 0.0000 0.0000
