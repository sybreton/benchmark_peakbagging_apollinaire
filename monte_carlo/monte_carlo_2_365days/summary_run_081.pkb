# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:29:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9679 0.1249 0.1402 4.7665 0.9679 1.1682 1.2615 0.2586 0.3330 90.0000 0.0000 0.0000 0.3520 0.0443 0.0441 0.0000 0.0000 0.0000
20 1 2963.0502 0.0597 0.0584 24.4895 3.3859 4.0038 1.1543 0.1263 0.1417 90.0000 0.0000 0.0000 0.4645 0.1023 0.0774 0.0000 0.0000 0.0000
