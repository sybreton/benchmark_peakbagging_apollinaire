# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:48:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8970 0.1105 0.1034 6.1740 1.3675 1.7717 1.0745 0.2436 0.3336 90.0000 0.0000 0.0000 0.3858 0.0491 0.0383 0.0000 0.0000 0.0000
20 1 2962.9254 0.0528 0.0540 31.2458 4.3355 5.2563 0.9968 0.1040 0.1173 90.0000 0.0000 0.0000 0.3087 0.1218 0.0832 0.0000 0.0000 0.0000
