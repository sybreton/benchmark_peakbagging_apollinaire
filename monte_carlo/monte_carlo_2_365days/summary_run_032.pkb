# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:25:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.7760 0.1406 0.1414 4.1728 1.0686 1.6186 1.4523 0.4471 0.5954 90.0000 0.0000 0.0000 0.3301 0.1168 0.0768 0.0000 0.0000 0.0000
20 1 2963.0230 0.0528 0.0549 29.1833 4.1654 5.1280 1.0162 0.1116 0.1236 90.0000 0.0000 0.0000 0.3232 0.1202 0.0834 0.0000 0.0000 0.0000
