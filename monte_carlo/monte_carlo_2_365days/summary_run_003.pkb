# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:17:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9730 0.1217 0.1049 5.8930 1.4469 1.8626 1.0528 0.2607 0.3732 90.0000 0.0000 0.0000 0.3973 0.0584 0.0418 0.0000 0.0000 0.0000
20 1 2962.9454 0.0512 0.0504 31.5142 4.6127 5.6326 0.9640 0.1087 0.1250 90.0000 0.0000 0.0000 0.3803 0.0924 0.0682 0.0000 0.0000 0.0000
