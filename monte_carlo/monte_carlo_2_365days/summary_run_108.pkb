# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:38:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9065 0.1423 0.1409 4.9551 1.1072 1.3473 1.4422 0.3236 0.4683 90.0000 0.0000 0.0000 0.3618 0.0838 0.0583 0.0000 0.0000 0.0000
20 1 2963.0448 0.0585 0.0574 25.0325 3.3212 4.0305 1.2799 0.1302 0.1339 90.0000 0.0000 0.0000 0.1610 0.1089 0.1300 0.0000 0.0000 0.0000
