# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:11:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0044 0.1453 0.1135 5.7210 1.3117 1.5289 0.9444 0.1993 0.2972 90.0000 0.0000 0.0000 0.4526 0.0564 0.0408 0.0000 0.0000 0.0000
20 1 2962.9088 0.0502 0.0500 33.7182 4.8722 5.9432 0.8919 0.1014 0.1134 90.0000 0.0000 0.0000 0.3968 0.0767 0.0600 0.0000 0.0000 0.0000
