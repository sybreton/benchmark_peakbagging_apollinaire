# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:43:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1880 0.0788 0.0667 7.5020 1.8157 2.2909 0.8187 0.1964 0.2951 90.0000 0.0000 0.0000 0.4727 0.0299 0.0246 0.0000 0.0000 0.0000
20 1 2963.0338 0.0586 0.0592 27.4952 3.7550 4.7036 1.1240 0.1193 0.1319 90.0000 0.0000 0.0000 0.3555 0.1307 0.0899 0.0000 0.0000 0.0000
