# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:57:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9958 0.1172 0.1210 5.4831 1.2355 1.4974 1.1217 0.2494 0.3651 90.0000 0.0000 0.0000 0.4082 0.0478 0.0367 0.0000 0.0000 0.0000
20 1 2962.9556 0.0453 0.0463 39.1812 5.6129 6.5740 0.8607 0.0914 0.1043 90.0000 0.0000 0.0000 0.4442 0.0625 0.0535 0.0000 0.0000 0.0000
