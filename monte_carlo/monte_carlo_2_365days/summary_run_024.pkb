# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:07:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9302 0.1375 0.1276 5.0741 1.1154 1.3684 1.0696 0.2335 0.3270 90.0000 0.0000 0.0000 0.4067 0.0470 0.0425 0.0000 0.0000 0.0000
20 1 2962.9952 0.0523 0.0543 27.1766 3.8567 4.6439 0.9846 0.1091 0.1236 90.0000 0.0000 0.0000 0.4927 0.0698 0.0587 0.0000 0.0000 0.0000
