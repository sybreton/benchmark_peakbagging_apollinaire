# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:00:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0427 0.1232 0.1129 6.3837 1.2607 1.6172 0.9086 0.1780 0.2280 90.0000 0.0000 0.0000 0.4153 0.0419 0.0387 0.0000 0.0000 0.0000
20 1 2962.9329 0.0535 0.0554 27.1289 3.7954 4.6362 1.0707 0.1175 0.1311 90.0000 0.0000 0.0000 0.3506 0.1200 0.0840 0.0000 0.0000 0.0000
