# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:04:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1770 0.1234 0.1342 4.9042 1.1026 1.4115 1.1626 0.2678 0.3738 90.0000 0.0000 0.0000 0.3911 0.0532 0.0423 0.0000 0.0000 0.0000
20 1 2962.9692 0.0519 0.0533 31.5994 4.3895 5.3729 1.0002 0.1078 0.1221 90.0000 0.0000 0.0000 0.4020 0.0909 0.0693 0.0000 0.0000 0.0000
