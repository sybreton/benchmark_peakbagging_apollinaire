# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:31:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9771 0.0868 0.0875 6.3016 1.3808 1.7437 0.7925 0.1780 0.2395 90.0000 0.0000 0.0000 0.3824 0.0254 0.0271 0.0000 0.0000 0.0000
20 1 2962.9706 0.0560 0.0547 25.5180 3.7605 4.5509 1.0574 0.1236 0.1456 90.0000 0.0000 0.0000 0.4075 0.1016 0.0717 0.0000 0.0000 0.0000
