# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:02:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9785 0.1402 0.1341 4.6654 0.9242 1.1645 1.7147 0.3686 0.4811 90.0000 0.0000 0.0000 0.3395 0.0777 0.0617 0.0000 0.0000 0.0000
20 1 2962.9454 0.0482 0.0468 37.3645 5.2572 6.5007 0.8791 0.0951 0.1049 90.0000 0.0000 0.0000 0.4171 0.0698 0.0559 0.0000 0.0000 0.0000
