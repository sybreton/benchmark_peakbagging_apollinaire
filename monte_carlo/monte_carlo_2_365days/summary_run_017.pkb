# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:51:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9894 0.1022 0.0969 7.3010 1.3478 1.6430 0.9847 0.1758 0.2258 90.0000 0.0000 0.0000 0.3821 0.0328 0.0309 0.0000 0.0000 0.0000
20 1 2962.9980 0.0535 0.0540 26.6280 3.6866 4.4307 1.1196 0.1151 0.1259 90.0000 0.0000 0.0000 0.2307 0.1364 0.1094 0.0000 0.0000 0.0000
