# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:29:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0102 0.0934 0.0981 5.9063 1.3597 1.6828 0.9621 0.2129 0.3127 90.0000 0.0000 0.0000 0.4463 0.0381 0.0330 0.0000 0.0000 0.0000
20 1 2962.9319 0.0592 0.0598 29.2026 4.1644 5.0126 1.0871 0.1193 0.1355 90.0000 0.0000 0.0000 0.3648 0.1268 0.0881 0.0000 0.0000 0.0000
