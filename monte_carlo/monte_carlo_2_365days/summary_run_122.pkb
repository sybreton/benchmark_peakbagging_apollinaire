# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:55:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9426 0.0927 0.0935 6.1669 1.2091 1.5043 0.9385 0.1831 0.2395 90.0000 0.0000 0.0000 0.4332 0.0297 0.0288 0.0000 0.0000 0.0000
20 1 2963.1104 0.0586 0.0576 26.1930 3.6118 4.4839 1.1609 0.1271 0.1380 90.0000 0.0000 0.0000 0.3327 0.1460 0.0981 0.0000 0.0000 0.0000
