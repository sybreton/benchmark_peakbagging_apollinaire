# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:22:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1308 0.1709 0.1563 3.0990 0.7784 1.2392 1.7303 0.5623 0.7160 90.0000 0.0000 0.0000 0.3208 0.1114 0.0712 0.0000 0.0000 0.0000
20 1 2963.0688 0.0573 0.0559 27.8277 3.9109 4.6188 1.1481 0.1261 0.1477 90.0000 0.0000 0.0000 0.4163 0.1182 0.0831 0.0000 0.0000 0.0000
