# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:51:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9948 0.0827 0.0940 6.8483 1.7667 2.2680 0.8752 0.2273 0.3416 90.0000 0.0000 0.0000 0.3560 0.0419 0.0292 0.0000 0.0000 0.0000
20 1 2962.8900 0.0471 0.0472 37.8281 5.4168 6.7908 0.8225 0.0915 0.0979 90.0000 0.0000 0.0000 0.4417 0.0580 0.0508 0.0000 0.0000 0.0000
