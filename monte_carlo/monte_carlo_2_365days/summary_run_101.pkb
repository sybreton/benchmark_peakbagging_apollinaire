# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:20:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8490 0.1526 0.1560 3.3828 0.7670 1.1237 1.7296 0.4736 0.5841 90.0000 0.0000 0.0000 0.2968 0.1154 0.0769 0.0000 0.0000 0.0000
20 1 2963.0658 0.0506 0.0496 34.4545 4.9546 6.0806 0.9025 0.0982 0.1114 90.0000 0.0000 0.0000 0.3852 0.0843 0.0654 0.0000 0.0000 0.0000
