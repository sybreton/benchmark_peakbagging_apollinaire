# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:37:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1507 0.1265 0.1194 5.1991 1.1149 1.3695 1.0554 0.2245 0.3032 90.0000 0.0000 0.0000 0.3598 0.0428 0.0379 0.0000 0.0000 0.0000
20 1 2963.0054 0.0560 0.0573 30.3230 4.2721 5.1757 0.9802 0.1076 0.1209 90.0000 0.0000 0.0000 0.3909 0.0987 0.0734 0.0000 0.0000 0.0000
