# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:11:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9773 0.1038 0.1102 6.2696 1.3715 1.7580 0.8785 0.1924 0.2714 90.0000 0.0000 0.0000 0.4025 0.0352 0.0320 0.0000 0.0000 0.0000
20 1 2963.0137 0.0583 0.0573 27.7973 3.6162 4.6129 1.1748 0.1236 0.1320 90.0000 0.0000 0.0000 0.2957 0.1476 0.1050 0.0000 0.0000 0.0000
