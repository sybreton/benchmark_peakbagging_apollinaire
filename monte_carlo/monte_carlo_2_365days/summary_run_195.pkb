# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:49:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9454 0.0896 0.0747 7.7747 1.6494 2.0544 0.7951 0.1652 0.2175 90.0000 0.0000 0.0000 0.3824 0.0263 0.0254 0.0000 0.0000 0.0000
20 1 2963.0086 0.0515 0.0517 29.9599 4.1445 5.1453 1.0582 0.1135 0.1194 90.0000 0.0000 0.0000 0.2707 0.1374 0.0956 0.0000 0.0000 0.0000
