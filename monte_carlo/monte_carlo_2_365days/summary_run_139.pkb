# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:48:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0591 0.1426 0.1389 3.9834 0.7478 1.1550 1.7693 0.4329 0.4176 90.0000 0.0000 0.0000 0.2473 0.1436 0.1097 0.0000 0.0000 0.0000
20 1 2963.0179 0.0506 0.0478 34.2306 4.7663 5.9693 0.8969 0.0974 0.1075 90.0000 0.0000 0.0000 0.4473 0.0701 0.0564 0.0000 0.0000 0.0000
