# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:02:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0125 0.1030 0.1177 5.3963 1.3695 1.8469 1.1060 0.3013 0.4154 90.0000 0.0000 0.0000 0.3782 0.0571 0.0426 0.0000 0.0000 0.0000
20 1 2962.9821 0.0515 0.0549 29.1164 3.9805 4.7283 1.0397 0.1105 0.1246 90.0000 0.0000 0.0000 0.4763 0.0781 0.0649 0.0000 0.0000 0.0000
