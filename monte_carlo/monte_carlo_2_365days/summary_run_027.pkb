# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:13:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0472 0.1139 0.1204 5.3625 1.1025 1.3256 1.0377 0.2132 0.2822 90.0000 0.0000 0.0000 0.3910 0.0374 0.0365 0.0000 0.0000 0.0000
20 1 2962.9238 0.0542 0.0532 27.7821 4.0000 4.8142 0.9931 0.1123 0.1239 90.0000 0.0000 0.0000 0.3466 0.1077 0.0757 0.0000 0.0000 0.0000
