# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:50:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9204 0.0732 0.0731 7.6356 1.4503 1.8204 0.6929 0.1235 0.1584 90.0000 0.0000 0.0000 0.4660 0.0243 0.0224 0.0000 0.0000 0.0000
20 1 2962.9349 0.0506 0.0515 34.8373 4.9803 6.0115 0.9529 0.1003 0.1128 90.0000 0.0000 0.0000 0.3278 0.1015 0.0768 0.0000 0.0000 0.0000
