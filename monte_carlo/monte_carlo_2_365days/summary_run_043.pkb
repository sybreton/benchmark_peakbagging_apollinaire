# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:53:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9407 0.0920 0.0885 8.0341 1.5673 1.8913 0.8244 0.1518 0.1955 90.0000 0.0000 0.0000 0.4082 0.0304 0.0294 0.0000 0.0000 0.0000
20 1 2962.9653 0.0537 0.0537 29.3204 4.0056 5.0083 0.9767 0.1079 0.1178 90.0000 0.0000 0.0000 0.4626 0.0748 0.0638 0.0000 0.0000 0.0000
