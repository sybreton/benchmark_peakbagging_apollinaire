# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:20:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9463 0.0911 0.0950 7.3478 1.5512 1.9286 0.9282 0.1929 0.2587 90.0000 0.0000 0.0000 0.4226 0.0400 0.0319 0.0000 0.0000 0.0000
20 1 2963.0530 0.0532 0.0526 31.8388 4.4709 5.4333 1.0361 0.1146 0.1238 90.0000 0.0000 0.0000 0.3115 0.1298 0.0882 0.0000 0.0000 0.0000
