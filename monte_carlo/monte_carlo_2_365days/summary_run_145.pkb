# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:08:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8572 0.1046 0.1044 5.9440 1.1684 1.4153 0.9745 0.1893 0.2496 90.0000 0.0000 0.0000 0.4365 0.0327 0.0317 0.0000 0.0000 0.0000
20 1 2962.9529 0.0497 0.0488 35.1936 4.8711 6.0024 0.9012 0.0958 0.1021 90.0000 0.0000 0.0000 0.4932 0.0618 0.0547 0.0000 0.0000 0.0000
