# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 14:03:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9895 0.1100 0.0987 6.2022 1.7980 2.3774 1.0187 0.3090 0.4858 90.0000 0.0000 0.0000 0.3996 0.0629 0.0401 0.0000 0.0000 0.0000
20 1 2962.9448 0.0549 0.0535 27.7665 3.8655 4.8376 1.0134 0.1080 0.1225 90.0000 0.0000 0.0000 0.4484 0.0856 0.0677 0.0000 0.0000 0.0000
