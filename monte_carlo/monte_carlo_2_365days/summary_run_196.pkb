# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:53:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1428 0.1135 0.0931 7.4643 1.6735 2.0656 0.8858 0.1986 0.2692 90.0000 0.0000 0.0000 0.4813 0.0429 0.0333 0.0000 0.0000 0.0000
20 1 2962.9050 0.0558 0.0546 31.9251 4.5843 5.4716 0.9631 0.1077 0.1239 90.0000 0.0000 0.0000 0.4459 0.0810 0.0659 0.0000 0.0000 0.0000
