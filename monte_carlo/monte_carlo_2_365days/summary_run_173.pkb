# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:45:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0657 0.0867 0.0912 7.0895 1.4139 1.8039 0.7797 0.1542 0.1982 90.0000 0.0000 0.0000 0.3987 0.0269 0.0265 0.0000 0.0000 0.0000
20 1 2963.0242 0.0545 0.0558 28.2397 3.9186 4.6606 1.1396 0.1217 0.1348 90.0000 0.0000 0.0000 0.3150 0.1445 0.0984 0.0000 0.0000 0.0000
