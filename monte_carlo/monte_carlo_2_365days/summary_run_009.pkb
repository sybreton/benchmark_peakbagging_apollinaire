# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:31:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0340 0.1090 0.1095 5.5811 1.1585 1.3525 1.0239 0.2022 0.2813 90.0000 0.0000 0.0000 0.4521 0.0368 0.0350 0.0000 0.0000 0.0000
20 1 2962.9642 0.0483 0.0479 34.5784 4.8579 5.9420 0.8680 0.0944 0.1042 90.0000 0.0000 0.0000 0.4577 0.0626 0.0535 0.0000 0.0000 0.0000
