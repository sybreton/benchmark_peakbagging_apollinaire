# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:07:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9530 0.0952 0.0940 5.9949 1.4270 1.8108 1.0128 0.2451 0.3682 90.0000 0.0000 0.0000 0.3111 0.0420 0.0328 0.0000 0.0000 0.0000
20 1 2962.9601 0.0557 0.0543 29.6741 4.0810 4.9780 1.1711 0.1290 0.1392 90.0000 0.0000 0.0000 0.3486 0.1415 0.0922 0.0000 0.0000 0.0000
