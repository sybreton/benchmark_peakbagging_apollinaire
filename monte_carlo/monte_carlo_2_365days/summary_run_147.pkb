# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:14:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1433 0.1109 0.1382 6.7393 1.3559 1.8924 0.8029 0.1762 0.2090 90.0000 0.0000 0.0000 0.3802 0.0379 0.0340 0.0000 0.0000 0.0000
20 1 2963.0384 0.0566 0.0601 24.1821 3.2967 4.0393 1.0846 0.1179 0.1372 90.0000 0.0000 0.0000 0.5073 0.0838 0.0662 0.0000 0.0000 0.0000
