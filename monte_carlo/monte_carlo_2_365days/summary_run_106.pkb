# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:33:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1693 0.1221 0.1074 5.9822 1.3226 1.7109 1.0501 0.2434 0.3235 90.0000 0.0000 0.0000 0.4497 0.0404 0.0360 0.0000 0.0000 0.0000
20 1 2962.9919 0.0580 0.0568 25.1313 3.6368 4.4905 1.1476 0.1340 0.1478 90.0000 0.0000 0.0000 0.3615 0.1314 0.0909 0.0000 0.0000 0.0000
