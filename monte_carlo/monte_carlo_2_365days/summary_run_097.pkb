# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:10:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0453 0.1138 0.1174 6.6189 1.4569 1.9461 0.9937 0.2350 0.2945 90.0000 0.0000 0.0000 0.3515 0.0441 0.0440 0.0000 0.0000 0.0000
20 1 2963.0526 0.0536 0.0531 27.6953 4.0436 4.7152 1.0609 0.1182 0.1337 90.0000 0.0000 0.0000 0.3490 0.1189 0.0819 0.0000 0.0000 0.0000
