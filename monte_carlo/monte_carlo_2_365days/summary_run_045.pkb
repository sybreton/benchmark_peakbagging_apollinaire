# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:58:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0508 0.1577 0.1572 3.3270 0.6293 1.0055 2.3789 0.6424 0.6634 90.0000 0.0000 0.0000 0.2813 0.1574 0.1011 0.0000 0.0000 0.0000
20 1 2962.9831 0.0575 0.0550 28.9814 3.9869 4.7548 1.0969 0.1167 0.1336 90.0000 0.0000 0.0000 0.3865 0.1157 0.0831 0.0000 0.0000 0.0000
