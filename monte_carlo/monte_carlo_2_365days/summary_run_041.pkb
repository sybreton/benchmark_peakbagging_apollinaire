# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:48:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9868 0.1073 0.0986 6.4015 1.2608 1.5207 1.0180 0.1955 0.2641 90.0000 0.0000 0.0000 0.4062 0.0341 0.0322 0.0000 0.0000 0.0000
20 1 2963.0207 0.0545 0.0526 28.3634 4.0491 4.8393 1.0421 0.1127 0.1287 90.0000 0.0000 0.0000 0.3078 0.1288 0.0886 0.0000 0.0000 0.0000
