# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:23:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9474 0.1272 0.1256 4.4946 1.0255 1.3138 1.3023 0.3173 0.4403 90.0000 0.0000 0.0000 0.3315 0.0598 0.0469 0.0000 0.0000 0.0000
20 1 2962.9771 0.0518 0.0532 30.0262 4.3696 5.0758 0.9967 0.1071 0.1284 90.0000 0.0000 0.0000 0.4130 0.0855 0.0675 0.0000 0.0000 0.0000
