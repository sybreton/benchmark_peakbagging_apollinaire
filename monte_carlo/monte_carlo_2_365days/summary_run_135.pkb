# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:35:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0797 0.1380 0.1275 4.1167 1.0415 1.3482 1.4634 0.3901 0.5851 90.0000 0.0000 0.0000 0.3342 0.0886 0.0539 0.0000 0.0000 0.0000
20 1 2962.9660 0.0531 0.0532 26.1775 3.9187 4.6674 1.0064 0.1187 0.1379 90.0000 0.0000 0.0000 0.3788 0.1051 0.0758 0.0000 0.0000 0.0000
