# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:40:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8589 0.0873 0.1000 7.5761 1.5888 2.0424 0.8273 0.1731 0.2267 90.0000 0.0000 0.0000 0.3601 0.0349 0.0283 0.0000 0.0000 0.0000
20 1 2962.9635 0.0540 0.0544 26.1927 3.7603 4.5486 1.1034 0.1278 0.1418 90.0000 0.0000 0.0000 0.3693 0.1219 0.0836 0.0000 0.0000 0.0000
