# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:10:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0713 0.0945 0.0954 6.7263 1.3744 1.7341 0.9362 0.1929 0.2455 90.0000 0.0000 0.0000 0.3940 0.0390 0.0339 0.0000 0.0000 0.0000
20 1 2962.9805 0.0561 0.0554 29.8191 4.1668 5.0579 0.9909 0.1069 0.1189 90.0000 0.0000 0.0000 0.4153 0.0885 0.0709 0.0000 0.0000 0.0000
