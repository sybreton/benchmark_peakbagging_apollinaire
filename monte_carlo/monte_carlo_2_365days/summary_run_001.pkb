# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:12:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0434 0.1482 0.1501 3.5840 0.8868 1.2433 1.5969 0.4623 0.6204 90.0000 0.0000 0.0000 0.3521 0.0863 0.0626 0.0000 0.0000 0.0000
20 1 2963.0565 0.0571 0.0574 25.9690 3.8110 4.7111 1.0988 0.1339 0.1428 90.0000 0.0000 0.0000 0.3204 0.1453 0.0937 0.0000 0.0000 0.0000
