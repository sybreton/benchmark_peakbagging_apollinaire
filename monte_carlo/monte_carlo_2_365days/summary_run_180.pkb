# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:06:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.7906 0.1519 0.1329 5.0187 0.9624 1.1401 1.3834 0.2607 0.3386 90.0000 0.0000 0.0000 0.4591 0.0505 0.0488 0.0000 0.0000 0.0000
20 1 2963.0317 0.0538 0.0535 28.1698 4.0588 4.8312 1.0415 0.1101 0.1243 90.0000 0.0000 0.0000 0.3017 0.1291 0.0918 0.0000 0.0000 0.0000
