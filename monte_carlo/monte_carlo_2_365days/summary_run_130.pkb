# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:19:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0209 0.1019 0.1187 4.9188 1.1880 1.6596 1.1685 0.3156 0.4185 90.0000 0.0000 0.0000 0.4240 0.0423 0.0366 0.0000 0.0000 0.0000
20 1 2962.9798 0.0506 0.0504 30.5402 4.2743 5.0494 1.0335 0.1125 0.1310 90.0000 0.0000 0.0000 0.4890 0.0769 0.0630 0.0000 0.0000 0.0000
