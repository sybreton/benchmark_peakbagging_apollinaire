# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:16:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0687 0.0982 0.0881 7.6048 1.5873 1.9927 0.8571 0.1762 0.2253 90.0000 0.0000 0.0000 0.3570 0.0339 0.0294 0.0000 0.0000 0.0000
20 1 2963.0113 0.0584 0.0564 27.8052 3.9122 4.8476 1.1058 0.1212 0.1268 90.0000 0.0000 0.0000 0.2389 0.1434 0.1124 0.0000 0.0000 0.0000
