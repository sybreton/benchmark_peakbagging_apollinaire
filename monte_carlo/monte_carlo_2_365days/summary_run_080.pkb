# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:27:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9438 0.1375 0.1413 4.0604 1.0872 1.4812 1.3860 0.4071 0.5966 90.0000 0.0000 0.0000 0.3546 0.0960 0.0591 0.0000 0.0000 0.0000
20 1 2963.0752 0.0509 0.0510 33.7556 4.8675 5.8968 0.9724 0.1088 0.1165 90.0000 0.0000 0.0000 0.3317 0.1138 0.0778 0.0000 0.0000 0.0000
