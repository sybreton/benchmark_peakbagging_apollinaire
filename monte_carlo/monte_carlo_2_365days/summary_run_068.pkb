# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:56:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9216 0.1121 0.1087 5.4832 1.2166 1.4898 1.1196 0.2509 0.3576 90.0000 0.0000 0.0000 0.4031 0.0407 0.0370 0.0000 0.0000 0.0000
20 1 2963.0153 0.0483 0.0492 40.9435 5.9247 7.3757 0.8908 0.0935 0.1060 90.0000 0.0000 0.0000 0.3804 0.0746 0.0608 0.0000 0.0000 0.0000
