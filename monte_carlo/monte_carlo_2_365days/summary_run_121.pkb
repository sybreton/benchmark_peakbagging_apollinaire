# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:52:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9432 0.0562 0.0639 9.0164 1.8368 2.3046 0.7246 0.1448 0.1939 90.0000 0.0000 0.0000 0.4555 0.0207 0.0204 0.0000 0.0000 0.0000
20 1 2963.0262 0.0579 0.0560 29.8903 4.4897 5.4354 0.9830 0.1182 0.1333 90.0000 0.0000 0.0000 0.3511 0.1238 0.0824 0.0000 0.0000 0.0000
