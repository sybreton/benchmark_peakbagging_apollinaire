# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:29:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9436 0.1475 0.1474 4.5516 0.9776 1.2634 1.2976 0.2874 0.3795 90.0000 0.0000 0.0000 0.3516 0.0613 0.0527 0.0000 0.0000 0.0000
20 1 2963.0586 0.0505 0.0521 32.8829 4.5551 5.7339 1.0372 0.1157 0.1273 90.0000 0.0000 0.0000 0.3048 0.1282 0.0868 0.0000 0.0000 0.0000
