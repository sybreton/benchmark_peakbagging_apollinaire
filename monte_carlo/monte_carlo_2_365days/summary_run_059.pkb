# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:33:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8468 0.1335 0.1355 5.3192 1.0356 1.3032 1.1674 0.2358 0.2934 90.0000 0.0000 0.0000 0.3620 0.0499 0.0472 0.0000 0.0000 0.0000
20 1 2963.0991 0.0495 0.0535 32.9266 4.6407 5.7368 0.9962 0.1086 0.1188 90.0000 0.0000 0.0000 0.3664 0.1035 0.0713 0.0000 0.0000 0.0000
