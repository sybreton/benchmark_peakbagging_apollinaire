# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:34:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9065 0.0985 0.0900 6.3816 1.3469 1.6625 0.9652 0.2017 0.2807 90.0000 0.0000 0.0000 0.4412 0.0368 0.0319 0.0000 0.0000 0.0000
20 1 2963.0794 0.0525 0.0516 34.5090 4.7869 6.0250 0.9961 0.1075 0.1197 90.0000 0.0000 0.0000 0.4123 0.0864 0.0680 0.0000 0.0000 0.0000
