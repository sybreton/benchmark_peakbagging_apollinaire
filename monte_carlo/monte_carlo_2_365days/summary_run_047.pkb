# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:03:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8855 0.0649 0.0655 9.0419 1.9544 2.2639 0.6727 0.1319 0.1898 90.0000 0.0000 0.0000 0.4307 0.0255 0.0227 0.0000 0.0000 0.0000
20 1 2963.0654 0.0531 0.0542 28.1983 3.8178 4.7508 1.0337 0.1133 0.1242 90.0000 0.0000 0.0000 0.4672 0.0822 0.0644 0.0000 0.0000 0.0000
