# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:28:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9759 0.1129 0.1115 5.8316 1.2357 1.5129 0.9797 0.2034 0.2782 90.0000 0.0000 0.0000 0.3801 0.0341 0.0331 0.0000 0.0000 0.0000
20 1 2962.9992 0.0534 0.0523 32.9552 4.5626 5.6453 0.9382 0.1037 0.1113 90.0000 0.0000 0.0000 0.4199 0.0787 0.0619 0.0000 0.0000 0.0000
