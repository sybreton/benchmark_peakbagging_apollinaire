# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:32:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1554 0.1419 0.1437 4.7607 1.0312 1.2718 1.2053 0.2592 0.3519 90.0000 0.0000 0.0000 0.3793 0.0590 0.0502 0.0000 0.0000 0.0000
20 1 2962.9109 0.0495 0.0489 36.8776 5.0669 6.0617 0.8650 0.0886 0.0998 90.0000 0.0000 0.0000 0.4522 0.0634 0.0553 0.0000 0.0000 0.0000
