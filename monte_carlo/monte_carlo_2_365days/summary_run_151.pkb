# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:28:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0269 0.1158 0.1243 5.6294 1.1780 1.5279 1.0757 0.2310 0.2962 90.0000 0.0000 0.0000 0.3288 0.0512 0.0423 0.0000 0.0000 0.0000
20 1 2962.9598 0.0494 0.0486 33.6778 4.8506 5.6969 0.9360 0.1023 0.1151 90.0000 0.0000 0.0000 0.4500 0.0686 0.0599 0.0000 0.0000 0.0000
