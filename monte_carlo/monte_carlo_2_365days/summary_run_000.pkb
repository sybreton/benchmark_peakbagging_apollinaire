# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:10:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8573 0.0788 0.1015 7.7641 1.6297 2.0648 0.7647 0.1556 0.2055 90.0000 0.0000 0.0000 0.4515 0.0301 0.0256 0.0000 0.0000 0.0000
20 1 2963.0930 0.0507 0.0512 33.3954 4.4375 5.2946 0.9431 0.0955 0.1058 90.0000 0.0000 0.0000 0.5733 0.0622 0.0543 0.0000 0.0000 0.0000
