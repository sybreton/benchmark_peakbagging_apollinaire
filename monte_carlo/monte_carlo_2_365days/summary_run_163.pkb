# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 12:11:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0880 0.1101 0.1189 5.7958 1.2867 1.5415 1.0207 0.2186 0.3068 90.0000 0.0000 0.0000 0.4399 0.0434 0.0363 0.0000 0.0000 0.0000
20 1 2963.0252 0.0519 0.0530 31.0512 4.1781 5.3201 1.0722 0.1123 0.1195 90.0000 0.0000 0.0000 0.2498 0.1429 0.1005 0.0000 0.0000 0.0000
