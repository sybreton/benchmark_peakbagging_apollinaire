# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:00:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0910 0.0993 0.0866 7.4689 1.4572 1.8632 0.9782 0.1996 0.2443 90.0000 0.0000 0.0000 0.4132 0.0286 0.0269 0.0000 0.0000 0.0000
20 1 2963.0154 0.0462 0.0470 33.3654 4.7834 6.0105 0.8961 0.1030 0.1135 90.0000 0.0000 0.0000 0.4293 0.0665 0.0560 0.0000 0.0000 0.0000
