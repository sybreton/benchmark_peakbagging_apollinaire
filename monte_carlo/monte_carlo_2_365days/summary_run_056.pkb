# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:25:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9374 0.1201 0.1108 5.4044 1.2226 1.4737 1.0408 0.2309 0.3342 90.0000 0.0000 0.0000 0.4074 0.0381 0.0328 0.0000 0.0000 0.0000
20 1 2963.0894 0.0515 0.0529 34.9006 5.1278 6.1195 0.9611 0.1057 0.1203 90.0000 0.0000 0.0000 0.3154 0.1188 0.0781 0.0000 0.0000 0.0000
