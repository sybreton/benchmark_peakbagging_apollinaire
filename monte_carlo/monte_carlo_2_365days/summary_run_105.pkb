# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:30:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9757 0.1355 0.1404 5.6992 1.0666 1.3052 1.1193 0.2026 0.2595 90.0000 0.0000 0.0000 0.3966 0.0415 0.0399 0.0000 0.0000 0.0000
20 1 2962.9875 0.0537 0.0530 32.7841 4.7107 5.8677 0.9480 0.1053 0.1169 90.0000 0.0000 0.0000 0.3471 0.1045 0.0738 0.0000 0.0000 0.0000
