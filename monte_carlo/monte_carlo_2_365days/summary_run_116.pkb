# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:38:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9427 0.0799 0.0753 8.2382 1.6571 2.0823 0.7400 0.1430 0.1819 90.0000 0.0000 0.0000 0.3882 0.0303 0.0269 0.0000 0.0000 0.0000
20 1 2963.0634 0.0542 0.0547 27.7824 3.9744 4.7462 1.0730 0.1215 0.1412 90.0000 0.0000 0.0000 0.4221 0.0955 0.0729 0.0000 0.0000 0.0000
