# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:38:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0617 0.1338 0.1356 4.5691 1.0438 1.3215 1.2183 0.2796 0.3882 90.0000 0.0000 0.0000 0.3747 0.0659 0.0524 0.0000 0.0000 0.0000
20 1 2963.0134 0.0537 0.0524 27.2832 3.8909 4.8706 1.0644 0.1202 0.1340 90.0000 0.0000 0.0000 0.3149 0.1363 0.0909 0.0000 0.0000 0.0000
