# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:53:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8951 0.1348 0.1224 4.4846 1.1194 1.4844 1.4294 0.3890 0.5363 90.0000 0.0000 0.0000 0.3861 0.0794 0.0494 0.0000 0.0000 0.0000
20 1 2962.9787 0.0508 0.0505 31.9984 4.6512 5.7533 0.9972 0.1138 0.1236 90.0000 0.0000 0.0000 0.3232 0.1183 0.0789 0.0000 0.0000 0.0000
