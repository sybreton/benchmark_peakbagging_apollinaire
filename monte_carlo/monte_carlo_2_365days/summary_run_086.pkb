# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:42:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1155 0.1187 0.1340 5.7274 1.3515 1.7192 0.9419 0.2201 0.3059 90.0000 0.0000 0.0000 0.3086 0.0609 0.0497 0.0000 0.0000 0.0000
20 1 2962.9625 0.0543 0.0538 27.2064 3.8151 4.5355 1.1561 0.1209 0.1278 90.0000 0.0000 0.0000 0.1736 0.1159 0.1166 0.0000 0.0000 0.0000
