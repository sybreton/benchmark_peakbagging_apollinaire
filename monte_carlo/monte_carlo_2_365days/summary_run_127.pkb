# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:10:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0499 0.0941 0.1133 6.6942 1.3922 1.7420 0.9066 0.1900 0.2483 90.0000 0.0000 0.0000 0.3963 0.0404 0.0336 0.0000 0.0000 0.0000
20 1 2963.0911 0.0541 0.0550 25.7266 3.6160 4.4994 1.1290 0.1231 0.1297 90.0000 0.0000 0.0000 0.2172 0.1335 0.1108 0.0000 0.0000 0.0000
