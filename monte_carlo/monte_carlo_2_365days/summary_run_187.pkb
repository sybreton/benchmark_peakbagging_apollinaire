# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:26:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9510 0.1183 0.1223 6.1490 1.2691 1.5603 1.0250 0.2059 0.2709 90.0000 0.0000 0.0000 0.3185 0.0476 0.0421 0.0000 0.0000 0.0000
20 1 2962.9679 0.0562 0.0555 32.2442 4.3316 5.2182 1.0120 0.1040 0.1142 90.0000 0.0000 0.0000 0.4241 0.0871 0.0701 0.0000 0.0000 0.0000
