# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:30:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0483 0.1095 0.0904 5.7930 1.4204 1.8627 0.9591 0.2394 0.3330 90.0000 0.0000 0.0000 0.3847 0.0386 0.0334 0.0000 0.0000 0.0000
20 1 2963.0672 0.0523 0.0526 32.4363 4.5895 5.5928 0.9379 0.1017 0.1114 90.0000 0.0000 0.0000 0.3778 0.0899 0.0663 0.0000 0.0000 0.0000
