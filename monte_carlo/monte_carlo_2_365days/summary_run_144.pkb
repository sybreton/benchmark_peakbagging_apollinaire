# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:04:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9354 0.1040 0.1029 6.0635 1.3011 1.5376 0.9901 0.2066 0.2950 90.0000 0.0000 0.0000 0.3779 0.0352 0.0321 0.0000 0.0000 0.0000
20 1 2962.9114 0.0549 0.0533 29.0569 4.0995 4.9825 1.1069 0.1264 0.1350 90.0000 0.0000 0.0000 0.2988 0.1415 0.0940 0.0000 0.0000 0.0000
