# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:24:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0181 0.1177 0.1101 5.8551 1.1821 1.4073 1.1869 0.2346 0.3176 90.0000 0.0000 0.0000 0.3761 0.0421 0.0376 0.0000 0.0000 0.0000
20 1 2962.9764 0.0530 0.0539 28.7263 3.8727 4.8648 1.0534 0.1076 0.1160 90.0000 0.0000 0.0000 0.2366 0.1368 0.1059 0.0000 0.0000 0.0000
