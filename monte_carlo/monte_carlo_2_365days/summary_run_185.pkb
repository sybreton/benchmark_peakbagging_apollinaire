# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:20:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0387 0.0884 0.0858 7.0914 1.3354 1.6297 0.8724 0.1609 0.2114 90.0000 0.0000 0.0000 0.4159 0.0280 0.0275 0.0000 0.0000 0.0000
20 1 2963.0673 0.0613 0.0601 20.0368 2.6854 3.4714 1.3255 0.1488 0.1501 90.0000 0.0000 0.0000 0.1759 0.1172 0.1342 0.0000 0.0000 0.0000
