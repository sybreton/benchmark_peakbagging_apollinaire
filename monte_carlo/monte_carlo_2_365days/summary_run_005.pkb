# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:21:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1042 0.1114 0.1094 6.0722 1.2194 1.4853 0.9918 0.1935 0.2511 90.0000 0.0000 0.0000 0.3873 0.0394 0.0365 0.0000 0.0000 0.0000
20 1 2962.9869 0.0547 0.0570 24.8883 3.4267 4.1516 1.1312 0.1173 0.1270 90.0000 0.0000 0.0000 0.2208 0.1358 0.1163 0.0000 0.0000 0.0000
