# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:55:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1086 0.1287 0.1202 4.4902 1.0812 1.3522 1.2521 0.3099 0.4449 90.0000 0.0000 0.0000 0.3547 0.0531 0.0446 0.0000 0.0000 0.0000
20 1 2963.0867 0.0544 0.0565 28.7428 3.8735 4.8378 1.1147 0.1126 0.1221 90.0000 0.0000 0.0000 0.2640 0.1435 0.1045 0.0000 0.0000 0.0000
