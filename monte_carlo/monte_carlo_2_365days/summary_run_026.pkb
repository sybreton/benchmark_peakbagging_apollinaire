# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:11:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0321 0.0833 0.0807 8.3849 1.5375 1.9159 0.8168 0.1409 0.1843 90.0000 0.0000 0.0000 0.4163 0.0253 0.0265 0.0000 0.0000 0.0000
20 1 2962.9554 0.0604 0.0602 19.3813 2.7468 3.5498 1.3008 0.1548 0.1606 90.0000 0.0000 0.0000 0.2600 0.1613 0.1258 0.0000 0.0000 0.0000
