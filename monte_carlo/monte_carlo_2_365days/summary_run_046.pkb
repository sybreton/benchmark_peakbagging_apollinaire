# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:00:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1588 0.1305 0.1302 4.9805 1.0673 1.3277 1.3367 0.2946 0.4009 90.0000 0.0000 0.0000 0.3576 0.0575 0.0471 0.0000 0.0000 0.0000
20 1 2962.9910 0.0599 0.0604 25.4205 3.3755 4.1185 1.1966 0.1251 0.1405 90.0000 0.0000 0.0000 0.4616 0.1058 0.0798 0.0000 0.0000 0.0000
