# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:53:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1101 0.1279 0.1344 4.1821 1.0843 1.4329 1.3804 0.3816 0.5627 90.0000 0.0000 0.0000 0.3426 0.0734 0.0497 0.0000 0.0000 0.0000
20 1 2962.9828 0.0575 0.0563 27.7068 3.7961 4.4000 1.0842 0.1122 0.1312 90.0000 0.0000 0.0000 0.4899 0.0840 0.0700 0.0000 0.0000 0.0000
