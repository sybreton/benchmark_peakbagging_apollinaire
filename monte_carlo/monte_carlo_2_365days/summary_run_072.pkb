# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:06:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9623 0.1067 0.1216 4.8221 1.2731 1.5265 1.0300 0.2605 0.4229 90.0000 0.0000 0.0000 0.4191 0.0488 0.0372 0.0000 0.0000 0.0000
20 1 2963.0264 0.0575 0.0583 26.5396 3.5819 4.3436 1.0822 0.1161 0.1297 90.0000 0.0000 0.0000 0.4260 0.0991 0.0765 0.0000 0.0000 0.0000
