# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:03:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8831 0.1468 0.1338 4.7368 1.0524 1.3075 1.2958 0.3024 0.4138 90.0000 0.0000 0.0000 0.4232 0.0472 0.0432 0.0000 0.0000 0.0000
20 1 2963.0407 0.0526 0.0518 30.6837 4.2922 5.0227 1.0192 0.1068 0.1272 90.0000 0.0000 0.0000 0.4003 0.0937 0.0710 0.0000 0.0000 0.0000
