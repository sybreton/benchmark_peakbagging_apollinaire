# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:09:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1050 0.1120 0.1210 5.1671 1.0884 1.3644 1.0374 0.2191 0.2761 90.0000 0.0000 0.0000 0.4098 0.0367 0.0376 0.0000 0.0000 0.0000
20 1 2962.9911 0.0474 0.0475 36.6667 5.0462 6.0697 0.8848 0.0896 0.1000 90.0000 0.0000 0.0000 0.4521 0.0625 0.0568 0.0000 0.0000 0.0000
