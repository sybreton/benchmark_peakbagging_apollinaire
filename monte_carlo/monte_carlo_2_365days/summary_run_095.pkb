# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:05:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9527 0.0863 0.0892 6.3012 1.4334 1.8335 0.9395 0.2177 0.3032 90.0000 0.0000 0.0000 0.4471 0.0334 0.0316 0.0000 0.0000 0.0000
20 1 2963.0257 0.0502 0.0486 34.0101 4.9958 6.3170 0.8822 0.1010 0.1101 90.0000 0.0000 0.0000 0.2856 0.1085 0.0764 0.0000 0.0000 0.0000
