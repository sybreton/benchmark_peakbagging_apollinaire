# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:13:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9637 0.0677 0.0692 8.2780 1.7226 2.1664 0.7380 0.1474 0.2031 90.0000 0.0000 0.0000 0.4042 0.0245 0.0230 0.0000 0.0000 0.0000
20 1 2962.9165 0.0482 0.0495 34.6242 4.8301 5.8684 0.9067 0.0953 0.1074 90.0000 0.0000 0.0000 0.4050 0.0755 0.0620 0.0000 0.0000 0.0000
