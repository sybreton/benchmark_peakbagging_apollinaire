# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 11:25:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9200 0.1271 0.1330 4.9683 1.0831 1.2756 1.2002 0.2529 0.3628 90.0000 0.0000 0.0000 0.4092 0.0513 0.0443 0.0000 0.0000 0.0000
20 1 2963.0304 0.0557 0.0567 29.2867 4.0768 4.8118 1.0207 0.1086 0.1208 90.0000 0.0000 0.0000 0.4457 0.0854 0.0676 0.0000 0.0000 0.0000
