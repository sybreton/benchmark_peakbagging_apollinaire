# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:18:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8589 0.1372 0.1317 4.3340 1.0193 1.3230 1.3010 0.3226 0.4536 90.0000 0.0000 0.0000 0.4402 0.0563 0.0458 0.0000 0.0000 0.0000
20 1 2962.9452 0.0551 0.0547 29.9415 4.1545 5.0339 0.9641 0.1019 0.1139 90.0000 0.0000 0.0000 0.4899 0.0723 0.0613 0.0000 0.0000 0.0000
