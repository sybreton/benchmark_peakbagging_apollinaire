# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:44:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9427 0.0811 0.0818 7.2946 1.5225 1.9454 0.8287 0.1727 0.2226 90.0000 0.0000 0.0000 0.3978 0.0296 0.0274 0.0000 0.0000 0.0000
20 1 2963.0278 0.0572 0.0567 27.4132 3.8386 4.6234 1.0556 0.1153 0.1321 90.0000 0.0000 0.0000 0.3760 0.1128 0.0809 0.0000 0.0000 0.0000
