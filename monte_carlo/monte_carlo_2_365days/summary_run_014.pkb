# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:43:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0702 0.1330 0.1356 4.1655 1.0583 1.3650 1.2723 0.3338 0.4723 90.0000 0.0000 0.0000 0.3940 0.0650 0.0506 0.0000 0.0000 0.0000
20 1 2962.9716 0.0498 0.0496 38.3935 5.6233 6.8989 0.8569 0.0961 0.1047 90.0000 0.0000 0.0000 0.4361 0.0692 0.0569 0.0000 0.0000 0.0000
