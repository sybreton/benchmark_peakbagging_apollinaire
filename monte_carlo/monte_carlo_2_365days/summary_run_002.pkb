# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:14:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9069 0.1332 0.1265 4.1977 0.9840 1.3596 1.4587 0.3925 0.5179 90.0000 0.0000 0.0000 0.2870 0.1062 0.0643 0.0000 0.0000 0.0000
20 1 2962.9768 0.0531 0.0556 29.5974 4.1074 4.7542 1.0682 0.1106 0.1315 90.0000 0.0000 0.0000 0.4765 0.0854 0.0666 0.0000 0.0000 0.0000
