# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:25:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0365 0.0950 0.0963 6.4997 1.3711 1.7881 0.9532 0.2123 0.2742 90.0000 0.0000 0.0000 0.3583 0.0324 0.0295 0.0000 0.0000 0.0000
20 1 2962.9600 0.0553 0.0543 25.9275 3.8362 4.6198 1.0897 0.1275 0.1391 90.0000 0.0000 0.0000 0.2953 0.1408 0.0942 0.0000 0.0000 0.0000
