# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:51:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9084 0.1509 0.1420 4.4059 0.9775 1.2039 1.3819 0.3104 0.4383 90.0000 0.0000 0.0000 0.3681 0.0699 0.0553 0.0000 0.0000 0.0000
20 1 2963.0697 0.0531 0.0528 30.8915 4.2884 5.0804 1.0261 0.1099 0.1272 90.0000 0.0000 0.0000 0.3796 0.1066 0.0745 0.0000 0.0000 0.0000
