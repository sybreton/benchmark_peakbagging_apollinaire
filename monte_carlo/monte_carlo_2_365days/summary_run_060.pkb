# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:36:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0498 0.1599 0.1584 3.6125 0.7020 0.9049 1.9171 0.4109 0.4916 90.0000 0.0000 0.0000 0.3039 0.1192 0.0869 0.0000 0.0000 0.0000
20 1 2963.0107 0.0547 0.0550 27.6036 3.8490 4.6316 1.0915 0.1166 0.1339 90.0000 0.0000 0.0000 0.3895 0.1091 0.0781 0.0000 0.0000 0.0000
