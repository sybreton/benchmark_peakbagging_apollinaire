# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 22:13:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.7808 0.1106 0.1208 5.7666 1.2716 1.5913 0.9233 0.1933 0.2633 90.0000 0.0000 0.0000 0.4577 0.0471 0.0432 0.0000 0.0000 0.0000
20 1 2963.1094 0.0575 0.0567 25.1788 3.5836 4.1672 1.1711 0.1280 0.1415 90.0000 0.0000 0.0000 0.2758 0.1489 0.1111 0.0000 0.0000 0.0000
