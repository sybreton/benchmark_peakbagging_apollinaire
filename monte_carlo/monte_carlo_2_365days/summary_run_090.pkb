# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:52:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0373 0.1239 0.1249 4.8818 1.0614 1.2609 1.1726 0.2540 0.3515 90.0000 0.0000 0.0000 0.4557 0.0419 0.0382 0.0000 0.0000 0.0000
20 1 2963.0391 0.0527 0.0526 29.9338 4.4481 5.3007 0.9932 0.1151 0.1230 90.0000 0.0000 0.0000 0.3085 0.1244 0.0837 0.0000 0.0000 0.0000
