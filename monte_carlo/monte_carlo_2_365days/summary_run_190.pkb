# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:34:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0393 0.1595 0.1752 3.1490 0.7421 1.0378 1.7486 0.4692 0.6420 90.0000 0.0000 0.0000 0.3289 0.1219 0.0768 0.0000 0.0000 0.0000
20 1 2963.0122 0.0558 0.0539 27.6273 3.7976 4.7457 1.0684 0.1177 0.1294 90.0000 0.0000 0.0000 0.4480 0.0913 0.0711 0.0000 0.0000 0.0000
