# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:38:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9438 0.0909 0.0994 7.8322 1.4910 1.9360 0.9295 0.1740 0.2146 90.0000 0.0000 0.0000 0.3901 0.0332 0.0296 0.0000 0.0000 0.0000
20 1 2962.9616 0.0469 0.0470 37.4675 5.4438 6.5537 0.8584 0.0931 0.1046 90.0000 0.0000 0.0000 0.4033 0.0685 0.0566 0.0000 0.0000 0.0000
