# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:14:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0624 0.0999 0.0898 6.6161 1.4526 1.7796 0.9047 0.1923 0.2708 90.0000 0.0000 0.0000 0.4393 0.0358 0.0330 0.0000 0.0000 0.0000
20 1 2962.9453 0.0529 0.0517 29.5247 4.2232 5.0816 1.0462 0.1141 0.1290 90.0000 0.0000 0.0000 0.3742 0.1029 0.0758 0.0000 0.0000 0.0000
