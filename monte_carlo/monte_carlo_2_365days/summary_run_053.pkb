# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 20:18:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1402 0.1517 0.1544 5.0805 1.1150 1.3498 1.1585 0.2489 0.3349 90.0000 0.0000 0.0000 0.4274 0.0620 0.0492 0.0000 0.0000 0.0000
20 1 2963.0122 0.0518 0.0522 30.5064 4.2898 5.1497 1.0665 0.1151 0.1226 90.0000 0.0000 0.0000 0.2687 0.1381 0.0951 0.0000 0.0000 0.0000
