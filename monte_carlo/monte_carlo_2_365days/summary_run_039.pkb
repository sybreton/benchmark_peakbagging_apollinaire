# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:43:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1904 0.1071 0.1097 5.8482 1.1964 1.4204 0.9992 0.1985 0.2684 90.0000 0.0000 0.0000 0.4605 0.0321 0.0325 0.0000 0.0000 0.0000
20 1 2962.9561 0.0471 0.0488 36.5442 5.3794 6.5426 0.8408 0.0986 0.1111 90.0000 0.0000 0.0000 0.4992 0.0608 0.0515 0.0000 0.0000 0.0000
