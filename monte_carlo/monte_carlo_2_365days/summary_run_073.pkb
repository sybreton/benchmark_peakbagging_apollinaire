# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 21:09:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0388 0.0905 0.0907 5.9132 1.5056 1.9316 0.8940 0.2339 0.3403 90.0000 0.0000 0.0000 0.3490 0.0363 0.0304 0.0000 0.0000 0.0000
20 1 2963.0000 0.0521 0.0521 28.6575 4.0541 5.1126 1.0504 0.1148 0.1217 90.0000 0.0000 0.0000 0.2228 0.1317 0.1038 0.0000 0.0000 0.0000
