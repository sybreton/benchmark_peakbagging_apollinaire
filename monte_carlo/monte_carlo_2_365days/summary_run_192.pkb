# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:40:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0224 0.1071 0.1268 5.0367 1.3347 1.7369 1.0890 0.2987 0.4368 90.0000 0.0000 0.0000 0.3731 0.0582 0.0439 0.0000 0.0000 0.0000
20 1 2963.0953 0.0533 0.0527 31.0249 4.4295 5.2815 0.9785 0.1051 0.1188 90.0000 0.0000 0.0000 0.3525 0.1084 0.0749 0.0000 0.0000 0.0000
