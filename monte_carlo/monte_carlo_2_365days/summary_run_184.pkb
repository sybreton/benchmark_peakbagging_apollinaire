# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 13:17:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.1294 0.1254 0.1534 4.7883 1.1670 1.4500 1.2088 0.2960 0.4358 90.0000 0.0000 0.0000 0.3379 0.0708 0.0464 0.0000 0.0000 0.0000
20 1 2963.0173 0.0504 0.0516 30.8905 4.6056 5.6906 0.9215 0.1077 0.1224 90.0000 0.0000 0.0000 0.3619 0.0954 0.0683 0.0000 0.0000 0.0000
