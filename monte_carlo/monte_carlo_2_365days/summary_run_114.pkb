# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 09:32:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.7951 0.0745 0.0908 6.8628 1.6041 1.8748 0.7503 0.1605 0.2404 90.0000 0.0000 0.0000 0.4904 0.0278 0.0243 0.0000 0.0000 0.0000
20 1 2963.1117 0.0544 0.0552 24.6645 3.3467 4.1786 1.2003 0.1262 0.1330 90.0000 0.0000 0.0000 0.1669 0.1123 0.1178 0.0000 0.0000 0.0000
