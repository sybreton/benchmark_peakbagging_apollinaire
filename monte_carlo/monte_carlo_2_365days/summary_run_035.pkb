# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 19:32:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8856 0.0964 0.0994 6.3546 1.2988 1.6542 0.9369 0.1965 0.2472 90.0000 0.0000 0.0000 0.3716 0.0359 0.0327 0.0000 0.0000 0.0000
20 1 2963.0234 0.0534 0.0530 32.7711 4.5689 5.6699 1.0425 0.1090 0.1151 90.0000 0.0000 0.0000 0.2120 0.1288 0.1069 0.0000 0.0000 0.0000
