# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 18:39:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9673 0.1048 0.1116 5.2479 1.2313 1.7073 1.1503 0.2922 0.3722 90.0000 0.0000 0.0000 0.3365 0.0660 0.0432 0.0000 0.0000 0.0000
20 1 2963.0677 0.0507 0.0507 35.8798 5.0432 6.1678 0.8949 0.0942 0.1039 90.0000 0.0000 0.0000 0.3629 0.0799 0.0635 0.0000 0.0000 0.0000
