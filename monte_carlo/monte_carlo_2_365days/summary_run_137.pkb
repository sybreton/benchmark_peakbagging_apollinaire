# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 10:41:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.3119 0.1141 0.0846 7.1027 1.5296 1.9928 0.8472 0.1862 0.2361 90.0000 0.0000 0.0000 0.4547 0.0441 0.0336 0.0000 0.0000 0.0000
20 1 2962.9755 0.0542 0.0554 29.3906 3.9822 4.8701 1.1571 0.1183 0.1276 90.0000 0.0000 0.0000 0.2387 0.1394 0.1105 0.0000 0.0000 0.0000
