# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:41:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9738 0.0560 0.0541 5.5922 0.6130 0.6865 1.0988 0.1254 0.1452 90.0000 0.0000 0.0000 0.3663 0.0201 0.0188 0.0000 0.0000 0.0000
20 1 2963.0384 0.0249 0.0256 33.1789 2.4109 2.7674 0.9448 0.0512 0.0553 90.0000 0.0000 0.0000 0.4034 0.0355 0.0328 0.0000 0.0000 0.0000
