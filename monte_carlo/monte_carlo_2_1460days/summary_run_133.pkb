# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:36:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0670 0.0577 0.0562 6.2391 0.6392 0.7290 0.9211 0.0962 0.1095 90.0000 0.0000 0.0000 0.4237 0.0174 0.0170 0.0000 0.0000 0.0000
20 1 2962.9931 0.0260 0.0258 31.2635 2.2524 2.4397 0.9590 0.0516 0.0538 90.0000 0.0000 0.0000 0.4439 0.0344 0.0313 0.0000 0.0000 0.0000
