# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:46:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0130 0.0415 0.0419 7.0510 0.8382 0.9725 0.8462 0.1044 0.1179 90.0000 0.0000 0.0000 0.3930 0.0157 0.0146 0.0000 0.0000 0.0000
20 1 2962.9949 0.0251 0.0254 33.2479 2.4212 2.6374 0.9545 0.0512 0.0544 90.0000 0.0000 0.0000 0.4066 0.0358 0.0322 0.0000 0.0000 0.0000
