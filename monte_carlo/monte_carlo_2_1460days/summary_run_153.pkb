# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:34:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9975 0.0499 0.0508 5.7389 0.7327 0.8395 1.0552 0.1423 0.1720 90.0000 0.0000 0.0000 0.4061 0.0219 0.0198 0.0000 0.0000 0.0000
20 1 2962.9630 0.0258 0.0259 32.3402 2.3738 2.5506 0.9556 0.0504 0.0541 90.0000 0.0000 0.0000 0.4097 0.0361 0.0332 0.0000 0.0000 0.0000
