# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:31:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9688 0.0555 0.0556 5.6005 0.6622 0.7353 1.0570 0.1276 0.1511 90.0000 0.0000 0.0000 0.4123 0.0230 0.0202 0.0000 0.0000 0.0000
20 1 2963.0205 0.0269 0.0264 29.3525 2.1624 2.3192 1.0161 0.0554 0.0602 90.0000 0.0000 0.0000 0.3809 0.0438 0.0385 0.0000 0.0000 0.0000
