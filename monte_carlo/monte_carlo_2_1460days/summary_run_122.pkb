# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:04:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9861 0.0492 0.0488 6.0402 0.6729 0.7306 0.9505 0.1056 0.1261 90.0000 0.0000 0.0000 0.4084 0.0158 0.0155 0.0000 0.0000 0.0000
20 1 2962.9809 0.0265 0.0263 29.0631 2.1022 2.2749 1.0136 0.0561 0.0602 90.0000 0.0000 0.0000 0.4258 0.0384 0.0346 0.0000 0.0000 0.0000
