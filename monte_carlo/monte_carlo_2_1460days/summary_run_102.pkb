# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:07:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9336 0.0576 0.0589 5.6958 0.6066 0.6758 1.0357 0.1107 0.1244 90.0000 0.0000 0.0000 0.3882 0.0203 0.0189 0.0000 0.0000 0.0000
20 1 2963.0144 0.0257 0.0256 31.1089 2.2744 2.4935 0.9882 0.0542 0.0572 90.0000 0.0000 0.0000 0.3790 0.0399 0.0352 0.0000 0.0000 0.0000
