# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:24:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0407 0.0508 0.0517 6.1306 0.6224 0.7147 0.9309 0.0955 0.1084 90.0000 0.0000 0.0000 0.3959 0.0159 0.0156 0.0000 0.0000 0.0000
20 1 2963.0264 0.0261 0.0265 33.4618 2.5053 2.6969 0.9217 0.0515 0.0531 90.0000 0.0000 0.0000 0.4308 0.0350 0.0310 0.0000 0.0000 0.0000
