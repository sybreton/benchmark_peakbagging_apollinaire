# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:57:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9091 0.0573 0.0590 5.3063 0.5911 0.6832 1.1520 0.1399 0.1545 90.0000 0.0000 0.0000 0.4189 0.0202 0.0193 0.0000 0.0000 0.0000
20 1 2962.9906 0.0268 0.0276 26.4005 1.9396 2.1119 1.0631 0.0613 0.0647 90.0000 0.0000 0.0000 0.4649 0.0380 0.0345 0.0000 0.0000 0.0000
