# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:12:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0313 0.0445 0.0438 6.7257 0.7771 0.8914 0.9020 0.1084 0.1217 90.0000 0.0000 0.0000 0.3991 0.0160 0.0150 0.0000 0.0000 0.0000
20 1 2962.9843 0.0261 0.0259 32.2827 2.3433 2.5998 0.9611 0.0526 0.0556 90.0000 0.0000 0.0000 0.4542 0.0347 0.0303 0.0000 0.0000 0.0000
