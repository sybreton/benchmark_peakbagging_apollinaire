# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:46:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9720 0.0581 0.0612 5.3774 0.5882 0.6458 1.0570 0.1146 0.1331 90.0000 0.0000 0.0000 0.4091 0.0202 0.0194 0.0000 0.0000 0.0000
20 1 2962.9683 0.0265 0.0260 30.9249 2.2763 2.5198 1.0256 0.0573 0.0607 90.0000 0.0000 0.0000 0.3056 0.0537 0.0431 0.0000 0.0000 0.0000
