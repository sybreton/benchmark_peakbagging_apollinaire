# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:12:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9923 0.0584 0.0609 5.1785 0.6141 0.6483 1.1375 0.1362 0.1605 90.0000 0.0000 0.0000 0.3938 0.0217 0.0200 0.0000 0.0000 0.0000
20 1 2963.0179 0.0272 0.0278 27.5514 2.0484 2.2047 1.0887 0.0605 0.0652 90.0000 0.0000 0.0000 0.3501 0.0525 0.0432 0.0000 0.0000 0.0000
