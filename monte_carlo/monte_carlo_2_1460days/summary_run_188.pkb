# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:20:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0186 0.0447 0.0449 6.5934 0.7899 0.9331 0.9518 0.1216 0.1383 90.0000 0.0000 0.0000 0.4282 0.0181 0.0161 0.0000 0.0000 0.0000
20 1 2962.9784 0.0264 0.0262 32.0600 2.3343 2.6586 0.9536 0.0518 0.0549 90.0000 0.0000 0.0000 0.3906 0.0364 0.0331 0.0000 0.0000 0.0000
