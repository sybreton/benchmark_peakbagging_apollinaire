# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:13:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0182 0.0585 0.0589 4.7803 0.6299 0.7020 1.1803 0.1596 0.1996 90.0000 0.0000 0.0000 0.3958 0.0237 0.0207 0.0000 0.0000 0.0000
20 1 2963.0013 0.0264 0.0265 31.2796 2.1992 2.4245 0.9891 0.0515 0.0557 90.0000 0.0000 0.0000 0.4302 0.0373 0.0322 0.0000 0.0000 0.0000
