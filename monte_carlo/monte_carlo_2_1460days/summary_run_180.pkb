# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:56:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9305 0.0616 0.0636 4.6937 0.5654 0.6761 1.3400 0.1831 0.2034 90.0000 0.0000 0.0000 0.3782 0.0294 0.0255 0.0000 0.0000 0.0000
20 1 2963.0186 0.0273 0.0267 27.0411 2.0508 2.2634 1.0949 0.0640 0.0702 90.0000 0.0000 0.0000 0.3085 0.0617 0.0485 0.0000 0.0000 0.0000
