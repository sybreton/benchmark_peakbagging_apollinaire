# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:28:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9922 0.0600 0.0590 5.9545 0.6713 0.7329 1.1639 0.1354 0.1554 90.0000 0.0000 0.0000 0.3678 0.0262 0.0235 0.0000 0.0000 0.0000
20 1 2963.0138 0.0262 0.0264 30.1435 2.2024 2.4430 1.0152 0.0583 0.0594 90.0000 0.0000 0.0000 0.3923 0.0407 0.0363 0.0000 0.0000 0.0000
