# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:36:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0250 0.0550 0.0562 5.8233 0.6048 0.6717 0.9919 0.1054 0.1210 90.0000 0.0000 0.0000 0.4135 0.0167 0.0173 0.0000 0.0000 0.0000
20 1 2962.9721 0.0261 0.0254 33.0767 2.4903 2.6413 0.9530 0.0520 0.0561 90.0000 0.0000 0.0000 0.4072 0.0351 0.0318 0.0000 0.0000 0.0000
