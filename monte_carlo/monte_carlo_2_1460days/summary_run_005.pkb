# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:21:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9309 0.0478 0.0506 6.3931 0.7039 0.8099 0.9680 0.1114 0.1265 90.0000 0.0000 0.0000 0.3671 0.0190 0.0173 0.0000 0.0000 0.0000
20 1 2962.9665 0.0265 0.0261 29.7286 2.2609 2.4676 1.0054 0.0584 0.0629 90.0000 0.0000 0.0000 0.3941 0.0398 0.0366 0.0000 0.0000 0.0000
