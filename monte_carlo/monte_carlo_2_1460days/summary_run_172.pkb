# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:32:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0331 0.0689 0.0653 4.6247 0.5560 0.6483 1.2531 0.1587 0.1855 90.0000 0.0000 0.0000 0.3892 0.0283 0.0250 0.0000 0.0000 0.0000
20 1 2963.0323 0.0262 0.0260 31.0916 2.2763 2.5186 0.9827 0.0547 0.0574 90.0000 0.0000 0.0000 0.4002 0.0384 0.0342 0.0000 0.0000 0.0000
