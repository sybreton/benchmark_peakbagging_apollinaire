# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:08:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0265 0.0455 0.0454 6.4744 0.7083 0.7654 0.8733 0.0958 0.1122 90.0000 0.0000 0.0000 0.4227 0.0150 0.0147 0.0000 0.0000 0.0000
20 1 2963.0536 0.0250 0.0245 32.5371 2.4177 2.6888 0.9325 0.0521 0.0544 90.0000 0.0000 0.0000 0.4099 0.0340 0.0312 0.0000 0.0000 0.0000
