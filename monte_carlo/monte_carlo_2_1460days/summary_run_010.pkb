# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:34:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0292 0.0565 0.0557 5.8382 0.6088 0.6949 1.0048 0.1064 0.1199 90.0000 0.0000 0.0000 0.4165 0.0181 0.0180 0.0000 0.0000 0.0000
20 1 2963.0109 0.0255 0.0261 31.8413 2.3652 2.6274 0.9205 0.0527 0.0533 90.0000 0.0000 0.0000 0.4083 0.0343 0.0317 0.0000 0.0000 0.0000
