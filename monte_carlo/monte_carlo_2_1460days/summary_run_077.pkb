# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:51:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0299 0.0552 0.0545 5.3456 0.6678 0.7337 1.0541 0.1342 0.1656 90.0000 0.0000 0.0000 0.3907 0.0226 0.0203 0.0000 0.0000 0.0000
20 1 2962.9280 0.0270 0.0263 28.7888 2.1473 2.2621 1.0355 0.0563 0.0628 90.0000 0.0000 0.0000 0.3384 0.0485 0.0418 0.0000 0.0000 0.0000
