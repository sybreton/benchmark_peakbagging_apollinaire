# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:44:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0627 0.0545 0.0526 5.7502 0.6620 0.7251 1.0763 0.1269 0.1536 90.0000 0.0000 0.0000 0.3632 0.0223 0.0201 0.0000 0.0000 0.0000
20 1 2963.0262 0.0270 0.0273 28.2230 2.0554 2.2711 1.0643 0.0589 0.0627 90.0000 0.0000 0.0000 0.3591 0.0480 0.0424 0.0000 0.0000 0.0000
