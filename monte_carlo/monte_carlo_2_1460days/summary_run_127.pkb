# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:19:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0161 0.0441 0.0454 6.6724 0.6966 0.7838 0.8563 0.0907 0.1018 90.0000 0.0000 0.0000 0.4038 0.0137 0.0134 0.0000 0.0000 0.0000
20 1 2962.9922 0.0247 0.0244 32.5174 2.3595 2.6986 0.9282 0.0522 0.0525 90.0000 0.0000 0.0000 0.4281 0.0320 0.0304 0.0000 0.0000 0.0000
