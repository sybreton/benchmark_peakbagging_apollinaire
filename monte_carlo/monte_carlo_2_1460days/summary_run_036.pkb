# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:48:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0309 0.0525 0.0544 5.6957 0.6282 0.7102 1.0764 0.1236 0.1401 90.0000 0.0000 0.0000 0.3806 0.0194 0.0180 0.0000 0.0000 0.0000
20 1 2962.9818 0.0260 0.0261 32.1685 2.4234 2.6269 0.9678 0.0533 0.0579 90.0000 0.0000 0.0000 0.3131 0.0512 0.0414 0.0000 0.0000 0.0000
