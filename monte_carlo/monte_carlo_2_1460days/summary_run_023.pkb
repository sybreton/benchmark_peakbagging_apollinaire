# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:10:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9568 0.0502 0.0490 6.5451 0.6944 0.7976 0.9321 0.0987 0.1114 90.0000 0.0000 0.0000 0.4080 0.0182 0.0171 0.0000 0.0000 0.0000
20 1 2962.9601 0.0273 0.0269 29.1871 2.1278 2.3633 1.0556 0.0592 0.0628 90.0000 0.0000 0.0000 0.3860 0.0454 0.0377 0.0000 0.0000 0.0000
