# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:08:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0125 0.0527 0.0545 5.4163 0.6641 0.7042 1.0747 0.1335 0.1636 90.0000 0.0000 0.0000 0.3606 0.0219 0.0192 0.0000 0.0000 0.0000
20 1 2963.0109 0.0273 0.0271 28.6115 2.1101 2.3125 1.0132 0.0568 0.0599 90.0000 0.0000 0.0000 0.4229 0.0382 0.0361 0.0000 0.0000 0.0000
