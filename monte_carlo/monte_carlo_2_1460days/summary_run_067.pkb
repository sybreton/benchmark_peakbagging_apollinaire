# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:21:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0271 0.0394 0.0400 7.1631 0.8676 0.9673 0.8151 0.0986 0.1148 90.0000 0.0000 0.0000 0.4264 0.0147 0.0133 0.0000 0.0000 0.0000
20 1 2963.0004 0.0257 0.0258 33.4064 2.4477 2.8049 0.9569 0.0538 0.0548 90.0000 0.0000 0.0000 0.4128 0.0347 0.0323 0.0000 0.0000 0.0000
