# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:56:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0613 0.0604 0.0580 5.8564 0.6936 0.7950 1.0041 0.1245 0.1430 90.0000 0.0000 0.0000 0.3676 0.0211 0.0198 0.0000 0.0000 0.0000
20 1 2963.0261 0.0263 0.0263 29.7790 2.1828 2.4468 0.9930 0.0564 0.0595 90.0000 0.0000 0.0000 0.3607 0.0439 0.0375 0.0000 0.0000 0.0000
