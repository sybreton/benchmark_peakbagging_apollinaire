# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:30:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0386 0.0473 0.0485 6.5189 0.6977 0.8071 0.9517 0.1058 0.1210 90.0000 0.0000 0.0000 0.3999 0.0170 0.0156 0.0000 0.0000 0.0000
20 1 2962.9792 0.0242 0.0250 34.7401 2.6106 2.9233 0.9034 0.0511 0.0525 90.0000 0.0000 0.0000 0.3870 0.0349 0.0310 0.0000 0.0000 0.0000
