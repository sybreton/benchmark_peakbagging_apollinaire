# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:08:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0363 0.0466 0.0463 6.5356 0.6868 0.7871 0.9715 0.1052 0.1204 90.0000 0.0000 0.0000 0.4325 0.0168 0.0159 0.0000 0.0000 0.0000
20 1 2963.0139 0.0258 0.0259 30.5825 2.2564 2.3924 1.0003 0.0539 0.0575 90.0000 0.0000 0.0000 0.3672 0.0432 0.0369 0.0000 0.0000 0.0000
