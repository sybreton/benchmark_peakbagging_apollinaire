# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:30:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0184 0.0553 0.0542 5.6150 0.6206 0.6987 1.0713 0.1203 0.1400 90.0000 0.0000 0.0000 0.3807 0.0212 0.0202 0.0000 0.0000 0.0000
20 1 2963.0245 0.0251 0.0246 33.1606 2.4352 2.7492 0.9428 0.0527 0.0547 90.0000 0.0000 0.0000 0.4097 0.0345 0.0322 0.0000 0.0000 0.0000
