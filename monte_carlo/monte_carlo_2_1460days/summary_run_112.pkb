# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:35:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0498 0.0423 0.0426 6.7360 0.7627 0.9008 0.8732 0.1063 0.1174 90.0000 0.0000 0.0000 0.4017 0.0146 0.0140 0.0000 0.0000 0.0000
20 1 2962.9835 0.0266 0.0265 29.9500 2.1383 2.3853 0.9754 0.0545 0.0552 90.0000 0.0000 0.0000 0.4447 0.0350 0.0328 0.0000 0.0000 0.0000
