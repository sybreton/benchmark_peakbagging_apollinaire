# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:51:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0520 0.0569 0.0587 5.4564 0.6184 0.6766 1.0090 0.1123 0.1339 90.0000 0.0000 0.0000 0.3972 0.0188 0.0186 0.0000 0.0000 0.0000
20 1 2962.9496 0.0281 0.0269 30.6724 2.2143 2.4776 0.9999 0.0557 0.0585 90.0000 0.0000 0.0000 0.4095 0.0391 0.0344 0.0000 0.0000 0.0000
