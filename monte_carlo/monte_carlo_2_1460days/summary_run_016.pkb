# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:50:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0094 0.0437 0.0459 6.4511 0.7957 0.9138 0.9346 0.1193 0.1411 90.0000 0.0000 0.0000 0.3926 0.0167 0.0153 0.0000 0.0000 0.0000
20 1 2962.9767 0.0269 0.0261 29.8460 2.2143 2.4677 0.9699 0.0548 0.0599 90.0000 0.0000 0.0000 0.3883 0.0386 0.0352 0.0000 0.0000 0.0000
