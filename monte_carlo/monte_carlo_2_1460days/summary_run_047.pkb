# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:21:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9543 0.0495 0.0485 6.1185 0.6432 0.7133 0.9317 0.0986 0.1110 90.0000 0.0000 0.0000 0.4014 0.0162 0.0153 0.0000 0.0000 0.0000
20 1 2963.0291 0.0289 0.0283 27.8340 1.9561 2.1832 1.0746 0.0581 0.0625 90.0000 0.0000 0.0000 0.4145 0.0426 0.0387 0.0000 0.0000 0.0000
