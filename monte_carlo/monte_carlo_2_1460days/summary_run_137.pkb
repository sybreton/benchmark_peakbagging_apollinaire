# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:48:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0088 0.0546 0.0562 5.6387 0.6227 0.6705 1.1235 0.1236 0.1446 90.0000 0.0000 0.0000 0.4005 0.0196 0.0191 0.0000 0.0000 0.0000
20 1 2962.9877 0.0249 0.0251 31.7466 2.3308 2.5444 0.9634 0.0522 0.0557 90.0000 0.0000 0.0000 0.4099 0.0357 0.0327 0.0000 0.0000 0.0000
