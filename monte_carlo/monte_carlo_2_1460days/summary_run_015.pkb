# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:48:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0203 0.0486 0.0493 6.9080 0.7534 0.8378 0.9686 0.1075 0.1210 90.0000 0.0000 0.0000 0.3973 0.0184 0.0161 0.0000 0.0000 0.0000
20 1 2962.9304 0.0249 0.0250 31.8918 2.3318 2.5871 0.9239 0.0503 0.0551 90.0000 0.0000 0.0000 0.4353 0.0331 0.0302 0.0000 0.0000 0.0000
