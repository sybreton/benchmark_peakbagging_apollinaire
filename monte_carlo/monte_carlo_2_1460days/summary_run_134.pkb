# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:39:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0835 0.0557 0.0548 5.2863 0.6595 0.7088 1.1226 0.1442 0.1754 90.0000 0.0000 0.0000 0.3886 0.0218 0.0199 0.0000 0.0000 0.0000
20 1 2962.9908 0.0263 0.0268 30.7992 2.2931 2.4382 1.0182 0.0553 0.0587 90.0000 0.0000 0.0000 0.4289 0.0375 0.0344 0.0000 0.0000 0.0000
