# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:33:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0877 0.0544 0.0549 6.0529 0.6472 0.7210 1.0853 0.1206 0.1346 90.0000 0.0000 0.0000 0.4177 0.0203 0.0188 0.0000 0.0000 0.0000
20 1 2962.9786 0.0262 0.0259 31.0439 2.2440 2.3902 0.9734 0.0526 0.0562 90.0000 0.0000 0.0000 0.4376 0.0355 0.0327 0.0000 0.0000 0.0000
