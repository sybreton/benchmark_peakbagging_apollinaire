# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:27:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0487 0.0465 0.0466 6.5409 0.7164 0.8540 0.8908 0.1032 0.1126 90.0000 0.0000 0.0000 0.4052 0.0159 0.0153 0.0000 0.0000 0.0000
20 1 2962.9810 0.0254 0.0255 30.5342 2.2937 2.4918 0.9704 0.0547 0.0575 90.0000 0.0000 0.0000 0.3644 0.0409 0.0366 0.0000 0.0000 0.0000
