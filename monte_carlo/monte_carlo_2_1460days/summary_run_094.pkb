# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:42:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0699 0.0606 0.0570 5.3425 0.5930 0.6494 1.1060 0.1241 0.1435 90.0000 0.0000 0.0000 0.4007 0.0217 0.0207 0.0000 0.0000 0.0000
20 1 2963.0365 0.0253 0.0260 31.5991 2.2725 2.5422 0.9702 0.0532 0.0568 90.0000 0.0000 0.0000 0.4578 0.0331 0.0314 0.0000 0.0000 0.0000
