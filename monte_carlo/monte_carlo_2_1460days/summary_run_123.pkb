# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:07:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0521 0.0534 0.0547 6.3022 0.6625 0.6952 1.0630 0.1065 0.1279 90.0000 0.0000 0.0000 0.4167 0.0193 0.0180 0.0000 0.0000 0.0000
20 1 2962.9639 0.0281 0.0271 30.1433 2.1707 2.3632 0.9829 0.0534 0.0567 90.0000 0.0000 0.0000 0.4027 0.0401 0.0359 0.0000 0.0000 0.0000
