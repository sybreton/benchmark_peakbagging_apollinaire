# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:58:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0136 0.0618 0.0622 5.8318 0.5883 0.6411 1.1053 0.1093 0.1270 90.0000 0.0000 0.0000 0.3849 0.0206 0.0192 0.0000 0.0000 0.0000
20 1 2963.0381 0.0256 0.0252 32.0820 2.3628 2.7209 0.9493 0.0549 0.0555 90.0000 0.0000 0.0000 0.3840 0.0373 0.0334 0.0000 0.0000 0.0000
