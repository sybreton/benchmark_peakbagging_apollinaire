# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:15:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9114 0.0394 0.0404 7.8569 0.8732 0.9541 0.7662 0.0813 0.0958 90.0000 0.0000 0.0000 0.4184 0.0124 0.0121 0.0000 0.0000 0.0000
20 1 2963.0136 0.0251 0.0254 32.9462 2.3879 2.7157 0.9284 0.0502 0.0527 90.0000 0.0000 0.0000 0.3775 0.0354 0.0333 0.0000 0.0000 0.0000
