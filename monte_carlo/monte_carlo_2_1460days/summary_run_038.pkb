# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:55:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9504 0.0516 0.0524 5.7989 0.6507 0.7219 0.9649 0.1094 0.1296 90.0000 0.0000 0.0000 0.4014 0.0183 0.0174 0.0000 0.0000 0.0000
20 1 2962.9913 0.0264 0.0262 29.9112 2.1214 2.3005 1.0162 0.0548 0.0583 90.0000 0.0000 0.0000 0.4026 0.0406 0.0359 0.0000 0.0000 0.0000
