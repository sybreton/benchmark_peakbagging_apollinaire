# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:13:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0483 0.0431 0.0423 7.1151 0.8131 0.9206 0.8690 0.1018 0.1163 90.0000 0.0000 0.0000 0.4043 0.0151 0.0145 0.0000 0.0000 0.0000
20 1 2962.9801 0.0284 0.0286 28.1491 1.9903 2.1994 1.0067 0.0541 0.0564 90.0000 0.0000 0.0000 0.4510 0.0358 0.0345 0.0000 0.0000 0.0000
