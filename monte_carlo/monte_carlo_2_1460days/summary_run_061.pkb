# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:03:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0136 0.0528 0.0506 5.5004 0.6451 0.6955 1.0613 0.1248 0.1533 90.0000 0.0000 0.0000 0.4427 0.0193 0.0178 0.0000 0.0000 0.0000
20 1 2963.0058 0.0284 0.0284 26.2982 2.0395 2.2577 1.1102 0.0657 0.0717 90.0000 0.0000 0.0000 0.2774 0.0781 0.0554 0.0000 0.0000 0.0000
