# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:45:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9880 0.0557 0.0557 5.8340 0.6139 0.6634 1.0376 0.1069 0.1276 90.0000 0.0000 0.0000 0.4068 0.0187 0.0183 0.0000 0.0000 0.0000
20 1 2963.0052 0.0248 0.0251 31.4178 2.2735 2.6187 0.9629 0.0540 0.0560 90.0000 0.0000 0.0000 0.3917 0.0378 0.0348 0.0000 0.0000 0.0000
