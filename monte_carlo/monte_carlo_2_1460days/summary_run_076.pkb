# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:48:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0522 0.0478 0.0479 6.0365 0.7469 0.8587 0.9637 0.1281 0.1513 90.0000 0.0000 0.0000 0.3945 0.0168 0.0154 0.0000 0.0000 0.0000
20 1 2963.0134 0.0276 0.0282 26.2534 1.8933 2.1684 1.0525 0.0608 0.0621 90.0000 0.0000 0.0000 0.4328 0.0400 0.0359 0.0000 0.0000 0.0000
