# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:58:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9581 0.0651 0.0617 4.5321 0.5975 0.6829 1.3543 0.1924 0.2276 90.0000 0.0000 0.0000 0.3700 0.0289 0.0248 0.0000 0.0000 0.0000
20 1 2962.9989 0.0268 0.0262 30.9680 2.2608 2.5139 1.0044 0.0544 0.0589 90.0000 0.0000 0.0000 0.3406 0.0464 0.0398 0.0000 0.0000 0.0000
