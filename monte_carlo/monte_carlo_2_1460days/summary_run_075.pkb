# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:45:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9615 0.0421 0.0406 7.7522 0.8218 0.9024 0.8632 0.0874 0.1010 90.0000 0.0000 0.0000 0.4095 0.0136 0.0135 0.0000 0.0000 0.0000
20 1 2962.9996 0.0266 0.0267 29.0465 2.0532 2.2815 1.0362 0.0565 0.0596 90.0000 0.0000 0.0000 0.4097 0.0405 0.0365 0.0000 0.0000 0.0000
