# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:11:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9843 0.0476 0.0491 6.2449 0.6616 0.7786 0.9546 0.1066 0.1223 90.0000 0.0000 0.0000 0.3864 0.0180 0.0165 0.0000 0.0000 0.0000
20 1 2962.9862 0.0279 0.0283 28.5679 2.0474 2.1905 1.0429 0.0560 0.0596 90.0000 0.0000 0.0000 0.4186 0.0403 0.0361 0.0000 0.0000 0.0000
