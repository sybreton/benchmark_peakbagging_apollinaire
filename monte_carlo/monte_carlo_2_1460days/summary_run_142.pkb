# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:02:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9691 0.0506 0.0497 6.3588 0.6543 0.7691 1.0497 0.1142 0.1269 90.0000 0.0000 0.0000 0.3825 0.0181 0.0170 0.0000 0.0000 0.0000
20 1 2962.9692 0.0270 0.0266 30.4175 2.2025 2.4323 1.0242 0.0558 0.0603 90.0000 0.0000 0.0000 0.3620 0.0461 0.0390 0.0000 0.0000 0.0000
