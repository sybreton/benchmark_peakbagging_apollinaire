# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:36:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9408 0.0460 0.0473 6.6694 0.7641 0.8783 0.9120 0.1086 0.1259 90.0000 0.0000 0.0000 0.4104 0.0182 0.0165 0.0000 0.0000 0.0000
20 1 2962.9797 0.0270 0.0268 28.6176 2.1616 2.3595 1.0920 0.0610 0.0674 90.0000 0.0000 0.0000 0.3051 0.0622 0.0501 0.0000 0.0000 0.0000
