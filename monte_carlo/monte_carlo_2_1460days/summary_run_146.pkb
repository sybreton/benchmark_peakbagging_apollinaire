# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:13:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0278 0.0496 0.0464 6.7278 0.7503 0.8425 0.9316 0.1039 0.1183 90.0000 0.0000 0.0000 0.3955 0.0172 0.0161 0.0000 0.0000 0.0000
20 1 2962.9792 0.0262 0.0258 30.3391 2.2148 2.3979 0.9814 0.0527 0.0575 90.0000 0.0000 0.0000 0.4186 0.0368 0.0344 0.0000 0.0000 0.0000
