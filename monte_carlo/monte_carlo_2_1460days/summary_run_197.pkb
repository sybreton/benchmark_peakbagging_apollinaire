# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:48:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0028 0.0518 0.0524 6.1672 0.6535 0.7438 1.0078 0.1103 0.1222 90.0000 0.0000 0.0000 0.4021 0.0179 0.0170 0.0000 0.0000 0.0000
20 1 2962.9944 0.0259 0.0257 33.0161 2.4228 2.7704 0.9628 0.0534 0.0562 90.0000 0.0000 0.0000 0.3326 0.0439 0.0374 0.0000 0.0000 0.0000
