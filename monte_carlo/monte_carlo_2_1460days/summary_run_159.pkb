# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:52:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9472 0.0537 0.0529 5.8794 0.6527 0.7390 0.9776 0.1134 0.1261 90.0000 0.0000 0.0000 0.3440 0.0177 0.0170 0.0000 0.0000 0.0000
20 1 2963.0508 0.0283 0.0282 26.9803 1.9751 2.1067 1.0731 0.0585 0.0635 90.0000 0.0000 0.0000 0.4151 0.0432 0.0383 0.0000 0.0000 0.0000
