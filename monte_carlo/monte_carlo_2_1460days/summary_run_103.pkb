# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:10:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9837 0.0520 0.0488 6.7110 0.7880 0.9133 0.8933 0.1080 0.1217 90.0000 0.0000 0.0000 0.4006 0.0185 0.0168 0.0000 0.0000 0.0000
20 1 2963.0087 0.0271 0.0276 29.2204 2.0409 2.3236 1.0516 0.0573 0.0592 90.0000 0.0000 0.0000 0.3541 0.0492 0.0427 0.0000 0.0000 0.0000
