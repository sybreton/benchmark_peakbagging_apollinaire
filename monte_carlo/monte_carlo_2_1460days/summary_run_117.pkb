# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:50:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9199 0.0549 0.0562 5.7239 0.6096 0.6777 1.0390 0.1114 0.1279 90.0000 0.0000 0.0000 0.4138 0.0201 0.0182 0.0000 0.0000 0.0000
20 1 2963.0054 0.0259 0.0261 31.3558 2.2414 2.5433 0.9872 0.0552 0.0572 90.0000 0.0000 0.0000 0.4323 0.0353 0.0324 0.0000 0.0000 0.0000
