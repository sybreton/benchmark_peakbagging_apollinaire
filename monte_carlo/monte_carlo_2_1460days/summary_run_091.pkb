# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:33:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0075 0.0411 0.0402 7.1243 0.8438 0.9080 0.8155 0.0950 0.1112 90.0000 0.0000 0.0000 0.3953 0.0142 0.0137 0.0000 0.0000 0.0000
20 1 2962.9757 0.0259 0.0269 28.6722 2.0976 2.2492 1.0455 0.0580 0.0618 90.0000 0.0000 0.0000 0.4210 0.0404 0.0367 0.0000 0.0000 0.0000
