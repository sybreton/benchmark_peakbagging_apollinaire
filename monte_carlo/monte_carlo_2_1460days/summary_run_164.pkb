# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:07:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0669 0.0497 0.0506 6.2428 0.7018 0.7419 0.9455 0.1023 0.1224 90.0000 0.0000 0.0000 0.4271 0.0184 0.0169 0.0000 0.0000 0.0000
20 1 2962.9689 0.0274 0.0278 29.3343 2.0592 2.2646 1.0673 0.0572 0.0615 90.0000 0.0000 0.0000 0.3841 0.0470 0.0400 0.0000 0.0000 0.0000
