# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:36:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9105 0.0404 0.0411 7.0856 0.8741 0.9435 0.8243 0.0988 0.1208 90.0000 0.0000 0.0000 0.4015 0.0158 0.0148 0.0000 0.0000 0.0000
20 1 2963.0016 0.0261 0.0266 32.1128 2.2772 2.5528 0.9600 0.0530 0.0546 90.0000 0.0000 0.0000 0.4346 0.0347 0.0323 0.0000 0.0000 0.0000
