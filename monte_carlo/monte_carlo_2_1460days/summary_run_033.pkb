# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:39:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9629 0.0503 0.0506 5.8495 0.6810 0.7521 1.0479 0.1250 0.1449 90.0000 0.0000 0.0000 0.3833 0.0180 0.0169 0.0000 0.0000 0.0000
20 1 2962.9572 0.0291 0.0283 25.6137 1.9458 2.0909 1.1363 0.0647 0.0715 90.0000 0.0000 0.0000 0.3474 0.0598 0.0470 0.0000 0.0000 0.0000
