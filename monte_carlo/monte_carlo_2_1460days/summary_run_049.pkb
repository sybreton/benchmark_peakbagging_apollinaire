# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:27:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9209 0.0496 0.0515 6.3355 0.6427 0.7368 1.0132 0.1066 0.1181 90.0000 0.0000 0.0000 0.4092 0.0167 0.0157 0.0000 0.0000 0.0000
20 1 2963.0425 0.0271 0.0273 29.3362 2.1512 2.3454 1.0020 0.0558 0.0596 90.0000 0.0000 0.0000 0.4228 0.0389 0.0346 0.0000 0.0000 0.0000
