# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:25:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9927 0.0491 0.0497 6.0060 0.6672 0.7572 0.9664 0.1114 0.1260 90.0000 0.0000 0.0000 0.3973 0.0161 0.0151 0.0000 0.0000 0.0000
20 1 2962.9757 0.0253 0.0255 30.7113 2.2184 2.4652 0.9605 0.0537 0.0553 90.0000 0.0000 0.0000 0.4496 0.0338 0.0315 0.0000 0.0000 0.0000
