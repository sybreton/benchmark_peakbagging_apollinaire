# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:33:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0376 0.0548 0.0537 5.8449 0.6458 0.7022 1.0827 0.1227 0.1433 90.0000 0.0000 0.0000 0.4080 0.0192 0.0177 0.0000 0.0000 0.0000
20 1 2963.0145 0.0269 0.0269 29.0535 2.1195 2.2905 0.9985 0.0553 0.0580 90.0000 0.0000 0.0000 0.4509 0.0352 0.0325 0.0000 0.0000 0.0000
