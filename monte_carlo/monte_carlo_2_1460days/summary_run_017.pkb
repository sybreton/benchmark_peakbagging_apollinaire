# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:53:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9780 0.0515 0.0539 5.8889 0.6431 0.6958 1.0048 0.1091 0.1256 90.0000 0.0000 0.0000 0.3887 0.0196 0.0182 0.0000 0.0000 0.0000
20 1 2962.9900 0.0283 0.0272 27.4480 1.9438 2.1257 1.0740 0.0569 0.0618 90.0000 0.0000 0.0000 0.4164 0.0426 0.0379 0.0000 0.0000 0.0000
