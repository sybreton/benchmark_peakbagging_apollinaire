# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:13:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0054 0.0403 0.0375 7.4101 0.8634 0.9976 0.7928 0.0909 0.1091 90.0000 0.0000 0.0000 0.4201 0.0148 0.0135 0.0000 0.0000 0.0000
20 1 2962.9793 0.0262 0.0262 30.5989 2.2271 2.4407 0.9951 0.0550 0.0578 90.0000 0.0000 0.0000 0.4032 0.0368 0.0340 0.0000 0.0000 0.0000
