# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:03:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9787 0.0511 0.0498 6.8199 0.7270 0.8197 0.9218 0.0971 0.1119 90.0000 0.0000 0.0000 0.3899 0.0166 0.0159 0.0000 0.0000 0.0000
20 1 2962.9802 0.0269 0.0270 28.5196 2.1063 2.2749 1.0356 0.0574 0.0614 90.0000 0.0000 0.0000 0.3789 0.0443 0.0379 0.0000 0.0000 0.0000
