# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:29:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0070 0.0597 0.0645 5.1801 0.6106 0.7013 1.1816 0.1499 0.1716 90.0000 0.0000 0.0000 0.3855 0.0246 0.0222 0.0000 0.0000 0.0000
20 1 2963.0081 0.0253 0.0259 30.8158 2.2880 2.5921 0.9854 0.0544 0.0585 90.0000 0.0000 0.0000 0.3754 0.0407 0.0358 0.0000 0.0000 0.0000
