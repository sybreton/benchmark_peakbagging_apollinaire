# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:40:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0379 0.0749 0.0745 4.5102 0.5078 0.5980 1.3941 0.1734 0.1949 90.0000 0.0000 0.0000 0.4076 0.0285 0.0259 0.0000 0.0000 0.0000
20 1 2963.0086 0.0275 0.0284 24.6236 1.8863 2.0906 1.0935 0.0654 0.0694 90.0000 0.0000 0.0000 0.3501 0.0534 0.0446 0.0000 0.0000 0.0000
