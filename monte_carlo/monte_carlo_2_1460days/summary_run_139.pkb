# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:53:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9536 0.0441 0.0431 6.6861 0.7815 0.9230 0.8698 0.1074 0.1206 90.0000 0.0000 0.0000 0.3802 0.0153 0.0144 0.0000 0.0000 0.0000
20 1 2963.0081 0.0269 0.0265 30.0407 2.1253 2.4201 1.0198 0.0552 0.0585 90.0000 0.0000 0.0000 0.3933 0.0419 0.0367 0.0000 0.0000 0.0000
