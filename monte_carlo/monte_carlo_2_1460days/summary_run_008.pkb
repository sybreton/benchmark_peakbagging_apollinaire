# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:29:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9753 0.0446 0.0437 6.8301 0.8158 0.8914 0.8716 0.1033 0.1243 90.0000 0.0000 0.0000 0.3972 0.0169 0.0159 0.0000 0.0000 0.0000
20 1 2963.0106 0.0264 0.0266 28.5908 2.0837 2.3391 0.9933 0.0561 0.0588 90.0000 0.0000 0.0000 0.4182 0.0371 0.0340 0.0000 0.0000 0.0000
