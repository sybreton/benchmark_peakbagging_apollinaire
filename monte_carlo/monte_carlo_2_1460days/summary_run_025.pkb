# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:16:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9417 0.0504 0.0485 6.4311 0.6509 0.7330 0.9770 0.1023 0.1125 90.0000 0.0000 0.0000 0.4072 0.0157 0.0152 0.0000 0.0000 0.0000
20 1 2963.0241 0.0267 0.0274 28.3200 2.1281 2.2944 1.0369 0.0590 0.0650 90.0000 0.0000 0.0000 0.3389 0.0518 0.0431 0.0000 0.0000 0.0000
