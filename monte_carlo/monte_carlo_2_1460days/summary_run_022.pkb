# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:07:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0314 0.0495 0.0506 6.8297 0.6759 0.7817 0.8808 0.0889 0.0985 90.0000 0.0000 0.0000 0.4173 0.0160 0.0157 0.0000 0.0000 0.0000
20 1 2962.9881 0.0275 0.0275 31.1142 2.1229 2.3999 0.9910 0.0518 0.0524 90.0000 0.0000 0.0000 0.4622 0.0349 0.0316 0.0000 0.0000 0.0000
