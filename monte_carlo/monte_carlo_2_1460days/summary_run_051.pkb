# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:33:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0029 0.0498 0.0513 6.3552 0.6622 0.7473 0.9375 0.0991 0.1102 90.0000 0.0000 0.0000 0.4058 0.0158 0.0156 0.0000 0.0000 0.0000
20 1 2962.9787 0.0268 0.0266 28.3289 2.0821 2.3021 1.0383 0.0580 0.0621 90.0000 0.0000 0.0000 0.3691 0.0456 0.0399 0.0000 0.0000 0.0000
