# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:27:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9893 0.0597 0.0560 5.2634 0.6069 0.6660 1.1287 0.1338 0.1565 90.0000 0.0000 0.0000 0.4357 0.0178 0.0175 0.0000 0.0000 0.0000
20 1 2962.9827 0.0286 0.0283 27.0712 1.9508 2.1819 1.0747 0.0589 0.0638 90.0000 0.0000 0.0000 0.4066 0.0446 0.0403 0.0000 0.0000 0.0000
