# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:26:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9740 0.0537 0.0534 5.6483 0.6153 0.6913 1.0004 0.1131 0.1257 90.0000 0.0000 0.0000 0.3812 0.0185 0.0180 0.0000 0.0000 0.0000
20 1 2962.9945 0.0262 0.0264 31.4552 2.2808 2.5137 0.9618 0.0531 0.0558 90.0000 0.0000 0.0000 0.4213 0.0356 0.0322 0.0000 0.0000 0.0000
