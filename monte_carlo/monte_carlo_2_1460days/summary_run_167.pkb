# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:16:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0620 0.0559 0.0588 5.2089 0.6123 0.6771 1.0778 0.1280 0.1517 90.0000 0.0000 0.0000 0.3871 0.0204 0.0193 0.0000 0.0000 0.0000
20 1 2962.9889 0.0275 0.0270 28.8224 2.0890 2.2247 1.0024 0.0539 0.0576 90.0000 0.0000 0.0000 0.4277 0.0372 0.0342 0.0000 0.0000 0.0000
