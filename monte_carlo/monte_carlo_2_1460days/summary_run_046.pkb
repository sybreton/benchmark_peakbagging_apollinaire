# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:18:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0715 0.0548 0.0535 5.9294 0.6715 0.7565 1.0062 0.1163 0.1372 90.0000 0.0000 0.0000 0.3854 0.0202 0.0188 0.0000 0.0000 0.0000
20 1 2962.9838 0.0269 0.0267 29.7158 2.1533 2.2676 1.0240 0.0544 0.0606 90.0000 0.0000 0.0000 0.4304 0.0387 0.0340 0.0000 0.0000 0.0000
