# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:09:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0166 0.0500 0.0479 5.8945 0.7361 0.8028 1.0046 0.1262 0.1512 90.0000 0.0000 0.0000 0.4165 0.0183 0.0165 0.0000 0.0000 0.0000
20 1 2962.9885 0.0252 0.0255 32.0789 2.3507 2.5964 0.9522 0.0526 0.0545 90.0000 0.0000 0.0000 0.3962 0.0370 0.0337 0.0000 0.0000 0.0000
