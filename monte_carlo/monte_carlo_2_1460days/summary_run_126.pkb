# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:16:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0024 0.0520 0.0522 5.8864 0.6270 0.7008 1.0136 0.1080 0.1261 90.0000 0.0000 0.0000 0.4037 0.0167 0.0158 0.0000 0.0000 0.0000
20 1 2963.0023 0.0245 0.0250 36.1324 2.7856 2.9933 0.8766 0.0483 0.0512 90.0000 0.0000 0.0000 0.4153 0.0317 0.0289 0.0000 0.0000 0.0000
