# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:14:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0036 0.0515 0.0522 5.7380 0.6163 0.6747 0.9665 0.1040 0.1198 90.0000 0.0000 0.0000 0.3847 0.0159 0.0157 0.0000 0.0000 0.0000
20 1 2963.0308 0.0256 0.0250 33.4825 2.5054 2.7714 0.9342 0.0526 0.0560 90.0000 0.0000 0.0000 0.3795 0.0379 0.0327 0.0000 0.0000 0.0000
