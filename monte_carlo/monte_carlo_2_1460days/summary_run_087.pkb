# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:21:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0057 0.0559 0.0604 5.7770 0.6928 0.7889 1.0770 0.1380 0.1548 90.0000 0.0000 0.0000 0.3740 0.0227 0.0205 0.0000 0.0000 0.0000
20 1 2962.9966 0.0262 0.0257 31.1268 2.3051 2.5179 0.9686 0.0544 0.0571 90.0000 0.0000 0.0000 0.3871 0.0384 0.0339 0.0000 0.0000 0.0000
