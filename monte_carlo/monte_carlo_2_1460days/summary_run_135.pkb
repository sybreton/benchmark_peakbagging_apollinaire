# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:42:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9832 0.0559 0.0549 5.5195 0.6136 0.7019 1.0122 0.1164 0.1344 90.0000 0.0000 0.0000 0.4003 0.0201 0.0190 0.0000 0.0000 0.0000
20 1 2963.0354 0.0280 0.0273 28.1573 2.0590 2.2216 1.0370 0.0575 0.0621 90.0000 0.0000 0.0000 0.3730 0.0455 0.0403 0.0000 0.0000 0.0000
