# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:02:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0308 0.0515 0.0519 5.6443 0.6696 0.7602 1.0055 0.1260 0.1426 90.0000 0.0000 0.0000 0.4006 0.0185 0.0175 0.0000 0.0000 0.0000
20 1 2962.9424 0.0274 0.0269 27.5986 1.9710 2.2396 1.0494 0.0587 0.0610 90.0000 0.0000 0.0000 0.4279 0.0393 0.0340 0.0000 0.0000 0.0000
