# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:00:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0083 0.0582 0.0565 5.4787 0.6115 0.6590 1.0773 0.1190 0.1396 90.0000 0.0000 0.0000 0.4421 0.0189 0.0187 0.0000 0.0000 0.0000
20 1 2962.9972 0.0257 0.0261 31.3959 2.2751 2.5285 0.9728 0.0545 0.0556 90.0000 0.0000 0.0000 0.4045 0.0377 0.0333 0.0000 0.0000 0.0000
