# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:09:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9443 0.0489 0.0511 6.0059 0.6849 0.7643 1.0227 0.1209 0.1351 90.0000 0.0000 0.0000 0.4126 0.0196 0.0176 0.0000 0.0000 0.0000
20 1 2963.0196 0.0247 0.0255 32.1302 2.3959 2.6227 0.9418 0.0520 0.0547 90.0000 0.0000 0.0000 0.4211 0.0345 0.0326 0.0000 0.0000 0.0000
