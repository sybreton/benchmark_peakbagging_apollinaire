# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:06:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9919 0.0567 0.0557 4.8839 0.7007 0.7815 1.1469 0.1730 0.2182 90.0000 0.0000 0.0000 0.3842 0.0250 0.0215 0.0000 0.0000 0.0000
20 1 2962.9941 0.0272 0.0273 29.4787 2.1850 2.4225 0.9987 0.0568 0.0586 90.0000 0.0000 0.0000 0.3567 0.0448 0.0388 0.0000 0.0000 0.0000
