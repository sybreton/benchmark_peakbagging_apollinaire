# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:26:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9248 0.0481 0.0500 6.5989 0.7902 0.8787 0.9552 0.1174 0.1393 90.0000 0.0000 0.0000 0.4112 0.0170 0.0159 0.0000 0.0000 0.0000
20 1 2963.0464 0.0266 0.0261 28.3696 2.0838 2.2352 0.9612 0.0517 0.0574 90.0000 0.0000 0.0000 0.4648 0.0335 0.0305 0.0000 0.0000 0.0000
