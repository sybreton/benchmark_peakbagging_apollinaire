# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:48:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9474 0.0595 0.0585 5.5450 0.6481 0.7122 1.0920 0.1251 0.1511 90.0000 0.0000 0.0000 0.3892 0.0232 0.0209 0.0000 0.0000 0.0000
20 1 2963.0087 0.0274 0.0261 29.1622 2.1316 2.2640 0.9888 0.0533 0.0582 90.0000 0.0000 0.0000 0.4412 0.0351 0.0323 0.0000 0.0000 0.0000
