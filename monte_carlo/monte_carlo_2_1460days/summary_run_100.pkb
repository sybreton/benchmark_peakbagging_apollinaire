# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:01:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0422 0.0594 0.0598 5.2053 0.5827 0.6217 1.0992 0.1211 0.1460 90.0000 0.0000 0.0000 0.3979 0.0202 0.0183 0.0000 0.0000 0.0000
20 1 2963.0322 0.0273 0.0268 30.2082 2.2450 2.4725 0.9824 0.0547 0.0587 90.0000 0.0000 0.0000 0.3926 0.0405 0.0358 0.0000 0.0000 0.0000
