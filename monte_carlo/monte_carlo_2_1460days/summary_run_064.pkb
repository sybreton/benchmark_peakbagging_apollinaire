# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:12:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0553 0.0539 0.0572 5.8147 0.6453 0.6893 1.0107 0.1106 0.1306 90.0000 0.0000 0.0000 0.4121 0.0185 0.0174 0.0000 0.0000 0.0000
20 1 2962.9908 0.0258 0.0261 29.6687 2.2314 2.4159 1.0040 0.0569 0.0609 90.0000 0.0000 0.0000 0.3404 0.0479 0.0401 0.0000 0.0000 0.0000
