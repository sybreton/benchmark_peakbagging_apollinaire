# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:45:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0155 0.0600 0.0615 4.5793 0.5643 0.6671 1.2541 0.1684 0.1967 90.0000 0.0000 0.0000 0.3846 0.0224 0.0202 0.0000 0.0000 0.0000
20 1 2962.9829 0.0275 0.0275 27.0435 1.9611 2.1681 1.0355 0.0582 0.0614 90.0000 0.0000 0.0000 0.4534 0.0380 0.0341 0.0000 0.0000 0.0000
