# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:45:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0102 0.0554 0.0554 5.6261 0.6148 0.6757 1.0129 0.1105 0.1310 90.0000 0.0000 0.0000 0.4120 0.0182 0.0173 0.0000 0.0000 0.0000
20 1 2962.9782 0.0275 0.0269 28.9154 2.1436 2.2756 0.9997 0.0558 0.0585 90.0000 0.0000 0.0000 0.4221 0.0392 0.0354 0.0000 0.0000 0.0000
