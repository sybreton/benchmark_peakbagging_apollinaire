# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:01:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0555 0.0392 0.0388 6.8170 0.8658 0.9888 0.8578 0.1170 0.1396 90.0000 0.0000 0.0000 0.4137 0.0129 0.0124 0.0000 0.0000 0.0000
20 1 2963.0081 0.0281 0.0278 27.6337 2.0879 2.2406 1.0729 0.0623 0.0674 90.0000 0.0000 0.0000 0.3280 0.0577 0.0459 0.0000 0.0000 0.0000
