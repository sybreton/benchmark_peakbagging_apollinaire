# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:56:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9767 0.0476 0.0499 6.8839 0.6762 0.7839 0.9034 0.0911 0.0987 90.0000 0.0000 0.0000 0.3959 0.0145 0.0147 0.0000 0.0000 0.0000
20 1 2963.0109 0.0278 0.0272 26.9426 1.9633 2.2384 1.0570 0.0611 0.0644 90.0000 0.0000 0.0000 0.3787 0.0460 0.0407 0.0000 0.0000 0.0000
