# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:22:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0019 0.0463 0.0465 5.8395 0.7611 0.8182 0.9435 0.1230 0.1517 90.0000 0.0000 0.0000 0.4120 0.0186 0.0177 0.0000 0.0000 0.0000
20 1 2962.9851 0.0251 0.0251 32.8039 2.3734 2.7114 0.9377 0.0520 0.0548 90.0000 0.0000 0.0000 0.4276 0.0334 0.0310 0.0000 0.0000 0.0000
