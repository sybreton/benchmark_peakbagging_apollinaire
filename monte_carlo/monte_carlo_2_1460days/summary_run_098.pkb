# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:54:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0818 0.0690 0.0730 4.6136 0.5194 0.5740 1.2094 0.1357 0.1606 90.0000 0.0000 0.0000 0.3711 0.0253 0.0248 0.0000 0.0000 0.0000
20 1 2962.9630 0.0259 0.0260 30.6271 2.2473 2.5450 1.0085 0.0549 0.0613 90.0000 0.0000 0.0000 0.3586 0.0458 0.0376 0.0000 0.0000 0.0000
