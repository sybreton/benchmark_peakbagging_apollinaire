# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:49:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9823 0.0475 0.0467 6.4794 0.6718 0.7770 1.0127 0.1103 0.1245 90.0000 0.0000 0.0000 0.4072 0.0164 0.0160 0.0000 0.0000 0.0000
20 1 2962.9802 0.0266 0.0263 31.5842 2.3154 2.5124 0.9675 0.0530 0.0575 90.0000 0.0000 0.0000 0.3972 0.0392 0.0346 0.0000 0.0000 0.0000
