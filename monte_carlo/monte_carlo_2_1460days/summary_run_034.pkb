# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:42:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9446 0.0439 0.0445 6.8132 0.7979 0.8927 0.9046 0.1058 0.1241 90.0000 0.0000 0.0000 0.3924 0.0171 0.0154 0.0000 0.0000 0.0000
20 1 2963.0325 0.0268 0.0264 28.4132 2.0498 2.2128 1.0447 0.0575 0.0608 90.0000 0.0000 0.0000 0.4411 0.0385 0.0346 0.0000 0.0000 0.0000
