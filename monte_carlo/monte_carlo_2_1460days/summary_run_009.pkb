# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:32:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9682 0.0431 0.0439 6.6832 0.7921 0.9171 0.9368 0.1175 0.1377 90.0000 0.0000 0.0000 0.3781 0.0162 0.0152 0.0000 0.0000 0.0000
20 1 2963.0142 0.0248 0.0253 32.7404 2.4067 2.6953 0.9487 0.0531 0.0544 90.0000 0.0000 0.0000 0.4130 0.0347 0.0321 0.0000 0.0000 0.0000
