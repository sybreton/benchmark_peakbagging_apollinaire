# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:05:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9099 0.0510 0.0506 5.8191 0.6771 0.7645 1.0246 0.1209 0.1424 90.0000 0.0000 0.0000 0.3983 0.0200 0.0185 0.0000 0.0000 0.0000
20 1 2962.9866 0.0253 0.0245 31.7730 2.3115 2.5844 0.9968 0.0568 0.0580 90.0000 0.0000 0.0000 0.3462 0.0445 0.0380 0.0000 0.0000 0.0000
