# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:04:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8706 0.0613 0.0588 5.5177 0.6095 0.6723 1.1011 0.1219 0.1449 90.0000 0.0000 0.0000 0.4008 0.0220 0.0217 0.0000 0.0000 0.0000
20 1 2962.9705 0.0260 0.0273 30.7541 2.2448 2.5267 1.0082 0.0572 0.0593 90.0000 0.0000 0.0000 0.3569 0.0445 0.0400 0.0000 0.0000 0.0000
