# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:51:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0070 0.0494 0.0482 6.4766 0.7128 0.7791 0.9080 0.0993 0.1114 90.0000 0.0000 0.0000 0.3917 0.0176 0.0159 0.0000 0.0000 0.0000
20 1 2962.9600 0.0251 0.0257 32.0876 2.3253 2.6167 0.9799 0.0542 0.0559 90.0000 0.0000 0.0000 0.3306 0.0449 0.0396 0.0000 0.0000 0.0000
