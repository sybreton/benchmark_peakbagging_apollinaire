# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:33:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9784 0.0516 0.0506 6.2087 0.6620 0.7561 1.0356 0.1144 0.1288 90.0000 0.0000 0.0000 0.4071 0.0173 0.0170 0.0000 0.0000 0.0000
20 1 2963.0146 0.0258 0.0268 30.6623 2.1708 2.4175 1.0043 0.0553 0.0585 90.0000 0.0000 0.0000 0.4115 0.0381 0.0346 0.0000 0.0000 0.0000
