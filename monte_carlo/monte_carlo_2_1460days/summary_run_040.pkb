# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:01:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9624 0.0556 0.0558 5.9064 0.6480 0.7258 1.0538 0.1174 0.1364 90.0000 0.0000 0.0000 0.3933 0.0208 0.0186 0.0000 0.0000 0.0000
20 1 2963.0148 0.0274 0.0274 28.4978 2.1344 2.3075 1.0721 0.0606 0.0637 90.0000 0.0000 0.0000 0.3526 0.0499 0.0414 0.0000 0.0000 0.0000
