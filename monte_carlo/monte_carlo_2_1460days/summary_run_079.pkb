# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:57:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9747 0.0502 0.0508 5.9510 0.6911 0.7618 0.9858 0.1164 0.1407 90.0000 0.0000 0.0000 0.4003 0.0184 0.0169 0.0000 0.0000 0.0000
20 1 2962.9860 0.0270 0.0261 29.3498 2.1717 2.3140 1.0487 0.0578 0.0613 90.0000 0.0000 0.0000 0.3560 0.0466 0.0405 0.0000 0.0000 0.0000
