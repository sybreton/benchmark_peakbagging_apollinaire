# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:51:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0142 0.0569 0.0598 5.1818 0.6119 0.6641 1.0699 0.1236 0.1507 90.0000 0.0000 0.0000 0.3640 0.0227 0.0208 0.0000 0.0000 0.0000
20 1 2963.0521 0.0272 0.0264 28.7447 2.0904 2.3263 1.0166 0.0576 0.0597 90.0000 0.0000 0.0000 0.3990 0.0418 0.0360 0.0000 0.0000 0.0000
