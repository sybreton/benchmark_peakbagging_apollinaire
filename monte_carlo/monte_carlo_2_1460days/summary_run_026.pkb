# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:19:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0233 0.0527 0.0532 6.0765 0.6616 0.7242 1.0475 0.1145 0.1328 90.0000 0.0000 0.0000 0.3856 0.0197 0.0178 0.0000 0.0000 0.0000
20 1 2962.9734 0.0272 0.0275 29.9388 2.1347 2.3225 1.0468 0.0561 0.0594 90.0000 0.0000 0.0000 0.4006 0.0418 0.0366 0.0000 0.0000 0.0000
