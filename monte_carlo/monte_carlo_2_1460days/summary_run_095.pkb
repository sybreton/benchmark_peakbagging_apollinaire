# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:45:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9200 0.0387 0.0396 7.5919 0.8862 0.9668 0.8144 0.0930 0.1125 90.0000 0.0000 0.0000 0.4076 0.0133 0.0128 0.0000 0.0000 0.0000
20 1 2962.9881 0.0263 0.0269 29.6672 2.1060 2.3098 1.0165 0.0547 0.0586 90.0000 0.0000 0.0000 0.4381 0.0374 0.0340 0.0000 0.0000 0.0000
