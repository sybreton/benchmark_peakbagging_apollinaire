# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:36:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0086 0.0422 0.0407 7.3556 0.7731 0.8649 0.8691 0.0912 0.1030 90.0000 0.0000 0.0000 0.4237 0.0139 0.0135 0.0000 0.0000 0.0000
20 1 2962.9525 0.0265 0.0256 30.7110 2.2351 2.4012 0.9559 0.0516 0.0553 90.0000 0.0000 0.0000 0.4467 0.0350 0.0314 0.0000 0.0000 0.0000
