# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:28:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0320 0.0603 0.0585 5.6717 0.6451 0.7302 1.0953 0.1302 0.1461 90.0000 0.0000 0.0000 0.4020 0.0216 0.0203 0.0000 0.0000 0.0000
20 1 2962.9781 0.0258 0.0259 32.0644 2.3878 2.5439 0.9714 0.0522 0.0558 90.0000 0.0000 0.0000 0.3966 0.0364 0.0337 0.0000 0.0000 0.0000
