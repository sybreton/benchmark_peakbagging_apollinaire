# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:42:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9297 0.0525 0.0507 5.7817 0.6788 0.7299 1.0335 0.1212 0.1472 90.0000 0.0000 0.0000 0.4162 0.0188 0.0178 0.0000 0.0000 0.0000
20 1 2963.0003 0.0258 0.0256 28.8713 2.1326 2.3535 0.9885 0.0570 0.0599 90.0000 0.0000 0.0000 0.3809 0.0410 0.0358 0.0000 0.0000 0.0000
