# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:24:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0526 0.0428 0.0415 7.3243 0.8379 0.9126 0.8773 0.0950 0.1131 90.0000 0.0000 0.0000 0.4127 0.0144 0.0133 0.0000 0.0000 0.0000
20 1 2962.9717 0.0277 0.0272 26.3895 2.0481 2.1931 1.1000 0.0649 0.0705 90.0000 0.0000 0.0000 0.3344 0.0574 0.0451 0.0000 0.0000 0.0000
