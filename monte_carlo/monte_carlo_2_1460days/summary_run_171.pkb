# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:29:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9909 0.0556 0.0545 5.4932 0.5894 0.6521 1.0331 0.1134 0.1319 90.0000 0.0000 0.0000 0.3933 0.0184 0.0173 0.0000 0.0000 0.0000
20 1 2962.9819 0.0252 0.0248 33.7923 2.4951 2.7168 0.9323 0.0498 0.0534 90.0000 0.0000 0.0000 0.3953 0.0362 0.0327 0.0000 0.0000 0.0000
