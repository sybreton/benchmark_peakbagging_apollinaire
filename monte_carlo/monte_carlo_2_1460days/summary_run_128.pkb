# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:21:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0148 0.0558 0.0564 5.4257 0.6296 0.6906 1.0860 0.1298 0.1505 90.0000 0.0000 0.0000 0.3885 0.0220 0.0202 0.0000 0.0000 0.0000
20 1 2963.0288 0.0268 0.0262 30.2434 2.1615 2.3619 1.0290 0.0556 0.0566 90.0000 0.0000 0.0000 0.3774 0.0431 0.0374 0.0000 0.0000 0.0000
