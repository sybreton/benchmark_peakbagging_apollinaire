# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:38:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0491 0.0572 0.0555 5.3303 0.5936 0.6399 1.0467 0.1137 0.1334 90.0000 0.0000 0.0000 0.4044 0.0184 0.0183 0.0000 0.0000 0.0000
20 1 2962.9925 0.0259 0.0263 31.2426 2.2665 2.4021 0.9783 0.0527 0.0557 90.0000 0.0000 0.0000 0.4470 0.0349 0.0319 0.0000 0.0000 0.0000
