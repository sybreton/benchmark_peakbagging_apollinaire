# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:39:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9629 0.0458 0.0482 6.7215 0.6755 0.7874 0.9291 0.0978 0.1051 90.0000 0.0000 0.0000 0.4272 0.0157 0.0152 0.0000 0.0000 0.0000
20 1 2963.0070 0.0274 0.0269 29.3508 2.0781 2.3026 1.0666 0.0575 0.0613 90.0000 0.0000 0.0000 0.3916 0.0460 0.0387 0.0000 0.0000 0.0000
