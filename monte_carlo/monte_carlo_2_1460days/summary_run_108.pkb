# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:24:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0270 0.0576 0.0569 5.3789 0.6411 0.7265 1.0910 0.1347 0.1515 90.0000 0.0000 0.0000 0.4018 0.0232 0.0209 0.0000 0.0000 0.0000
20 1 2963.0034 0.0276 0.0265 28.7232 2.0638 2.2756 1.0339 0.0559 0.0591 90.0000 0.0000 0.0000 0.4207 0.0407 0.0357 0.0000 0.0000 0.0000
