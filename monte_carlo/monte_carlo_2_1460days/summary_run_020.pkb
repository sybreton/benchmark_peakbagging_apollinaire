# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:02:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9432 0.0539 0.0549 5.8846 0.6307 0.6892 0.9816 0.1064 0.1187 90.0000 0.0000 0.0000 0.4199 0.0193 0.0179 0.0000 0.0000 0.0000
20 1 2963.0103 0.0270 0.0279 28.7252 2.0600 2.1850 1.0561 0.0566 0.0617 90.0000 0.0000 0.0000 0.4152 0.0415 0.0370 0.0000 0.0000 0.0000
