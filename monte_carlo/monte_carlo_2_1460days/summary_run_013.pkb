# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:42:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0070 0.0577 0.0590 4.8688 0.5805 0.6717 1.2992 0.1679 0.1957 90.0000 0.0000 0.0000 0.3564 0.0249 0.0219 0.0000 0.0000 0.0000
20 1 2962.9934 0.0275 0.0276 25.7929 2.0494 2.1767 1.0956 0.0672 0.0729 90.0000 0.0000 0.0000 0.3915 0.0477 0.0408 0.0000 0.0000 0.0000
