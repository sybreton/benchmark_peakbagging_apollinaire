# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:17:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0154 0.0450 0.0446 6.7834 0.7579 0.8259 0.8022 0.0863 0.1035 90.0000 0.0000 0.0000 0.4093 0.0147 0.0141 0.0000 0.0000 0.0000
20 1 2963.0319 0.0263 0.0269 30.3718 2.1420 2.4122 0.9465 0.0509 0.0546 90.0000 0.0000 0.0000 0.4514 0.0339 0.0311 0.0000 0.0000 0.0000
