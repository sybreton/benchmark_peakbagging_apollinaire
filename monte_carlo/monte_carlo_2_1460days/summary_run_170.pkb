# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:26:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9152 0.0436 0.0439 6.7844 0.7572 0.8503 0.8609 0.0974 0.1097 90.0000 0.0000 0.0000 0.3770 0.0147 0.0144 0.0000 0.0000 0.0000
20 1 2963.0340 0.0252 0.0258 34.4073 2.5062 2.7964 0.9311 0.0521 0.0539 90.0000 0.0000 0.0000 0.4212 0.0328 0.0315 0.0000 0.0000 0.0000
