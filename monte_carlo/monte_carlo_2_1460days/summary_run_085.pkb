# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:15:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9798 0.0611 0.0592 5.0273 0.5908 0.6579 1.2772 0.1604 0.1844 90.0000 0.0000 0.0000 0.3694 0.0239 0.0230 0.0000 0.0000 0.0000
20 1 2963.0099 0.0262 0.0273 29.8646 2.1428 2.2494 1.0439 0.0545 0.0589 90.0000 0.0000 0.0000 0.3965 0.0429 0.0379 0.0000 0.0000 0.0000
