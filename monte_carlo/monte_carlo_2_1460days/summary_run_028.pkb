# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:25:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9941 0.0408 0.0412 7.0705 0.8230 0.9300 0.8915 0.1070 0.1185 90.0000 0.0000 0.0000 0.4264 0.0146 0.0140 0.0000 0.0000 0.0000
20 1 2963.0541 0.0259 0.0260 31.2697 2.2405 2.4780 0.9310 0.0507 0.0525 90.0000 0.0000 0.0000 0.4423 0.0325 0.0303 0.0000 0.0000 0.0000
