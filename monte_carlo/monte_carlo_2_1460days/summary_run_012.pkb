# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:39:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0163 0.0459 0.0475 7.0435 0.7319 0.8208 0.8814 0.0912 0.1010 90.0000 0.0000 0.0000 0.3960 0.0152 0.0148 0.0000 0.0000 0.0000
20 1 2962.9716 0.0244 0.0249 33.1640 2.4893 2.6697 0.9082 0.0509 0.0515 90.0000 0.0000 0.0000 0.4374 0.0315 0.0293 0.0000 0.0000 0.0000
