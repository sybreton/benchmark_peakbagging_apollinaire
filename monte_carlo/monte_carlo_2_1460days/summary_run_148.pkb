# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:19:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9703 0.0462 0.0449 6.6249 0.7251 0.8398 0.9021 0.0997 0.1131 90.0000 0.0000 0.0000 0.4155 0.0160 0.0148 0.0000 0.0000 0.0000
20 1 2962.9855 0.0264 0.0258 28.0049 2.0933 2.2398 1.0196 0.0579 0.0623 90.0000 0.0000 0.0000 0.4547 0.0372 0.0334 0.0000 0.0000 0.0000
