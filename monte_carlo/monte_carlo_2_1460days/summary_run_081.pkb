# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:03:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9900 0.0493 0.0502 6.4893 0.7833 0.9226 0.8872 0.1137 0.1298 90.0000 0.0000 0.0000 0.3888 0.0182 0.0170 0.0000 0.0000 0.0000
20 1 2963.0009 0.0260 0.0260 30.4058 2.1478 2.3919 0.9210 0.0501 0.0523 90.0000 0.0000 0.0000 0.4525 0.0324 0.0306 0.0000 0.0000 0.0000
