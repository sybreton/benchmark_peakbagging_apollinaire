# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:30:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0678 0.0595 0.0589 5.3412 0.6006 0.6570 1.1641 0.1316 0.1560 90.0000 0.0000 0.0000 0.3857 0.0217 0.0203 0.0000 0.0000 0.0000
20 1 2963.0112 0.0258 0.0259 28.4920 2.1175 2.3807 1.0253 0.0588 0.0622 90.0000 0.0000 0.0000 0.2607 0.0683 0.0521 0.0000 0.0000 0.0000
