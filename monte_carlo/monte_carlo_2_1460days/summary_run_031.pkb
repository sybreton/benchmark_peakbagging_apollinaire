# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:33:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0092 0.0515 0.0545 6.3463 0.6699 0.7274 1.0046 0.1050 0.1238 90.0000 0.0000 0.0000 0.4288 0.0202 0.0177 0.0000 0.0000 0.0000
20 1 2963.0250 0.0252 0.0259 32.1765 2.3103 2.5858 0.9429 0.0514 0.0527 90.0000 0.0000 0.0000 0.4331 0.0341 0.0316 0.0000 0.0000 0.0000
