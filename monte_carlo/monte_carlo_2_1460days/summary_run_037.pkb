# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:51:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0262 0.0600 0.0578 5.6982 0.6428 0.6899 1.0902 0.1228 0.1430 90.0000 0.0000 0.0000 0.4282 0.0214 0.0203 0.0000 0.0000 0.0000
20 1 2963.0227 0.0261 0.0254 29.9707 2.1504 2.3672 0.9788 0.0528 0.0561 90.0000 0.0000 0.0000 0.4578 0.0353 0.0316 0.0000 0.0000 0.0000
