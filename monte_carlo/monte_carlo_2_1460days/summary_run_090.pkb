# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:30:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0139 0.0443 0.0427 6.7606 0.7552 0.8339 0.8859 0.0999 0.1153 90.0000 0.0000 0.0000 0.4208 0.0155 0.0145 0.0000 0.0000 0.0000
20 1 2963.0148 0.0262 0.0265 31.0901 2.2400 2.4062 0.9917 0.0532 0.0571 90.0000 0.0000 0.0000 0.4046 0.0398 0.0341 0.0000 0.0000 0.0000
