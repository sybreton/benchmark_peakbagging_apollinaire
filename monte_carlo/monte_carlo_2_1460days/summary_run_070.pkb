# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:30:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0875 0.0490 0.0480 6.2722 0.6602 0.7626 0.9745 0.1065 0.1228 90.0000 0.0000 0.0000 0.3784 0.0187 0.0170 0.0000 0.0000 0.0000
20 1 2962.9955 0.0281 0.0285 25.1411 1.9163 2.0728 1.0883 0.0648 0.0667 90.0000 0.0000 0.0000 0.4508 0.0419 0.0367 0.0000 0.0000 0.0000
