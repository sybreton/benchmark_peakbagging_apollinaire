# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:37:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0754 0.0604 0.0590 5.4059 0.5950 0.6801 1.1099 0.1289 0.1461 90.0000 0.0000 0.0000 0.3931 0.0224 0.0208 0.0000 0.0000 0.0000
20 1 2962.9886 0.0278 0.0271 26.8894 1.9321 2.1136 1.0606 0.0568 0.0618 90.0000 0.0000 0.0000 0.4125 0.0429 0.0378 0.0000 0.0000 0.0000
