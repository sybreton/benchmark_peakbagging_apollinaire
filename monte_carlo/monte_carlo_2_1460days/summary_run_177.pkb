# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:47:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0367 0.0470 0.0462 6.4632 0.6838 0.7831 0.9220 0.1014 0.1107 90.0000 0.0000 0.0000 0.4107 0.0162 0.0154 0.0000 0.0000 0.0000
20 1 2962.9787 0.0254 0.0263 30.0792 2.1317 2.3373 0.9889 0.0540 0.0564 90.0000 0.0000 0.0000 0.4630 0.0341 0.0310 0.0000 0.0000 0.0000
