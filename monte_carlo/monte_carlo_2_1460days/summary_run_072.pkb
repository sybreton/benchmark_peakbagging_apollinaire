# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:36:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9774 0.0562 0.0548 5.9035 0.6090 0.6711 1.0326 0.1057 0.1221 90.0000 0.0000 0.0000 0.3922 0.0182 0.0177 0.0000 0.0000 0.0000
20 1 2962.9554 0.0266 0.0260 29.7017 2.2636 2.4167 1.0492 0.0578 0.0642 90.0000 0.0000 0.0000 0.3023 0.0580 0.0481 0.0000 0.0000 0.0000
