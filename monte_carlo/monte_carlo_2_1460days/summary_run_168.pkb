# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:19:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0783 0.0514 0.0495 5.9225 0.6850 0.7779 0.9841 0.1187 0.1351 90.0000 0.0000 0.0000 0.4080 0.0190 0.0170 0.0000 0.0000 0.0000
20 1 2962.9435 0.0261 0.0274 29.2670 2.1317 2.3322 1.0282 0.0563 0.0601 90.0000 0.0000 0.0000 0.3774 0.0420 0.0383 0.0000 0.0000 0.0000
