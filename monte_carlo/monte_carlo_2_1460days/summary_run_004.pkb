# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:18:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0184 0.0466 0.0478 6.4982 0.7255 0.8144 0.9757 0.1141 0.1290 90.0000 0.0000 0.0000 0.4288 0.0170 0.0156 0.0000 0.0000 0.0000
20 1 2963.0186 0.0250 0.0253 31.5541 2.3510 2.6254 0.9799 0.0548 0.0589 90.0000 0.0000 0.0000 0.4419 0.0349 0.0318 0.0000 0.0000 0.0000
