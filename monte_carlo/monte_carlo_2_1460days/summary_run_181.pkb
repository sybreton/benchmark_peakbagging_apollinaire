# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:59:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9943 0.0541 0.0542 5.7604 0.6083 0.6747 1.0801 0.1184 0.1343 90.0000 0.0000 0.0000 0.4113 0.0184 0.0177 0.0000 0.0000 0.0000
20 1 2962.9879 0.0276 0.0269 26.3208 1.9841 2.0974 1.0709 0.0605 0.0649 90.0000 0.0000 0.0000 0.3738 0.0476 0.0408 0.0000 0.0000 0.0000
