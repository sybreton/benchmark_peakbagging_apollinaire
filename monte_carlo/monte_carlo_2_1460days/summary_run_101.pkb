# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:04:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9777 0.0564 0.0513 5.5481 0.6420 0.7020 1.0835 0.1266 0.1535 90.0000 0.0000 0.0000 0.4206 0.0199 0.0177 0.0000 0.0000 0.0000
20 1 2962.9889 0.0275 0.0270 32.0639 2.3015 2.5374 0.9733 0.0518 0.0539 90.0000 0.0000 0.0000 0.4320 0.0356 0.0338 0.0000 0.0000 0.0000
