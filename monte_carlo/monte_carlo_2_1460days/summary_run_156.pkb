# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:43:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9656 0.0582 0.0595 5.1837 0.5634 0.6285 1.1465 0.1285 0.1485 90.0000 0.0000 0.0000 0.4069 0.0202 0.0195 0.0000 0.0000 0.0000
20 1 2962.9797 0.0247 0.0252 32.4424 2.4211 2.6886 0.9535 0.0540 0.0572 90.0000 0.0000 0.0000 0.4008 0.0371 0.0328 0.0000 0.0000 0.0000
