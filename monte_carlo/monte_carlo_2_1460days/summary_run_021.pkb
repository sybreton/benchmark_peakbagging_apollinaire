# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:05:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0033 0.0488 0.0521 6.2000 0.6834 0.7912 0.9582 0.1089 0.1261 90.0000 0.0000 0.0000 0.3931 0.0180 0.0171 0.0000 0.0000 0.0000
20 1 2962.9794 0.0269 0.0270 29.5766 2.1181 2.2717 1.0041 0.0534 0.0564 90.0000 0.0000 0.0000 0.4355 0.0363 0.0333 0.0000 0.0000 0.0000
