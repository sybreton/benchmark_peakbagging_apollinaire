# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:18:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0388 0.0524 0.0504 5.9589 0.7037 0.7618 0.9870 0.1205 0.1351 90.0000 0.0000 0.0000 0.4068 0.0204 0.0188 0.0000 0.0000 0.0000
20 1 2962.9891 0.0262 0.0253 30.4951 2.2394 2.4807 0.9984 0.0558 0.0593 90.0000 0.0000 0.0000 0.4287 0.0375 0.0325 0.0000 0.0000 0.0000
