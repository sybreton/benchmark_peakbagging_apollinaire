# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:39:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8792 0.0536 0.0530 5.7068 0.6592 0.7217 0.9964 0.1154 0.1460 90.0000 0.0000 0.0000 0.4307 0.0180 0.0171 0.0000 0.0000 0.0000
20 1 2963.0796 0.0268 0.0274 27.8983 2.1295 2.2698 1.0024 0.0574 0.0619 90.0000 0.0000 0.0000 0.3391 0.0477 0.0402 0.0000 0.0000 0.0000
