# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:54:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9650 0.0611 0.0580 5.7150 0.6012 0.6786 1.1592 0.1283 0.1446 90.0000 0.0000 0.0000 0.3840 0.0231 0.0215 0.0000 0.0000 0.0000
20 1 2962.9678 0.0264 0.0271 29.3664 2.1645 2.3338 1.0010 0.0548 0.0569 90.0000 0.0000 0.0000 0.4048 0.0392 0.0358 0.0000 0.0000 0.0000
