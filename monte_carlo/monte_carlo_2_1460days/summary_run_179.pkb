# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:53:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9799 0.0483 0.0489 6.2350 0.6936 0.7870 0.9091 0.1022 0.1167 90.0000 0.0000 0.0000 0.3891 0.0165 0.0160 0.0000 0.0000 0.0000
20 1 2962.9924 0.0261 0.0258 28.4487 2.1153 2.3719 1.0184 0.0583 0.0623 90.0000 0.0000 0.0000 0.4042 0.0388 0.0354 0.0000 0.0000 0.0000
