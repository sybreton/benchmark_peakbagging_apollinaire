# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:58:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0042 0.0527 0.0515 6.1522 0.6253 0.7088 1.0071 0.1038 0.1182 90.0000 0.0000 0.0000 0.4196 0.0168 0.0176 0.0000 0.0000 0.0000
20 1 2963.0204 0.0251 0.0255 31.1930 2.3048 2.4919 0.9487 0.0521 0.0551 90.0000 0.0000 0.0000 0.3847 0.0372 0.0338 0.0000 0.0000 0.0000
