# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:53:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0230 0.0469 0.0449 6.7148 0.7654 0.8678 0.9217 0.1065 0.1257 90.0000 0.0000 0.0000 0.3795 0.0165 0.0153 0.0000 0.0000 0.0000
20 1 2962.9842 0.0289 0.0285 24.3920 1.8884 2.1302 1.1402 0.0718 0.0755 90.0000 0.0000 0.0000 0.3294 0.0641 0.0507 0.0000 0.0000 0.0000
