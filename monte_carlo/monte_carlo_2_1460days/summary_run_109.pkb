# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:27:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9469 0.0430 0.0425 6.9892 0.7806 0.8499 0.8504 0.0931 0.1096 90.0000 0.0000 0.0000 0.4202 0.0143 0.0138 0.0000 0.0000 0.0000
20 1 2962.9586 0.0270 0.0270 28.2759 2.0586 2.2616 1.0007 0.0548 0.0574 90.0000 0.0000 0.0000 0.4555 0.0358 0.0320 0.0000 0.0000 0.0000
