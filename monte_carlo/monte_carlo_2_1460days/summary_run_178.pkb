# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:50:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9980 0.0594 0.0571 5.2965 0.6085 0.6981 1.1565 0.1395 0.1599 90.0000 0.0000 0.0000 0.3738 0.0238 0.0212 0.0000 0.0000 0.0000
20 1 2962.9847 0.0271 0.0269 28.5769 2.0447 2.2633 0.9967 0.0548 0.0576 90.0000 0.0000 0.0000 0.4191 0.0390 0.0354 0.0000 0.0000 0.0000
