# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:51:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9903 0.0496 0.0507 6.3183 0.6932 0.7819 1.0048 0.1123 0.1309 90.0000 0.0000 0.0000 0.4146 0.0179 0.0169 0.0000 0.0000 0.0000
20 1 2963.0095 0.0268 0.0263 29.0283 2.0890 2.2130 0.9860 0.0525 0.0568 90.0000 0.0000 0.0000 0.4300 0.0366 0.0338 0.0000 0.0000 0.0000
