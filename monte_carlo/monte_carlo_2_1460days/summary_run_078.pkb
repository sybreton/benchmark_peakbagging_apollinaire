# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:54:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0005 0.0606 0.0610 4.8679 0.6367 0.7329 1.3046 0.1851 0.2169 90.0000 0.0000 0.0000 0.3765 0.0299 0.0246 0.0000 0.0000 0.0000
20 1 2963.0201 0.0245 0.0237 34.6761 2.6530 2.8732 0.9319 0.0510 0.0548 90.0000 0.0000 0.0000 0.3869 0.0351 0.0322 0.0000 0.0000 0.0000
