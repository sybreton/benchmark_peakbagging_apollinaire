# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:10:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0338 0.0563 0.0597 5.1902 0.6096 0.6808 1.1962 0.1479 0.1663 90.0000 0.0000 0.0000 0.3801 0.0235 0.0216 0.0000 0.0000 0.0000
20 1 2963.0281 0.0268 0.0266 30.0715 2.1860 2.4953 1.0434 0.0585 0.0617 90.0000 0.0000 0.0000 0.3582 0.0474 0.0412 0.0000 0.0000 0.0000
