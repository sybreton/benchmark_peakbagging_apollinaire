# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:05:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9844 0.0472 0.0496 6.4755 0.7357 0.8648 0.9500 0.1166 0.1272 90.0000 0.0000 0.0000 0.4047 0.0188 0.0167 0.0000 0.0000 0.0000
20 1 2963.0195 0.0261 0.0266 29.5664 2.1775 2.4060 0.9977 0.0574 0.0596 90.0000 0.0000 0.0000 0.3494 0.0468 0.0387 0.0000 0.0000 0.0000
