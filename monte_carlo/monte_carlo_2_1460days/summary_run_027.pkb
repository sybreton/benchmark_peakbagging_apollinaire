# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:22:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9713 0.0463 0.0439 6.3748 0.7117 0.8031 0.9281 0.1074 0.1255 90.0000 0.0000 0.0000 0.4018 0.0153 0.0151 0.0000 0.0000 0.0000
20 1 2962.9845 0.0259 0.0254 31.7826 2.2983 2.4783 0.9526 0.0512 0.0531 90.0000 0.0000 0.0000 0.4171 0.0357 0.0330 0.0000 0.0000 0.0000
