# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:15:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9672 0.0511 0.0514 6.1540 0.6812 0.7676 0.9928 0.1143 0.1320 90.0000 0.0000 0.0000 0.3857 0.0182 0.0164 0.0000 0.0000 0.0000
20 1 2963.0575 0.0255 0.0252 33.3574 2.4660 2.7950 0.9335 0.0530 0.0534 90.0000 0.0000 0.0000 0.3733 0.0369 0.0338 0.0000 0.0000 0.0000
