# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:57:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0568 0.0601 0.0616 4.9421 0.5565 0.6384 1.2524 0.1517 0.1681 90.0000 0.0000 0.0000 0.3601 0.0232 0.0214 0.0000 0.0000 0.0000
20 1 2963.0229 0.0256 0.0259 29.3870 2.1704 2.3985 0.9934 0.0561 0.0588 90.0000 0.0000 0.0000 0.3853 0.0401 0.0350 0.0000 0.0000 0.0000
