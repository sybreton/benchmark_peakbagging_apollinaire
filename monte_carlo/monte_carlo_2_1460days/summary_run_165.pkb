# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:10:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9809 0.0623 0.0599 5.3196 0.5765 0.6120 1.1344 0.1220 0.1409 90.0000 0.0000 0.0000 0.4071 0.0205 0.0198 0.0000 0.0000 0.0000
20 1 2962.9860 0.0268 0.0264 30.0825 2.1409 2.3940 0.9935 0.0533 0.0568 90.0000 0.0000 0.0000 0.4246 0.0367 0.0332 0.0000 0.0000 0.0000
