# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:06:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9562 0.0483 0.0500 6.6113 0.6613 0.7593 1.0283 0.1056 0.1165 90.0000 0.0000 0.0000 0.3753 0.0169 0.0162 0.0000 0.0000 0.0000
20 1 2963.0141 0.0266 0.0263 29.6049 2.2324 2.4296 1.0362 0.0576 0.0635 90.0000 0.0000 0.0000 0.3001 0.0587 0.0476 0.0000 0.0000 0.0000
