# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:27:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9943 0.0451 0.0443 6.6480 0.7083 0.7865 0.9406 0.1006 0.1165 90.0000 0.0000 0.0000 0.4293 0.0140 0.0137 0.0000 0.0000 0.0000
20 1 2963.0343 0.0257 0.0257 31.6532 2.3115 2.5000 0.9752 0.0529 0.0561 90.0000 0.0000 0.0000 0.4032 0.0378 0.0344 0.0000 0.0000 0.0000
