# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:06:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0480 0.0541 0.0550 5.8041 0.6777 0.7403 1.0094 0.1181 0.1381 90.0000 0.0000 0.0000 0.4169 0.0186 0.0169 0.0000 0.0000 0.0000
20 1 2963.0261 0.0277 0.0283 27.5569 2.0192 2.1868 1.0518 0.0580 0.0615 90.0000 0.0000 0.0000 0.4014 0.0434 0.0383 0.0000 0.0000 0.0000
