# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:24:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0052 0.0584 0.0593 5.0375 0.5934 0.6638 1.2852 0.1625 0.1855 90.0000 0.0000 0.0000 0.3899 0.0251 0.0223 0.0000 0.0000 0.0000
20 1 2963.0149 0.0284 0.0281 26.5076 1.9443 2.1267 1.0948 0.0615 0.0666 90.0000 0.0000 0.0000 0.4136 0.0455 0.0400 0.0000 0.0000 0.0000
