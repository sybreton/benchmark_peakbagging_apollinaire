# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:18:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0062 0.0429 0.0444 6.7267 0.7962 0.8832 0.9816 0.1196 0.1404 90.0000 0.0000 0.0000 0.4207 0.0179 0.0159 0.0000 0.0000 0.0000
20 1 2963.0511 0.0239 0.0240 37.9166 2.8979 3.2898 0.8869 0.0499 0.0522 90.0000 0.0000 0.0000 0.4086 0.0324 0.0306 0.0000 0.0000 0.0000
