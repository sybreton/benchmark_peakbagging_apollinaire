# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:22:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0152 0.0541 0.0520 5.8130 0.6148 0.6907 0.9612 0.1044 0.1207 90.0000 0.0000 0.0000 0.4024 0.0167 0.0160 0.0000 0.0000 0.0000
20 1 2962.9795 0.0251 0.0261 32.8500 2.3769 2.6381 0.9447 0.0514 0.0536 90.0000 0.0000 0.0000 0.4240 0.0342 0.0317 0.0000 0.0000 0.0000
