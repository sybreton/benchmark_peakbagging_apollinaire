# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:45:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0114 0.0468 0.0463 5.9341 0.6712 0.7492 0.9004 0.1014 0.1208 90.0000 0.0000 0.0000 0.4195 0.0156 0.0149 0.0000 0.0000 0.0000
20 1 2962.9893 0.0283 0.0270 25.0004 1.9460 2.1408 1.1171 0.0676 0.0727 90.0000 0.0000 0.0000 0.3461 0.0560 0.0448 0.0000 0.0000 0.0000
