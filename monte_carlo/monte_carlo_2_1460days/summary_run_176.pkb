# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:44:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9591 0.0489 0.0482 6.7416 0.6823 0.7907 0.8924 0.0926 0.0974 90.0000 0.0000 0.0000 0.3802 0.0144 0.0144 0.0000 0.0000 0.0000
20 1 2962.9958 0.0253 0.0258 30.6916 2.2918 2.5680 0.9655 0.0559 0.0599 90.0000 0.0000 0.0000 0.3847 0.0387 0.0346 0.0000 0.0000 0.0000
