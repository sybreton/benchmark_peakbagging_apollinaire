# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:55:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9866 0.0528 0.0540 6.1803 0.6311 0.6843 0.9668 0.0976 0.1098 90.0000 0.0000 0.0000 0.3729 0.0186 0.0173 0.0000 0.0000 0.0000
20 1 2962.9782 0.0266 0.0266 31.3925 2.2655 2.4372 0.9666 0.0510 0.0549 90.0000 0.0000 0.0000 0.4485 0.0334 0.0321 0.0000 0.0000 0.0000
