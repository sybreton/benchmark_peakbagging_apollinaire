# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:09:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0626 0.0460 0.0462 6.4977 0.6924 0.7994 0.9503 0.1070 0.1226 90.0000 0.0000 0.0000 0.4037 0.0171 0.0156 0.0000 0.0000 0.0000
20 1 2963.0214 0.0259 0.0265 31.4201 2.2969 2.5509 0.9978 0.0557 0.0584 90.0000 0.0000 0.0000 0.3541 0.0440 0.0394 0.0000 0.0000 0.0000
