# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:59:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.8925 0.0581 0.0598 4.9334 0.6191 0.7168 1.1527 0.1514 0.1745 90.0000 0.0000 0.0000 0.3977 0.0228 0.0209 0.0000 0.0000 0.0000
20 1 2963.0181 0.0271 0.0269 31.4691 2.2362 2.4681 0.9394 0.0502 0.0536 90.0000 0.0000 0.0000 0.4191 0.0362 0.0341 0.0000 0.0000 0.0000
