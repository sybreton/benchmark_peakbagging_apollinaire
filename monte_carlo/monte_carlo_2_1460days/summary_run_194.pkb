# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:39:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9867 0.0525 0.0520 5.8699 0.5949 0.6701 1.0166 0.1050 0.1220 90.0000 0.0000 0.0000 0.4161 0.0166 0.0163 0.0000 0.0000 0.0000
20 1 2962.9932 0.0273 0.0270 28.6163 2.1278 2.2571 1.0498 0.0577 0.0631 90.0000 0.0000 0.0000 0.3748 0.0446 0.0395 0.0000 0.0000 0.0000
