# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:15:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0473 0.0587 0.0599 5.3118 0.5780 0.6395 1.1021 0.1231 0.1447 90.0000 0.0000 0.0000 0.4060 0.0195 0.0184 0.0000 0.0000 0.0000
20 1 2963.0573 0.0249 0.0245 34.9205 2.5389 2.8298 0.9109 0.0490 0.0524 90.0000 0.0000 0.0000 0.4028 0.0348 0.0305 0.0000 0.0000 0.0000
