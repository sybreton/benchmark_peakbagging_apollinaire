# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:15:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9549 0.0533 0.0502 5.6204 0.6570 0.7347 1.0821 0.1325 0.1541 90.0000 0.0000 0.0000 0.3660 0.0204 0.0181 0.0000 0.0000 0.0000
20 1 2963.0409 0.0237 0.0233 37.5407 2.9181 3.1971 0.8759 0.0489 0.0535 90.0000 0.0000 0.0000 0.3873 0.0333 0.0302 0.0000 0.0000 0.0000
