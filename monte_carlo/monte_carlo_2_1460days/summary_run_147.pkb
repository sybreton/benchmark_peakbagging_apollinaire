# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:16:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0641 0.0541 0.0554 5.4605 0.5857 0.6523 0.9925 0.1072 0.1232 90.0000 0.0000 0.0000 0.3620 0.0173 0.0176 0.0000 0.0000 0.0000
20 1 2963.0034 0.0264 0.0267 29.7799 2.1973 2.3671 1.0290 0.0563 0.0601 90.0000 0.0000 0.0000 0.3491 0.0470 0.0396 0.0000 0.0000 0.0000
