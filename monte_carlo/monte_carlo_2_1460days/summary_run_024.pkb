# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 09:13:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0052 0.0518 0.0524 5.9912 0.6463 0.7079 0.9666 0.1058 0.1210 90.0000 0.0000 0.0000 0.3876 0.0173 0.0165 0.0000 0.0000 0.0000
20 1 2963.0322 0.0256 0.0259 29.5334 2.2381 2.3988 0.9852 0.0559 0.0602 90.0000 0.0000 0.0000 0.3438 0.0459 0.0381 0.0000 0.0000 0.0000
