# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:38:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0319 0.0529 0.0517 5.8327 0.6155 0.6800 0.9810 0.1043 0.1210 90.0000 0.0000 0.0000 0.4271 0.0162 0.0162 0.0000 0.0000 0.0000
20 1 2962.9623 0.0271 0.0266 29.3303 2.1548 2.3634 1.0437 0.0569 0.0611 90.0000 0.0000 0.0000 0.3560 0.0484 0.0413 0.0000 0.0000 0.0000
