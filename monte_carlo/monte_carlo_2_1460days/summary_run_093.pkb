# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 12:39:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9430 0.0535 0.0505 5.9445 0.7296 0.8181 0.9813 0.1242 0.1451 90.0000 0.0000 0.0000 0.3970 0.0217 0.0200 0.0000 0.0000 0.0000
20 1 2963.0275 0.0248 0.0248 33.0629 2.3504 2.7232 0.9533 0.0525 0.0538 90.0000 0.0000 0.0000 0.4533 0.0335 0.0310 0.0000 0.0000 0.0000
