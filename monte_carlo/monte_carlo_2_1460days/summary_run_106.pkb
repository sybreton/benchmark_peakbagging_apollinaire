# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:18:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9759 0.0490 0.0487 6.4720 0.6769 0.7639 0.9885 0.1036 0.1209 90.0000 0.0000 0.0000 0.3899 0.0161 0.0155 0.0000 0.0000 0.0000
20 1 2962.9945 0.0244 0.0241 35.1485 2.7098 2.9375 0.8763 0.0505 0.0527 90.0000 0.0000 0.0000 0.4438 0.0300 0.0277 0.0000 0.0000 0.0000
