# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:54:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9608 0.0452 0.0465 6.2786 0.7692 0.8270 0.8790 0.1080 0.1318 90.0000 0.0000 0.0000 0.4198 0.0165 0.0153 0.0000 0.0000 0.0000
20 1 2963.0083 0.0268 0.0265 29.0216 2.1941 2.4142 0.9980 0.0578 0.0619 90.0000 0.0000 0.0000 0.4001 0.0392 0.0352 0.0000 0.0000 0.0000
