# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 11:00:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0345 0.0520 0.0517 5.7724 0.6414 0.6902 1.0627 0.1196 0.1394 90.0000 0.0000 0.0000 0.3899 0.0180 0.0172 0.0000 0.0000 0.0000
20 1 2963.0108 0.0267 0.0268 30.5414 2.2468 2.4456 1.0223 0.0548 0.0602 90.0000 0.0000 0.0000 0.3924 0.0412 0.0365 0.0000 0.0000 0.0000
