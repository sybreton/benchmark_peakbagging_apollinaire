# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 15:37:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9419 0.0570 0.0588 5.4931 0.6109 0.6504 1.0600 0.1174 0.1370 90.0000 0.0000 0.0000 0.3924 0.0200 0.0181 0.0000 0.0000 0.0000
20 1 2962.9890 0.0268 0.0267 28.8108 2.1383 2.3940 1.0025 0.0570 0.0606 90.0000 0.0000 0.0000 0.3591 0.0447 0.0382 0.0000 0.0000 0.0000
