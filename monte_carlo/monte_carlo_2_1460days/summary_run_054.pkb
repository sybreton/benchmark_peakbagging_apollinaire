# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:42:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0547 0.0553 0.0529 5.6513 0.6371 0.6987 1.0249 0.1167 0.1354 90.0000 0.0000 0.0000 0.4001 0.0190 0.0182 0.0000 0.0000 0.0000
20 1 2962.9813 0.0272 0.0277 28.7867 2.0894 2.2771 1.0126 0.0543 0.0595 90.0000 0.0000 0.0000 0.4037 0.0398 0.0366 0.0000 0.0000 0.0000
