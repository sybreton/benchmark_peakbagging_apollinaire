# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 08:23:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0651 0.0529 0.0525 6.0690 0.6455 0.7045 1.0154 0.1092 0.1290 90.0000 0.0000 0.0000 0.3726 0.0190 0.0177 0.0000 0.0000 0.0000
20 1 2963.0212 0.0257 0.0260 33.7045 2.5135 2.7579 0.9202 0.0507 0.0525 90.0000 0.0000 0.0000 0.3929 0.0349 0.0326 0.0000 0.0000 0.0000
