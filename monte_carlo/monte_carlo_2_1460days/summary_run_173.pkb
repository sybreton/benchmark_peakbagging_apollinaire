# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:35:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9645 0.0552 0.0556 5.5204 0.6415 0.6822 1.1079 0.1276 0.1568 90.0000 0.0000 0.0000 0.4045 0.0217 0.0201 0.0000 0.0000 0.0000
20 1 2963.0013 0.0282 0.0285 27.9537 2.0207 2.2083 1.0660 0.0595 0.0619 90.0000 0.0000 0.0000 0.3864 0.0476 0.0413 0.0000 0.0000 0.0000
