# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:01:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9289 0.0641 0.0602 5.1996 0.6065 0.6701 1.1087 0.1340 0.1578 90.0000 0.0000 0.0000 0.3982 0.0229 0.0202 0.0000 0.0000 0.0000
20 1 2962.9915 0.0268 0.0274 30.3440 2.1522 2.4057 0.9795 0.0538 0.0558 90.0000 0.0000 0.0000 0.4669 0.0344 0.0312 0.0000 0.0000 0.0000
