# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:33:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0959 0.0619 0.0605 4.9513 0.5464 0.5979 1.3816 0.1612 0.1859 90.0000 0.0000 0.0000 0.3585 0.0272 0.0249 0.0000 0.0000 0.0000
20 1 2962.9538 0.0275 0.0278 27.7446 1.9804 2.2011 0.9831 0.0537 0.0558 90.0000 0.0000 0.0000 0.4626 0.0349 0.0320 0.0000 0.0000 0.0000
