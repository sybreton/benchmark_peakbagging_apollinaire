# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:42:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0380 0.0602 0.0606 4.8469 0.5880 0.6625 1.1672 0.1474 0.1765 90.0000 0.0000 0.0000 0.4051 0.0230 0.0209 0.0000 0.0000 0.0000
20 1 2962.9615 0.0272 0.0272 27.1187 1.9582 2.1225 1.0868 0.0607 0.0643 90.0000 0.0000 0.0000 0.3860 0.0478 0.0413 0.0000 0.0000 0.0000
