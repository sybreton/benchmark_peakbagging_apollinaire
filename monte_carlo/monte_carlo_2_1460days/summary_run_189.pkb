# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:23:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9841 0.0432 0.0434 6.7145 0.7631 0.8496 0.8845 0.0997 0.1179 90.0000 0.0000 0.0000 0.4139 0.0157 0.0148 0.0000 0.0000 0.0000
20 1 2963.0481 0.0253 0.0260 30.6294 2.2088 2.4005 0.9529 0.0525 0.0545 90.0000 0.0000 0.0000 0.4168 0.0351 0.0316 0.0000 0.0000 0.0000
