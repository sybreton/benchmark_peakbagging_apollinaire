# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 10:48:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0022 0.0463 0.0455 6.0859 0.7249 0.8240 0.9992 0.1255 0.1449 90.0000 0.0000 0.0000 0.4198 0.0165 0.0149 0.0000 0.0000 0.0000
20 1 2962.9815 0.0267 0.0270 27.7304 2.0593 2.2861 1.0173 0.0571 0.0618 90.0000 0.0000 0.0000 0.3986 0.0401 0.0360 0.0000 0.0000 0.0000
