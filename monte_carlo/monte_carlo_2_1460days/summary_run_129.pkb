# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:24:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0698 0.0621 0.0613 4.6853 0.6175 0.7193 1.2532 0.1791 0.2047 90.0000 0.0000 0.0000 0.4022 0.0269 0.0235 0.0000 0.0000 0.0000
20 1 2963.0479 0.0256 0.0264 32.4233 2.3707 2.6016 0.9764 0.0531 0.0562 90.0000 0.0000 0.0000 0.3737 0.0401 0.0352 0.0000 0.0000 0.0000
