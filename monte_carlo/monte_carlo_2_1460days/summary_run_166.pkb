# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 16:13:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9853 0.0596 0.0610 4.9443 0.5987 0.6803 1.1678 0.1486 0.1707 90.0000 0.0000 0.0000 0.3681 0.0256 0.0218 0.0000 0.0000 0.0000
20 1 2963.0179 0.0265 0.0268 31.7939 2.3519 2.5705 1.0123 0.0537 0.0588 90.0000 0.0000 0.0000 0.3399 0.0486 0.0407 0.0000 0.0000 0.0000
