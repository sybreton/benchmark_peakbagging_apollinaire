# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:21:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2946.0613 0.0516 0.0505 5.8225 0.6708 0.7650 1.0216 0.1236 0.1437 90.0000 0.0000 0.0000 0.3720 0.0183 0.0174 0.0000 0.0000 0.0000
20 1 2962.9904 0.0274 0.0284 28.9053 2.1041 2.2121 1.0917 0.0586 0.0636 90.0000 0.0000 0.0000 0.3244 0.0565 0.0478 0.0000 0.0000 0.0000
