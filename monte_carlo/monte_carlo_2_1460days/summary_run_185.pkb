# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 17:11:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9241 0.0555 0.0527 5.9599 0.6432 0.6876 1.0119 0.1057 0.1231 90.0000 0.0000 0.0000 0.4085 0.0188 0.0178 0.0000 0.0000 0.0000
20 1 2962.9978 0.0266 0.0269 30.5754 2.1998 2.4570 0.9622 0.0533 0.0574 90.0000 0.0000 0.0000 0.3913 0.0385 0.0347 0.0000 0.0000 0.0000
