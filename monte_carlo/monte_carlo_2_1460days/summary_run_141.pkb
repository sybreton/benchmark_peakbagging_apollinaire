# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 14:59:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9686 0.0508 0.0524 6.2662 0.6743 0.7302 0.9279 0.0967 0.1141 90.0000 0.0000 0.0000 0.3651 0.0167 0.0165 0.0000 0.0000 0.0000
20 1 2963.0181 0.0271 0.0270 27.9312 2.0272 2.2400 1.0229 0.0568 0.0601 90.0000 0.0000 0.0000 0.3859 0.0427 0.0361 0.0000 0.0000 0.0000
