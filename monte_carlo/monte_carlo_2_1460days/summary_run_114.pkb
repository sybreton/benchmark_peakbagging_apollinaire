# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 31/03/2022 13:41:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
19 3 2945.9342 0.0534 0.0563 5.0851 0.6395 0.7365 1.0467 0.1380 0.1617 90.0000 0.0000 0.0000 0.4083 0.0207 0.0188 0.0000 0.0000 0.0000
20 1 2962.9528 0.0280 0.0275 26.5355 1.9838 2.1103 1.0916 0.0616 0.0669 90.0000 0.0000 0.0000 0.3764 0.0492 0.0427 0.0000 0.0000 0.0000
