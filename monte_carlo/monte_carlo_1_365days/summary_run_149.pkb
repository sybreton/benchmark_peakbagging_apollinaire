# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:02:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9954 0.0218 0.0211 18.0313 3.7924 5.0581 0.2493 0.0443 0.0546 90.0000 0.0000 0.0000 0.4072 0.0122 0.0121 0.0000 0.0000 0.0000
12 0 1822.0398 0.0358 0.0355 14.0541 3.0248 4.5461 0.4258 0.0716 0.0808 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
