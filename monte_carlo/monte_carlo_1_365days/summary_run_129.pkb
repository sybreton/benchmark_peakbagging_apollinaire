# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:17:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0096 0.0304 0.0311 14.3589 2.8545 3.7322 0.3747 0.0659 0.0811 90.0000 0.0000 0.0000 0.4145 0.0161 0.0161 0.0000 0.0000 0.0000
12 0 1821.9944 0.0316 0.0308 17.2893 4.1424 6.3203 0.3536 0.0677 0.0765 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
