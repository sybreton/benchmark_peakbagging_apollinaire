# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:00:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0011 0.0246 0.0259 16.4993 3.3187 4.4717 0.3015 0.0526 0.0643 90.0000 0.0000 0.0000 0.4081 0.0162 0.0152 0.0000 0.0000 0.0000
12 0 1821.9588 0.0318 0.0317 18.3676 4.0792 6.1098 0.3757 0.0641 0.0707 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
