# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:33:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0142 0.0378 0.0412 10.1297 2.2430 2.7643 0.4266 0.0853 0.1144 90.0000 0.0000 0.0000 0.3952 0.0199 0.0206 0.0000 0.0000 0.0000
12 0 1822.0156 0.0311 0.0315 19.6954 4.4935 6.8941 0.3699 0.0666 0.0740 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
