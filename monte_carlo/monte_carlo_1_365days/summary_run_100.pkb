# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:10:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9624 0.0335 0.0347 15.2953 2.9594 3.8316 0.3784 0.0612 0.0759 90.0000 0.0000 0.0000 0.3938 0.0182 0.0174 0.0000 0.0000 0.0000
12 0 1821.9996 0.0229 0.0231 34.7440 8.9467 14.1307 0.2522 0.0484 0.0536 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
