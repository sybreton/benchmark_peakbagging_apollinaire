# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:44:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9949 0.0478 0.0455 10.3019 2.1108 2.5797 0.5018 0.0893 0.1209 90.0000 0.0000 0.0000 0.4158 0.0255 0.0246 0.0000 0.0000 0.0000
12 0 1822.0295 0.0224 0.0225 34.4795 9.1451 15.1589 0.2407 0.0501 0.0544 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
