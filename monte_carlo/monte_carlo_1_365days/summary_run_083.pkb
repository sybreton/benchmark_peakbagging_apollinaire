# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:48:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9546 0.0328 0.0322 14.9883 3.1823 4.2860 0.3531 0.0667 0.0849 90.0000 0.0000 0.0000 0.3993 0.0217 0.0208 0.0000 0.0000 0.0000
12 0 1822.0255 0.0336 0.0318 18.4575 4.2315 6.0419 0.3950 0.0699 0.0807 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
