# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:39:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0507 0.0317 0.0318 15.2397 2.8769 3.7947 0.3649 0.0580 0.0703 90.0000 0.0000 0.0000 0.3925 0.0154 0.0153 0.0000 0.0000 0.0000
12 0 1822.0251 0.0231 0.0239 28.0795 7.4301 12.4492 0.2549 0.0553 0.0629 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
