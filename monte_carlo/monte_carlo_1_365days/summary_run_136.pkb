# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:33:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0193 0.0295 0.0291 15.5659 3.1226 4.0950 0.3608 0.0654 0.0814 90.0000 0.0000 0.0000 0.4075 0.0164 0.0163 0.0000 0.0000 0.0000
12 0 1821.9822 0.0323 0.0311 16.2325 3.8982 5.7333 0.3648 0.0679 0.0785 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
