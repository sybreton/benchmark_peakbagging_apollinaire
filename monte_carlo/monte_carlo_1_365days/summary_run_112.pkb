# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:38:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9470 0.0404 0.0389 11.6297 2.4899 3.1008 0.4259 0.0780 0.1046 90.0000 0.0000 0.0000 0.3647 0.0235 0.0216 0.0000 0.0000 0.0000
12 0 1822.0462 0.0221 0.0224 33.2621 8.6843 14.5336 0.2352 0.0483 0.0532 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
