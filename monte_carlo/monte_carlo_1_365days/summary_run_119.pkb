# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:54:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9634 0.0357 0.0354 11.9018 2.4374 3.0583 0.3809 0.0688 0.0894 90.0000 0.0000 0.0000 0.4025 0.0177 0.0184 0.0000 0.0000 0.0000
12 0 1822.0115 0.0244 0.0242 32.3517 8.1007 12.7875 0.2790 0.0542 0.0589 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
