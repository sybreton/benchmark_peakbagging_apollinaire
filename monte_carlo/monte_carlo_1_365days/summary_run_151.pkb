# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:07:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0355 0.0273 0.0282 14.8442 3.0042 3.8568 0.3314 0.0580 0.0710 90.0000 0.0000 0.0000 0.4065 0.0147 0.0147 0.0000 0.0000 0.0000
12 0 1822.0140 0.0287 0.0292 22.5723 5.2508 8.0450 0.3425 0.0624 0.0694 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
