# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:06:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0322 0.0285 0.0283 16.6549 3.2437 4.2582 0.3508 0.0592 0.0744 90.0000 0.0000 0.0000 0.4013 0.0168 0.0161 0.0000 0.0000 0.0000
12 0 1821.9879 0.0377 0.0369 13.7567 3.0839 4.4472 0.4579 0.0818 0.0937 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
