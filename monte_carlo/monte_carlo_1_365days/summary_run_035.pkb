# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:37:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0286 0.0365 0.0378 10.1978 2.1206 2.6348 0.4092 0.0745 0.0965 90.0000 0.0000 0.0000 0.3900 0.0174 0.0184 0.0000 0.0000 0.0000
12 0 1821.9757 0.0303 0.0304 21.2613 4.8226 7.5112 0.3595 0.0640 0.0709 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
