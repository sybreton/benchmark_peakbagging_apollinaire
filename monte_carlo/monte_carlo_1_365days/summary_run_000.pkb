# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:35:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0161 0.0304 0.0295 14.2270 2.9164 3.9303 0.3179 0.0573 0.0726 90.0000 0.0000 0.0000 0.3894 0.0165 0.0157 0.0000 0.0000 0.0000
12 0 1821.9552 0.0299 0.0288 20.5898 4.8221 7.2460 0.3339 0.0581 0.0649 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
