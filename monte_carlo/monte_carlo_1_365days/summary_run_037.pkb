# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:42:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0106 0.0285 0.0295 14.9231 3.1051 4.0262 0.3223 0.0567 0.0705 90.0000 0.0000 0.0000 0.3694 0.0163 0.0164 0.0000 0.0000 0.0000
12 0 1822.0463 0.0327 0.0328 16.5346 3.7750 5.4581 0.3936 0.0716 0.0798 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
