# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:25:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9781 0.0312 0.0327 14.3907 2.9155 3.7552 0.3551 0.0611 0.0760 90.0000 0.0000 0.0000 0.4156 0.0195 0.0180 0.0000 0.0000 0.0000
12 0 1821.9805 0.0278 0.0272 23.5918 5.6543 8.6861 0.3179 0.0590 0.0679 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
