# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:10:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0202 0.0247 0.0244 15.6571 3.2483 4.4007 0.2772 0.0488 0.0603 90.0000 0.0000 0.0000 0.3833 0.0143 0.0135 0.0000 0.0000 0.0000
12 0 1821.9703 0.0329 0.0329 16.0298 3.8978 5.6451 0.3848 0.0729 0.0892 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
