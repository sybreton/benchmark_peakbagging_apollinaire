# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:44:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0079 0.0253 0.0255 16.6754 3.4395 4.5962 0.2910 0.0524 0.0639 90.0000 0.0000 0.0000 0.4010 0.0148 0.0148 0.0000 0.0000 0.0000
12 0 1822.0262 0.0265 0.0269 25.8397 6.1516 9.5895 0.3176 0.0609 0.0660 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
