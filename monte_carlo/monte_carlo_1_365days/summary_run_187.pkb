# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:08:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0079 0.0248 0.0236 17.7755 3.6373 4.8221 0.2980 0.0533 0.0634 90.0000 0.0000 0.0000 0.4094 0.0130 0.0128 0.0000 0.0000 0.0000
12 0 1822.0221 0.0347 0.0330 16.4436 3.6922 5.4841 0.4042 0.0699 0.0796 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
