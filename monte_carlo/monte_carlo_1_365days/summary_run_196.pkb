# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:29:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0141 0.0303 0.0303 13.4391 2.7906 3.7336 0.3265 0.0580 0.0745 90.0000 0.0000 0.0000 0.3940 0.0179 0.0176 0.0000 0.0000 0.0000
12 0 1821.9980 0.0243 0.0245 29.3268 7.3101 12.1898 0.2739 0.0540 0.0590 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
