# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:42:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0187 0.0314 0.0304 14.2657 2.8675 3.7423 0.3461 0.0595 0.0735 90.0000 0.0000 0.0000 0.3958 0.0172 0.0167 0.0000 0.0000 0.0000
12 0 1821.9707 0.0270 0.0266 26.0472 6.0662 9.3316 0.3105 0.0536 0.0583 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
