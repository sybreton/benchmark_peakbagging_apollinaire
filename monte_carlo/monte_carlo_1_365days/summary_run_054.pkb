# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:45:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9568 0.0329 0.0336 12.4341 2.5733 3.2828 0.3573 0.0626 0.0804 90.0000 0.0000 0.0000 0.4199 0.0203 0.0194 0.0000 0.0000 0.0000
12 0 1822.0324 0.0316 0.0313 17.9016 4.1801 6.3621 0.3487 0.0638 0.0701 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
