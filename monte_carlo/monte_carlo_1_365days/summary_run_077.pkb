# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:22:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9235 0.0377 0.0368 11.4649 2.4999 3.2337 0.4080 0.0786 0.1059 90.0000 0.0000 0.0000 0.3983 0.0220 0.0207 0.0000 0.0000 0.0000
12 0 1821.9562 0.0297 0.0295 17.6253 4.2391 6.5349 0.3226 0.0629 0.0719 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
