# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:31:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0299 0.0257 0.0248 15.5094 3.1284 4.4240 0.2973 0.0549 0.0647 90.0000 0.0000 0.0000 0.3977 0.0146 0.0155 0.0000 0.0000 0.0000
12 0 1821.9675 0.0315 0.0320 19.3539 4.4799 6.2583 0.3853 0.0687 0.0821 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
