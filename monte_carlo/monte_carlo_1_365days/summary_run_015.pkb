# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:33:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0056 0.0335 0.0348 11.9018 2.5152 3.2051 0.3825 0.0718 0.0921 90.0000 0.0000 0.0000 0.3990 0.0169 0.0168 0.0000 0.0000 0.0000
12 0 1821.9527 0.0246 0.0241 32.5884 8.3877 13.8311 0.2649 0.0510 0.0580 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
