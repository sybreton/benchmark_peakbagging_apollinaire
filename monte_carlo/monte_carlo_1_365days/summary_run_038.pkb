# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:45:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0117 0.0309 0.0303 13.9350 2.8397 3.6674 0.3446 0.0611 0.0771 90.0000 0.0000 0.0000 0.4033 0.0174 0.0174 0.0000 0.0000 0.0000
12 0 1821.9901 0.0266 0.0272 26.0864 6.2839 9.7093 0.3070 0.0552 0.0619 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
