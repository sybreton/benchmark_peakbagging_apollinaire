# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:41:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0309 0.0314 0.0295 13.2891 2.6875 3.5100 0.3457 0.0628 0.0772 90.0000 0.0000 0.0000 0.3902 0.0155 0.0154 0.0000 0.0000 0.0000
12 0 1822.0443 0.0314 0.0309 16.7692 3.8249 5.8870 0.3659 0.0686 0.0764 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
