# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:15:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9292 0.0378 0.0372 12.0336 2.4634 3.1350 0.4075 0.0735 0.0985 90.0000 0.0000 0.0000 0.3795 0.0199 0.0188 0.0000 0.0000 0.0000
12 0 1822.0566 0.0304 0.0306 19.9939 4.5817 6.8049 0.3484 0.0599 0.0673 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
