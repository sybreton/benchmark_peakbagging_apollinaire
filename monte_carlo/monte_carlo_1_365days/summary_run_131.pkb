# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:22:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0088 0.0340 0.0336 13.2561 2.5593 3.1513 0.3748 0.0600 0.0751 90.0000 0.0000 0.0000 0.4028 0.0171 0.0168 0.0000 0.0000 0.0000
12 0 1822.0264 0.0311 0.0309 18.5434 4.2863 6.4224 0.3605 0.0643 0.0697 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
