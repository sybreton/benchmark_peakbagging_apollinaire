# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:52:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9907 0.0336 0.0330 11.3137 2.4472 3.1510 0.3919 0.0760 0.0993 90.0000 0.0000 0.0000 0.4336 0.0183 0.0197 0.0000 0.0000 0.0000
12 0 1821.9854 0.0308 0.0310 20.0783 4.6232 7.1841 0.3576 0.0650 0.0692 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
