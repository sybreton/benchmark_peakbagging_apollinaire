# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:53:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9942 0.0241 0.0247 18.7092 3.6358 5.0380 0.2955 0.0491 0.0573 90.0000 0.0000 0.0000 0.4034 0.0134 0.0132 0.0000 0.0000 0.0000
12 0 1822.0903 0.0318 0.0333 17.1728 3.8763 5.6964 0.4103 0.0776 0.0897 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
