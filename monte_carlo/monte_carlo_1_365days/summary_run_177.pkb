# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:46:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0336 0.0315 0.0336 12.7385 2.6683 3.5219 0.3521 0.0655 0.0824 90.0000 0.0000 0.0000 0.4025 0.0169 0.0159 0.0000 0.0000 0.0000
12 0 1822.0104 0.0321 0.0312 17.2746 4.0368 6.0283 0.3568 0.0630 0.0704 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
