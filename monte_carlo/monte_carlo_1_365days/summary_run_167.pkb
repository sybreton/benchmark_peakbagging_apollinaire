# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:24:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0244 0.0319 0.0323 14.7335 2.8880 3.7139 0.3742 0.0632 0.0792 90.0000 0.0000 0.0000 0.3917 0.0183 0.0173 0.0000 0.0000 0.0000
12 0 1822.0276 0.0326 0.0322 13.5366 3.3199 5.1783 0.3541 0.0719 0.0833 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
