# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:44:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0230 0.0428 0.0409 10.4880 2.3256 3.0034 0.4476 0.0880 0.1195 90.0000 0.0000 0.0000 0.4031 0.0246 0.0218 0.0000 0.0000 0.0000
12 0 1822.0032 0.0326 0.0329 18.7430 4.0368 5.8762 0.4045 0.0684 0.0747 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
