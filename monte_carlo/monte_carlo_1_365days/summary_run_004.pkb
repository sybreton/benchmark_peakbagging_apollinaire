# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:48:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0118 0.0355 0.0357 12.8901 2.5842 3.2735 0.4043 0.0701 0.0895 90.0000 0.0000 0.0000 0.4263 0.0219 0.0206 0.0000 0.0000 0.0000
12 0 1822.0005 0.0278 0.0287 21.0770 5.0016 7.6720 0.3302 0.0606 0.0689 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
