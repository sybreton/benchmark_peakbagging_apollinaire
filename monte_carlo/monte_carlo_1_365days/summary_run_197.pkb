# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:31:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0718 0.0352 0.0338 13.0296 2.6245 3.3965 0.3729 0.0666 0.0843 90.0000 0.0000 0.0000 0.4123 0.0188 0.0185 0.0000 0.0000 0.0000
12 0 1821.9882 0.0374 0.0377 12.7210 2.8156 4.0136 0.4544 0.0780 0.0865 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
