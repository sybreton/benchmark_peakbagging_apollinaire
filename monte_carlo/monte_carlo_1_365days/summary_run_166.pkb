# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:41:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0166 0.0269 0.0261 16.5374 3.1409 4.1602 0.3138 0.0516 0.0620 90.0000 0.0000 0.0000 0.4039 0.0134 0.0129 0.0000 0.0000 0.0000
12 0 1822.0387 0.0307 0.0317 19.5377 4.2342 6.3008 0.3913 0.0661 0.0732 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
