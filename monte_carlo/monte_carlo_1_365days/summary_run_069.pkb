# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:48:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0277 0.0303 0.0297 15.8479 3.1697 4.1525 0.3211 0.0532 0.0647 90.0000 0.0000 0.0000 0.4017 0.0183 0.0178 0.0000 0.0000 0.0000
12 0 1822.0316 0.0336 0.0336 14.7837 3.4666 5.2714 0.3739 0.0700 0.0775 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
