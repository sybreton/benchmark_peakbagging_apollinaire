# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:12:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0001 0.0227 0.0227 20.0609 3.9401 5.2684 0.2647 0.0436 0.0533 90.0000 0.0000 0.0000 0.4213 0.0124 0.0130 0.0000 0.0000 0.0000
12 0 1822.0122 0.0287 0.0290 22.4522 5.2930 7.7912 0.3470 0.0628 0.0716 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
