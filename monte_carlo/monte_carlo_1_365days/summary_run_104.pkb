# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:19:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9725 0.0225 0.0230 19.6511 3.8029 5.2885 0.2776 0.0458 0.0535 90.0000 0.0000 0.0000 0.4150 0.0129 0.0128 0.0000 0.0000 0.0000
12 0 1821.9676 0.0276 0.0275 24.0276 5.7724 8.8578 0.3077 0.0568 0.0637 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
