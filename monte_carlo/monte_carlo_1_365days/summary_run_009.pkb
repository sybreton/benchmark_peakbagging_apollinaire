# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:08:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0090 0.0263 0.0270 16.6789 3.3211 4.2781 0.3169 0.0529 0.0667 90.0000 0.0000 0.0000 0.3928 0.0141 0.0141 0.0000 0.0000 0.0000
12 0 1821.9830 0.0314 0.0306 21.3395 4.6839 6.9072 0.3844 0.0648 0.0713 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
