# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:14:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0015 0.0368 0.0376 12.8657 2.5831 3.2496 0.4029 0.0688 0.0876 90.0000 0.0000 0.0000 0.3893 0.0192 0.0189 0.0000 0.0000 0.0000
12 0 1822.0230 0.0248 0.0247 30.5241 7.2567 11.6683 0.2863 0.0526 0.0579 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
