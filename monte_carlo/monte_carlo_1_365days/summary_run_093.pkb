# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:47:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9466 0.0365 0.0347 11.1180 2.5639 3.3454 0.3860 0.0806 0.1091 90.0000 0.0000 0.0000 0.3648 0.0211 0.0205 0.0000 0.0000 0.0000
12 0 1822.0176 0.0247 0.0237 33.7597 8.3588 13.4654 0.2715 0.0507 0.0553 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
