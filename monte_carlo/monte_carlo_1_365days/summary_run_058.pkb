# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:02:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9962 0.0311 0.0325 14.5537 2.9321 3.8017 0.3571 0.0601 0.0769 90.0000 0.0000 0.0000 0.3855 0.0172 0.0167 0.0000 0.0000 0.0000
12 0 1821.9941 0.0295 0.0295 20.8658 4.8517 7.4449 0.3353 0.0614 0.0685 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
