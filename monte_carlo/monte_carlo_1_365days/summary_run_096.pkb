# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:01:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0212 0.0313 0.0313 12.2986 2.6099 3.3840 0.3658 0.0710 0.0910 90.0000 0.0000 0.0000 0.4233 0.0167 0.0164 0.0000 0.0000 0.0000
12 0 1822.0132 0.0234 0.0227 27.6381 7.5484 12.6396 0.2288 0.0506 0.0570 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
