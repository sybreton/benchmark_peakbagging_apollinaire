# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:28:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0256 0.0267 0.0263 18.2349 3.7290 4.9843 0.3078 0.0532 0.0651 90.0000 0.0000 0.0000 0.3957 0.0166 0.0159 0.0000 0.0000 0.0000
12 0 1821.9988 0.0262 0.0253 21.7489 5.7331 9.5145 0.2708 0.0584 0.0661 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
