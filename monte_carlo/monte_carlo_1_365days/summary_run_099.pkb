# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:07:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0086 0.0291 0.0304 16.0670 3.1555 4.2466 0.3501 0.0600 0.0728 90.0000 0.0000 0.0000 0.3741 0.0158 0.0160 0.0000 0.0000 0.0000
12 0 1822.0444 0.0357 0.0353 17.0329 3.6027 4.9845 0.4413 0.0713 0.0798 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
