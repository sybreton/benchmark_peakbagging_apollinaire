# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:10:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0092 0.0247 0.0266 15.8069 3.2651 4.3312 0.2928 0.0523 0.0625 90.0000 0.0000 0.0000 0.4011 0.0151 0.0144 0.0000 0.0000 0.0000
12 0 1821.9639 0.0234 0.0242 32.8555 8.1299 13.6781 0.2644 0.0513 0.0558 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
