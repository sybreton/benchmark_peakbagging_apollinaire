# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:32:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9974 0.0226 0.0226 19.8125 4.0810 5.4403 0.2646 0.0457 0.0556 90.0000 0.0000 0.0000 0.3714 0.0127 0.0125 0.0000 0.0000 0.0000
12 0 1822.0060 0.0311 0.0305 19.0220 4.3276 6.6389 0.3605 0.0631 0.0718 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
