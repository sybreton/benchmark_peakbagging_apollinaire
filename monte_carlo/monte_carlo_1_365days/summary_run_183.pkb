# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:59:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0068 0.0351 0.0360 12.8249 2.5898 3.1654 0.4183 0.0749 0.0959 90.0000 0.0000 0.0000 0.3910 0.0196 0.0191 0.0000 0.0000 0.0000
12 0 1822.0331 0.0356 0.0363 13.9744 3.1618 4.5559 0.4347 0.0767 0.0871 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
