# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:40:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0587 0.0627 0.0576 8.1096 2.2690 3.0194 0.6183 0.1675 0.2683 90.0000 0.0000 0.0000 0.3241 0.0493 0.0336 0.0000 0.0000 0.0000
12 0 1822.0400 0.0289 0.0297 18.7016 4.4292 6.6621 0.3424 0.0642 0.0733 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
