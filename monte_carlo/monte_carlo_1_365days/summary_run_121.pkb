# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:59:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0189 0.0322 0.0313 16.2418 3.0189 3.9942 0.3261 0.0517 0.0622 90.0000 0.0000 0.0000 0.3814 0.0161 0.0156 0.0000 0.0000 0.0000
12 0 1822.0236 0.0336 0.0333 17.7040 3.9164 5.6406 0.4103 0.0698 0.0787 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
