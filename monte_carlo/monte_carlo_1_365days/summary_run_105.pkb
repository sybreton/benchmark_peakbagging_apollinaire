# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:22:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9644 0.0357 0.0377 12.0214 2.4327 3.1665 0.3810 0.0686 0.0850 90.0000 0.0000 0.0000 0.4038 0.0184 0.0178 0.0000 0.0000 0.0000
12 0 1822.0101 0.0352 0.0335 16.2092 3.6352 5.3391 0.4123 0.0739 0.0841 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
