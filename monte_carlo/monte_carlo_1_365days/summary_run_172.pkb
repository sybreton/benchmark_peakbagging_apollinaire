# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:35:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9654 0.0402 0.0430 10.3505 2.1905 2.8620 0.4729 0.0940 0.1237 90.0000 0.0000 0.0000 0.3862 0.0207 0.0214 0.0000 0.0000 0.0000
12 0 1821.9850 0.0339 0.0339 18.1187 3.8397 5.4389 0.4385 0.0724 0.0832 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
