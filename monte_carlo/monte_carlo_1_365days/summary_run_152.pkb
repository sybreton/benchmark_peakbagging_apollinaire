# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:09:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0101 0.0271 0.0280 16.1452 3.2164 4.1855 0.3354 0.0590 0.0724 90.0000 0.0000 0.0000 0.4065 0.0138 0.0138 0.0000 0.0000 0.0000
12 0 1821.9516 0.0325 0.0327 14.5932 3.4361 5.2184 0.3668 0.0684 0.0794 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
