# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:00:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9886 0.0234 0.0226 17.4476 3.5797 4.7933 0.2732 0.0473 0.0578 90.0000 0.0000 0.0000 0.4132 0.0131 0.0131 0.0000 0.0000 0.0000
12 0 1822.0254 0.0334 0.0339 16.1309 3.7709 5.4467 0.4033 0.0731 0.0884 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
