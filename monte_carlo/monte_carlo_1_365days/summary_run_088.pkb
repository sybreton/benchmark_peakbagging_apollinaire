# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:37:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9311 0.0597 0.0544 8.7145 1.9546 2.4300 0.5922 0.1247 0.1804 90.0000 0.0000 0.0000 0.3846 0.0285 0.0275 0.0000 0.0000 0.0000
12 0 1822.0133 0.0343 0.0341 15.8861 3.6244 5.3679 0.3964 0.0713 0.0813 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
