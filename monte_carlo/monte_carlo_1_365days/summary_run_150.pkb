# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:04:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0146 0.0343 0.0338 12.7015 2.8077 3.6805 0.3437 0.0673 0.0885 90.0000 0.0000 0.0000 0.3628 0.0185 0.0177 0.0000 0.0000 0.0000
12 0 1821.9943 0.0216 0.0221 35.9711 9.3872 15.9303 0.2378 0.0487 0.0537 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
