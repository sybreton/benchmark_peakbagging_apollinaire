# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:22:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9846 0.0325 0.0325 12.8802 2.7760 3.5754 0.3676 0.0688 0.0899 90.0000 0.0000 0.0000 0.3996 0.0185 0.0176 0.0000 0.0000 0.0000
12 0 1821.9632 0.0329 0.0327 16.5433 3.8279 5.7041 0.3756 0.0671 0.0755 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
