# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:05:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9955 0.0320 0.0324 14.5581 2.8380 3.5624 0.3763 0.0607 0.0740 90.0000 0.0000 0.0000 0.4054 0.0177 0.0173 0.0000 0.0000 0.0000
12 0 1821.9864 0.0312 0.0326 18.8776 4.3268 6.5036 0.3666 0.0644 0.0714 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
