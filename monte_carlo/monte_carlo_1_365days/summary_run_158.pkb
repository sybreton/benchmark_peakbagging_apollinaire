# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:23:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0409 0.0488 0.0529 8.8601 2.1889 2.7925 0.5280 0.1213 0.1786 90.0000 0.0000 0.0000 0.3771 0.0301 0.0267 0.0000 0.0000 0.0000
12 0 1821.9695 0.0315 0.0321 16.3661 3.7484 5.7837 0.3727 0.0714 0.0788 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
