# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:36:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9896 0.0230 0.0238 17.4830 3.6576 5.0780 0.2599 0.0489 0.0584 90.0000 0.0000 0.0000 0.3848 0.0118 0.0119 0.0000 0.0000 0.0000
12 0 1822.0049 0.0273 0.0267 21.5020 5.4183 8.8514 0.2783 0.0564 0.0614 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
