# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:08:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9864 0.0273 0.0273 15.3800 3.1902 4.2212 0.3138 0.0560 0.0726 90.0000 0.0000 0.0000 0.3906 0.0156 0.0154 0.0000 0.0000 0.0000
12 0 1821.9497 0.0272 0.0266 21.0467 5.2698 8.6284 0.2815 0.0571 0.0636 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
