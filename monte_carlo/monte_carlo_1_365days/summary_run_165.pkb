# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:39:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9955 0.0364 0.0385 12.2499 2.3668 3.0075 0.4322 0.0754 0.0933 90.0000 0.0000 0.0000 0.3950 0.0183 0.0183 0.0000 0.0000 0.0000
12 0 1822.0046 0.0307 0.0315 18.7576 4.3752 6.5811 0.3644 0.0655 0.0756 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
