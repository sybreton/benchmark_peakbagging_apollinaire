# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:25:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0193 0.0460 0.0445 10.5434 2.5462 3.1925 0.4237 0.0869 0.1287 90.0000 0.0000 0.0000 0.3495 0.0302 0.0244 0.0000 0.0000 0.0000
12 0 1822.0272 0.0318 0.0320 16.2577 3.9077 5.8084 0.3792 0.0728 0.0857 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
