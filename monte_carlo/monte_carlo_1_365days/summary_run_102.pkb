# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:15:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0587 0.0251 0.0258 15.6691 3.1602 4.2625 0.3044 0.0542 0.0659 90.0000 0.0000 0.0000 0.4199 0.0141 0.0139 0.0000 0.0000 0.0000
12 0 1821.9892 0.0293 0.0287 23.3742 5.2558 7.7810 0.3529 0.0603 0.0676 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
