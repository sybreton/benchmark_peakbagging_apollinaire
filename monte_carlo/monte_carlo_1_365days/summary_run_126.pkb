# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:10:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9884 0.0229 0.0225 19.4445 3.9498 5.5541 0.2696 0.0475 0.0570 90.0000 0.0000 0.0000 0.4527 0.0145 0.0147 0.0000 0.0000 0.0000
12 0 1821.9796 0.0303 0.0302 20.6246 4.7763 7.0754 0.3531 0.0614 0.0683 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
