# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:03:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9985 0.0274 0.0276 15.6599 3.2608 4.3065 0.2961 0.0527 0.0636 90.0000 0.0000 0.0000 0.3843 0.0161 0.0154 0.0000 0.0000 0.0000
12 0 1821.9876 0.0301 0.0297 19.3623 4.5340 6.6014 0.3418 0.0600 0.0708 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
