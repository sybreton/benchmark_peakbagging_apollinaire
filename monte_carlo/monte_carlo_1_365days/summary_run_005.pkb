# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:52:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9735 0.0234 0.0246 16.3258 3.3746 4.6746 0.2782 0.0517 0.0648 90.0000 0.0000 0.0000 0.3926 0.0123 0.0122 0.0000 0.0000 0.0000
12 0 1821.9984 0.0315 0.0302 20.9575 4.5578 6.9805 0.3811 0.0656 0.0733 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
