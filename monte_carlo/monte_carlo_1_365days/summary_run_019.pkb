# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:47:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9872 0.0226 0.0225 20.3766 3.9663 5.4002 0.2715 0.0435 0.0499 90.0000 0.0000 0.0000 0.3996 0.0118 0.0119 0.0000 0.0000 0.0000
12 0 1822.0293 0.0296 0.0291 22.0799 5.1216 7.6729 0.3348 0.0583 0.0631 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
