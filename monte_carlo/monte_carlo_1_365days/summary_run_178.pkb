# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:48:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9913 0.0288 0.0282 16.4444 3.1013 4.0651 0.3359 0.0539 0.0632 90.0000 0.0000 0.0000 0.3906 0.0169 0.0168 0.0000 0.0000 0.0000
12 0 1822.0553 0.0318 0.0317 17.4069 3.9841 5.7387 0.3822 0.0673 0.0771 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
