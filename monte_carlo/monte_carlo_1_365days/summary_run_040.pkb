# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:51:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9794 0.0313 0.0319 16.0502 2.9853 3.9397 0.3449 0.0553 0.0659 90.0000 0.0000 0.0000 0.3794 0.0159 0.0159 0.0000 0.0000 0.0000
12 0 1822.0124 0.0254 0.0249 28.2647 7.1538 11.8580 0.2728 0.0553 0.0622 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
