# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:24:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0172 0.0229 0.0240 15.8035 3.3906 4.5937 0.2461 0.0453 0.0545 90.0000 0.0000 0.0000 0.3874 0.0124 0.0125 0.0000 0.0000 0.0000
12 0 1821.9879 0.0258 0.0259 28.9017 6.8169 10.9466 0.2915 0.0532 0.0574 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
