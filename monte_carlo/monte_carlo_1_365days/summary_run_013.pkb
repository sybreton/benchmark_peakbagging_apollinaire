# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:26:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9595 0.0246 0.0244 16.0266 3.2808 4.5168 0.2646 0.0475 0.0558 90.0000 0.0000 0.0000 0.3980 0.0129 0.0131 0.0000 0.0000 0.0000
12 0 1821.9943 0.0356 0.0351 17.0836 3.7194 5.1154 0.4441 0.0733 0.0823 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
