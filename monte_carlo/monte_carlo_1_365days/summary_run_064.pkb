# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:27:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9896 0.0294 0.0290 16.2610 3.1079 4.3010 0.3444 0.0599 0.0707 90.0000 0.0000 0.0000 0.3930 0.0147 0.0143 0.0000 0.0000 0.0000
12 0 1822.0227 0.0273 0.0266 25.0878 5.8988 9.0846 0.3074 0.0561 0.0617 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
