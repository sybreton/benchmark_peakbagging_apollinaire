# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:37:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9943 0.0337 0.0364 12.9021 2.5201 3.1181 0.4033 0.0691 0.0854 90.0000 0.0000 0.0000 0.4020 0.0189 0.0188 0.0000 0.0000 0.0000
12 0 1822.0066 0.0237 0.0242 28.7211 7.5998 12.7869 0.2456 0.0515 0.0560 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
