# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:06:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9718 0.0462 0.0410 9.5085 2.2667 2.7695 0.4315 0.0910 0.1365 90.0000 0.0000 0.0000 0.4010 0.0237 0.0206 0.0000 0.0000 0.0000
12 0 1821.9751 0.0301 0.0293 21.4396 4.8486 7.2383 0.3531 0.0610 0.0689 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
