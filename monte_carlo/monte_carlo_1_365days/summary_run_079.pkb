# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:31:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0272 0.0289 0.0286 15.6251 2.9127 3.9356 0.3272 0.0524 0.0620 90.0000 0.0000 0.0000 0.3963 0.0151 0.0147 0.0000 0.0000 0.0000
12 0 1822.0125 0.0308 0.0299 20.7992 4.6635 6.8057 0.3762 0.0646 0.0712 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
