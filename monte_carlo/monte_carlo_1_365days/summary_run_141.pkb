# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:44:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0220 0.0393 0.0398 10.7125 2.2482 2.7964 0.4142 0.0761 0.0969 90.0000 0.0000 0.0000 0.4038 0.0188 0.0187 0.0000 0.0000 0.0000
12 0 1821.9668 0.0297 0.0300 21.8724 4.9602 7.4137 0.3564 0.0629 0.0709 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
