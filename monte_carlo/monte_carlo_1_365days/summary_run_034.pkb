# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:34:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0026 0.0243 0.0245 18.1127 3.5868 4.9020 0.2758 0.0467 0.0536 90.0000 0.0000 0.0000 0.3838 0.0135 0.0129 0.0000 0.0000 0.0000
12 0 1821.9815 0.0293 0.0292 24.2669 5.4204 7.8611 0.3663 0.0595 0.0678 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
