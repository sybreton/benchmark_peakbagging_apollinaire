# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:40:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0181 0.0281 0.0276 17.1156 3.4164 4.7137 0.2959 0.0526 0.0627 90.0000 0.0000 0.0000 0.3823 0.0151 0.0146 0.0000 0.0000 0.0000
12 0 1821.9631 0.0248 0.0250 26.4914 6.4733 10.5992 0.2859 0.0560 0.0600 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
