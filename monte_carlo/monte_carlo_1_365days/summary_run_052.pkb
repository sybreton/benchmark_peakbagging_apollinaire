# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:36:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0286 0.0464 0.0417 11.2902 2.5077 3.2720 0.4351 0.0862 0.1158 90.0000 0.0000 0.0000 0.3799 0.0270 0.0234 0.0000 0.0000 0.0000
12 0 1821.9964 0.0335 0.0339 16.5266 3.6743 5.3556 0.4209 0.0733 0.0825 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
