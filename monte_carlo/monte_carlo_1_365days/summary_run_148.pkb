# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:00:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9582 0.0352 0.0357 11.6943 2.3646 3.1634 0.3664 0.0651 0.0790 90.0000 0.0000 0.0000 0.3956 0.0197 0.0198 0.0000 0.0000 0.0000
12 0 1821.9990 0.0305 0.0308 20.7981 4.6371 6.7893 0.3627 0.0600 0.0666 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
