# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:45:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9308 0.0373 0.0359 12.3652 2.5099 3.1441 0.3966 0.0684 0.0874 90.0000 0.0000 0.0000 0.3993 0.0184 0.0180 0.0000 0.0000 0.0000
12 0 1822.0200 0.0246 0.0249 27.6613 6.6267 10.8068 0.2885 0.0548 0.0585 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
