# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:58:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9911 0.0355 0.0368 14.8552 2.7252 3.4298 0.4076 0.0639 0.0807 90.0000 0.0000 0.0000 0.3899 0.0183 0.0184 0.0000 0.0000 0.0000
12 0 1822.0170 0.0249 0.0253 26.3670 6.4590 10.6375 0.2754 0.0537 0.0584 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
