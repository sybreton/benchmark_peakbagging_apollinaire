# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:57:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9849 0.0257 0.0251 19.1756 3.7457 4.9491 0.3058 0.0510 0.0610 90.0000 0.0000 0.0000 0.4092 0.0152 0.0150 0.0000 0.0000 0.0000
12 0 1821.9430 0.0276 0.0279 23.0715 5.5161 8.3879 0.3284 0.0602 0.0671 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
