# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:44:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0142 0.0234 0.0247 17.1864 3.6872 5.0118 0.2825 0.0542 0.0677 90.0000 0.0000 0.0000 0.4056 0.0131 0.0134 0.0000 0.0000 0.0000
12 0 1821.9692 0.0255 0.0266 23.5275 5.8170 9.3952 0.2973 0.0589 0.0654 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
