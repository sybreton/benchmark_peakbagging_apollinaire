# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:11:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9928 0.0306 0.0290 15.5611 2.9795 3.9453 0.3219 0.0529 0.0605 90.0000 0.0000 0.0000 0.3858 0.0151 0.0148 0.0000 0.0000 0.0000
12 0 1821.9756 0.0233 0.0229 32.7725 8.6958 14.0851 0.2448 0.0508 0.0559 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
