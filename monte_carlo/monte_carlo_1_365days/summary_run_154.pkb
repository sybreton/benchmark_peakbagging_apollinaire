# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:13:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0181 0.0340 0.0348 13.2783 2.5645 3.3811 0.3649 0.0611 0.0733 90.0000 0.0000 0.0000 0.4118 0.0194 0.0181 0.0000 0.0000 0.0000
12 0 1822.0125 0.0285 0.0273 22.3892 5.4716 8.5932 0.2999 0.0569 0.0648 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
