# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:59:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0132 0.0345 0.0336 12.1912 2.4378 3.1434 0.3907 0.0688 0.0852 90.0000 0.0000 0.0000 0.4264 0.0197 0.0193 0.0000 0.0000 0.0000
12 0 1822.0263 0.0334 0.0324 17.1200 3.8172 5.6667 0.3807 0.0658 0.0750 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
