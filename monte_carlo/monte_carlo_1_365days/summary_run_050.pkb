# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:27:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9812 0.0316 0.0315 13.2329 2.7141 3.5560 0.3593 0.0645 0.0821 90.0000 0.0000 0.0000 0.3956 0.0156 0.0158 0.0000 0.0000 0.0000
12 0 1821.9653 0.0307 0.0302 18.2390 4.4550 6.6705 0.3407 0.0659 0.0754 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
