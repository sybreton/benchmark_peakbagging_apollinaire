# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:40:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0121 0.0399 0.0377 11.7176 2.3657 3.0033 0.4203 0.0747 0.0950 90.0000 0.0000 0.0000 0.4067 0.0201 0.0196 0.0000 0.0000 0.0000
12 0 1821.9113 0.0266 0.0273 22.4845 5.5276 8.6109 0.3002 0.0609 0.0677 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
