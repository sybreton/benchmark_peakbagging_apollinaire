# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:18:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9656 0.0286 0.0280 15.3880 3.0143 3.9043 0.3195 0.0517 0.0624 90.0000 0.0000 0.0000 0.4255 0.0156 0.0155 0.0000 0.0000 0.0000
12 0 1822.0453 0.0292 0.0284 20.2430 4.9848 7.6633 0.3160 0.0615 0.0724 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
