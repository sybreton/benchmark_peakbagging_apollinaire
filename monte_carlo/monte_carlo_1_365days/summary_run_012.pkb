# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:21:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9879 0.0301 0.0327 12.4803 2.7968 3.5666 0.3578 0.0706 0.0946 90.0000 0.0000 0.0000 0.4192 0.0195 0.0192 0.0000 0.0000 0.0000
12 0 1821.9959 0.0313 0.0313 16.0893 3.7412 5.8231 0.3538 0.0671 0.0748 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
