# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:47:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0496 0.0312 0.0309 12.3562 2.4887 3.1309 0.3277 0.0548 0.0669 90.0000 0.0000 0.0000 0.4053 0.0157 0.0157 0.0000 0.0000 0.0000
12 0 1821.9811 0.0280 0.0284 20.6039 5.0185 7.8717 0.3076 0.0607 0.0674 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
