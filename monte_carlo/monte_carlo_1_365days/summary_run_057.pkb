# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:58:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9404 0.0255 0.0257 18.1137 3.5412 4.9038 0.3041 0.0511 0.0590 90.0000 0.0000 0.0000 0.3904 0.0139 0.0137 0.0000 0.0000 0.0000
12 0 1821.9694 0.0312 0.0311 22.3532 4.8274 7.0573 0.3949 0.0632 0.0724 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
