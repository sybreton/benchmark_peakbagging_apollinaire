# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:11:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9855 0.0180 0.0180 21.8995 5.0644 7.3218 0.1989 0.0400 0.0489 90.0000 0.0000 0.0000 0.3896 0.0112 0.0112 0.0000 0.0000 0.0000
12 0 1821.9847 0.0213 0.0215 42.7593 11.9346 18.3330 0.2231 0.0435 0.0523 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
