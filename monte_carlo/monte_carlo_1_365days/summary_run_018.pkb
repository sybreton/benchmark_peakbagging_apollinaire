# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:43:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0533 0.0297 0.0332 13.3693 2.7337 3.6839 0.3372 0.0620 0.0783 90.0000 0.0000 0.0000 0.4082 0.0164 0.0171 0.0000 0.0000 0.0000
12 0 1821.9655 0.0275 0.0273 23.2446 5.6274 8.7542 0.2984 0.0544 0.0617 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
