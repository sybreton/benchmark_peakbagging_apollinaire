# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:52:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0016 0.0211 0.0204 21.7196 4.3794 6.0903 0.2687 0.0478 0.0567 90.0000 0.0000 0.0000 0.3874 0.0115 0.0109 0.0000 0.0000 0.0000
12 0 1821.9548 0.0261 0.0258 26.2166 6.4794 10.1846 0.2989 0.0572 0.0649 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
