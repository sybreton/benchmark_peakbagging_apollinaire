# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:57:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9870 0.0314 0.0331 16.7919 3.0703 3.8200 0.4166 0.0659 0.0819 90.0000 0.0000 0.0000 0.3866 0.0161 0.0166 0.0000 0.0000 0.0000
12 0 1821.9981 0.0368 0.0379 12.8123 2.8696 4.1862 0.4624 0.0840 0.0955 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
