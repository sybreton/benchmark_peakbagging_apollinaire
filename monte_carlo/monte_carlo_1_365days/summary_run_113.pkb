# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:40:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9816 0.0253 0.0247 16.4081 3.2367 4.2399 0.3077 0.0522 0.0621 90.0000 0.0000 0.0000 0.4479 0.0148 0.0146 0.0000 0.0000 0.0000
12 0 1822.0171 0.0251 0.0256 24.9727 6.1376 9.9002 0.2784 0.0546 0.0610 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
