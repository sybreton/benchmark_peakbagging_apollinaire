# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:38:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0363 0.0289 0.0297 15.1185 2.9722 3.9297 0.3345 0.0587 0.0722 90.0000 0.0000 0.0000 0.4176 0.0152 0.0147 0.0000 0.0000 0.0000
12 0 1821.9709 0.0314 0.0317 14.8158 3.6501 5.4434 0.3445 0.0684 0.0800 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
