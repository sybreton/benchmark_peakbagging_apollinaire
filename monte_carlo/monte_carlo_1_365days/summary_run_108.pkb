# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:29:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9921 0.0280 0.0264 16.4236 3.2369 4.2602 0.3129 0.0516 0.0631 90.0000 0.0000 0.0000 0.4073 0.0142 0.0146 0.0000 0.0000 0.0000
12 0 1821.9989 0.0264 0.0260 27.9457 6.6720 10.7081 0.2981 0.0547 0.0588 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
