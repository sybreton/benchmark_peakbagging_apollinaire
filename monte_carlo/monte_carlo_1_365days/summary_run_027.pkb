# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:13:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9822 0.0270 0.0277 16.2713 3.3173 4.3950 0.3153 0.0567 0.0687 90.0000 0.0000 0.0000 0.3748 0.0158 0.0149 0.0000 0.0000 0.0000
12 0 1821.9789 0.0302 0.0305 20.1845 4.6421 6.6987 0.3534 0.0611 0.0669 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
