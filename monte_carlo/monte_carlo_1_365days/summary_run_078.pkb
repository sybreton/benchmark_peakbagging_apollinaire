# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:27:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9668 0.0257 0.0254 16.2516 3.4946 4.7877 0.2773 0.0511 0.0622 90.0000 0.0000 0.0000 0.3924 0.0152 0.0154 0.0000 0.0000 0.0000
12 0 1821.9788 0.0384 0.0391 12.5403 2.7969 4.2481 0.4667 0.0851 0.0955 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
