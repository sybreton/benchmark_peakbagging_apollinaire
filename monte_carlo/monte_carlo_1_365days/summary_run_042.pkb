# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:57:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9818 0.0257 0.0246 16.5933 3.4512 4.6035 0.2813 0.0488 0.0598 90.0000 0.0000 0.0000 0.3971 0.0153 0.0147 0.0000 0.0000 0.0000
12 0 1822.0580 0.0267 0.0271 24.6170 5.8775 9.1364 0.3136 0.0576 0.0657 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
