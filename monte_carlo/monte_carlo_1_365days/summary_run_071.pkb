# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:56:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9679 0.0361 0.0345 11.4197 2.3256 3.0181 0.3768 0.0669 0.0846 90.0000 0.0000 0.0000 0.4185 0.0176 0.0182 0.0000 0.0000 0.0000
12 0 1822.0400 0.0264 0.0272 19.9489 5.0513 8.0109 0.2846 0.0595 0.0663 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
