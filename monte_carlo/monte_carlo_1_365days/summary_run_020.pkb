# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:50:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9948 0.0372 0.0362 12.0930 2.4115 3.1290 0.4198 0.0746 0.0900 90.0000 0.0000 0.0000 0.4055 0.0194 0.0192 0.0000 0.0000 0.0000
12 0 1822.0183 0.0262 0.0279 28.0815 6.6225 10.2056 0.3164 0.0556 0.0626 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
