# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 17:02:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9770 0.0328 0.0306 13.1575 2.7606 3.4767 0.3732 0.0699 0.0903 90.0000 0.0000 0.0000 0.4023 0.0170 0.0166 0.0000 0.0000 0.0000
12 0 1821.9020 0.0339 0.0342 14.1350 3.3613 5.0482 0.3757 0.0701 0.0836 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
