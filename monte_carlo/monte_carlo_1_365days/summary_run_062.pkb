# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:19:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9835 0.0264 0.0265 14.7855 3.0129 3.9500 0.3067 0.0533 0.0651 90.0000 0.0000 0.0000 0.4184 0.0152 0.0148 0.0000 0.0000 0.0000
12 0 1821.9628 0.0271 0.0284 21.4765 5.1128 7.9269 0.3051 0.0552 0.0609 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
