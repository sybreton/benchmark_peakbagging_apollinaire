# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:54:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9520 0.0381 0.0372 12.4478 2.5458 3.2720 0.4231 0.0783 0.1000 90.0000 0.0000 0.0000 0.3811 0.0201 0.0201 0.0000 0.0000 0.0000
12 0 1822.0116 0.0258 0.0260 23.9092 5.8977 9.3869 0.2835 0.0553 0.0622 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
