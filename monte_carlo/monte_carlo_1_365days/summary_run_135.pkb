# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:31:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9796 0.0291 0.0283 14.9431 2.8962 3.7814 0.3288 0.0546 0.0648 90.0000 0.0000 0.0000 0.3896 0.0148 0.0149 0.0000 0.0000 0.0000
12 0 1822.0191 0.0343 0.0352 16.3771 3.5901 5.1249 0.4400 0.0770 0.0888 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
