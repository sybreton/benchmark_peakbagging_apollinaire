# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:09:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0239 0.0223 0.0211 20.0594 4.2219 5.9736 0.2304 0.0419 0.0496 90.0000 0.0000 0.0000 0.3922 0.0119 0.0114 0.0000 0.0000 0.0000
12 0 1822.0090 0.0328 0.0336 18.4914 4.0333 5.7886 0.4114 0.0676 0.0751 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
