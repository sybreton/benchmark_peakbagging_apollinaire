# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:36:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9825 0.0452 0.0456 10.2740 2.2884 2.8667 0.4602 0.0891 0.1236 90.0000 0.0000 0.0000 0.3663 0.0248 0.0233 0.0000 0.0000 0.0000
12 0 1822.0313 0.0252 0.0249 32.3986 7.8646 12.2961 0.2905 0.0525 0.0567 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
