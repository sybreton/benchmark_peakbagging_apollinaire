# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:50:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9839 0.0273 0.0263 16.1104 3.2282 4.1884 0.3227 0.0559 0.0674 90.0000 0.0000 0.0000 0.4102 0.0141 0.0144 0.0000 0.0000 0.0000
12 0 1821.9695 0.0301 0.0302 16.7337 4.1052 6.2383 0.3417 0.0682 0.0782 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
