# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:46:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0538 0.0409 0.0442 11.5540 2.3182 2.8654 0.4372 0.0760 0.0987 90.0000 0.0000 0.0000 0.3912 0.0206 0.0198 0.0000 0.0000 0.0000
12 0 1821.9883 0.0273 0.0277 23.4859 5.7277 8.6035 0.3065 0.0550 0.0641 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
