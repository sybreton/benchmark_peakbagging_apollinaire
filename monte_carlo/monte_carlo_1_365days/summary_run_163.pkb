# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:34:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0133 0.0264 0.0249 17.7490 3.3860 4.4837 0.3140 0.0507 0.0605 90.0000 0.0000 0.0000 0.4107 0.0134 0.0132 0.0000 0.0000 0.0000
12 0 1821.9777 0.0325 0.0318 20.1795 4.4402 6.3784 0.4060 0.0698 0.0771 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
