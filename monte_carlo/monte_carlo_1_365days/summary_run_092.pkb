# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:45:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0191 0.0494 0.0479 9.2198 1.8809 2.3558 0.4970 0.0899 0.1113 90.0000 0.0000 0.0000 0.3932 0.0228 0.0229 0.0000 0.0000 0.0000
12 0 1821.9598 0.0264 0.0272 25.3670 6.0862 9.3322 0.2992 0.0552 0.0587 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
