# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:36:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9815 0.0257 0.0264 17.3670 3.5326 4.7197 0.2993 0.0537 0.0637 90.0000 0.0000 0.0000 0.3981 0.0142 0.0139 0.0000 0.0000 0.0000
12 0 1821.9861 0.0320 0.0321 17.1817 3.8950 5.6706 0.3833 0.0681 0.0794 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
