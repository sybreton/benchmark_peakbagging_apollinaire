# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:35:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9791 0.0391 0.0398 11.4075 2.3751 2.9857 0.4201 0.0756 0.0969 90.0000 0.0000 0.0000 0.4106 0.0216 0.0206 0.0000 0.0000 0.0000
12 0 1822.0332 0.0295 0.0290 16.2474 4.0932 6.5821 0.3016 0.0613 0.0696 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
