# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:03:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9850 0.0327 0.0338 13.1097 2.6529 3.4709 0.3554 0.0595 0.0761 90.0000 0.0000 0.0000 0.4223 0.0216 0.0199 0.0000 0.0000 0.0000
12 0 1821.9362 0.0353 0.0343 15.1773 3.2986 4.9856 0.4133 0.0711 0.0778 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
