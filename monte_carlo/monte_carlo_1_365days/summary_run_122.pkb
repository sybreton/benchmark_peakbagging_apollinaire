# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:01:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0261 0.0372 0.0340 12.6753 2.6059 3.2143 0.4008 0.0730 0.0946 90.0000 0.0000 0.0000 0.4058 0.0180 0.0186 0.0000 0.0000 0.0000
12 0 1822.0159 0.0276 0.0279 20.9936 5.0081 7.8067 0.3032 0.0570 0.0627 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
