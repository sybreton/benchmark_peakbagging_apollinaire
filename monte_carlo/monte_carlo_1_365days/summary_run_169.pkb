# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:29:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9825 0.0278 0.0287 14.6340 3.0043 3.9877 0.3183 0.0559 0.0706 90.0000 0.0000 0.0000 0.3699 0.0150 0.0151 0.0000 0.0000 0.0000
12 0 1821.9803 0.0310 0.0319 18.0442 4.3787 6.2470 0.3440 0.0630 0.0717 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
