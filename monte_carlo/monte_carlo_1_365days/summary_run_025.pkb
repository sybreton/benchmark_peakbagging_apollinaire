# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:07:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0198 0.0263 0.0257 19.3155 3.5779 4.8120 0.3163 0.0488 0.0579 90.0000 0.0000 0.0000 0.4085 0.0141 0.0143 0.0000 0.0000 0.0000
12 0 1821.9854 0.0321 0.0308 18.5910 4.2199 6.3374 0.3782 0.0680 0.0767 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
