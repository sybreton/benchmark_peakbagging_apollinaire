# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:35:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0496 0.0361 0.0353 14.3538 2.6664 3.3751 0.3684 0.0582 0.0690 90.0000 0.0000 0.0000 0.4435 0.0178 0.0178 0.0000 0.0000 0.0000
12 0 1822.0061 0.0258 0.0251 24.6801 6.0912 10.0561 0.2670 0.0503 0.0564 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
