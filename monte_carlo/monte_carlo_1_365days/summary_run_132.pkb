# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:24:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9877 0.0189 0.0185 19.7841 4.5523 6.4788 0.2126 0.0429 0.0519 90.0000 0.0000 0.0000 0.4163 0.0111 0.0111 0.0000 0.0000 0.0000
12 0 1821.9487 0.0273 0.0275 23.8913 5.8277 9.2179 0.3043 0.0601 0.0669 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
