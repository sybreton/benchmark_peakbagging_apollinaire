# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:02:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0064 0.0232 0.0224 20.2200 3.8268 5.1143 0.2860 0.0449 0.0533 90.0000 0.0000 0.0000 0.4152 0.0119 0.0119 0.0000 0.0000 0.0000
12 0 1821.9735 0.0350 0.0343 16.9472 3.6720 5.2646 0.4214 0.0699 0.0797 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
