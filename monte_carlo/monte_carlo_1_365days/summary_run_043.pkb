# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:00:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0085 0.0262 0.0264 17.1936 3.2936 4.4202 0.3309 0.0558 0.0671 90.0000 0.0000 0.0000 0.4433 0.0146 0.0145 0.0000 0.0000 0.0000
12 0 1822.0306 0.0297 0.0296 19.1488 4.4778 6.7581 0.3374 0.0626 0.0682 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
