# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:19:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9753 0.0376 0.0344 11.7179 2.4579 3.0673 0.3743 0.0680 0.0875 90.0000 0.0000 0.0000 0.4247 0.0200 0.0182 0.0000 0.0000 0.0000
12 0 1821.9893 0.0273 0.0278 25.0155 5.8365 9.0817 0.3143 0.0566 0.0608 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
