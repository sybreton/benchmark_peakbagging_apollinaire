# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:54:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0152 0.0272 0.0262 15.3766 3.1887 4.1632 0.3196 0.0580 0.0732 90.0000 0.0000 0.0000 0.3771 0.0141 0.0146 0.0000 0.0000 0.0000
12 0 1821.9901 0.0371 0.0359 14.2791 3.1196 4.6567 0.4613 0.0821 0.0913 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
