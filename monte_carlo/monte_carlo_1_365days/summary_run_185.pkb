# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:04:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9602 0.0306 0.0308 14.4196 2.8650 3.8334 0.3545 0.0612 0.0752 90.0000 0.0000 0.0000 0.3849 0.0162 0.0161 0.0000 0.0000 0.0000
12 0 1821.9834 0.0328 0.0329 19.1709 4.3375 6.5394 0.4039 0.0716 0.0803 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
