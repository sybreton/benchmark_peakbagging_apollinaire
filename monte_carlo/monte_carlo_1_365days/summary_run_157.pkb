# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:20:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9871 0.0308 0.0317 13.0385 2.8149 3.5018 0.3580 0.0687 0.0845 90.0000 0.0000 0.0000 0.3755 0.0162 0.0172 0.0000 0.0000 0.0000
12 0 1822.0119 0.0334 0.0337 15.3230 3.4894 5.2369 0.3901 0.0700 0.0795 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
