# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:55:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0381 0.0311 0.0302 14.7534 2.9161 3.6417 0.3405 0.0577 0.0722 90.0000 0.0000 0.0000 0.4155 0.0151 0.0145 0.0000 0.0000 0.0000
12 0 1822.0131 0.0263 0.0256 25.3178 6.1001 9.9708 0.2925 0.0557 0.0595 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
