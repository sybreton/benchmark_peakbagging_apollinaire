# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:15:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9834 0.0315 0.0359 12.9808 3.0933 4.0207 0.3602 0.0754 0.1068 90.0000 0.0000 0.0000 0.3972 0.0239 0.0215 0.0000 0.0000 0.0000
12 0 1822.0308 0.0343 0.0364 13.7581 3.1220 4.5375 0.4227 0.0771 0.0900 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
