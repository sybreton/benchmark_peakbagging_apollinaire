# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:27:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9719 0.0301 0.0282 13.1992 2.7325 3.6468 0.3402 0.0648 0.0776 90.0000 0.0000 0.0000 0.3967 0.0155 0.0153 0.0000 0.0000 0.0000
12 0 1822.0213 0.0267 0.0261 24.9098 6.1576 9.9208 0.2819 0.0545 0.0621 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
