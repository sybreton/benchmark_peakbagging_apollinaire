# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:51:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9899 0.0284 0.0281 15.5176 3.0803 3.9286 0.3353 0.0561 0.0695 90.0000 0.0000 0.0000 0.3977 0.0158 0.0155 0.0000 0.0000 0.0000
12 0 1822.0251 0.0295 0.0299 23.4986 5.2636 8.0963 0.3651 0.0656 0.0714 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
