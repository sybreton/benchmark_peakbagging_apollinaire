# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:48:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0275 0.0346 0.0283 15.9170 3.5461 4.5686 0.3247 0.0635 0.0842 90.0000 0.0000 0.0000 0.4010 0.0203 0.0182 0.0000 0.0000 0.0000
12 0 1821.9109 0.0308 0.0311 18.7181 4.2862 6.3423 0.3727 0.0654 0.0729 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
