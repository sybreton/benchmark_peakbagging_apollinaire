# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:52:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0010 0.0303 0.0300 13.3745 2.6327 3.4630 0.3525 0.0602 0.0746 90.0000 0.0000 0.0000 0.4268 0.0171 0.0173 0.0000 0.0000 0.0000
12 0 1822.0070 0.0280 0.0287 22.9782 5.3022 8.1235 0.3304 0.0591 0.0663 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
