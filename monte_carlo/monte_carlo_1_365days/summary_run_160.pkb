# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:27:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9850 0.0276 0.0287 14.1824 2.9130 3.8160 0.3097 0.0547 0.0648 90.0000 0.0000 0.0000 0.3872 0.0147 0.0146 0.0000 0.0000 0.0000
12 0 1822.0574 0.0428 0.0423 11.1100 2.3397 3.3661 0.5651 0.0995 0.1116 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
