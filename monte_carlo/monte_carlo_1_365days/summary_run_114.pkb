# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:43:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9848 0.0291 0.0295 15.9014 2.9789 3.9335 0.3299 0.0516 0.0600 90.0000 0.0000 0.0000 0.4160 0.0153 0.0147 0.0000 0.0000 0.0000
12 0 1822.0296 0.0326 0.0323 17.8341 4.0593 6.0120 0.3836 0.0693 0.0753 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
