# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:17:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9585 0.0352 0.0350 13.6040 2.6213 3.3805 0.3849 0.0649 0.0800 90.0000 0.0000 0.0000 0.4253 0.0196 0.0186 0.0000 0.0000 0.0000
12 0 1821.9696 0.0330 0.0333 15.5933 3.6006 5.3576 0.3947 0.0728 0.0840 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
