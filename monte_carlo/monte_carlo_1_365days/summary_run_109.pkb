# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:31:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0104 0.0233 0.0229 16.1906 3.2971 4.4399 0.2819 0.0501 0.0615 90.0000 0.0000 0.0000 0.4305 0.0135 0.0127 0.0000 0.0000 0.0000
12 0 1821.9800 0.0360 0.0359 14.5502 3.1563 4.4779 0.4397 0.0742 0.0835 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
