# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:26:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0028 0.0355 0.0337 11.5576 2.3892 3.1681 0.3667 0.0682 0.0829 90.0000 0.0000 0.0000 0.4129 0.0171 0.0169 0.0000 0.0000 0.0000
12 0 1821.9868 0.0282 0.0288 18.1566 4.4839 6.8741 0.3104 0.0619 0.0702 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
