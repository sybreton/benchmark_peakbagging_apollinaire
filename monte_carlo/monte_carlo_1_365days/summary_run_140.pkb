# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:42:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0076 0.0280 0.0276 16.9701 3.1730 4.2369 0.3293 0.0529 0.0611 90.0000 0.0000 0.0000 0.4146 0.0166 0.0164 0.0000 0.0000 0.0000
12 0 1821.9663 0.0293 0.0295 22.5517 5.1942 7.5387 0.3439 0.0586 0.0663 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
