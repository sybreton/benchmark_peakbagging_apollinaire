# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:35:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0136 0.0239 0.0250 16.6073 3.4341 4.8667 0.2660 0.0479 0.0570 90.0000 0.0000 0.0000 0.3919 0.0127 0.0125 0.0000 0.0000 0.0000
12 0 1822.0048 0.0283 0.0278 23.2227 5.5807 8.4347 0.3397 0.0647 0.0727 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
