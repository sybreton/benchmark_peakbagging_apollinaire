# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:36:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0208 0.0443 0.0419 10.2942 2.1987 2.8510 0.4394 0.0830 0.1063 90.0000 0.0000 0.0000 0.3689 0.0224 0.0218 0.0000 0.0000 0.0000
12 0 1822.0060 0.0299 0.0296 19.1394 4.5209 6.6878 0.3515 0.0641 0.0721 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
