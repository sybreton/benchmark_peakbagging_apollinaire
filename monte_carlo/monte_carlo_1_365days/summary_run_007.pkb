# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:01:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9853 0.0578 0.0491 9.9086 2.2036 2.6708 0.4847 0.0919 0.1284 90.0000 0.0000 0.0000 0.4137 0.0322 0.0265 0.0000 0.0000 0.0000
12 0 1822.0228 0.0426 0.0424 10.7558 2.3814 3.5384 0.5116 0.0957 0.1090 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
