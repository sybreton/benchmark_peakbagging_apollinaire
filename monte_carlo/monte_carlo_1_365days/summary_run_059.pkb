# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:06:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9974 0.0371 0.0363 12.1360 2.4715 3.0121 0.4253 0.0762 0.0997 90.0000 0.0000 0.0000 0.4013 0.0185 0.0182 0.0000 0.0000 0.0000
12 0 1822.0144 0.0234 0.0241 27.6416 7.1271 12.1454 0.2578 0.0539 0.0590 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
