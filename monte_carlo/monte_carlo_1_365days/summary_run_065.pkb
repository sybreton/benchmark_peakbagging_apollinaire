# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:32:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0389 0.0270 0.0284 19.8161 3.7944 5.2049 0.3277 0.0529 0.0616 90.0000 0.0000 0.0000 0.3825 0.0162 0.0152 0.0000 0.0000 0.0000
12 0 1821.9962 0.0249 0.0248 31.8455 7.9070 12.6642 0.2875 0.0538 0.0597 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
