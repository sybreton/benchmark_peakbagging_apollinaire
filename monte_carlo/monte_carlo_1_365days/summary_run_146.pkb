# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:55:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9743 0.0292 0.0290 14.4919 3.0701 4.1149 0.3242 0.0619 0.0761 90.0000 0.0000 0.0000 0.4031 0.0161 0.0166 0.0000 0.0000 0.0000
12 0 1821.9869 0.0279 0.0291 18.7920 4.6254 6.8746 0.3116 0.0608 0.0710 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
