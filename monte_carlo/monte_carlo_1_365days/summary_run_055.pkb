# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:50:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0154 0.0352 0.0356 10.5399 2.1570 2.8466 0.3871 0.0701 0.0901 90.0000 0.0000 0.0000 0.4183 0.0181 0.0178 0.0000 0.0000 0.0000
12 0 1822.0350 0.0271 0.0280 23.2471 5.6437 8.5706 0.3137 0.0586 0.0644 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
