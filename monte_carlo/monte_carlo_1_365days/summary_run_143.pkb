# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:49:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9985 0.0257 0.0262 17.4775 3.4400 4.6598 0.3083 0.0528 0.0641 90.0000 0.0000 0.0000 0.4014 0.0140 0.0136 0.0000 0.0000 0.0000
12 0 1822.0255 0.0295 0.0301 18.8938 4.4802 6.5913 0.3354 0.0614 0.0682 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
