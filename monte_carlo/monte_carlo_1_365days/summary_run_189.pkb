# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:13:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9993 0.0469 0.0383 10.6599 2.4383 2.9695 0.4524 0.0920 0.1329 90.0000 0.0000 0.0000 0.4071 0.0227 0.0208 0.0000 0.0000 0.0000
12 0 1821.9743 0.0347 0.0328 17.4398 3.7543 5.5652 0.4032 0.0687 0.0771 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
