# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:03:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0262 0.0399 0.0391 11.0133 2.3586 3.0218 0.3947 0.0711 0.0955 90.0000 0.0000 0.0000 0.4075 0.0236 0.0223 0.0000 0.0000 0.0000
12 0 1821.9560 0.0312 0.0301 17.9543 4.3079 6.4081 0.3439 0.0631 0.0709 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
