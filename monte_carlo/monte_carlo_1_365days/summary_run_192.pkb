# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:20:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9704 0.0307 0.0307 14.9222 3.0997 4.0895 0.3454 0.0637 0.0807 90.0000 0.0000 0.0000 0.3941 0.0168 0.0167 0.0000 0.0000 0.0000
12 0 1821.9884 0.0311 0.0301 20.1049 4.6391 6.9383 0.3647 0.0647 0.0752 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
