# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:17:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9573 0.0240 0.0241 17.0495 3.5252 4.6856 0.2642 0.0463 0.0557 90.0000 0.0000 0.0000 0.4053 0.0138 0.0133 0.0000 0.0000 0.0000
12 0 1821.9462 0.0296 0.0292 21.8528 5.1072 8.0841 0.3424 0.0633 0.0704 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
