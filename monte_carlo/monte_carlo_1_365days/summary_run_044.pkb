# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:03:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9619 0.0201 0.0207 19.9771 4.2822 5.9456 0.2372 0.0444 0.0546 90.0000 0.0000 0.0000 0.3769 0.0117 0.0116 0.0000 0.0000 0.0000
12 0 1821.9909 0.0214 0.0220 33.3141 9.1552 15.9662 0.2200 0.0485 0.0552 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
