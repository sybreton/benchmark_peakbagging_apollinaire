# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:34:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9811 0.0280 0.0273 16.6463 3.2361 4.2535 0.3256 0.0533 0.0651 90.0000 0.0000 0.0000 0.3996 0.0144 0.0142 0.0000 0.0000 0.0000
12 0 1821.9841 0.0338 0.0336 16.9464 3.7456 5.3306 0.4177 0.0717 0.0830 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
