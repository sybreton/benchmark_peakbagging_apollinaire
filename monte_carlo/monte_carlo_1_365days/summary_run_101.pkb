# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:12:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0641 0.0235 0.0240 20.1643 4.0767 5.3553 0.2915 0.0487 0.0591 90.0000 0.0000 0.0000 0.3808 0.0126 0.0126 0.0000 0.0000 0.0000
12 0 1821.9891 0.0286 0.0287 23.2060 5.4212 8.5384 0.3423 0.0644 0.0683 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
