# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:36:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9903 0.0396 0.0400 10.4389 2.1440 2.7818 0.4134 0.0750 0.0986 90.0000 0.0000 0.0000 0.3952 0.0196 0.0191 0.0000 0.0000 0.0000
12 0 1821.9810 0.0374 0.0358 13.2653 3.0856 4.6059 0.4152 0.0769 0.0855 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
