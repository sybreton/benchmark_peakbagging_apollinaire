# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:19:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9638 0.0297 0.0295 13.6246 2.7709 3.6048 0.3427 0.0625 0.0760 90.0000 0.0000 0.0000 0.3891 0.0151 0.0156 0.0000 0.0000 0.0000
12 0 1821.9937 0.0298 0.0296 18.5628 4.4839 6.9101 0.3342 0.0654 0.0740 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
