# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:15:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9875 0.0206 0.0203 17.1727 3.7089 5.3864 0.2318 0.0458 0.0542 90.0000 0.0000 0.0000 0.4024 0.0103 0.0104 0.0000 0.0000 0.0000
12 0 1822.0454 0.0336 0.0345 16.4155 3.6083 5.3388 0.4256 0.0749 0.0844 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
