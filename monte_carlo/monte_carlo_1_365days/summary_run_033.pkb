# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:31:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9862 0.0196 0.0190 18.7632 4.1776 5.9034 0.2149 0.0428 0.0506 90.0000 0.0000 0.0000 0.4140 0.0110 0.0106 0.0000 0.0000 0.0000
12 0 1822.0369 0.0329 0.0320 18.3848 3.9817 5.7781 0.4096 0.0703 0.0798 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
