# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:54:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9523 0.0282 0.0277 13.0033 2.8783 3.8474 0.3169 0.0644 0.0829 90.0000 0.0000 0.0000 0.3908 0.0170 0.0174 0.0000 0.0000 0.0000
12 0 1822.0282 0.0279 0.0274 22.5201 5.2737 8.1979 0.3345 0.0614 0.0685 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
