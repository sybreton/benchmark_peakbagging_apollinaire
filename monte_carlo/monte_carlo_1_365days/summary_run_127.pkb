# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:12:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9787 0.0299 0.0304 12.7832 2.7872 3.6038 0.3338 0.0633 0.0800 90.0000 0.0000 0.0000 0.3985 0.0176 0.0173 0.0000 0.0000 0.0000
12 0 1821.9828 0.0275 0.0279 23.5478 5.5492 8.4551 0.3104 0.0561 0.0618 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
