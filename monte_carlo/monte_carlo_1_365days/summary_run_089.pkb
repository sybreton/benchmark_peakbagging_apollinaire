# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 19:39:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9789 0.0251 0.0250 16.7412 3.4186 4.4556 0.2760 0.0471 0.0572 90.0000 0.0000 0.0000 0.3943 0.0127 0.0130 0.0000 0.0000 0.0000
12 0 1821.9996 0.0276 0.0267 22.7041 5.5749 8.6702 0.3079 0.0598 0.0671 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
