# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:18:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9959 0.0233 0.0246 17.0344 3.5647 4.8676 0.2678 0.0493 0.0593 90.0000 0.0000 0.0000 0.3945 0.0123 0.0126 0.0000 0.0000 0.0000
12 0 1822.0633 0.0310 0.0296 17.1835 4.1276 6.3540 0.3421 0.0704 0.0801 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
