# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:13:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9758 0.0309 0.0298 13.0282 2.9369 3.8819 0.3380 0.0693 0.0895 90.0000 0.0000 0.0000 0.3754 0.0178 0.0189 0.0000 0.0000 0.0000
12 0 1821.9885 0.0249 0.0254 26.5635 6.7381 10.7432 0.2666 0.0513 0.0580 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
