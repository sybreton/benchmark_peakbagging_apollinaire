# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:40:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9654 0.0240 0.0241 15.8245 3.2354 4.2367 0.2749 0.0470 0.0571 90.0000 0.0000 0.0000 0.4207 0.0132 0.0132 0.0000 0.0000 0.0000
12 0 1822.0212 0.0377 0.0378 12.4227 2.8028 4.0993 0.4307 0.0773 0.0891 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
