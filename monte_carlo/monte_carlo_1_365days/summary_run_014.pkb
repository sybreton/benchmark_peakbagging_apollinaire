# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:29:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0168 0.0264 0.0264 17.2533 3.3321 4.5569 0.3183 0.0538 0.0624 90.0000 0.0000 0.0000 0.4197 0.0141 0.0141 0.0000 0.0000 0.0000
12 0 1821.9907 0.0302 0.0296 19.2326 4.5092 6.8437 0.3355 0.0619 0.0676 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
