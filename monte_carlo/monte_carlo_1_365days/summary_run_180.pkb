# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:53:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0216 0.0362 0.0354 15.1606 3.1042 3.9523 0.3552 0.0616 0.0788 90.0000 0.0000 0.0000 0.3754 0.0215 0.0203 0.0000 0.0000 0.0000
12 0 1821.9987 0.0295 0.0294 20.6690 4.7207 7.1097 0.3488 0.0623 0.0685 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
