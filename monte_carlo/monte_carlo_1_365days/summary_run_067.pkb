# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:40:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0301 0.0294 0.0284 16.1423 3.1335 4.1786 0.3344 0.0577 0.0692 90.0000 0.0000 0.0000 0.3909 0.0159 0.0157 0.0000 0.0000 0.0000
12 0 1822.0192 0.0321 0.0319 16.4495 3.8750 5.5900 0.3852 0.0723 0.0827 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
