# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 14:22:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0115 0.0331 0.0291 13.4299 2.8207 3.7998 0.3393 0.0651 0.0800 90.0000 0.0000 0.0000 0.3926 0.0161 0.0156 0.0000 0.0000 0.0000
12 0 1822.0438 0.0257 0.0251 25.4539 6.3806 10.0072 0.2757 0.0534 0.0605 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
