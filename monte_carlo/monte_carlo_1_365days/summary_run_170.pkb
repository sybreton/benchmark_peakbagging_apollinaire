# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:31:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9935 0.0273 0.0283 16.4980 3.3698 4.3517 0.3387 0.0607 0.0751 90.0000 0.0000 0.0000 0.3728 0.0169 0.0165 0.0000 0.0000 0.0000
12 0 1821.9801 0.0317 0.0320 19.6239 4.3640 6.4749 0.3712 0.0622 0.0692 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
