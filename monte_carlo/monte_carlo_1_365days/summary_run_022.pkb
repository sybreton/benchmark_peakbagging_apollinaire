# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:57:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9923 0.0242 0.0234 17.9040 3.6852 5.0732 0.2755 0.0487 0.0579 90.0000 0.0000 0.0000 0.3901 0.0136 0.0137 0.0000 0.0000 0.0000
12 0 1821.9880 0.0308 0.0309 20.6717 4.6493 6.9303 0.3759 0.0661 0.0743 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
