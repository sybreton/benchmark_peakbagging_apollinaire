# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:57:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9779 0.0268 0.0276 16.5187 3.2410 4.2719 0.3306 0.0554 0.0685 90.0000 0.0000 0.0000 0.3917 0.0147 0.0152 0.0000 0.0000 0.0000
12 0 1821.9822 0.0296 0.0297 21.2205 4.8887 7.4346 0.3455 0.0614 0.0697 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
