# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:24:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0435 0.0277 0.0271 14.5782 2.9595 4.0098 0.3025 0.0532 0.0665 90.0000 0.0000 0.0000 0.4062 0.0150 0.0140 0.0000 0.0000 0.0000
12 0 1822.0286 0.0311 0.0306 21.7549 4.8717 7.3752 0.3668 0.0617 0.0687 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
