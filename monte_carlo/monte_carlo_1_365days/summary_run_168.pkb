# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:27:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9715 0.0407 0.0431 10.3859 2.1059 2.7132 0.4443 0.0807 0.1015 90.0000 0.0000 0.0000 0.4120 0.0227 0.0214 0.0000 0.0000 0.0000
12 0 1821.9666 0.0435 0.0441 9.8305 2.0569 3.1112 0.5809 0.1099 0.1270 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
