# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:16:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0136 0.0388 0.0386 9.1831 2.0972 2.7123 0.3786 0.0757 0.1034 90.0000 0.0000 0.0000 0.4380 0.0203 0.0206 0.0000 0.0000 0.0000
12 0 1822.0031 0.0273 0.0273 25.6864 6.1104 9.4587 0.3142 0.0563 0.0635 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
