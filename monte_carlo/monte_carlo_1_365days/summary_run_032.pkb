# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:28:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0281 0.0254 0.0249 18.8010 3.7805 5.1378 0.2725 0.0464 0.0562 90.0000 0.0000 0.0000 0.3914 0.0137 0.0136 0.0000 0.0000 0.0000
12 0 1821.9738 0.0308 0.0313 19.1886 4.4219 6.4436 0.3820 0.0689 0.0779 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
