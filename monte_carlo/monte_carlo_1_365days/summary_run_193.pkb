# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:22:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9976 0.0246 0.0245 15.3522 3.1695 4.3504 0.2765 0.0498 0.0601 90.0000 0.0000 0.0000 0.4001 0.0150 0.0150 0.0000 0.0000 0.0000
12 0 1821.9917 0.0399 0.0404 11.9109 2.5684 3.8379 0.5221 0.0964 0.1111 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
