# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:27:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0137 0.0307 0.0305 11.1119 2.3100 3.0958 0.3260 0.0599 0.0755 90.0000 0.0000 0.0000 0.4086 0.0163 0.0161 0.0000 0.0000 0.0000
12 0 1822.0554 0.0311 0.0309 18.4909 4.3258 6.2109 0.3628 0.0657 0.0715 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
