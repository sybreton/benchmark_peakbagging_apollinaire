# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 16:04:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9830 0.0326 0.0309 14.1859 2.8478 3.6227 0.3659 0.0635 0.0813 90.0000 0.0000 0.0000 0.4166 0.0172 0.0168 0.0000 0.0000 0.0000
12 0 1821.9943 0.0364 0.0374 12.7323 2.8356 4.4317 0.4513 0.0889 0.1025 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
