# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:18:32
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9793 0.0430 0.0423 11.2150 2.1356 2.6946 0.4917 0.0820 0.1072 90.0000 0.0000 0.0000 0.4041 0.0215 0.0204 0.0000 0.0000 0.0000
12 0 1821.9636 0.0256 0.0252 26.4702 6.3971 10.3509 0.2924 0.0553 0.0599 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
