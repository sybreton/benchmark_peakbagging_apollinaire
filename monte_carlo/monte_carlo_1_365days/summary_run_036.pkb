# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:39:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0299 0.0314 0.0321 14.0793 2.7902 3.6244 0.3828 0.0667 0.0833 90.0000 0.0000 0.0000 0.4074 0.0168 0.0167 0.0000 0.0000 0.0000
12 0 1822.0375 0.0376 0.0378 13.0715 2.7940 3.9566 0.4971 0.0882 0.0974 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
