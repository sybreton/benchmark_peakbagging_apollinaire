# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:57:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0011 0.0352 0.0351 12.4942 2.7883 3.7324 0.3805 0.0758 0.1003 90.0000 0.0000 0.0000 0.3855 0.0231 0.0211 0.0000 0.0000 0.0000
12 0 1822.0028 0.0230 0.0236 30.9935 8.2922 13.3988 0.2407 0.0497 0.0574 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
