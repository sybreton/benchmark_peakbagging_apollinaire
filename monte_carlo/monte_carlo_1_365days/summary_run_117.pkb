# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:50:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9946 0.0279 0.0270 15.3815 3.2855 4.3173 0.3155 0.0586 0.0757 90.0000 0.0000 0.0000 0.3674 0.0168 0.0156 0.0000 0.0000 0.0000
12 0 1822.0064 0.0265 0.0259 23.3706 5.5380 9.0889 0.2978 0.0575 0.0625 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
