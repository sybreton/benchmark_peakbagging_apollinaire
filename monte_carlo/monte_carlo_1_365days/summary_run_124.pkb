# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:06:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9758 0.0302 0.0305 15.7879 3.0351 3.9737 0.3497 0.0571 0.0705 90.0000 0.0000 0.0000 0.4084 0.0174 0.0167 0.0000 0.0000 0.0000
12 0 1822.0095 0.0319 0.0312 17.9109 4.2481 6.0153 0.3801 0.0690 0.0828 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
