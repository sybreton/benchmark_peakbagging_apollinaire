# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 13:10:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0181 0.0313 0.0324 14.7024 2.8415 3.6308 0.3462 0.0555 0.0661 90.0000 0.0000 0.0000 0.3986 0.0172 0.0168 0.0000 0.0000 0.0000
12 0 1822.0231 0.0327 0.0330 19.4126 4.0888 5.7679 0.4160 0.0673 0.0752 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
