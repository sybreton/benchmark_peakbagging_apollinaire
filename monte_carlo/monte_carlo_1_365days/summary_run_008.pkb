# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 12:05:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9551 0.0410 0.0402 12.5744 2.3418 2.8118 0.4447 0.0689 0.0866 90.0000 0.0000 0.0000 0.4214 0.0201 0.0187 0.0000 0.0000 0.0000
12 0 1821.9711 0.0330 0.0336 15.8662 3.6589 5.2306 0.4091 0.0757 0.0901 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
