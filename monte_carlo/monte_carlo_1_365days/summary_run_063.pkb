# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 15:23:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9646 0.0232 0.0227 19.2286 3.8448 5.5178 0.2764 0.0493 0.0577 90.0000 0.0000 0.0000 0.3970 0.0123 0.0125 0.0000 0.0000 0.0000
12 0 1821.9853 0.0295 0.0296 19.6362 4.6937 7.4281 0.3214 0.0597 0.0657 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
