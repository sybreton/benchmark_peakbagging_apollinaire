# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 11:41:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0149 0.0302 0.0292 13.8329 2.8143 3.6543 0.3311 0.0572 0.0670 90.0000 0.0000 0.0000 0.4104 0.0149 0.0149 0.0000 0.0000 0.0000
12 0 1822.0207 0.0246 0.0248 29.7001 7.3257 11.8463 0.2746 0.0524 0.0574 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
