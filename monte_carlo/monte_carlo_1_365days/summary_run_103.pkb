# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 20:17:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0008 0.0332 0.0326 13.8896 2.9273 3.7887 0.3676 0.0668 0.0830 90.0000 0.0000 0.0000 0.3733 0.0214 0.0201 0.0000 0.0000 0.0000
12 0 1821.9117 0.0304 0.0308 17.7055 4.1998 6.2742 0.3529 0.0668 0.0799 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
