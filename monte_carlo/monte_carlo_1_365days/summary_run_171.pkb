# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 10:33:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1810.0127 0.0397 0.0410 10.4216 2.3197 2.9438 0.4493 0.0881 0.1231 90.0000 0.0000 0.0000 0.4023 0.0218 0.0213 0.0000 0.0000 0.0000
12 0 1821.9677 0.0258 0.0260 26.7968 6.4626 10.2760 0.2964 0.0544 0.0600 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
