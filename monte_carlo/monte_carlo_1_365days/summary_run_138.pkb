# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 21:37:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9908 0.0277 0.0277 14.8582 3.0772 4.0698 0.3400 0.0630 0.0790 90.0000 0.0000 0.0000 0.3788 0.0153 0.0157 0.0000 0.0000 0.0000
12 0 1821.9506 0.0325 0.0328 16.9232 3.8833 5.8430 0.3794 0.0692 0.0802 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
