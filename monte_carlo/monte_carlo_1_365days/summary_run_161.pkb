# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 30/03/2022 22:30:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
11 2 1809.9872 0.0389 0.0326 10.6554 2.7804 3.7065 0.3605 0.0903 0.1335 90.0000 0.0000 0.0000 0.4216 0.0220 0.0250 0.0000 0.0000 0.0000
12 0 1822.0128 0.0258 0.0268 27.4697 6.3915 9.9768 0.3068 0.0555 0.0606 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
