# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 14:51:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2043 0.1829 0.1972 21.9119 1.9139 2.2549 4.1428 0.3823 0.4022 90.0000 0.0000 0.0000 0.2246 0.1526 0.1912 0.0000 0.0000 0.0000
26 0 3710.1874 0.1426 0.1366 33.1013 2.9660 3.3646 3.4609 0.2796 0.2996 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
