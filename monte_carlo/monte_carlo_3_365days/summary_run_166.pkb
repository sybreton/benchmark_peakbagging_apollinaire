# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:04:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0311 0.2083 0.2162 20.2448 2.2434 2.7245 3.9949 0.5281 0.5402 90.0000 0.0000 0.0000 0.5674 0.2681 0.1686 0.0000 0.0000 0.0000
26 0 3710.0743 0.1783 0.1696 24.2707 1.9534 2.1239 4.7782 0.3551 0.3696 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
