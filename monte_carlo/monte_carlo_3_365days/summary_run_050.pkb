# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:25:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0846 0.2008 0.2250 22.3388 2.4919 2.9369 3.7709 0.4406 0.4880 90.0000 0.0000 0.0000 0.5555 0.2586 0.1782 0.0000 0.0000 0.0000
26 0 3709.8863 0.1564 0.1525 28.8325 2.4170 2.6865 4.1003 0.3152 0.3350 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
