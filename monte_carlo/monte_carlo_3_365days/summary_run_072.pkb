# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:17:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8199 0.1592 0.1664 22.4908 2.2399 2.6788 3.8287 0.4068 0.4042 90.0000 0.0000 0.0000 0.3408 0.2217 0.2011 0.0000 0.0000 0.0000
26 0 3709.9350 0.1340 0.1287 32.0825 2.7307 3.0497 3.8918 0.2886 0.3093 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
