# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:11:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9971 0.2037 0.2257 19.8289 2.0466 2.4047 4.0465 0.4329 0.4333 90.0000 0.0000 0.0000 0.4691 0.2656 0.2031 0.0000 0.0000 0.0000
26 0 3710.1339 0.1540 0.1510 30.1539 2.3951 2.8144 4.2336 0.3168 0.3225 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
