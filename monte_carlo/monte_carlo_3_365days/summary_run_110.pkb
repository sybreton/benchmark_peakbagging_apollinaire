# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:44:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0227 0.1670 0.1821 23.9361 2.5477 3.0939 3.4915 0.4201 0.4208 90.0000 0.0000 0.0000 0.4438 0.2404 0.1672 0.0000 0.0000 0.0000
26 0 3710.0334 0.1440 0.1442 31.3779 2.5927 2.8172 4.0629 0.2886 0.3057 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
