# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:47:12
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0367 0.2217 0.2457 18.8667 1.7268 1.9351 4.6998 0.4592 0.4901 90.0000 0.0000 0.0000 0.2320 0.1615 0.2146 0.0000 0.0000 0.0000
26 0 3709.9831 0.1645 0.1621 28.5149 2.3999 2.6652 4.2424 0.3474 0.3640 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
