# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:05:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1549 0.1930 0.2145 20.7677 2.0083 2.3566 4.2216 0.4536 0.4594 90.0000 0.0000 0.0000 0.3292 0.2188 0.2149 0.0000 0.0000 0.0000
26 0 3709.7939 0.1704 0.1594 27.2763 2.2430 2.4713 4.2877 0.3149 0.3569 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
