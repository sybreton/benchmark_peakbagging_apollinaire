# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:42:02
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0599 0.2012 0.2213 20.4953 2.1111 2.4081 4.2692 0.5021 0.4951 90.0000 0.0000 0.0000 0.4719 0.2782 0.2044 0.0000 0.0000 0.0000
26 0 3709.9893 0.1619 0.1557 28.5873 2.3627 2.6966 4.1525 0.3095 0.3330 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
