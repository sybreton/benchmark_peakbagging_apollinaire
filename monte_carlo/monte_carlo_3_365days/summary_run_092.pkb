# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:03:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9935 0.1670 0.1746 24.3648 2.4133 2.8062 3.5649 0.3579 0.3685 90.0000 0.0000 0.0000 0.2747 0.1836 0.1982 0.0000 0.0000 0.0000
26 0 3709.9251 0.1349 0.1292 37.0918 3.1973 3.6255 3.6411 0.2701 0.2826 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
