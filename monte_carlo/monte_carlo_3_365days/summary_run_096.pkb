# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:12:44
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0174 0.1693 0.1830 22.2858 2.1485 2.5825 3.6917 0.3765 0.3872 90.0000 0.0000 0.0000 0.2957 0.1956 0.1973 0.0000 0.0000 0.0000
26 0 3710.0837 0.1455 0.1401 31.0651 2.6340 2.8989 3.9187 0.2931 0.3194 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
