# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:19:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.6125 0.2855 0.3259 19.6563 1.9722 2.3596 4.8508 0.4830 0.5050 90.0000 0.0000 0.0000 0.5343 0.3270 0.2617 0.0000 0.0000 0.0000
26 0 3710.1453 0.1710 0.1642 28.6942 2.6996 3.0267 3.5228 0.3413 0.3436 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
