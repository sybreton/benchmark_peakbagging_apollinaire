# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:10:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8795 0.1733 0.1862 21.3119 1.9779 2.3258 4.1540 0.4052 0.4090 90.0000 0.0000 0.0000 0.3287 0.2148 0.2176 0.0000 0.0000 0.0000
26 0 3710.1243 0.1395 0.1332 28.2328 2.3840 2.8653 3.7962 0.2860 0.3071 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
