# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:04:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.5811 0.2258 0.2367 18.1888 1.9999 2.3110 3.8626 0.4671 0.4849 90.0000 0.0000 0.0000 0.5234 0.2643 0.1902 0.0000 0.0000 0.0000
26 0 3709.8106 0.1495 0.1494 31.8221 2.4849 2.8250 4.2309 0.2955 0.3164 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
