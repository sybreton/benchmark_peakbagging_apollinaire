# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:53:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2043 0.2269 0.2428 17.5354 1.6752 1.9687 4.7379 0.5181 0.5052 90.0000 0.0000 0.0000 0.3447 0.2348 0.2466 0.0000 0.0000 0.0000
26 0 3709.9703 0.1439 0.1407 31.3033 2.6801 2.9562 3.8720 0.2944 0.3138 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
