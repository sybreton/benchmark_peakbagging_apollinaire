# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:10:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2490 0.1864 0.1932 20.2606 1.8495 2.1820 4.1725 0.3968 0.4255 90.0000 0.0000 0.0000 0.1901 0.1314 0.1832 0.0000 0.0000 0.0000
26 0 3709.8898 0.1581 0.1527 28.6085 2.3758 2.7293 4.0953 0.3187 0.3239 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
