# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:16:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8820 0.1791 0.1879 21.2414 2.2167 2.6807 3.7211 0.4468 0.4460 90.0000 0.0000 0.0000 0.4140 0.2428 0.1823 0.0000 0.0000 0.0000
26 0 3709.9800 0.1295 0.1246 33.1632 2.8630 3.1995 3.6870 0.2659 0.2866 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
