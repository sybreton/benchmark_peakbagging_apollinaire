# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:31:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0781 0.1895 0.2006 21.1321 1.9601 2.3022 4.0317 0.4001 0.3995 90.0000 0.0000 0.0000 0.3156 0.2074 0.2019 0.0000 0.0000 0.0000
26 0 3709.9484 0.1605 0.1570 28.7546 2.3670 2.6051 4.2881 0.3181 0.3340 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
