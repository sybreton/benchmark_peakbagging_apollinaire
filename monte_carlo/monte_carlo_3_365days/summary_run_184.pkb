# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:46:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8891 0.1674 0.1760 19.8659 2.0594 2.3775 3.5016 0.3743 0.3999 90.0000 0.0000 0.0000 0.2199 0.1480 0.1777 0.0000 0.0000 0.0000
26 0 3709.7531 0.1435 0.1365 31.9894 2.5486 2.9144 4.2282 0.2978 0.3122 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
