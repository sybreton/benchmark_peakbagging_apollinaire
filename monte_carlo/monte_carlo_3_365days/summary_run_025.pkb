# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:27:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8255 0.1630 0.1710 21.9096 2.2350 2.8567 3.6758 0.4442 0.4271 90.0000 0.0000 0.0000 0.3642 0.2262 0.1858 0.0000 0.0000 0.0000
26 0 3709.8145 0.1380 0.1323 32.1700 2.6568 2.9509 4.0244 0.2885 0.2981 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
