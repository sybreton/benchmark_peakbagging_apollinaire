# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 14:45:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9039 0.1660 0.1785 22.9356 2.1657 2.6634 3.9327 0.4089 0.4104 90.0000 0.0000 0.0000 0.3986 0.2400 0.1846 0.0000 0.0000 0.0000
26 0 3710.2216 0.1409 0.1352 28.8859 2.3857 2.7769 3.8398 0.2858 0.3077 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
