# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:26:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1487 0.2026 0.2256 19.7770 2.0055 2.3614 3.9922 0.4117 0.4276 90.0000 0.0000 0.0000 0.2938 0.1946 0.2148 0.0000 0.0000 0.0000
26 0 3709.9819 0.1621 0.1575 31.7454 2.5642 2.9690 4.1923 0.3129 0.3244 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
