# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:45:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0531 0.1724 0.1817 22.2800 2.3062 2.6991 3.9219 0.4270 0.4506 90.0000 0.0000 0.0000 0.5047 0.2508 0.1735 0.0000 0.0000 0.0000
26 0 3710.1120 0.1398 0.1333 28.5211 2.4312 2.8159 3.8511 0.2985 0.3109 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
