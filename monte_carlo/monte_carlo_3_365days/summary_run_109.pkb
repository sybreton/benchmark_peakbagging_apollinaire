# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:42:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1075 0.2017 0.2272 21.0503 2.0373 2.3161 4.2378 0.4338 0.4184 90.0000 0.0000 0.0000 0.3711 0.2441 0.2508 0.0000 0.0000 0.0000
26 0 3710.0692 0.1729 0.1690 26.2538 2.2244 2.5223 4.3315 0.3548 0.3637 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
