# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:23:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1109 0.1960 0.2107 22.4362 2.4390 2.8794 3.7554 0.4399 0.4625 90.0000 0.0000 0.0000 0.5141 0.2561 0.1795 0.0000 0.0000 0.0000
26 0 3710.0346 0.1629 0.1588 27.8622 2.2835 2.5719 4.2817 0.3292 0.3489 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
