# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:44:22
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0322 0.1923 0.1953 21.1433 2.0279 2.4527 4.1109 0.4365 0.4312 90.0000 0.0000 0.0000 0.3449 0.2249 0.2160 0.0000 0.0000 0.0000
26 0 3710.0526 0.1418 0.1354 31.3302 2.7426 3.1247 3.7491 0.2931 0.3046 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
