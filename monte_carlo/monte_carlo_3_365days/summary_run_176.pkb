# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:27:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6431 0.1908 0.1964 20.6077 2.2703 2.6400 3.4574 0.4079 0.4224 90.0000 0.0000 0.0000 0.5029 0.2247 0.1593 0.0000 0.0000 0.0000
26 0 3709.8908 0.1669 0.1621 26.2034 2.0393 2.2935 4.7245 0.3357 0.3554 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
