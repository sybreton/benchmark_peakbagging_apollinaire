# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:49:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1104 0.1956 0.2184 24.0790 2.7098 3.2457 3.7378 0.5010 0.5259 90.0000 0.0000 0.0000 0.6469 0.2435 0.1523 0.0000 0.0000 0.0000
26 0 3709.9714 0.1709 0.1576 27.5669 2.2453 2.6159 4.1156 0.3239 0.3394 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
