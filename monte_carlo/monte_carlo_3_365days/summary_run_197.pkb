# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:17:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7174 0.1925 0.2036 22.7984 2.9347 3.3803 3.1700 0.4533 0.5047 90.0000 0.0000 0.0000 0.6649 0.1786 0.1261 0.0000 0.0000 0.0000
26 0 3709.8981 0.1486 0.1419 30.9045 2.4311 2.8287 4.1601 0.3022 0.3147 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
