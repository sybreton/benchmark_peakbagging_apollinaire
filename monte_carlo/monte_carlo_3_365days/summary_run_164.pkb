# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:59:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0682 0.2056 0.2170 20.3996 2.0538 2.5563 4.1530 0.4562 0.4688 90.0000 0.0000 0.0000 0.4722 0.2786 0.2173 0.0000 0.0000 0.0000
26 0 3710.2140 0.1399 0.1375 30.7432 2.6365 3.0814 3.6502 0.2798 0.2973 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
