# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:16:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9896 0.1948 0.2041 23.0958 2.3316 2.7372 4.2039 0.4678 0.4758 90.0000 0.0000 0.0000 0.4786 0.2777 0.2084 0.0000 0.0000 0.0000
26 0 3710.0272 0.1477 0.1502 29.6947 2.5698 2.9427 3.9674 0.2973 0.3192 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
