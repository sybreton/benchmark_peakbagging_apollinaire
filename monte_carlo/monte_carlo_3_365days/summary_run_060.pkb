# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:48:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1865 0.2002 0.2101 20.9010 2.0642 2.3971 4.3628 0.4585 0.4462 90.0000 0.0000 0.0000 0.3711 0.2421 0.2258 0.0000 0.0000 0.0000
26 0 3709.8264 0.1374 0.1343 33.0934 2.8653 3.2667 3.6176 0.2822 0.2884 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
