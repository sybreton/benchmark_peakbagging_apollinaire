# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:12:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8097 0.1530 0.1595 25.2581 2.6271 3.1553 3.2506 0.3523 0.3563 90.0000 0.0000 0.0000 0.3272 0.2032 0.1750 0.0000 0.0000 0.0000
26 0 3709.8733 0.1396 0.1342 30.8907 2.5168 2.8610 4.0084 0.2855 0.2981 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
