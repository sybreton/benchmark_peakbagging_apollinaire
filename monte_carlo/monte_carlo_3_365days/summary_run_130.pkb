# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:34:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7671 0.1607 0.1705 22.5177 2.1632 2.5370 3.6362 0.3573 0.3704 90.0000 0.0000 0.0000 0.2481 0.1701 0.1906 0.0000 0.0000 0.0000
26 0 3709.9088 0.1389 0.1318 31.8303 2.5946 2.9171 3.9455 0.2765 0.2850 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
