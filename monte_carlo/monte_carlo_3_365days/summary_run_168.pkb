# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:09:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7794 0.1814 0.1861 21.6992 2.2345 2.7287 3.6816 0.4150 0.4195 90.0000 0.0000 0.0000 0.4103 0.2507 0.1962 0.0000 0.0000 0.0000
26 0 3709.9446 0.1405 0.1352 33.0869 2.8114 3.2229 3.8319 0.2900 0.2970 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
