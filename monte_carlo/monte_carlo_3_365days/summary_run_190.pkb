# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:00:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0896 0.1618 0.1698 23.4281 2.1864 2.4860 3.8354 0.3503 0.3789 90.0000 0.0000 0.0000 0.1719 0.1182 0.1640 0.0000 0.0000 0.0000
26 0 3709.8195 0.1549 0.1448 29.8129 2.4714 2.7611 4.1282 0.3017 0.3235 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
