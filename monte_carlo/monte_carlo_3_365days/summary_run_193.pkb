# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:07:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9106 0.2180 0.2286 18.7285 1.9151 2.2320 4.3257 0.4883 0.4903 90.0000 0.0000 0.0000 0.5037 0.2896 0.2117 0.0000 0.0000 0.0000
26 0 3709.9979 0.1455 0.1471 31.0570 2.6354 2.8659 3.8605 0.2772 0.2976 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
