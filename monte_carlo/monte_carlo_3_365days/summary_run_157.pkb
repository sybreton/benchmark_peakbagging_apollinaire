# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:43:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8292 0.1618 0.1592 24.3584 2.3285 2.9456 3.7416 0.4092 0.3984 90.0000 0.0000 0.0000 0.3828 0.2302 0.1899 0.0000 0.0000 0.0000
26 0 3710.0309 0.1199 0.1166 34.2995 3.0019 3.5055 3.5253 0.2729 0.2804 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
