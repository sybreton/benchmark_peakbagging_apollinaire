# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:37:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0259 0.1844 0.1891 21.0713 2.0366 2.4379 4.1221 0.4345 0.4357 90.0000 0.0000 0.0000 0.3651 0.2302 0.2062 0.0000 0.0000 0.0000
26 0 3710.0494 0.1357 0.1287 32.7451 2.8142 3.1626 3.6991 0.2786 0.2911 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
