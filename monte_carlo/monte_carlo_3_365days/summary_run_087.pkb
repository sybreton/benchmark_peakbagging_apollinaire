# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:52:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.4162 0.2286 0.2572 19.6808 1.8655 2.2224 4.6128 0.4844 0.4928 90.0000 0.0000 0.0000 0.4037 0.2559 0.2312 0.0000 0.0000 0.0000
26 0 3709.8477 0.1414 0.1398 33.5042 3.0660 3.5147 3.4369 0.2887 0.3057 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
