# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:38:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0034 0.1690 0.1777 22.9153 2.2203 2.5936 3.8393 0.3857 0.3910 90.0000 0.0000 0.0000 0.2921 0.1939 0.2082 0.0000 0.0000 0.0000
26 0 3709.9588 0.1287 0.1285 34.5320 2.8847 3.3235 3.7440 0.2715 0.2900 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
