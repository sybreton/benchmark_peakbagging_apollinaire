# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:53:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1428 0.2418 0.2540 19.8165 2.1277 2.3558 4.2253 0.4940 0.5282 90.0000 0.0000 0.0000 0.6919 0.2849 0.1795 0.0000 0.0000 0.0000
26 0 3710.2112 0.1505 0.1516 30.0012 2.5096 2.9739 3.8094 0.2960 0.2998 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
