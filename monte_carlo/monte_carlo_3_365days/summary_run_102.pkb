# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:26:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9970 0.2001 0.2098 20.5812 2.0277 2.4021 4.4817 0.5061 0.5006 90.0000 0.0000 0.0000 0.5138 0.2911 0.2118 0.0000 0.0000 0.0000
26 0 3710.1155 0.1473 0.1396 28.5079 2.4405 2.8274 3.9335 0.3098 0.3212 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
