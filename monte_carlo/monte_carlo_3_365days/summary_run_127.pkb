# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:26:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6621 0.1832 0.2011 19.4309 1.9190 2.2766 3.8692 0.4323 0.4490 90.0000 0.0000 0.0000 0.2320 0.1589 0.1966 0.0000 0.0000 0.0000
26 0 3709.7431 0.1713 0.1651 27.1508 2.1287 2.3983 4.5537 0.3348 0.3666 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
