# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:21:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8714 0.1694 0.1715 23.0066 2.1725 2.5052 3.6861 0.3526 0.3822 90.0000 0.0000 0.0000 0.2496 0.1663 0.1944 0.0000 0.0000 0.0000
26 0 3709.9251 0.1421 0.1372 31.7591 2.6586 2.9906 3.9116 0.2869 0.3030 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
