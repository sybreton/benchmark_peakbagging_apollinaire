# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:21:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9134 0.1660 0.1726 23.6319 2.6885 3.2433 3.2878 0.3990 0.4204 90.0000 0.0000 0.0000 0.5545 0.1944 0.1379 0.0000 0.0000 0.0000
26 0 3710.0397 0.1252 0.1184 35.6317 3.0654 3.5641 3.5410 0.2549 0.2708 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
