# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:11:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1231 0.1687 0.1774 26.0702 2.5874 3.0204 3.7521 0.3929 0.4033 90.0000 0.0000 0.0000 0.3911 0.2312 0.1849 0.0000 0.0000 0.0000
26 0 3709.9853 0.1421 0.1392 31.3790 2.6391 3.1254 3.7951 0.2999 0.3065 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
