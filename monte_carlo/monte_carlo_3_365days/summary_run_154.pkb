# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:35:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8968 0.1468 0.1501 27.0054 2.5960 3.1702 3.4292 0.3467 0.3464 90.0000 0.0000 0.0000 0.2712 0.1791 0.1761 0.0000 0.0000 0.0000
26 0 3709.8603 0.1291 0.1270 34.2251 2.9010 3.4356 3.5218 0.2684 0.2749 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
