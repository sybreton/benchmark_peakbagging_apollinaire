# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:52:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0881 0.1578 0.1612 24.4095 2.2196 2.6665 3.7184 0.3412 0.3537 90.0000 0.0000 0.0000 0.2384 0.1633 0.1920 0.0000 0.0000 0.0000
26 0 3710.0146 0.1456 0.1423 29.3866 2.4744 2.8191 3.8998 0.2849 0.3059 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
