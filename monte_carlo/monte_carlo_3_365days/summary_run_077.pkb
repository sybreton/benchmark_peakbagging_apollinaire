# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:28:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6273 0.1428 0.1529 24.5841 2.4234 2.8977 3.1421 0.3058 0.3351 90.0000 0.0000 0.0000 0.1859 0.1273 0.1585 0.0000 0.0000 0.0000
26 0 3709.8336 0.1460 0.1365 31.7220 2.6117 2.8974 4.0147 0.2836 0.2958 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
