# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:49:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7500 0.2244 0.2304 20.8718 2.4584 2.8262 3.7490 0.5127 0.5935 90.0000 0.0000 0.0000 0.7608 0.1988 0.1335 0.0000 0.0000 0.0000
26 0 3710.0844 0.1466 0.1446 29.5705 2.4715 2.7216 4.1889 0.3083 0.3310 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
