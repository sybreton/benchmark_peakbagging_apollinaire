# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:52:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2988 0.2441 0.2673 23.0836 2.6040 2.9479 3.8286 0.4606 0.4873 90.0000 0.0000 0.0000 0.7043 0.2503 0.1675 0.0000 0.0000 0.0000
26 0 3710.1869 0.1760 0.1755 28.1800 2.3999 2.7510 4.0747 0.3307 0.3490 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
