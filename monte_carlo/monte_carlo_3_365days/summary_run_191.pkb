# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:03:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2022 0.2234 0.2343 20.8766 2.2323 2.6293 4.0455 0.4612 0.4873 90.0000 0.0000 0.0000 0.5934 0.2700 0.1784 0.0000 0.0000 0.0000
26 0 3709.8066 0.1501 0.1439 31.6584 2.6741 3.0709 3.8428 0.2997 0.3162 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
