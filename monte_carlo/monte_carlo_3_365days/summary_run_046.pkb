# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:15:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0721 0.1725 0.1846 21.1732 1.9455 2.3856 4.1334 0.4241 0.4225 90.0000 0.0000 0.0000 0.3129 0.2062 0.2165 0.0000 0.0000 0.0000
26 0 3710.1069 0.1329 0.1286 33.5990 2.8493 3.2574 3.7582 0.2851 0.2995 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
