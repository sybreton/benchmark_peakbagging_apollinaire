# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:57:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1470 0.2310 0.2396 20.3891 2.2364 2.6125 4.2040 0.5484 0.5438 90.0000 0.0000 0.0000 0.6212 0.3011 0.1948 0.0000 0.0000 0.0000
26 0 3710.0968 0.1519 0.1454 28.0812 2.4436 2.7772 3.8777 0.2977 0.3144 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
