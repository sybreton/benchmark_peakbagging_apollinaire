# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:21:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1259 0.1624 0.1694 24.9745 2.7176 3.2112 3.6768 0.4265 0.4460 90.0000 0.0000 0.0000 0.5549 0.2196 0.1445 0.0000 0.0000 0.0000
26 0 3710.1672 0.1367 0.1327 29.1137 2.5051 2.8682 3.8039 0.2902 0.3068 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
