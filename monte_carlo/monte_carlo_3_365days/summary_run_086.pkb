# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:49:48
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6566 0.1970 0.1979 20.5565 2.2532 2.8161 3.4346 0.4113 0.4362 90.0000 0.0000 0.0000 0.4578 0.2653 0.1992 0.0000 0.0000 0.0000
26 0 3709.8252 0.1432 0.1372 32.8460 2.6440 3.0520 4.1718 0.3034 0.3186 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
