# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:40:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8418 0.1643 0.1692 20.8735 1.9137 2.2950 4.1036 0.3870 0.3987 90.0000 0.0000 0.0000 0.2524 0.1726 0.2005 0.0000 0.0000 0.0000
26 0 3710.1550 0.1271 0.1233 32.6505 2.7555 3.2009 3.6146 0.2729 0.2829 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
