# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:12:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9718 0.1834 0.1889 23.1435 2.2714 2.7309 4.0652 0.4692 0.4418 90.0000 0.0000 0.0000 0.4193 0.2526 0.2020 0.0000 0.0000 0.0000
26 0 3709.8959 0.1328 0.1271 32.3509 2.8170 3.0980 3.6081 0.2852 0.3000 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
