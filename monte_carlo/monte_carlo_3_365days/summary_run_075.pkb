# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:24:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8586 0.1557 0.1565 22.8618 2.2076 2.5589 3.4401 0.3228 0.3356 90.0000 0.0000 0.0000 0.2365 0.1564 0.1805 0.0000 0.0000 0.0000
26 0 3709.8939 0.1298 0.1258 34.3630 2.7632 3.2806 3.7373 0.2699 0.2777 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
