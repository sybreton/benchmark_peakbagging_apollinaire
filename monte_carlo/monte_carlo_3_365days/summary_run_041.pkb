# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:04:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1142 0.2210 0.2408 16.8932 1.5079 1.6714 5.0299 0.4828 0.5169 90.0000 0.0000 0.0000 0.2519 0.1708 0.2205 0.0000 0.0000 0.0000
26 0 3710.1374 0.1462 0.1458 28.6853 2.4504 2.7986 3.8797 0.3072 0.3281 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
