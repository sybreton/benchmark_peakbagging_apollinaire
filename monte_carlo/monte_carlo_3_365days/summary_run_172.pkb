# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:18:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2535 0.2079 0.2282 22.7448 2.6185 3.0075 3.7233 0.4873 0.5423 90.0000 0.0000 0.0000 0.7698 0.1942 0.1304 0.0000 0.0000 0.0000
26 0 3710.1458 0.1550 0.1498 27.9254 2.4144 2.8405 3.7389 0.3102 0.3207 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
