# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:45:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8882 0.2091 0.2350 20.2368 2.3678 2.7451 3.7859 0.4705 0.4911 90.0000 0.0000 0.0000 0.5560 0.2669 0.1837 0.0000 0.0000 0.0000
26 0 3709.8008 0.1495 0.1530 31.9027 2.6461 3.0417 3.9258 0.2897 0.3024 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
