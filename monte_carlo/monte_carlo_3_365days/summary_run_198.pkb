# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:19:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3360 0.3063 0.3445 19.2882 2.1163 2.4506 4.5748 0.5479 0.5609 90.0000 0.0000 0.0000 0.6599 0.3239 0.2104 0.0000 0.0000 0.0000
26 0 3709.9639 0.1971 0.1986 26.1024 2.3091 2.5267 4.2083 0.3622 0.3692 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
