# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:02:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0008 0.2278 0.2431 20.6196 2.5155 2.9617 3.5282 0.4512 0.4909 90.0000 0.0000 0.0000 0.6541 0.2298 0.1609 0.0000 0.0000 0.0000
26 0 3709.8743 0.1655 0.1605 28.8441 2.2771 2.5948 4.3648 0.3214 0.3352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
