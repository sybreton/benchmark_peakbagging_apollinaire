# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:24:04
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1031 0.1777 0.1798 23.8895 2.7363 3.0242 3.4433 0.3988 0.4331 90.0000 0.0000 0.0000 0.4853 0.2285 0.1565 0.0000 0.0000 0.0000
26 0 3710.0652 0.1458 0.1418 31.1996 2.4997 2.7947 4.0257 0.2891 0.2970 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
