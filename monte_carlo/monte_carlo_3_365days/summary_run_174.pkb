# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:23:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0035 0.1813 0.1919 21.2009 2.1257 2.5194 3.9260 0.4133 0.4093 90.0000 0.0000 0.0000 0.3424 0.2189 0.2114 0.0000 0.0000 0.0000
26 0 3709.9423 0.1524 0.1466 30.0113 2.4494 2.7973 3.9561 0.2944 0.3107 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
