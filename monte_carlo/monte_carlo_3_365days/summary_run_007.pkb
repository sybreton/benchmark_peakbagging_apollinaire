# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:08:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8257 0.1581 0.1629 22.3194 2.0561 2.4008 3.6507 0.3378 0.3641 90.0000 0.0000 0.0000 0.1767 0.1221 0.1583 0.0000 0.0000 0.0000
26 0 3709.9465 0.1537 0.1476 26.6001 2.1579 2.5078 4.2237 0.3221 0.3416 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
