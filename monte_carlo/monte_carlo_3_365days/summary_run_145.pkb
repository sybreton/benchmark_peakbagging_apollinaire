# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:14:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0002 0.2461 0.2636 18.6511 2.0474 2.4353 4.3957 0.5091 0.5133 90.0000 0.0000 0.0000 0.5489 0.3021 0.2200 0.0000 0.0000 0.0000
26 0 3709.9508 0.1601 0.1492 31.6043 2.5898 3.0293 4.0491 0.3160 0.3338 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
