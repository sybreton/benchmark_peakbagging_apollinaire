# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:58:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7930 0.1903 0.1931 21.4840 2.0838 2.4898 3.9773 0.4122 0.4128 90.0000 0.0000 0.0000 0.3373 0.2195 0.2157 0.0000 0.0000 0.0000
26 0 3709.9851 0.1502 0.1477 30.3746 2.5252 2.8041 4.1057 0.3113 0.3347 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
