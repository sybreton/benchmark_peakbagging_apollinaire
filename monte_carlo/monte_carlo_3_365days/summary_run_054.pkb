# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:34:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9651 0.1913 0.2098 21.8720 2.4219 2.9048 3.8922 0.4735 0.4770 90.0000 0.0000 0.0000 0.5362 0.2649 0.1750 0.0000 0.0000 0.0000
26 0 3709.9821 0.1596 0.1484 28.8640 2.3977 2.6419 4.1384 0.3049 0.3273 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
