# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:59:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8821 0.1951 0.2119 21.1036 2.3849 2.8344 3.6044 0.4409 0.4725 90.0000 0.0000 0.0000 0.6094 0.2466 0.1630 0.0000 0.0000 0.0000
26 0 3710.1457 0.1329 0.1362 32.6573 2.6014 3.0030 3.9990 0.2792 0.2829 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
