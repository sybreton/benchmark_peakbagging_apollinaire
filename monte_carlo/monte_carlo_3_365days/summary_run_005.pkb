# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:01:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1550 0.2133 0.2599 22.5919 2.6637 3.0785 3.4888 0.4165 0.4530 90.0000 0.0000 0.0000 0.6024 0.2238 0.1683 0.0000 0.0000 0.0000
26 0 3710.0110 0.1496 0.1529 33.4091 2.7496 3.1674 3.7609 0.2791 0.2855 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
