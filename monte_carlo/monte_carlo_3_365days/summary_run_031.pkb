# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:39:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0881 0.1889 0.1982 20.1742 1.8034 2.0555 4.4149 0.4203 0.4387 90.0000 0.0000 0.0000 0.3001 0.2022 0.2179 0.0000 0.0000 0.0000
26 0 3710.1839 0.1513 0.1492 26.0935 2.2424 2.4886 4.0471 0.3366 0.3476 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
