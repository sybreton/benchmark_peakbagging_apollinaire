# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:13:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9385 0.1962 0.2020 19.9127 1.9153 2.3169 4.1346 0.4523 0.4466 90.0000 0.0000 0.0000 0.4214 0.2581 0.2214 0.0000 0.0000 0.0000
26 0 3709.9585 0.1495 0.1464 25.9381 2.2669 2.5319 3.9741 0.3212 0.3352 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
