# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:26:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1541 0.1644 0.1789 22.9392 2.1908 2.4739 3.9863 0.3819 0.4007 90.0000 0.0000 0.0000 0.2433 0.1658 0.1922 0.0000 0.0000 0.0000
26 0 3710.0780 0.1522 0.1454 30.0124 2.5328 2.7746 4.0564 0.2997 0.3229 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
