# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:55:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9422 0.1815 0.1896 20.2165 2.1899 2.5604 3.8020 0.4742 0.4961 90.0000 0.0000 0.0000 0.4523 0.2543 0.1857 0.0000 0.0000 0.0000
26 0 3710.0237 0.1528 0.1493 26.2209 2.1795 2.4448 4.2436 0.3182 0.3341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
