# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:14:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2102 0.1789 0.1878 22.7632 2.2132 2.6874 4.1323 0.4236 0.4301 90.0000 0.0000 0.0000 0.3814 0.2422 0.2195 0.0000 0.0000 0.0000
26 0 3710.1351 0.1470 0.1433 28.8209 2.4737 2.7214 4.0718 0.3158 0.3378 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
