# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:46:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2363 0.2199 0.2337 19.1724 1.8457 2.1911 4.4101 0.4348 0.4544 90.0000 0.0000 0.0000 0.3733 0.2490 0.2428 0.0000 0.0000 0.0000
26 0 3709.9591 0.1670 0.1594 26.9059 2.3022 2.6558 4.0662 0.3288 0.3543 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
