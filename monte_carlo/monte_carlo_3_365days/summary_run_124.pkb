# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:18:28
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9555 0.1765 0.1868 20.8797 1.9863 2.2618 3.8673 0.3704 0.3915 90.0000 0.0000 0.0000 0.2519 0.1692 0.1967 0.0000 0.0000 0.0000
26 0 3709.8005 0.1653 0.1591 28.4955 2.2825 2.5935 4.2308 0.3052 0.3199 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
