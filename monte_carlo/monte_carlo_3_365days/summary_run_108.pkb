# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:40:09
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7534 0.1398 0.1422 25.8964 2.5923 3.1830 3.0972 0.3118 0.3262 90.0000 0.0000 0.0000 0.2100 0.1420 0.1624 0.0000 0.0000 0.0000
26 0 3709.8510 0.1496 0.1441 29.5865 2.3308 2.6295 4.2190 0.2917 0.3175 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
