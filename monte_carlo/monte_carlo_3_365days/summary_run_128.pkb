# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:28:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8243 0.1490 0.1496 24.8930 2.5580 3.0081 3.3469 0.3603 0.3683 90.0000 0.0000 0.0000 0.3094 0.1927 0.1762 0.0000 0.0000 0.0000
26 0 3709.9770 0.1389 0.1255 31.1005 2.5407 2.9448 3.9540 0.2889 0.2979 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
