# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:28:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8380 0.1607 0.1725 19.8787 1.9416 2.2458 3.7458 0.3759 0.3917 90.0000 0.0000 0.0000 0.1676 0.1173 0.1670 0.0000 0.0000 0.0000
26 0 3710.0479 0.1363 0.1339 30.5844 2.4066 2.8110 4.0356 0.2878 0.2945 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
