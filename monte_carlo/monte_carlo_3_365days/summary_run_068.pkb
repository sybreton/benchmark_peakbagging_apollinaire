# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:07:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8314 0.1587 0.1713 21.1602 1.9233 2.2153 3.9958 0.3545 0.3820 90.0000 0.0000 0.0000 0.2291 0.1567 0.1883 0.0000 0.0000 0.0000
26 0 3710.1152 0.1333 0.1313 30.2093 2.5243 2.8426 3.9902 0.3029 0.3169 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
