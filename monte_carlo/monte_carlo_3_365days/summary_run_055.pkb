# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:37:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3345 0.2291 0.2493 19.3430 2.0186 2.3378 4.4202 0.4963 0.5115 90.0000 0.0000 0.0000 0.5488 0.2955 0.2129 0.0000 0.0000 0.0000
26 0 3709.8629 0.1524 0.1490 28.5441 2.5064 2.8011 3.9095 0.3092 0.3204 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
