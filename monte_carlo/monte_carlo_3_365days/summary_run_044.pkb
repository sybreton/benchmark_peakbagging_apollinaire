# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:11:18
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1326 0.2000 0.2116 20.4667 2.1260 2.5716 4.0922 0.4711 0.4780 90.0000 0.0000 0.0000 0.4869 0.2691 0.1961 0.0000 0.0000 0.0000
26 0 3710.0461 0.1418 0.1380 31.0169 2.5910 3.0400 3.8889 0.2987 0.3119 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
