# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:32:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1069 0.1773 0.1902 21.2037 2.1390 2.5088 3.9607 0.4255 0.4454 90.0000 0.0000 0.0000 0.4140 0.2503 0.1976 0.0000 0.0000 0.0000
26 0 3710.1197 0.1366 0.1314 30.2019 2.5573 3.0158 3.8232 0.3017 0.3090 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
