# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:33:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9100 0.1904 0.1921 21.2977 2.1381 2.5813 3.9415 0.4223 0.4359 90.0000 0.0000 0.0000 0.3811 0.2364 0.2082 0.0000 0.0000 0.0000
26 0 3709.7890 0.1734 0.1631 25.4386 2.0662 2.3845 4.5178 0.3478 0.3617 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
