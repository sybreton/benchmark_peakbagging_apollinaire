# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:20:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6443 0.1659 0.1723 20.4823 1.9154 2.2112 3.8061 0.3587 0.3829 90.0000 0.0000 0.0000 0.1655 0.1148 0.1578 0.0000 0.0000 0.0000
26 0 3709.9363 0.1296 0.1214 35.9595 3.0442 3.4557 3.6964 0.2811 0.2866 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
