# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:20:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0595 0.2249 0.2410 18.4291 1.7518 2.0187 4.6445 0.4467 0.4696 90.0000 0.0000 0.0000 0.4025 0.2576 0.2392 0.0000 0.0000 0.0000
26 0 3710.1190 0.1285 0.1272 36.2230 3.1805 3.7634 3.4307 0.2620 0.2781 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
