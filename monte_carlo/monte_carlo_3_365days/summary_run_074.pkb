# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:21:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2621 0.2231 0.2441 21.6706 2.3368 2.7721 4.0138 0.4617 0.4773 90.0000 0.0000 0.0000 0.5376 0.2779 0.2003 0.0000 0.0000 0.0000
26 0 3710.0655 0.1910 0.1921 25.5070 2.1303 2.3805 4.7080 0.3749 0.3983 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
