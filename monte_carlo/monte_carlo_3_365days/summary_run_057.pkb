# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:41:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8116 0.2246 0.2167 21.1622 2.3244 2.8565 3.8132 0.5477 0.5363 90.0000 0.0000 0.0000 0.5781 0.2820 0.1756 0.0000 0.0000 0.0000
26 0 3709.8727 0.1354 0.1358 32.7692 2.7467 3.2191 3.6856 0.2756 0.2834 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
