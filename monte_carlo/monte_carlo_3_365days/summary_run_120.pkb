# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:08:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0046 0.1759 0.1887 19.7338 1.9285 2.2603 4.1168 0.4427 0.4353 90.0000 0.0000 0.0000 0.3485 0.2264 0.2110 0.0000 0.0000 0.0000
26 0 3710.2715 0.1325 0.1282 31.6855 2.6258 3.0002 3.8526 0.2698 0.2856 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
