# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:44:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9935 0.1646 0.1802 24.7590 2.4252 2.8536 3.6244 0.3720 0.3811 90.0000 0.0000 0.0000 0.2374 0.1596 0.1837 0.0000 0.0000 0.0000
26 0 3709.8021 0.1941 0.1830 24.4874 1.9609 2.1901 4.6774 0.3568 0.3801 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
