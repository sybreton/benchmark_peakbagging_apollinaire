# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:43:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9169 0.2111 0.2291 20.0915 2.2885 2.7198 3.6764 0.4835 0.5097 90.0000 0.0000 0.0000 0.5695 0.2483 0.1632 0.0000 0.0000 0.0000
26 0 3709.9791 0.1601 0.1577 28.3092 2.3195 2.5873 4.2781 0.3162 0.3480 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
