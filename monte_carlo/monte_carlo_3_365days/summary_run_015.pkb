# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:37:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7874 0.2187 0.2271 17.3774 1.7358 2.1164 4.5674 0.5633 0.5480 90.0000 0.0000 0.0000 0.4260 0.2708 0.2222 0.0000 0.0000 0.0000
26 0 3710.0491 0.1511 0.1481 28.1548 2.2349 2.5624 4.3118 0.3101 0.3111 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
