# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:56:20
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2091 0.1817 0.1923 22.3233 2.0445 2.3692 4.1771 0.3913 0.4199 90.0000 0.0000 0.0000 0.2514 0.1727 0.2079 0.0000 0.0000 0.0000
26 0 3710.0403 0.1525 0.1503 29.4710 2.4527 2.9353 3.9291 0.3077 0.3212 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
