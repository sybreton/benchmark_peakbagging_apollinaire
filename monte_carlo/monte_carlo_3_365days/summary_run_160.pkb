# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:50:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0979 0.1691 0.1751 23.5133 2.4289 2.9272 3.5933 0.4028 0.4175 90.0000 0.0000 0.0000 0.3803 0.2296 0.1900 0.0000 0.0000 0.0000
26 0 3709.9777 0.1631 0.1608 26.9266 2.2335 2.4619 4.3721 0.3244 0.3398 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
