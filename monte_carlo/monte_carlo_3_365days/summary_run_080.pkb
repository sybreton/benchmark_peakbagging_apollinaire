# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:35:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0471 0.1555 0.1630 20.8632 2.0363 2.3551 3.7733 0.3806 0.3961 90.0000 0.0000 0.0000 0.2346 0.1590 0.1853 0.0000 0.0000 0.0000
26 0 3710.2002 0.1566 0.1490 26.0336 2.1505 2.3437 4.3536 0.3129 0.3349 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
