# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:39:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8706 0.1710 0.1749 21.7363 2.2474 2.6183 3.6801 0.4069 0.4184 90.0000 0.0000 0.0000 0.3733 0.2240 0.1873 0.0000 0.0000 0.0000
26 0 3710.0839 0.1433 0.1334 29.8915 2.4558 2.8734 4.0464 0.2902 0.3025 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
