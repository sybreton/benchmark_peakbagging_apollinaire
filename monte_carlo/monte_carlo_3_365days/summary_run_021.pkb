# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:07:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1138 0.2118 0.2516 21.7298 2.1631 2.5675 4.0851 0.4262 0.4437 90.0000 0.0000 0.0000 0.3725 0.2390 0.2335 0.0000 0.0000 0.0000
26 0 3710.0691 0.1780 0.1750 28.3999 2.3317 2.6438 4.1249 0.3301 0.3435 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
