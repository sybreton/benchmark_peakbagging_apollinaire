# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:51:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8203 0.2114 0.2120 18.8008 1.8675 2.1362 4.1408 0.4464 0.4419 90.0000 0.0000 0.0000 0.3969 0.2530 0.2284 0.0000 0.0000 0.0000
26 0 3709.7590 0.1503 0.1472 29.9767 2.4590 2.8285 4.1090 0.3128 0.3135 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
