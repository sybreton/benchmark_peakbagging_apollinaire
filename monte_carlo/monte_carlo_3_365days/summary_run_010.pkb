# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:18:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7196 0.2140 0.2273 17.5391 1.6126 1.8052 4.5183 0.4472 0.4508 90.0000 0.0000 0.0000 0.2955 0.1992 0.2398 0.0000 0.0000 0.0000
26 0 3709.9724 0.1418 0.1389 31.5380 2.7523 2.9580 3.9191 0.3034 0.3276 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
