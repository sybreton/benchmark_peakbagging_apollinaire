# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:15:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9204 0.1615 0.1719 23.9645 2.4184 3.0008 3.6415 0.4244 0.4108 90.0000 0.0000 0.0000 0.3839 0.2262 0.1818 0.0000 0.0000 0.0000
26 0 3710.0007 0.1318 0.1277 31.7776 2.5932 3.0160 3.8344 0.2817 0.3004 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
