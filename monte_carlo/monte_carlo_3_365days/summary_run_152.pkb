# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:31:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9133 0.1986 0.2069 24.1899 2.9784 3.6434 3.2493 0.5045 0.5169 90.0000 0.0000 0.0000 0.6117 0.1907 0.1361 0.0000 0.0000 0.0000
26 0 3710.0494 0.1695 0.1580 28.5197 2.2384 2.5629 4.3207 0.3165 0.3342 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
