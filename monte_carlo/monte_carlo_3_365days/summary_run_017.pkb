# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:43:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0798 0.1721 0.1845 20.0492 1.7667 1.9835 4.3216 0.3626 0.3999 90.0000 0.0000 0.0000 0.2520 0.1741 0.2145 0.0000 0.0000 0.0000
26 0 3710.2041 0.1204 0.1156 35.2851 3.0976 3.6762 3.3673 0.2664 0.2754 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
