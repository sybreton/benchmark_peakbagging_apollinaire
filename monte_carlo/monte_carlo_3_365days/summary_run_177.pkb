# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:30:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0220 0.2066 0.2236 22.4535 2.4534 2.8692 3.7620 0.4403 0.4551 90.0000 0.0000 0.0000 0.5188 0.2609 0.1860 0.0000 0.0000 0.0000
26 0 3710.0809 0.1564 0.1522 29.0865 2.4244 2.7774 3.9201 0.2996 0.3259 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
