# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:29:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.5133 0.2557 0.2918 19.3646 1.8970 2.3507 4.6286 0.4428 0.4603 90.0000 0.0000 0.0000 0.4590 0.2943 0.2836 0.0000 0.0000 0.0000
26 0 3710.2498 0.1678 0.1697 29.1635 2.6368 3.0010 3.5282 0.3068 0.3151 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
