# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:40:37
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1721 0.1821 0.1921 21.4873 2.0007 2.1898 4.4193 0.3994 0.4244 90.0000 0.0000 0.0000 0.2290 0.1558 0.2037 0.0000 0.0000 0.0000
26 0 3710.0920 0.1533 0.1465 28.0501 2.3712 2.7147 4.0754 0.3117 0.3334 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
