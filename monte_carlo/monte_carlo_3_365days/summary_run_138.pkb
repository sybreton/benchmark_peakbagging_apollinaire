# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:56:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9261 0.1787 0.1859 21.4225 1.9067 2.2222 4.1434 0.3748 0.3888 90.0000 0.0000 0.0000 0.2631 0.1765 0.2039 0.0000 0.0000 0.0000
26 0 3710.0648 0.1300 0.1274 32.9593 2.8092 3.3352 3.5365 0.2728 0.2842 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
