# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:58:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0298 0.2320 0.2572 19.6016 1.9672 2.2960 4.4075 0.4383 0.4651 90.0000 0.0000 0.0000 0.3406 0.2318 0.2613 0.0000 0.0000 0.0000
26 0 3709.8137 0.1744 0.1692 28.9262 2.4345 2.7062 4.1791 0.3445 0.3707 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
