# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:39:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9577 0.2048 0.2185 19.2532 1.9293 2.2299 4.1479 0.4505 0.4639 90.0000 0.0000 0.0000 0.4941 0.2746 0.2118 0.0000 0.0000 0.0000
26 0 3710.1225 0.1407 0.1368 32.0551 2.6915 3.0620 3.7286 0.2770 0.2870 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
