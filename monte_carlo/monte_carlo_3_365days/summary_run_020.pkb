# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:00:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1774 0.1972 0.2071 24.5251 2.7132 3.0927 3.9382 0.4533 0.4987 90.0000 0.0000 0.0000 0.6651 0.2429 0.1613 0.0000 0.0000 0.0000
26 0 3710.1778 0.1445 0.1462 29.0317 2.6228 2.8213 3.7182 0.3070 0.3160 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
