# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:06:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9110 0.1895 0.2058 21.4055 2.1505 2.5055 4.0574 0.4376 0.4457 90.0000 0.0000 0.0000 0.3992 0.2458 0.2104 0.0000 0.0000 0.0000
26 0 3709.8556 0.1572 0.1515 28.8259 2.4072 2.6980 4.1109 0.3137 0.3306 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
