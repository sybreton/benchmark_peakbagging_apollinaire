# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 14:57:51
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9001 0.1897 0.1980 19.4658 1.8256 2.1510 4.2243 0.4211 0.4393 90.0000 0.0000 0.0000 0.2795 0.1911 0.2169 0.0000 0.0000 0.0000
26 0 3709.9822 0.1513 0.1404 30.4236 2.5135 2.8096 4.0317 0.2909 0.3102 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
