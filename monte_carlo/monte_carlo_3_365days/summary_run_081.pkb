# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:38:14
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9621 0.1608 0.1689 28.7859 3.4306 4.0220 3.0168 0.3879 0.4177 90.0000 0.0000 0.0000 0.6413 0.1525 0.1131 0.0000 0.0000 0.0000
26 0 3709.8706 0.1520 0.1416 29.8498 2.5124 2.8024 4.0046 0.3028 0.3297 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
