# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:03:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1654 0.1941 0.2045 19.7163 1.8310 2.0682 4.3329 0.4099 0.4269 90.0000 0.0000 0.0000 0.2582 0.1748 0.2019 0.0000 0.0000 0.0000
26 0 3710.0088 0.1713 0.1680 24.6681 2.0726 2.3603 4.3629 0.3445 0.3631 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
