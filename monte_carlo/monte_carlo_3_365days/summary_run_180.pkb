# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:37:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.5048 0.2289 0.2423 21.6270 2.0456 2.3529 4.6100 0.4547 0.4727 90.0000 0.0000 0.0000 0.4492 0.2835 0.2442 0.0000 0.0000 0.0000
26 0 3710.2301 0.1649 0.1630 28.1898 2.5618 2.8658 3.6913 0.3142 0.3341 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
