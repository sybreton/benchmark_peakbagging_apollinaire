# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:33:38
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2587 0.2067 0.2282 22.2248 2.2598 2.6950 4.2836 0.4773 0.4839 90.0000 0.0000 0.0000 0.5386 0.2804 0.1923 0.0000 0.0000 0.0000
26 0 3710.0099 0.1422 0.1365 31.6245 2.7554 3.1759 3.7275 0.3079 0.3082 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
