# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:06:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7007 0.1695 0.1753 21.4616 2.1756 2.6391 3.7862 0.4186 0.4076 90.0000 0.0000 0.0000 0.4048 0.2486 0.2032 0.0000 0.0000 0.0000
26 0 3709.9543 0.1249 0.1246 31.5701 2.6727 3.0171 3.8294 0.2741 0.2918 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
