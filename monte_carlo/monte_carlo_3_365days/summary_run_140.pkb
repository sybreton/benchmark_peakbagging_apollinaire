# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:01:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8887 0.1722 0.1803 21.5284 2.1633 2.6958 3.8168 0.4265 0.4219 90.0000 0.0000 0.0000 0.4094 0.2502 0.2024 0.0000 0.0000 0.0000
26 0 3710.0924 0.1288 0.1291 35.2411 2.8383 3.3260 3.7878 0.2703 0.2753 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
