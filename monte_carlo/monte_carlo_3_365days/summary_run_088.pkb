# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:54:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8892 0.1799 0.1854 23.1873 2.4438 2.8627 3.6007 0.3787 0.3959 90.0000 0.0000 0.0000 0.4900 0.2309 0.1653 0.0000 0.0000 0.0000
26 0 3709.9135 0.1300 0.1273 32.9530 2.7829 3.1594 3.7628 0.2764 0.2918 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
