# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:27:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8874 0.1457 0.1462 25.1852 2.8269 3.3873 3.0735 0.3699 0.3778 90.0000 0.0000 0.0000 0.3856 0.2138 0.1526 0.0000 0.0000 0.0000
26 0 3709.9931 0.1289 0.1252 34.1053 2.7374 3.0816 3.9814 0.2635 0.2745 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
