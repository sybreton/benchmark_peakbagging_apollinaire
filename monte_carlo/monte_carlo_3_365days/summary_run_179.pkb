# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:34:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7103 0.1686 0.1811 21.2754 2.1280 2.4264 3.7935 0.3932 0.4077 90.0000 0.0000 0.0000 0.3639 0.2302 0.2041 0.0000 0.0000 0.0000
26 0 3709.9703 0.1282 0.1235 34.1717 2.8477 3.3430 3.6208 0.2679 0.2852 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
