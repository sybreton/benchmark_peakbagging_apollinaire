# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:47:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2764 0.1873 0.2013 19.4345 1.7732 2.0824 4.3694 0.4080 0.4351 90.0000 0.0000 0.0000 0.2523 0.1718 0.2108 0.0000 0.0000 0.0000
26 0 3710.0803 0.1574 0.1532 27.6077 2.3411 2.5956 4.0953 0.3143 0.3220 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
