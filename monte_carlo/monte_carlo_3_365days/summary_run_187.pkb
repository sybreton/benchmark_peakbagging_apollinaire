# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:53:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3845 0.2731 0.3139 19.1402 1.9417 2.3405 4.8166 0.5059 0.5158 90.0000 0.0000 0.0000 0.4608 0.2892 0.2631 0.0000 0.0000 0.0000
26 0 3709.8810 0.1776 0.1738 29.9105 2.5511 2.8727 4.1105 0.3354 0.3429 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
