# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:52:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7397 0.2026 0.2190 21.4585 2.2904 2.6533 4.0420 0.4771 0.4758 90.0000 0.0000 0.0000 0.5384 0.2769 0.1872 0.0000 0.0000 0.0000
26 0 3709.9197 0.1826 0.1708 26.0709 2.0775 2.3411 4.7082 0.3453 0.3563 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
