# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:21:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9361 0.1636 0.1661 25.3510 2.7060 3.0680 3.4505 0.3613 0.3753 90.0000 0.0000 0.0000 0.4331 0.2251 0.1711 0.0000 0.0000 0.0000
26 0 3709.9849 0.1232 0.1224 34.7106 3.0103 3.4911 3.5329 0.2626 0.2691 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
