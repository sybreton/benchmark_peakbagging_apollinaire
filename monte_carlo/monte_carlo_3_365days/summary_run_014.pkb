# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:33:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3729 0.2267 0.2441 22.3339 2.3140 2.7024 4.2783 0.4575 0.4788 90.0000 0.0000 0.0000 0.5465 0.2806 0.2041 0.0000 0.0000 0.0000
26 0 3710.2266 0.1696 0.1646 27.7182 2.4199 2.6618 3.9547 0.3230 0.3368 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
