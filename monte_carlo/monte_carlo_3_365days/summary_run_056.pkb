# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:39:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8598 0.1329 0.1363 24.4248 2.5536 2.9452 3.1922 0.3093 0.3340 90.0000 0.0000 0.0000 0.1440 0.0990 0.1315 0.0000 0.0000 0.0000
26 0 3709.9883 0.1499 0.1439 27.5756 2.0911 2.3508 4.6036 0.3158 0.3322 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
