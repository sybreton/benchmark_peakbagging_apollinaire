# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:51:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9785 0.1870 0.1913 22.2196 2.4229 2.7830 3.8409 0.4536 0.4640 90.0000 0.0000 0.0000 0.4779 0.2726 0.1967 0.0000 0.0000 0.0000
26 0 3710.0429 0.1594 0.1578 28.7354 2.3056 2.6855 4.3328 0.3187 0.3355 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
