# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:01:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1751 0.2075 0.2278 19.6785 1.8226 2.1190 4.4807 0.4257 0.4498 90.0000 0.0000 0.0000 0.2937 0.1971 0.2288 0.0000 0.0000 0.0000
26 0 3709.9561 0.1448 0.1424 34.1994 2.8840 3.2836 3.7672 0.2797 0.3016 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
