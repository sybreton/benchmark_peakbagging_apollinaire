# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:40:33
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.4043 0.1781 0.1903 23.7080 2.2112 2.6974 4.0139 0.4004 0.3977 90.0000 0.0000 0.0000 0.3455 0.2198 0.2022 0.0000 0.0000 0.0000
26 0 3710.3134 0.1383 0.1301 34.2455 2.9985 3.3927 3.4868 0.2693 0.2906 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
