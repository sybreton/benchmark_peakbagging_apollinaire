# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:05:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6295 0.1644 0.1697 22.1048 2.3123 2.8261 3.5319 0.4158 0.4125 90.0000 0.0000 0.0000 0.3771 0.2233 0.1760 0.0000 0.0000 0.0000
26 0 3710.0706 0.1367 0.1330 32.7282 2.6681 2.9856 3.9635 0.2754 0.2953 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
