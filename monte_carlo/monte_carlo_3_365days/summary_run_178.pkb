# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:32:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1335 0.1599 0.1662 26.5141 2.8229 3.2908 3.3268 0.3843 0.3686 90.0000 0.0000 0.0000 0.4011 0.2286 0.1759 0.0000 0.0000 0.0000
26 0 3709.9714 0.1453 0.1474 28.4239 2.3962 2.6912 4.1022 0.3040 0.3334 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
