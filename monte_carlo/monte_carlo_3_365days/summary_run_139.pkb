# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:59:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9033 0.1602 0.1733 21.8427 1.9940 2.2848 3.9704 0.3650 0.3704 90.0000 0.0000 0.0000 0.1745 0.1222 0.1561 0.0000 0.0000 0.0000
26 0 3710.0483 0.1396 0.1313 30.8468 2.6214 2.9612 3.7819 0.2847 0.3014 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
