# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:01:57
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0689 0.1843 0.1946 20.1376 1.8094 2.0740 4.3009 0.3998 0.4068 90.0000 0.0000 0.0000 0.2060 0.1414 0.1896 0.0000 0.0000 0.0000
26 0 3709.9225 0.1562 0.1444 28.7259 2.4832 2.7638 3.9953 0.3068 0.3215 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
