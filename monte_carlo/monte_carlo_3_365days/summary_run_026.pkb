# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:32:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2688 0.1937 0.2084 21.2276 1.9910 2.3298 4.3867 0.4631 0.4543 90.0000 0.0000 0.0000 0.4151 0.2577 0.2081 0.0000 0.0000 0.0000
26 0 3710.1352 0.1473 0.1462 28.1936 2.4398 2.8082 3.7910 0.3006 0.3088 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
