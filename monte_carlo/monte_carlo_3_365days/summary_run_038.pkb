# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:57:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8881 0.1771 0.1946 20.9322 2.2735 2.8266 3.6589 0.4669 0.4583 90.0000 0.0000 0.0000 0.3897 0.2354 0.1822 0.0000 0.0000 0.0000
26 0 3709.9402 0.1608 0.1556 28.5160 2.2821 2.6080 4.4126 0.3298 0.3474 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
