# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:10:13
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9975 0.2067 0.2232 18.8812 1.6772 1.8763 4.6650 0.4239 0.4510 90.0000 0.0000 0.0000 0.2522 0.1768 0.2241 0.0000 0.0000 0.0000
26 0 3710.0835 0.1454 0.1413 30.1073 2.5427 2.9613 3.9282 0.3040 0.3154 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
