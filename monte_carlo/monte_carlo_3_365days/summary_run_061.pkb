# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:51:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.4220 0.2386 0.2550 16.7011 1.5042 1.7227 5.1436 0.4918 0.5342 90.0000 0.0000 0.0000 0.2386 0.1649 0.2117 0.0000 0.0000 0.0000
26 0 3710.1497 0.1676 0.1655 26.1218 2.2988 2.5467 4.1180 0.3327 0.3486 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
