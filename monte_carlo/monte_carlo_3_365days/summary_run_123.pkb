# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:15:54
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0255 0.2008 0.2112 17.8048 1.5507 1.7458 4.8547 0.4232 0.4612 90.0000 0.0000 0.0000 0.1979 0.1370 0.1978 0.0000 0.0000 0.0000
26 0 3709.9928 0.1186 0.1148 37.3032 3.2568 3.7251 3.3676 0.2537 0.2651 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
