# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:15:00
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9491 0.1470 0.1497 24.4764 2.2830 2.6537 3.6028 0.3297 0.3489 90.0000 0.0000 0.0000 0.1600 0.1097 0.1526 0.0000 0.0000 0.0000
26 0 3709.9375 0.1542 0.1497 28.3676 2.2580 2.4947 4.3913 0.3146 0.3370 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
