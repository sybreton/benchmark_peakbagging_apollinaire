# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:37:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2249 0.1957 0.2079 22.5859 2.1429 2.5466 4.2421 0.4365 0.4363 90.0000 0.0000 0.0000 0.4145 0.2518 0.2158 0.0000 0.0000 0.0000
26 0 3710.1091 0.1315 0.1293 33.7917 2.9417 3.4808 3.4341 0.2770 0.2934 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
