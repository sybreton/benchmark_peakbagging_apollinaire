# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:47:46
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7233 0.1762 0.1859 21.0933 2.1274 2.4394 3.7834 0.3979 0.4122 90.0000 0.0000 0.0000 0.2733 0.1847 0.2069 0.0000 0.0000 0.0000
26 0 3709.8535 0.1663 0.1540 28.0051 2.1583 2.4214 4.6053 0.3314 0.3459 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
