# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:33:17
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1943 0.1920 0.2035 21.7780 2.3814 2.8226 3.9747 0.4649 0.4838 90.0000 0.0000 0.0000 0.5496 0.2784 0.1840 0.0000 0.0000 0.0000
26 0 3710.1332 0.1312 0.1278 32.6617 2.8170 3.1994 3.6470 0.2783 0.2978 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
