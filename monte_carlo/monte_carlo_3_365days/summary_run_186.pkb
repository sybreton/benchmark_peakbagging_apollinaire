# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:51:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2940 0.2007 0.2247 21.2667 2.0456 2.3974 4.1784 0.4362 0.4504 90.0000 0.0000 0.0000 0.3807 0.2456 0.2192 0.0000 0.0000 0.0000
26 0 3709.9499 0.1462 0.1407 30.2018 2.6713 2.9578 3.7267 0.3141 0.3287 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
