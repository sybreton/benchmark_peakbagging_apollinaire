# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:09:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9553 0.2251 0.2442 19.4419 1.9074 2.2671 4.5047 0.5002 0.5136 90.0000 0.0000 0.0000 0.5708 0.3260 0.2246 0.0000 0.0000 0.0000
26 0 3710.1515 0.1509 0.1442 30.6447 2.5282 3.0062 4.0103 0.3144 0.3216 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
