# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:05:59
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9524 0.1918 0.1924 19.0540 1.8674 2.1620 4.1010 0.4434 0.4298 90.0000 0.0000 0.0000 0.2920 0.1947 0.2119 0.0000 0.0000 0.0000
26 0 3710.1218 0.1677 0.1705 24.7334 1.9437 2.1725 4.7224 0.3439 0.3585 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
