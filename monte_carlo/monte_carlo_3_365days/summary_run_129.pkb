# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:31:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9808 0.1687 0.1699 21.4380 2.0605 2.4676 4.0572 0.4096 0.4157 90.0000 0.0000 0.0000 0.3426 0.2192 0.2021 0.0000 0.0000 0.0000
26 0 3709.9946 0.1420 0.1398 28.1987 2.3580 2.6221 4.1679 0.2952 0.3159 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
