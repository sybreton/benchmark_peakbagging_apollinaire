# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:06:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3865 0.2116 0.2337 25.4611 2.7599 3.1753 3.6749 0.4074 0.4416 90.0000 0.0000 0.0000 0.6855 0.1920 0.1410 0.0000 0.0000 0.0000
26 0 3710.0795 0.1595 0.1529 29.9041 2.5930 2.8861 3.8475 0.3014 0.3253 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
