# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:36:53
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0327 0.2294 0.2541 23.2966 2.5509 3.0349 3.7259 0.4120 0.4343 90.0000 0.0000 0.0000 0.5526 0.2637 0.1965 0.0000 0.0000 0.0000
26 0 3709.9718 0.1946 0.1896 26.7015 2.1574 2.4157 4.4705 0.3592 0.3794 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
