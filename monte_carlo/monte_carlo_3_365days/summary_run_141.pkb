# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:04:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0728 0.1833 0.1983 20.2492 1.9566 2.2157 4.0126 0.3958 0.4210 90.0000 0.0000 0.0000 0.2353 0.1590 0.1982 0.0000 0.0000 0.0000
26 0 3709.8931 0.1564 0.1524 29.1692 2.3765 2.6584 4.1147 0.3124 0.3267 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
