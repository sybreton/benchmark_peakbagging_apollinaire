# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:17:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9128 0.1486 0.1517 26.3586 2.8377 3.3291 3.2002 0.3611 0.3693 90.0000 0.0000 0.0000 0.4755 0.2100 0.1436 0.0000 0.0000 0.0000
26 0 3710.3375 0.1488 0.1464 26.1118 2.1456 2.4093 4.0969 0.3042 0.3146 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
