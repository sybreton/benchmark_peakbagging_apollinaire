# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:31:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9282 0.1568 0.1567 25.3061 2.6376 3.0983 3.3490 0.3476 0.3557 90.0000 0.0000 0.0000 0.3726 0.2254 0.1731 0.0000 0.0000 0.0000
26 0 3709.9933 0.1351 0.1292 32.9243 2.6735 3.0953 3.8931 0.2765 0.2957 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
