# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:21:39
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8979 0.1683 0.1724 22.6421 2.4467 2.9785 3.7894 0.4947 0.4791 90.0000 0.0000 0.0000 0.4909 0.2593 0.1700 0.0000 0.0000 0.0000
26 0 3710.2110 0.1278 0.1232 29.8169 2.4736 2.8256 3.7205 0.2723 0.3006 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
