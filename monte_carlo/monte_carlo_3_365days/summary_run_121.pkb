# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:10:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1351 0.1897 0.2005 19.8742 1.9417 2.2407 4.1783 0.4476 0.4405 90.0000 0.0000 0.0000 0.3560 0.2318 0.2175 0.0000 0.0000 0.0000
26 0 3710.0259 0.1373 0.1392 30.3391 2.6125 3.0095 3.7397 0.2920 0.3056 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
