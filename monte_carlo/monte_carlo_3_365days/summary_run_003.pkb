# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 14:54:25
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8573 0.2115 0.2313 15.9501 1.4585 1.6386 4.8701 0.4889 0.5268 90.0000 0.0000 0.0000 0.2190 0.1495 0.2024 0.0000 0.0000 0.0000
26 0 3710.0951 0.1429 0.1381 29.6139 2.4185 2.8554 4.0066 0.3005 0.3223 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
