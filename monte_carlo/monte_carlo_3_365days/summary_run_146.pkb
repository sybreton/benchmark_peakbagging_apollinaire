# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:16:50
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8997 0.1805 0.1925 18.9715 1.8395 2.0961 4.0828 0.3906 0.4035 90.0000 0.0000 0.0000 0.2385 0.1627 0.2006 0.0000 0.0000 0.0000
26 0 3709.9309 0.1437 0.1359 31.9498 2.6087 2.9536 4.1201 0.2948 0.3118 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
