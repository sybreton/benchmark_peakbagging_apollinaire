# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:35:34
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0035 0.1641 0.1783 24.2986 2.3696 2.8220 3.5051 0.3549 0.3667 90.0000 0.0000 0.0000 0.2512 0.1706 0.1874 0.0000 0.0000 0.0000
26 0 3709.8468 0.1537 0.1516 31.0019 2.5102 2.8736 4.1311 0.3033 0.3179 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
