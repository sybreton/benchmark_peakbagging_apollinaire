# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:13:42
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1450 0.2192 0.2454 17.4125 1.6850 1.9576 4.3017 0.4405 0.4759 90.0000 0.0000 0.0000 0.2463 0.1673 0.2135 0.0000 0.0000 0.0000
26 0 3709.9656 0.1600 0.1565 30.1592 2.5113 2.8218 4.0122 0.3032 0.3166 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
