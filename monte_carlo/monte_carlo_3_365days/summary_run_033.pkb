# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:44:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.2612 0.1746 0.1787 24.3773 2.4668 2.9216 3.8552 0.4142 0.4147 90.0000 0.0000 0.0000 0.4623 0.2592 0.1865 0.0000 0.0000 0.0000
26 0 3710.1388 0.1484 0.1437 29.8405 2.5223 2.8562 3.8570 0.2892 0.3048 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
