# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:47:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9715 0.2015 0.2088 20.9523 2.1311 2.4463 4.1798 0.4677 0.4668 90.0000 0.0000 0.0000 0.4895 0.2695 0.1969 0.0000 0.0000 0.0000
26 0 3709.9672 0.1528 0.1517 28.1738 2.3190 2.7046 4.1342 0.3015 0.3303 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
