# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:19:36
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.6971 0.1762 0.1865 18.9573 1.9275 2.2470 3.7448 0.4113 0.4197 90.0000 0.0000 0.0000 0.2477 0.1707 0.1988 0.0000 0.0000 0.0000
26 0 3709.8215 0.1487 0.1450 29.2294 2.3001 2.5689 4.2055 0.2999 0.3176 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
