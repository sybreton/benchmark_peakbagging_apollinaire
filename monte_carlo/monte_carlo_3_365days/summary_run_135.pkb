# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:49:49
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3144 0.1959 0.2080 20.9941 1.9987 2.3105 4.2515 0.4212 0.4326 90.0000 0.0000 0.0000 0.3583 0.2339 0.2199 0.0000 0.0000 0.0000
26 0 3709.9953 0.1495 0.1478 28.7698 2.4141 2.6587 4.0856 0.3063 0.3234 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
