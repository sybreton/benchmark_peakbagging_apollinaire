# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:54:52
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9526 0.1902 0.2037 21.9096 2.2687 2.6606 3.8903 0.4361 0.4513 90.0000 0.0000 0.0000 0.4829 0.2615 0.1845 0.0000 0.0000 0.0000
26 0 3709.9900 0.1583 0.1463 29.2790 2.3962 2.7463 4.0256 0.3085 0.3198 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
