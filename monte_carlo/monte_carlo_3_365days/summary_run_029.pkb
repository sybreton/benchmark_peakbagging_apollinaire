# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:47:56
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0360 0.2620 0.2855 20.2853 2.3785 2.7017 4.0108 0.4931 0.5346 90.0000 0.0000 0.0000 0.6533 0.2887 0.1904 0.0000 0.0000 0.0000
26 0 3709.7852 0.1903 0.1838 26.0265 2.1471 2.4233 4.3908 0.3493 0.3584 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
