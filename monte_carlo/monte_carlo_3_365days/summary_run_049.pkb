# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:22:47
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1792 0.2161 0.2467 18.3093 1.8063 2.0450 4.4177 0.4867 0.4933 90.0000 0.0000 0.0000 0.3596 0.2318 0.2344 0.0000 0.0000 0.0000
26 0 3710.0925 0.1558 0.1556 29.8966 2.5270 2.8634 3.9872 0.3089 0.3365 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
