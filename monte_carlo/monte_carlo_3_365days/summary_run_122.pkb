# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 22:13:21
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8833 0.2323 0.2615 19.1254 1.9825 2.3527 4.5354 0.4984 0.5200 90.0000 0.0000 0.0000 0.4777 0.3029 0.2510 0.0000 0.0000 0.0000
26 0 3709.9383 0.1557 0.1499 30.7593 2.5821 2.9559 3.8780 0.3051 0.3133 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
