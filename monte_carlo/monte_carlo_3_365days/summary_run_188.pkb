# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:56:07
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0548 0.2356 0.2501 19.8035 2.2752 2.6214 4.2106 0.5059 0.5373 90.0000 0.0000 0.0000 0.6449 0.2927 0.1936 0.0000 0.0000 0.0000
26 0 3709.9092 0.1500 0.1421 31.5801 2.7716 3.1677 3.6347 0.2876 0.3031 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
