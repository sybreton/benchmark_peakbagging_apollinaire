# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:09:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9239 0.1738 0.1715 24.0139 2.6764 3.1619 3.4098 0.4372 0.4473 90.0000 0.0000 0.0000 0.5000 0.2210 0.1458 0.0000 0.0000 0.0000
26 0 3710.0519 0.1442 0.1421 29.2719 2.3643 2.8207 4.1287 0.3078 0.3185 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
