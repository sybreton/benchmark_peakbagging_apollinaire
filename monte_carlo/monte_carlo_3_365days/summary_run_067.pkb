# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:05:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1026 0.1785 0.1892 25.3634 2.6721 3.0964 3.6150 0.3904 0.4221 90.0000 0.0000 0.0000 0.6262 0.1969 0.1421 0.0000 0.0000 0.0000
26 0 3710.2184 0.1492 0.1415 29.1192 2.5125 2.7695 3.7375 0.2798 0.2958 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
