# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 19:18:06
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9058 0.1811 0.1888 21.7344 1.9873 2.3253 4.1343 0.3980 0.4230 90.0000 0.0000 0.0000 0.2508 0.1711 0.2067 0.0000 0.0000 0.0000
26 0 3709.9608 0.1640 0.1554 27.1601 2.3052 2.4986 4.3413 0.3333 0.3481 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
