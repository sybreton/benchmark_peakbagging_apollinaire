# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:03:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7838 0.1862 0.1960 20.2246 1.9909 2.3658 3.7984 0.4000 0.4095 90.0000 0.0000 0.0000 0.2853 0.1882 0.2052 0.0000 0.0000 0.0000
26 0 3709.8737 0.1592 0.1547 26.9851 2.1887 2.5080 4.2433 0.3266 0.3418 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
