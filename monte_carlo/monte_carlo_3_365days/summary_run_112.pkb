# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:49:19
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3322 0.2496 0.2608 23.2938 2.6819 3.0794 3.8379 0.4065 0.4356 90.0000 0.0000 0.0000 0.6093 0.2846 0.1943 0.0000 0.0000 0.0000
26 0 3710.0789 0.1618 0.1552 32.0944 2.7230 3.1756 3.6932 0.2992 0.3172 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
