# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:25:29
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8515 0.2352 0.2522 17.6496 1.7726 2.0334 4.4751 0.4803 0.4862 90.0000 0.0000 0.0000 0.4518 0.2830 0.2369 0.0000 0.0000 0.0000
26 0 3709.9956 0.1508 0.1494 30.1003 2.5267 2.9184 4.0252 0.3113 0.3168 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
