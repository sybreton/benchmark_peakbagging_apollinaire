# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:58:45
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0352 0.1895 0.1969 19.9974 2.0344 2.2163 3.9837 0.4111 0.4221 90.0000 0.0000 0.0000 0.2738 0.1819 0.2150 0.0000 0.0000 0.0000
26 0 3709.8696 0.1656 0.1625 26.9675 2.1390 2.4076 4.5969 0.3362 0.3660 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
