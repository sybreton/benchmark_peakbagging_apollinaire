# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:56:40
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9266 0.1816 0.1882 19.9943 1.9196 2.2653 3.9397 0.4060 0.4122 90.0000 0.0000 0.0000 0.3478 0.2210 0.1903 0.0000 0.0000 0.0000
26 0 3709.8710 0.1293 0.1253 33.5007 2.8751 3.3848 3.5424 0.2704 0.2767 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
