# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 14:22:05
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9280 0.1716 0.1805 20.1514 1.8646 2.0692 4.2653 0.3867 0.4210 90.0000 0.0000 0.0000 0.1679 0.1165 0.1680 0.0000 0.0000 0.0000
26 0 3710.0559 0.1600 0.1551 25.8073 2.0823 2.2803 4.4590 0.3284 0.3504 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
