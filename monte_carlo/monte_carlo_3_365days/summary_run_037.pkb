# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:55:11
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7482 0.2024 0.2241 19.0924 2.0176 2.4042 4.1241 0.4588 0.4640 90.0000 0.0000 0.0000 0.5017 0.2854 0.2029 0.0000 0.0000 0.0000
26 0 3710.0588 0.1412 0.1308 33.3504 2.7064 3.1075 3.8774 0.2733 0.2879 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
