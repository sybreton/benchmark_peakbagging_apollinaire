# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 16:11:35
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8832 0.1920 0.1992 23.0130 2.6336 3.0887 3.5141 0.4625 0.4999 90.0000 0.0000 0.0000 0.6405 0.2274 0.1497 0.0000 0.0000 0.0000
26 0 3709.8039 0.1267 0.1247 32.7725 2.7575 3.1802 3.5716 0.2646 0.2796 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
