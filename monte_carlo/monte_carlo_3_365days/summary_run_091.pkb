# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:01:16
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0700 0.1642 0.1741 23.4944 2.7802 3.3207 3.1603 0.4078 0.4294 90.0000 0.0000 0.0000 0.4641 0.2119 0.1449 0.0000 0.0000 0.0000
26 0 3709.8884 0.1458 0.1455 29.1669 2.3401 2.6525 4.1388 0.2954 0.3250 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
