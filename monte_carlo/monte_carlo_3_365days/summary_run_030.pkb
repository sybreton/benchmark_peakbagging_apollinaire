# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 18:36:58
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1059 0.2254 0.2372 20.9480 2.5113 2.8807 3.7035 0.4440 0.4797 90.0000 0.0000 0.0000 0.6939 0.2225 0.1536 0.0000 0.0000 0.0000
26 0 3709.9251 0.1564 0.1520 29.9980 2.5644 2.7384 3.9516 0.3045 0.3150 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
