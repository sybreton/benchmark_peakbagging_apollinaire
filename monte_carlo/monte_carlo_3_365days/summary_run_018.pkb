# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:47:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0001 0.2097 0.2258 17.5880 1.5990 1.8762 4.6413 0.4522 0.4745 90.0000 0.0000 0.0000 0.2829 0.1928 0.2235 0.0000 0.0000 0.0000
26 0 3710.2321 0.1471 0.1513 30.7055 2.5575 2.8847 4.1127 0.3110 0.3304 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
