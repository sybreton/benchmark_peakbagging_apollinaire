# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:58:30
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3327 0.2216 0.2348 18.9031 1.8780 2.1746 4.4030 0.4528 0.4711 90.0000 0.0000 0.0000 0.3950 0.2567 0.2353 0.0000 0.0000 0.0000
26 0 3710.1150 0.1610 0.1520 29.4336 2.4919 2.7951 4.2061 0.3229 0.3408 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
