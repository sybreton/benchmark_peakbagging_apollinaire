# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:42:55
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8742 0.1712 0.1745 22.2465 2.2684 2.7321 3.7910 0.4260 0.4424 90.0000 0.0000 0.0000 0.4747 0.2526 0.1732 0.0000 0.0000 0.0000
26 0 3710.2147 0.1301 0.1267 33.4015 2.7560 3.2553 3.7809 0.2756 0.2911 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
