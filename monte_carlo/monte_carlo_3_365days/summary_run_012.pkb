# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:25:26
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8387 0.2205 0.2443 17.5218 1.6841 1.9228 4.4162 0.4581 0.4736 90.0000 0.0000 0.0000 0.2299 0.1583 0.2201 0.0000 0.0000 0.0000
26 0 3709.8247 0.1439 0.1407 33.4755 2.7237 3.0822 3.9747 0.2975 0.3085 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
