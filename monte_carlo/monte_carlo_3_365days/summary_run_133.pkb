# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:45:24
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9492 0.2184 0.2366 18.1391 1.8649 2.2172 4.2331 0.4534 0.4606 90.0000 0.0000 0.0000 0.4438 0.2808 0.2412 0.0000 0.0000 0.0000
26 0 3710.2085 0.1429 0.1442 30.0159 2.5194 2.8917 3.7616 0.2825 0.2982 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
