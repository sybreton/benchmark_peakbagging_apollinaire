# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 14:48:03
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0097 0.1584 0.1704 23.6375 2.1939 2.5435 3.7571 0.3426 0.3580 90.0000 0.0000 0.0000 0.2306 0.1548 0.1893 0.0000 0.0000 0.0000
26 0 3710.2326 0.1622 0.1580 25.8711 2.1409 2.4210 4.3678 0.3320 0.3556 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
