# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:47:31
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.0344 0.2037 0.2189 20.7553 2.1605 2.5929 4.1643 0.4723 0.4763 90.0000 0.0000 0.0000 0.4211 0.2556 0.2159 0.0000 0.0000 0.0000
26 0 3709.8794 0.1704 0.1598 27.1850 2.2743 2.5756 4.2546 0.3150 0.3313 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
