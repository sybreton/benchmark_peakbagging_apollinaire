# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 12:19:15
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1501 0.1808 0.1963 21.1326 2.0971 2.4656 3.8422 0.4028 0.4183 90.0000 0.0000 0.0000 0.3177 0.2050 0.2117 0.0000 0.0000 0.0000
26 0 3710.1175 0.1630 0.1554 27.3780 2.1969 2.5262 4.2950 0.3186 0.3357 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
