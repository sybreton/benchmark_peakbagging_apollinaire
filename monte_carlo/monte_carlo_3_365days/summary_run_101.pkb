# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:24:10
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8352 0.1646 0.1665 20.6139 1.8639 2.2091 4.0320 0.3723 0.3880 90.0000 0.0000 0.0000 0.2300 0.1541 0.1926 0.0000 0.0000 0.0000
26 0 3710.2154 0.1513 0.1503 25.8983 2.1752 2.4933 4.1732 0.3116 0.3429 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
