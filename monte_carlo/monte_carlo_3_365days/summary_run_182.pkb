# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 13:42:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.9372 0.1813 0.1928 20.5197 1.9196 2.2140 4.0681 0.4061 0.4218 90.0000 0.0000 0.0000 0.2471 0.1688 0.2021 0.0000 0.0000 0.0000
26 0 3710.0134 0.1570 0.1503 28.7185 2.3776 2.6926 4.1138 0.3148 0.3320 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
