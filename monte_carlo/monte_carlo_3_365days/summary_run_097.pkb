# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:15:01
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1101 0.2106 0.2223 17.7839 1.7451 1.9411 4.3747 0.4478 0.4799 90.0000 0.0000 0.0000 0.2771 0.1886 0.2228 0.0000 0.0000 0.0000
26 0 3709.8814 0.1540 0.1498 28.9934 2.3551 2.5950 4.1320 0.3077 0.3307 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
