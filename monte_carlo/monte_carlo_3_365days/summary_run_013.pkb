# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 15:29:23
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.1651 0.1721 0.1816 22.5236 2.0821 2.3066 4.0719 0.3618 0.3861 90.0000 0.0000 0.0000 0.2434 0.1670 0.1924 0.0000 0.0000 0.0000
26 0 3709.9668 0.1441 0.1397 30.0694 2.5106 2.8979 3.8086 0.2893 0.3048 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
