# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:28:43
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3703.3811 0.2351 0.2517 18.5581 1.7890 2.1197 4.6832 0.5237 0.5119 90.0000 0.0000 0.0000 0.4557 0.2849 0.2307 0.0000 0.0000 0.0000
26 0 3710.1079 0.1666 0.1554 27.5808 2.4528 2.7061 3.9094 0.3055 0.3290 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
