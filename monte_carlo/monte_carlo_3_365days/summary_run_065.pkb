# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 20:00:41
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8868 0.2015 0.2109 18.4041 1.8127 2.0526 4.2403 0.4448 0.4625 90.0000 0.0000 0.0000 0.3142 0.2116 0.2287 0.0000 0.0000 0.0000
26 0 3709.9522 0.1612 0.1562 26.2618 2.1088 2.3294 4.4565 0.3211 0.3439 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
