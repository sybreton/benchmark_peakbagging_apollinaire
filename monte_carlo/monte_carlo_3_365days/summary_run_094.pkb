# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 01/04/2022 21:08:08
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.7485 0.1847 0.1935 18.6918 1.8145 2.0858 3.9220 0.4036 0.4339 90.0000 0.0000 0.0000 0.2448 0.1658 0.2012 0.0000 0.0000 0.0000
26 0 3709.9617 0.1475 0.1430 28.9160 2.3694 2.6751 4.0368 0.3017 0.3201 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
