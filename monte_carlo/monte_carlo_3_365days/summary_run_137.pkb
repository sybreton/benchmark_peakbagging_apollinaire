# Peakbagging performed by SNB with apollinaire (v1.0) 
# File creation date: 02/04/2022 11:54:27
# https://apollinaire.readthedocs.io/en/latest 
# ######################
# Settings:
# nwalkers=500
# nsteps=500
# discarded_steps=100
# fit_amp=False
# #######################
# Parameters: n, l, nu, e_nu-, e_nu+, h, e_h-, e_h+, w, e_w-, e_w+, a, e_a-, e_a+, s, e_s-, e_s+, asym, e_asym-, e_asym+
#             ., ., muHz, muHz, muHz, (m/s)^2/muHz, (m/s)^2/muHz, (m/s)^2/muHz, muHz, muHz, muHz, deg, deg, deg, muHz, muHz, muHz, ., ., . 
# 
25 2 3702.8814 0.1872 0.1921 20.3977 1.9558 2.3404 4.1032 0.4282 0.4446 90.0000 0.0000 0.0000 0.4136 0.2594 0.2150 0.0000 0.0000 0.0000
26 0 3710.0623 0.1282 0.1265 34.8265 2.9862 3.4541 3.6562 0.2665 0.2827 90.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
